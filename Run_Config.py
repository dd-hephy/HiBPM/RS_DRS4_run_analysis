'''
    File name: run_config.py
    Author: Philipp Gaggl
    Date created: 2021.11.15
    Python Version: 3.8, 3.9
'''

import configparser
import os
import numpy as np
from collections import Counter
from tool_box import Warn_Me


class Ini_Handler():
    '''
    Class used to handle .ini files. Came only from run.ini, therefore the code may seem a bit messy 
    now with the additional settings.ini handling. This may come a day that this will be cleaned up, but 
    it is not today.
    '''
    
    def __init__(self):

        # Warning helper
        self.warn = Warn_Me()

        self.double_prefix = '0_double_key_prefix_'
        self.raw_prefix = '_double_key_prefix_'
        
        # Declare predefined parameters
        self.init_all_parameters()
        
        return None

    def init_all_parameters(self):
        '''
        Within here, all parameters that are handled within .ini files (run.ini and settings.ini) are 
        defined in a way, such that the function convert_to_ini/convert_from_ini can be used to convert read out ini 
        parameters as string to obtain the right format (float, int, str). Also included are optional
        boundaries for the parameters, such that each parameter can be checked for validity after 
        conversion (check(content)). All of them are stored in the dict self.all.
        
        All parameter information are given in form of a list with the follwing syntax:
        
        self.all['ini_name'] = [type, unit, list?, min, max, weak_boundaries, display_factor, options, max_list_entries, mandatory_info]
        
        With follwing options:
        
        ini_name: A str of the key under which this parameter is accessed within the corresponding .ini file
        type: int, float, str, bool. If the parameter is a list, the type refers to its entries.
        unit: string of the unit IN SI UNITS!!!, None if no unit
        list?: True if this parameter comes as list within the .ini file, False otherwise
        min: Minimum value the parameter shall not be below, None if there is none
        max: Maximum value the parameter shall not surpass, None if there is none
        weak_boundaries: True, if the min and max values are within the allowed range of the parameter, 
                         False if not. None, if the parameter can't have boundaries
        display_factor: Sometimes, parameters will be displayed in different units as the SI unit (e.g. in plotting). 
                      This usually also happens in a quite fixed unit (e.g. s and ns). If this is the 
                      case for the parameter, the original parameter times the display factor result 
                      in the displayed value (e.g. 1e9 to display s in ns)
        options: Some parameters have fixed options that ought not to be something else. These are given
                 in form of a list. None if no such condition.        
        max_list_entries: How many entries dies the list have, usually 4. If no list, None. 
        mandatory_info: If True, thee value must be given, an empty string is not allowed.    
                      
                      
        The following initialization is written, such that additional options can be added halfway 
        efficiently shall there be new ones that are necessary. Just add a corresponding key in the
        variable definitions and add (or insert of you want to do the work) the parameter specific 
        factor in all parameters. If you add variables that are part of the defined ini section stored
        in self.sections, add the section name there. 
        
        Keep in mind that changing things in here can cause massive errors throughout all programs, 
        as it is intended to implement this method here more widely to keep the whole code more or less
        straight forward.
        There are also subcategories given, which parameters may be present in which files, to allow 
        for easier checking of completeness.
        It is motivated, to give a small description for each parameter such that future users understand 
        their meaning and influence onto the software.
        '''

        # The mother variable
        self.all = {}
        self.sections = ['ANALYSIS_PARAMETERS', 'BASIC', 'OPTIONAL']
        definitions = ['type', 'unit', 'list', 'min', 'max', 'weak_boundaries', 'display_factor', 'options', 'max_list_entries', 'mandatory_info']
        # Subcategories of the defined parameters
        self.ana_params = ['min_tot_factor', 'min_time_between_peaks', 'use_fixed_threshold', 'std_factor', 'fixed_threshold', 'analysis_till_rms_noise', 'buffer_analysis_left', 'buffer_analysis_right', 'skip_borders', 'compensate_offset', 'buffer_plot', 'no_example_plots', 'plot_analysis_features', 'plot_peak_parameters', 'apply_constant_fraction_discrimination', 'cfd_factor', 'search_direction', 'advanced_noise_offset', 'pure_simulation_analysis']
        self.basic_run_params = ['run_no', 'particle', 'energy', 'rate', 'use_custom_ramp_param', 'custom_ramp_param', 'custom_ramp_val', 'custom_ramp_unit', 'configuration', 'oscilloscope', 'channels', 'detectors', 'bias_voltage']
        self.optional_run_params = ['degrader', 'geometry', 'iso_distance', 'start_time', 'additional_info']
        
        ##################### Analysis parameters
        # MIN_TOT_FACTOR
        '''
        Describes the minimal number of samples, a signal needs to be above threshold consecutively
        in order to be considered as signal. 
        '''
        self.all['min_tot_factor'] = [int, None, False, 0, None, False, None, None, None, True]
        
        # MIN_TIME_BETWEEN_PEAKS
        '''
        Describes the minimal time that should be between two peaks, or more precise: Between the stop
        sample of one peak and the start sample of the following. If the time is lower, the next peak
        will be chained to the previous one, and so on. This is necessary to identify pile ups in 
        histograms rather than falsifying data without recognizing it
        '''
        self.all['min_time_between_peaks'] = [float, 's', False, 0, None, True, 1e9, None, None, True]
        
        # USE_FIXED_THRESHOLD
        '''
        If True a fixed threshold will be used for peak finding (FIXED_THRESHOLD). Is positive and will
        be adapted by the file reader to the right sign.
        '''
        self.all['use_fixed_threshold'] = [bool, None, False, None, None, None, None, None, None, True]
        
        # STD_FACTOR
        '''
        If an automated threshold evaluation is used (use_fixed_threshold == False), then the rms value
        of the calculated noise for the file is multiplied with this factor to get the threshold. Should 
        be at least 6. 
        '''
        self.all['std_factor'] = [int, None, False, 0, None, False, None, None, None, True]
        
        # FIXED_THRESHOLD
        '''
        If use_fixed_threshold is True, this value functions as threshold. It is positive, as the 
        filereader determines the search direction. Maybe should be implemented as a list to allow for
        efficient multiple channel analysis.
        '''
        self.all['fixed_threshold'] = [float, 'V', False, 0, None, False, 1e3, None, None, True]
        
        # ANALYSIS_TILL_RMS_NOISE
        self.all['analysis_till_rms_noise'] = [bool, None, False, None, None, None, None, None, None, True]
        
        # BUFFER_ANALYSIS_LEFT
        '''
        Determines the time range left from the start sample of a signal that is added to the region of 
        peak analysis to calculate e.g. the area.
        '''
        self.all['buffer_analysis_left'] = [float, 's', False, 0, None, True, 1e9, None, None, True]
        
        # BUFFER_ANALYSIS_RIGHT
        '''
        Determines the time range right from the stop sample of a signal that is added to the region of 
        peak analysis to calculate e.g. the area.
        '''
        self.all['buffer_analysis_right'] = [float, 's', False, 0, None, True, 1e9, None, None, True]
        
        # SKIP_BORDERS
        '''
        Number of events that should be deleted at the very beginning and the very end of the recording.
        Sometimes necessary because we get very intense artifacts when switching on/off the oscis.
        '''
        self.all['skip_borders'] = [int, None, False, 0, None, True, None, None, None, True]
        
        # COMPENSATE_OFFSET
        '''
        If True, an average calculation including outlier removal will be used before peak finding to 
        ground the noise level to zero. Necessary if one encounters obvious hardware offsets, but one 
        should be careful to use this option. 
        '''
        self.all['compensate_offset'] = [bool, None, False, None, None, None, None, None, None, True]
        
        # BUFFER_PLOT
        '''
        Time range left from start sample and right from stop sample that will be plotted in example plots.
        '''
        self.all['buffer_plot'] = [float, 's', False, 0, None, True, 1e9, None, None, True]
        
        # NO_EXAMPLE_PLOTS
        '''
        Defines the number of example plots that will be done by the Analyse_Run class.
        '''
        self.all['no_example_plots'] = [int, None, False, 0, None, True, None, None, None, True]
        
        # PLOT_ANALYSIS_FEATURES
        '''
        If True, example plots will include all sorts of fancy addings to visualize the parameters.
        '''
        self.all['plot_analysis_features'] = [bool, None, False, None, None, None, None, None, None, True]
        
        # PLOT_ANALYSIS_PARAMETERS
        '''
        If True, the obtained parameters by the signal analysis are added into the example plots.
        '''
        self.all['plot_peak_parameters'] = [bool, None, False, None, None, None, None, None, None, True]
        
        # APPLY_CONSTANT_FRACTION_DISCRIMINATION
        '''
        If True, CFD is applied during peak analysis, using the variable 'cfd_factor'
        '''
        self.all['apply_constant_fraction_discrimination'] = [bool, None, False, None, None, None, None, None, None, True]
        
        # CFD_FACTOR
        '''
        Factor for constant fraction discrimination. The peak maximum times this factor determines the 
        individual threshold of each peak. New start and stop times calculated, resulting in new 
        analysis parameters, especially ToT and area.
        '''
        self.all['cfd_factor'] = [float, None, False, 0, 1, False, None, None, None, True]
        
        # SEARCH_DIRECTION
        '''
        If artifacts higher than the highest peak are present, it can happen that peaks are searched
        for on the wrong side. With this factor, one can force a side. Must be either 'none', 'positive'
        or 'negative'.
        '''
        self.all['search_direction'] = [str, None, False, None, None, None, None, ['none', 'positive', 'negative'], None, True]
        
        # ADVANCED_NOISE_OFFSET
        '''
        If True, an advanced method for offset compensation and noise calculation based on a preceeding
        rudimentary peak finding is used. It specifically uses only points before (after if not possible) the
        found peaks and applies a Gaussian fit to them. This should only be applied for fast segmentation
        mode and low fluxes, as it automatically assumes just a single peak per event. In case of multiple
        peaks, the fit may be bad and the whole event is skipped.
        '''
        self.all['advanced_noise_offset'] = [bool, None, False, None, None, None, None, None, None, True]
        
        # PURE_SIMULATION_ANALYSIS
        '''
        If True, no pulse finding will be executed. Instead, it is assumed that the whole event is a simulated
        one, without noise, without offset. The complete event data is given directly to the pulse analysis. 
        '''
        self.all['pure_simulation_analysis'] = [bool, None, False, None, None, None, None, None, None, True]
        ##################### 

        ##################### Mandatory (basic) run documentation
        # RUN_NO
        '''
        The individual run number of the current run folder. Crucial when analyzing multiple runs
        within one file. This parameter is used for a main part of logistics in result output and 
        storage. 
        '''
        self.all['run_no'] = [int, None, False, 0, None, True, None, None, None, True]
        
        # PARTICLE
        '''
        The particle that was used for the run.
        '''
        self.all['particle'] = [str, None, False, None, None, None, None, None, None, True]
        
        # ENERGY
        '''
        Energy of the used particles.
        '''
        self.all['energy'] = [float, 'MeV', False, 0, None, False, None, None, None, True]
        
        # RATE
        '''
        Particle rate of the analyzed run.
        '''
        self.all['rate'] = [float, 'kHz', False, 0, None, False, None, None, None, True]
        
        # USE_CUSTOM_RAMP_PARAM
        '''
        Bool if the user wants to use an individual ramp parameter, self named and set
        '''
        self.all['use_custom_ramp_param'] = [bool, None, False, None, None, None, None, None, None, True]
        
        # CUSTOM_RAMP_PARAM
        '''
        Name of the customized ramp parameter
        '''
        self.all['custom_ramp_param'] = [str, None, False, None, None, None, None, None, None, False]
        
        # CUSTOM_RAMP_VAL
        '''
        Value of the custom ramp parameter
        '''
        self.all['custom_ramp_val'] = [float, None, False, None, None, False, None, None, None, False]
        
        # CUSTOM_RAMP_UNIT
        '''
        Unit of the custom ramp parameter
        '''
        self.all['custom_ramp_unit'] = [str, None, False, None, None, None, None, None, None, False]
        
        # CONFIGURATION
        '''
        Configuration of the setup. Either a beam setting at MedAustron, Alpha, Laser...
        '''
        self.all['configuration'] = [str, None, False, None, None, None, None, None, None, False]
        
        # OSCILLOSCOPE
        '''
        The oscilloscope with which the data has been recorded. Either 'RS', 'DRS4', 'Weightfield' or 'Allpix' (currently).
        '''
        self.all['oscilloscope'] = [str, None, False, None, None, None, None, ['RS', 'DRS4', 'Weightfield', 'Allpix', 'CNM'], None, True]
        
        # CHANNELS
        '''
        Recorded channels. It is crucial the number of entries in this list represent the number of 
        used channels.
        '''
        self.all['channels'] = [int, None, True, 1, 4, True, None, None, 4, True]
        
        # DETECTORS
        '''
        Names of the detectors. It is crucial the number of entries in this list represent the number of 
        used channels.
        '''
        self.all['detectors'] = [str, None, True, None, None, None, None, None, 4, True]

        # BIAS_VOLTAGE
        '''
        Applied bias voltage for the detectors on the corresponding channel. It is crucial the number of 
        entries in this list represent the number of used channels.
        '''
        self.all['bias_voltage'] = [float, 'V', True, None, None, None, None, None, 4, True]
        #####################
        
        ##################### Optional run documentation
        # DEGRADER
        '''
        Degrader setting for the run. 0 means no degrader, 100 means full degrader.
        '''
        self.all['degrader'] = [int, '%', False, 0, 100, True, None, None, None, False]
        
        # Geometry
        '''
        Additional information about the setup geometry (e.g. stacked, telescope,...).
        '''
        self.all['geometry'] = [str, None, False, None, None, None, None, None, None, False]
        
        # ISO_DISTANCE
        '''
        Distance from the detectors to the iso center of the beam. The order should be the same as 
        DETECTORS. 
        '''
        self.all['iso_distance'] = [float, 'mm', True, None, None, None, None, None, 4, False]
        
        # START_TIME
        '''
        Information about the start/end time of the recording
        '''
        self.all['start_time'] = [str, None, False, None, None, None, None, None, None, False]
        
        # ADDITIONAL INFO
        '''
        Self explanatory.
        '''
        self.all['additional_info'] = [str, None, False, None, None, None, None, None, None, False]
        #####################

        # Now convert this variable such that each parameter descriptor can be accessed via the 
        # corresponding handle and not via index, such that modification of additional descriptors
        # becomes easier
        temp = {}
        for handle in self.all.keys():
            temp[handle] = {}
            for i, descriptor in enumerate(definitions):
                temp[handle][descriptor] = self.all[handle][i]
        
        self.all = temp
        del(temp)
        
        # Store the names of the parameters separately
        self.valid_keys = list(self.all.keys())
        
        # We also keep a list of those sources which represent simulated data, where we are allowed to 
        # adapt the analysis buffer 
        self.simu_sources = ['Weightfield', 'Allpix']
        
        # All informations and options for the ramp analysis are stored in here as well
        # Possible parameters to perform ramp analysis on
        self.ramp_parameters = ['energy', 'rate', 'bias_voltage']
        # Options for ramp analysis as used in the main GUI
        self.ramp_options = ['None', 'Gauss', 'Langau', 'Landau', 'All']
        # Fitting methods
        self.ramp_fit_methods = ['Gauss', 'Langau', 'Landau']
        # Evaluation methods (includes fitting methods as well as the average values)
        self.ramp_eval_methods = ['average', 'Gauss', 'Langau', 'Landau']
        # Fitting parameters for the respective fit method and their respective units, display units and conversion factors
        self.ramp_fit_params = {'average':['avg', 'std'], 'Gauss':['A_Gauss', 'mu_Gauss', 'sigma_Gauss'], 'Langau':['mpv_Langau', 'eta_Langau', 'sigma_Langau', 'A_Langau'], 'Landau':['mpv_Landau', 'eta_Landau', 'A_Landau']}
        # If scaling is necessary to even perform the fit, the parameters to act on are marked as True
        self.ramp_fit_params_to_scale = {'Langau':[True, True, True, False], 'Landau':[True, True, False]}
        # Which of the fit parameters are considered for the value and the uncertainty in the summary plots. The first is the value, the second the uncertainty
        self.ramp_plot_values = {'average':['avg', 'std'], 'Gauss':['mu_Gauss', 'sigma_Gauss'], 'Langau':['mpv_Langau', 'sigma_Langau'], 'Landau':['mpv_Landau', 'eta_Landau']}
        
        
        # Furthermore, pulse parameters are defined
        self.pulse_params = ['peak_maximum_[V]', 'peak_area_[Vs]', 'peak_area_below_threshold_[Vs]', 'time_over_threshold_[s]', 'time_over_noise_[s]', 'rise_time_[s]', 'rms_[V]', 'rms_noise_[V]', 'SNR', 'hardware_offset_[V]', 'aspect_ratio_[V_per_s]']
        self.pulse_params_converted = {'peak_maximum_[V]':'peak_maximum_[mV]', 'peak_area_[Vs]':'peak_area_[mV ns]', 'peak_area_below_threshold_[Vs]':'peak_area_below_threshold_[mV ns]', 'time_over_threshold_[s]':'time_over_threshold_[ns]', 'time_over_noise_[s]':'time_over_noise_[ns]', 'rise_time_[s]':'rise_time_[ps]', 'rms_[V]':'rms_[mV]', 'rms_noise_[V]':'rms_noise_[mV]', 'SNR':'SNR', 'hardware_offset_[V]':'hardware_offset_[mV]', 'aspect_ratio_[V_per_s]':'aspect_ratio_[mV_per_ns]'}
        self.pulse_params_conversion_factors = {'peak_maximum_[V]':1e3, 'peak_area_[Vs]':1e12, 'peak_area_below_threshold_[Vs]':1e12, 'time_over_threshold_[s]':1e9, 'time_over_noise_[s]':1e9, 'rise_time_[s]':1e12, 'rms_[V]':1e3, 'rms_noise_[V]':1e3, 'SNR':1, 'hardware_offset_[V]':1e3, 'aspect_ratio_[V_per_s]':1e-6}
        self.pulse_params_unit = {'peak_maximum_[V]':'V', 'peak_area_[Vs]':'Vs', 'peak_area_below_threshold_[Vs]':'Vs', 'time_over_threshold_[s]':'s', 'time_over_noise_[s]':'s', 'rise_time_[s]':'s', 'rms_[V]':'V', 'rms_noise_[V]':'V', 'SNR':'', 'hardware_offset_[V]':'V', 'aspect_ratio_[V_per_s]':'V_per_s'}
        self.pulse_params_unit_converted = {'peak_maximum_[V]':'mV', 'peak_area_[Vs]':'mV ns', 'peak_area_below_threshold_[Vs]':'mV ns', 'time_over_threshold_[s]':'ns', 'time_over_noise_[s]':'ns', 'rise_time_[s]':'ps', 'rms_[V]':'mV', 'rms_noise_[V]':'mV', 'SNR':'', 'hardware_offset_[V]':'mV', 'aspect_ratio_[V_per_s]':'mV_per_ns'}
        
        return None
    
    def generate_prefix(self, key):
        '''
        If we encounter same parameter names during decomposing, this function will be used to generate
        unique prefixes that are later deleted again when composing.
        
        :param: The key that should be modified using a prefix
        :returns: Unique prefix based on the number of times this function has been called
        '''
        
        for i, char in enumerate(self.double_prefix):
            if char == '_':
                number = int(self.double_prefix[0:i])
                break

        number += 1
        
        self.double_prefix = str(number) + '_double_key_prefix_'
        new_key = self.double_prefix + key
        
        return new_key
    
    def decompose(self, content):
        '''
        To handle 1-2 layered dicts, we decompose them into one dict and store the sections into a class 
        variable. Therefore, it is necessary that no key of an ini file is of type dict. 
        
        :param content: Dictionary directly from ini read out
        :returns: A decomposed dict and structure, which can be used in compose() to build back
        '''
        
        # Check if we have a double layered dict corresponding to ini sections and keys
        if not all(type(param) == dict for param in content.values()):
            return content, None

        # If we do, we decompose all keys into one single dict and store the old structure in a variable
        # to compose later
        else:
            decomposition = {}
            structure = {}
            # We need to ensure that parameters with equal names, but in different sections are
            # not overwritten in this context. As such cases will only ever appear for parameters 
            # that are not predefined, we can just add a prefix to the name, which will be removed
            # again in the composition function.
            key_names = []
            for sec in content.keys():
                for key in content[sec].keys():
                    key_names.append(key)
            count = Counter(key_names)
            multiple = [key for key, val in count.items() if val > 1]
            # If we have at least one double name, we search within the whole content
            if len(multiple) > 0:
                copy = {}
                for sec, items in content.items():
                    copy[sec] = {}
                    for param, val in items.items():
                        if param in multiple:
                            copy[sec][self.generate_prefix(param)] = val
                        else:
                            copy[sec][param] = val
                content = copy     
            
            for sec, params in content.items():
                # Store ini structure for composing later
                structure[sec] = list(params.keys())
                for key, val in params.items():
                    decomposition[key] = val

        return decomposition, structure
    
    def compose(self, content, struct):
        '''
        Composes a decomposed content using the class variable ini_structure.
        
        :param content: Decomposed dictionary
        :returns: The composed dict according to the structure
        '''
        
        if struct is None:
            return content
        
        else:
            composition = {}
            for sec, params in struct.items():
                composition[sec] = {}
                for key in params:
                    # Here we transform double key prefixes back
                    if self.raw_prefix in key:
                        for i in range(0, len(key)):
                            if key[i:(i + len(self.raw_prefix))] == self.raw_prefix:
                                new_key = key[(i + len(self.raw_prefix)):]
                                break
                        composition[sec][new_key] = content[key]
                    else:
                        composition[sec][key] = content[key]
            return composition    
    
    def check_for_keys(self, content):
        '''
        Checks if all given parameter handles of the dict content are predefined parameter handles
        
        :param content: Decomposed dictionary
        :returns: Bool if all parameters are known
        '''
        
        valid_handles = True
        
        # Decompose if we also have section names to check
        content, struct = self.decompose(content)
        
        # Check if given section names are known. If not we give a warning.
        if not struct is None:
            if not all(sec in self.sections for sec in struct.keys()):
                valid_handles = False
                for sec in struct.keys():
                    if not sec in self.sections:
                        self.warn.warn(f'Section name \'{sec}\' not in predefined sections')
        
        # Check for par correct ameter handles. If not, we give a warning.
        if not all(handle in self.valid_keys for handle in content.keys()):
            valid_handles = False
            for key in content.keys():
                if not key in self.valid_keys:
                    self.warn.warn(f'Handle \'{key}\' not in predefined parameter list')
        
        return valid_handles
        
    def check_content(self, content):
        '''
        Receives a parameter and checks, if it is included in the predefined parameter list. Also checks
        if all descriptors are fullfilled. The parameter already needs to be of the right type.
        
        :param content: Content to check (already as the right type)
        :returns: Boolean if all parameters are correct
        '''
            
        all_good = True
        
        # If called from scratch, we need to check whether all handles and sections are right.
        if not self.check_for_keys(content):
            all_good = False
        
        # We decompose if the function is called from scratch with a whole ini content.
        content, struct = self.decompose(content)
        
        # Checking all parameters
        for param, val in content.items():
            # As some parameters are a list, we convert all others to lists for easier handling
            if not self.all[param]['list']:
                val_list = [val]
            else:
                val_list = val
                # CHeck here if we do not succeed maximal allowed number of list entries
                max_entries = self.all[param]['max_list_entries']
                if not len(val_list) <= max_entries or not len(val_list) > 0:
                    self.warn.warn(f'\'{param}\' either has no or too much (given max of {max_entries}) list entries')
                    all_good = False
                
            # Check if mandatory information is really given. We only need to check those that are strings, 
            # as others can either not be defined without a value, or are already called during typecasting
            # within the convert method. Therefore we only need to search for empty strings. 
            if self.all[param]['mandatory_info']:
                for val in val_list:
                    if val == '':
                        self.warn.warn(f'\'{param}\' is a mandatory information, but an empty value was given')
                        all_good = False
                        
            # Check if parameters that need to be some predefined options are valid
            if not self.all[param]['options'] is None:
                if not all(val in self.all[param]['options'] for val in val_list):
                    all_good = False
                    options = self.all[param]['options']
                    self.warn.warn(f'\'{param}\' is not one of the mandatory options {options}')

            # Now we check if variables are within predefined boundaries if given
            in_bounds = True
            if not self.all[param]['weak_boundaries'] is None:
                wb = self.all[param]['weak_boundaries']
            try:
                # Maximum
                if not self.all[param]['max'] is None:
                    max = self.all[param]['max']
                    if wb:
                        if not all(val <= max for val in val_list):
                            in_bounds = False
                    else:
                        if not all(val < max for val in val_list):
                            in_bounds = False
                else:
                    max = 'inf'
                # Minimum
                if not self.all[param]['min'] is None:
                    min = self.all[param]['min']
                    if wb:
                        if not all(val >= min for val in val_list):
                            in_bounds = False
                    else:
                        if not all(val > min for val in val_list):
                            in_bounds = False    
                else:
                    min = '-inf'           
            except:
                in_bounds = False
                self.warn.warn(f'Format of \'{param}\' did not allow for boundary checking')     
            # Warn if not in bounds
            if not in_bounds:
                all_good = False
                if wb: 
                    self.warn.warn(f'Parameter \'{param}\' is not within predefined boundaries: {min} <= {param} <= {max}')
                else:    
                    self.warn.warn(f'Parameter \'{param}\' is not within predefined boundaries: {min} < {param} < {max}') 
        
        del(content)

        return all_good      
    
    def convert_from_ini(self, content, display = False):
        '''
        Converts content from an ini file into the right format according to the predefined parameters.
        Can receive full ini content as well as just one section, as decomposing and composing happens
        within this method
        
        :param content: decomposed dict
        :param display: If True, parameters with the corresponding definition are converted
        :returns: A dict including converted and checked values
        '''
        
        # We check if all given handles and sectionsare defined ones, Even if this is done twice when 
        # this method is called from read_ini.
        if not self.check_for_keys(content):
            return None 
        # We decompose the given dictionary.
        content, struct = self.decompose(content)  
        # Now that we now we only deal with predefined parameters, we can convert them according to 
        # their respective definitions.
        converted_content = {}
        
        for param, value in content.items():
            # As some parameters come as lists, we convert all parameters to list while leaving the others unaffected.
            if self.all[param]['list']:
                val = value
                # Only if given as string
                if not type(val) is list:
                    if not val[0] == '[' and not val[-1] == ']':
                        self.warn.warn(f'\'{param}\' is not a list as intended')
                        return None
                    val = ''.join(val.split())
                    val = val.strip('][').split(',')

            else:
                val = [value]
                
            # Now we try to assign the right type
            for i in range(0, len(val)):
                try:
                    if not self.all[param]['type'] is bool:
                        val[i] = self.all[param]['type'](val[i])
                    else:
                        if val[i] == 'True':
                            val[i] = True
                        elif val[i] == 'False':
                            val[i] = False
                        else:
                            self.warn.warn(f'\'{param}\' is not stored as boolean (True or False)')
                            return None
                except:
                    cast = self.all[param]['type']
                    self.warn.warn(f'Typecast of \'{param}\' to {cast} not possible')
                    return None
                   
            # Now we reconsider list objects and turn others back
            if not self.all[param]['list']:
                val = val[0]
                    
            # Store into returned object
            converted_content[param] = val
            
        # Now that we have all parameters at the right type, we need to check if the values are within 
        # their predefined borders and if some of them are one of their allowed options
        if not self.check_content(converted_content):
            self.warn.warn(f'One or multiple parameters to convert do not fullfill their predefined requirements, see warnings')
            return None
        
        # At the very last, convert to display units if desired
        if display:
            for param in converted_content.keys():
                if not self.all[param]['display_factor'] is None:
                    converted_content[param] *= self.all[param]['display_factor']
        
        # Compose the dict according to stored structure
        converted_content = self.compose(converted_content, struct)
        
        del(content)
        
        return converted_content
  
    def read_ini(self, dir, convert = False, all_defined = False, display = False):
        '''
        Reads ini content and returns a dictionary of the content. If the convert option is chosen, 
        predefined parameters are converted according to their definition. This already includes
        a check. Depending on the argument all_defined, either only recognized parameters are converted
        and checked, or all of them. The first option will not throw any error if not defined handles are
        present and simply return unknown content as strings, while the second one will throw an error
        if any unknown parameters are present.
        This way, one can use this function as a simple ini reader (convert and all_defined == False), 
        as a mixture of parameter checking and simple ini reader (convert == True, all_defined == False) 
        or as an exclusive ini reader for a file with only predefined parameters, which are all checked. 
        
        :param dir: Directory of the ini file to read
        :param convert: Looks for predefined parameters and converts them. Others are just returned as strings
        :param all_defined: If True, all read out parameters need to be predefined
        :param display: If True, parameters which have a display factor defined are returned including this
        :returns: A dict of the file content, optionally converted, checked and in display units
        '''
                
        # First of all, we receive the ini content.
        if not os.path.isfile(dir):
            self.warn.warn(f'Ini directory does not exist')
            return None
        else:
            config = configparser.RawConfigParser()
            config.optionxform = str
            config.read(dir)
            content = config._sections

        # If all parameters should be defined, we check that here.
        if all_defined:
            if not self.check_for_keys(content):
                return None          

        # We decompose the given dictionary.
        content, struct = self.decompose(content)  
        
        # If we wanto convert, we only convert predefined parameters. We need to split the content.
        if convert:
            to_convert = {}
            for key, val in content.items():
                if key in self.valid_keys:
                    to_convert[key] = val
            # Convert 
            to_convert = self.convert_from_ini(to_convert, display)
            
            # Update the converted variables in the file content. 
            for key, val in to_convert.items():
                content[key] = val
        
        # If we do not want to convert, but displayed is True, we need to separate them, apply the factor
        # and convert back to string
        if not convert and display:
            # Get handles that have a display factor
            disp_params = []
            for param, definitions in self.all.items():
                if not definitions['display_factor'] is None:
                    disp_params.append(param)
            # Search for these parameters and convert them with flag display True
            to_display = {}
            for param, val in content.items():
                if param in disp_params:
                    to_display[param] = val
            # Apply display factot
            to_display = self.convert_from_ini(to_display, display = True)
            # Convert back to string
            to_display = self.convert_to_string(to_display)
            # Assign the displayed values back
            for param, val in to_display.items():
                content[param] = val
        
        # At the very last, we compose our input content into the original structure.
        ini_content = self.compose(content, struct)
        del(content)
        
        return ini_content
        
    def write_ini(self, content, dir, all_defined = False, displayed = False):
        '''
        Writes the dict (of dicts) into an ini file. It can be used various ways of different security
        levels. If all_defined is True, all parameters must be known and predefined, and are checked 
        that way. If that is not the case, only recognized parameters are checked. 
        If all_defined is False, the method works as a simple ini writer converting all content into strings.
        This method works with unconverted as well as converted data, even mixed together. However, not
        predefined parameters are of course just passed on and written as strings.
        
        :param content: ini structure conform dictionary
        :param dir: Directory to store the ini file 
        :param file_name: Name of the ini file
        :param check: If True, all parameters recognized as predefined are checked for their validity 
                      corresponding to their definitions
        :param all_defined: If True, all parameters must be predefined, which is checked
        :param displayed: If True, parameters are given in their display-units, which the program accounts
                          for and converts back into SI
        :returns: None
        '''
        
        # Allow for filenames without right format
        if not dir[-4:] == '.ini':
            dir += '.ini'
                
        # If we only deal with predefined parameters, check that.
        if all_defined:
            if not self.check_for_keys(content):
                raise(RuntimeError('One or more section names or parameter handles are not predefined, see warnings. Writing aborted.'))

        # We decompose the dict
        content, struct = self.decompose(content)

        # Check parameters that can be checked
        # We get all predefined parameters that we can check.
        to_check = {}
        for param, val in content.items():
            if param in self.valid_keys:
                to_check[param] = val
        # Now we filter those that need to be converted before we can check them. To be safe, we 
        # include all with type string, even if they are already in the right format.
        to_convert = {}
        for param, val in to_check.items():
            convert = False
            # We need to account, that list items have type list
            if not self.all[param]['list']:
                if not self.all[param]['type'] is type(val):
                    convert = True
            else:
                if not type(val) is list:
                    convert = True
                else:
                    if not type(val[0]) is self.all[param]['type']:
                        convert = True
            if convert:
                to_convert[param] = val
 
        # Convert
        to_convert = self.convert_from_ini(to_convert)
        
        # Add again for checking
        for param, val in to_convert.items():
            to_check[param] = val
        if not self.check_content(to_check):
            raise(RuntimeError('One or more parameters are not valid, see warnings. Writing aborted'))

        # If we want to correct display factors, we make sure that those parameters are converted
        # Make sure all parameters to refactor are of right type
        if displayed:
            for param, val in to_check.items():
                if not self.all[param]['display_factor'] is None:
                    content[param] = val
            for param in self.valid_keys:
                if not self.all[param]['display_factor'] is None and param in content.keys():
                    content[param] /= self.all[param]['display_factor']
                                        
        # To write to ini, we need to convert all parameters to string.
        ini_content = self.convert_to_string(content)
        # Last but not least, recompose the dict and write the content onto the ini file
        ini_content = self.compose(ini_content, struct)
        del(content)
    
        config = configparser.RawConfigParser()
        config.optionxform = str
        for section, info in ini_content.items():
            config[section] = info
            
        # Write the configfile
        with open(dir, 'w') as configfile:
            config.write(configfile)
        
        return None
  
    def convert_to_string(self, content):
        '''
        Converts a dict into strings such that it can be used by configparser.
        
        :param content: Dict in right type
        :returns: The dict in strings, ready to write to ini file
        '''
        
        string_content = {}
        
        for param, val in content.items():
            if not type(val) is list:
                string_content[param] = str(val)
            else:
                list_string = '['
                for i, item in enumerate(val):
                    if not i == 0:
                        list_string += ','
                    list_string += str(item)
                list_string += ']'
                string_content[param] = list_string
        
        return string_content
  
    def update_ini(self, content, dir):
        '''
        Updates an ini file with the given content. If a new section is given, it is added. If it is an
        existing one, it is switched.
        
        :param dir: Directory of the ini file
        :param content: Ini update which should be added to the ini file
        :returns: None
        '''
        
        # We only need string content
        ini_content = self.read_ini(dir, convert = False, all_defined = False, display = False)
        
        # Update the content
        for sec, params in content.items():
            ini_content[sec] = params
        
        # Write to file again
        self.write_ini(ini_content, dir, all_defined = False, displayed = False)
        
        return None
