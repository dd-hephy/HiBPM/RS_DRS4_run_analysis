'''
    File name: Analyze_Run.py
    Author: Philipp Gaggl
    Date created: 15.11.2021
    Python Version: 3.8, 3.9
'''

import posixpath
import numpy as np
import pandas as pd
import os
import time
from collections import Counter
import matplotlib.pyplot as plt
import shutil

from Run_Config import Ini_Handler
from fileReader import Det_File
from tool_box import Warn_Me

class Analyse_Run():
    '''
    Evaluates all files from a source folder. Reads the included .ini file with the run-settings and 
    creates a corresponding one in the target folder. This one also includes a list of all already analyzed
    files. The peak data is written onto a separate .csv file within the target folder. Overall statistics 
    can be created by using this file. 
    Also creates an .ini file for each data file summarizing the unique statistics. Furthermore, 
    exemplary plotting for each file is possible.
    '''
    
    REMOVE_HIST_OUTLIERS = True
        
    def __init__(self, source_path, target_path, folder_appendix = None):
        '''
        Class to analyze a whole run folder.
        
        :param source_path: path to the run folder containing the osci-data files and the mandatory .ini file
                            with the run settings
        :param target_path: folder path where the results should be written to
        '''
        
        # Warning helper
        self.warn = Warn_Me()

        # Ini handler
        self.ini_handler = Ini_Handler()
        
        print('-------------------------------------------------------')
        print('Initializing run analysis...')
        
        # Store arguments in class variables
        self.source_dir = source_path
        self.target_dir = target_path
        self.single_file_ramp = None
        
        # Check whether the source folder exists
        if not os.path.isdir(source_path):
            raise(RuntimeError('The given source directory does not exist!!!'))
        
        # Check if any .ini file is included within the source directory
        self.config_file = self.find_config_file(self.source_dir)
        if not self.config_file is None:
            self.config_file = posixpath.join(self.source_dir, self.config_file)
        else:
            raise(RuntimeError('There is no .ini file containing mandatory run information within the source directory!!!'))
        
        # Extracts the config information. Checks the validity of all entries
        self.config_data = self.ini_handler.read_ini(self.config_file, convert = True, all_defined = True, display = False)
        if self.config_data is None:
            raise(RuntimeError('Missing mandatory information in .ini file, aborted!!!'))
        
        # Get the osci and adapt the file formats to search for
        self.osci = self.config_data['BASIC']['oscilloscope']
        if self.osci == 'RS':
            self.file_formats = ['.bin']
        elif self.osci == 'DRS4':
            self.file_formats = ['.dat']
        elif self.osci == 'Weightfield':
            self.file_formats = ['.txt']
        elif self.osci == 'Allpix':
            self.file_formats = ['.csv' , '.npz']
        elif self.osci == 'CNM':
            self.file_formats = ['.csv']
        else:
            raise(RuntimeError('The given oscilloscope is not a valid one. Either use RS, DRS4, AllPix or Weightfield'))      
                
        # Get the run number for correct result data saving
        self.run_no = self.config_data['BASIC']['run_no']
        if int(self.run_no / 10) == 0:
            self.run_no_str = '0' + str(self.run_no)
        else:
            self.run_no_str = str(self.run_no)
        
        # Store directions for analysis folder
        if not folder_appendix is None:
            self.target_dir = posixpath.join(self.target_dir, (f'run_{self.run_no_str}_analysis_{folder_appendix}'))
        else:
            self.target_dir = posixpath.join(self.target_dir, (f'run_{self.run_no_str}_analysis'))
        
        # Get the channels
        self.channels = self.config_data['BASIC']['detectors']
        
        # Print some information
        print('Working on run-' + self.run_no_str)
        print('Channels to analyze:')
        for channel in self.channels:
            print('-->', channel)
        
        # If there already is an analysis directory, we delete it beforehand
        if os.path.isdir(self.target_dir):
            shutil.rmtree(self.target_dir)
        # Create target folder if not already there
        try:
            os.mkdir(self.target_dir)
        except:
            pass
                
        return None
       
    def find_config_file(self, folder):
        '''
        Checks whether the given director includes any .ini file. Only one is allowed!!!
        
        :param folder: Folder path in which to search for an .ini file
        :returns: Filename of the ini file
        '''
        # Get all files within the source folder
        files = os.listdir(folder)
        
        # Check for .ini file
        for file in files:
            body, base = os.path.splitext(file)
            if base == '.ini':
                return file
        
        return None
    
    def get_files_with_format(self, folder, formats):
        '''
        Searches a folder for files with fitting format and returns a list
        
        :param folder: Folder directory to search within
        :param format: List of file endings to search for (e.g. .bin, .dat)
        :returns: List of all analyzable files within the folder
        '''
        
        files = os.listdir(folder)
        files_with_format = []
        for file in files:
            for format in formats:
                body, base = os.path.splitext(file)
                if base == format:
                    # Need to exclude .Wfm.bin files in case of RS-osci 
                    if format == '.bin' and '.Wfm' in file:
                        continue
                    files_with_format.append(file)            
                
        return files_with_format 

    def analyse_peaks(self):
        '''
        Here happens the actual analysis. Each file is analyzed. The peak results are appended to the 
        peak_data.csv files according to their channels. Average parameters within the summary.ini
        file are updated. A result folder for each file is created, which holds an individual summary.ini
        file and some exemplary plots for each channel. 
        
        :returns: List of the .csv files (full path) with the peak data.
        '''
        
        t_run = time.time()

        # Get files to analyze
        files = self.get_files_with_format(self.source_dir, self.file_formats)
        print('Files to be analyzed:')
        for file in files:
            print('-->', file)
            
        # Convert to directories
        file_paths = []
        for file in files:
            file_paths.append(posixpath.join(self.source_dir, file))
        no_files = len(files)
        detectors = self.channels
        output_files = dict.fromkeys(detectors)
        
        if len(files) == 0:
            print('-------------------------------------------------------')
            print('No run analysis')
            print('-------------------------------------------------------')
            return None
        else:
            print('-------------------------------------------------------')
            print(f'Beginning analysis of {no_files} files...')
                
        # Create .csv file
        self.result_data_file = []
        self.result_data_path = []
        for i, detector in enumerate(self.channels):
            self.result_data_file.append('run_' + self.run_no_str + '_' + detector + '_peak_data.csv')
            self.result_data_path.append(posixpath.join(self.target_dir, self.result_data_file[i]))
        self.result_params = self.ini_handler.pulse_params
        for i, path in enumerate(self.result_data_path):
            print(f'Creating peak_data.csv file for {self.channels[i]}...')
            header = pd.DataFrame(columns = self.result_params)
            header.to_csv(path, sep = ';', index = False)
                    
        # Variable for avg noise info 
        self.avg_noise_info = dict.fromkeys(detectors)
        for det in self.avg_noise_info.keys():
            self.avg_noise_info[det] = {'rms_noise_[V]':[], 'ptp_noise_[V]':[], 'threshold_[V]':[], 'compensated_offset_[V]':[]}    
        
        # Now go through each file to analyze            
        for i, file in enumerate(file_paths):
            print(f'\nFile {i+1}/{no_files}: {files[i]}')
            t_file = time.time()
            
            # Initialize the Det_File object to work on as well as result parameters for later
            det_file = Det_File(file)
            
            # Inform the user about the sample length
            self.sample_length = det_file.meta['t_sample']
            print(f'Sample length: {self.sample_length} s')
            
            file_results = dict.fromkeys(detectors)
            file_statistics = dict.fromkeys(detectors)
            
            # Store the analysis parameters and t_sample. Only do that for the first file, since they 
            # stay the same through all files
            if i == 0:
                self.ana_params = det_file.ana_params.copy()
                self.ana_params['t_sample_[s]'] = det_file.meta['t_sample']
            
            # Connect the detector names given in the .ini file with the channel names within the meta-data
            # from the Det_File object.
            self.ch_det = {}
            if not len(detectors) == len(det_file.meta['source_names']):
                self.warn.warn('The number of channels in the file and the .ini file are not compatible.')
                raise(RuntimeError('The number of channels in the file and the .ini file are not compatible!!!'))
            else:
                for j, det in enumerate(detectors):
                    self.ch_det[det] = det_file.meta['source_names'][j]

            # Keep count of the overall peak numbers
            if i == 0:
                num_run_peaks = dict.fromkeys(self.ch_det.values())
                for det in num_run_peaks.keys():
                    num_run_peaks[det] = 0

            # Depending on the option of the advanced_noise_offset, we choose between the two methods
            # to calculate our offset and noise
            if self.ana_params['advanced_noise_offset']:
                print('\nPerforming advanced offset and noise calculation...')
                t = time.time()
                det_file.advanced_noise_offset()
                print(f'Done {time.time() - t} s')
                
            else:
                # Apply artificial offset compensation if desired within fileReader
                if self.ana_params['compensate_offset']:
                    print('\nCompensate for artificial offset...')
                    t = time.time()
                    det_file.compensate_offset()
                    print(f'Done {time.time() - t} s')

                # Get threshold and statistics
                print('Get statistics...')
                t = time.time()
                det_file.getStatistics()
                print(f'Done {time.time() - t} s')

            # Get the noise-rms and the used threshold for later summary
            det_file.get_threshold()
            self.noise_info = {}

            # Now go through each channel, find the peaks and analyse them
            print('\nPeak finding and analysis...')
            
            for det, ch in self.ch_det.items():
                t_channel = time.time()
                print(f'---Channel: {det}')
                print(f'   {np.sum(det_file.valid_events[ch])} valid events to investigate...')

                self.noise_info[det] = {}
                self.noise_info[det]['rms_noise_[V]'] = det_file.converted_noise[ch]
                self.avg_noise_info[det]['rms_noise_[V]'].append(det_file.converted_noise[ch])
                self.noise_info[det]['ptp_noise_[V]'] = det_file.converted_noise_ptp[ch]
                self.avg_noise_info[det]['ptp_noise_[V]'].append(det_file.converted_noise_ptp[ch])
                self.noise_info[det]['threshold_[V]'] = det_file.avg_thresholds[ch]
                self.avg_noise_info[det]['threshold_[V]'].append(det_file.avg_thresholds[ch])
                if self.ana_params['compensate_offset']:
                    self.noise_info[det]['compensated_offset_[V]'] = det_file.converted_offset[ch]
                    self.avg_noise_info[det]['compensated_offset_[V]'].append(det_file.converted_offset[ch])
                else:
                    self.avg_noise_info[det]['compensated_offset_[V]'].append(0.0)

                # Get time over threshold
                print('   Get time over threshold...')
                t = time.time()
                channel_events = det_file.getTot(ch)
                
                # Only proceed if events are found
                if channel_events is None:
                    print('   No peaks found')
                else:
                    print(f'   {len(channel_events[1])} peaks found')
                    num_run_peaks[ch] += len(channel_events[1])
                    print(f'   Done {time.time() - t} s')
                    
                    # If Constant fraction discrimination is set to True in fileReader, we apply it here
                    if self.ana_params['apply_constant_fraction_discrimination']:
                        CFD_factor = self.ana_params['cfd_factor']
                        print(f'   Apply constant fraction discrimination using CFD_factor: {CFD_factor}...')
                        t = time.time()
                        channel_events = det_file.constant_fraction_discrimination(channel_events, ch, CFD_factor)
                        print(f'   Done {time.time() - t} s')
                    
                    # It could be that we have None as channel events
                    if not channel_events is None:
                        # Find the right analysis buffer according to the rms in each event
                        if det_file.ana_params['analysis_till_rms_noise']:
                            t = time.time()
                            print(f'   Searching for rms-analysis-buffer...')
                            channel_events = det_file.get_rms_buffer(ch, channel_events)
                            print(f'   Done {time.time() - t} s')
                            
                        if not channel_events is None:
                            # Analyse the events and append to the overall result data
                            print('   Analysing peaks...')
                            t = time.time()
                            file_results[det] = det_file.analyse_peaks(channel_events, ch)
                            print(f'   Write to .csv...')
                            dir_out = self.write_to_csv(file_results[det], det)
                            
                            # Extract single file ramp data
                            if not det_file.SINGLE_FILE_RAMP is None:
                                print(f'   Extracting single file ramp information...')
                                self.single_file_ramp_param = det_file.SINGLE_FILE_RAMP
                                self.extract_single_file_ramp_info(det_file.ramp_info)
                                        
                            # Update output path for the output
                            if not output_files[det] == dir_out:
                                output_files[det] = dir_out
                            print(f'   Done {time.time() - t} s') 
                                
                            # Create exemplary plots if wanted
                            if not self.ana_params['no_example_plots'] == 0:
                                print('   Create example plots...')
                                t = time.time()
                                # Create plot folder and file folders within
                                plot_folder = posixpath.join(self.target_dir, 'plots_single_peaks')
                                try:
                                    os.mkdir(plot_folder)
                                except:
                                    pass    
                                file_plot_folder = posixpath.join(plot_folder, os.path.splitext(files[i])[0])
                                try:
                                    os.mkdir(file_plot_folder)
                                except:
                                    pass
                                # Make a channel plot folder and apply plotting
                                plot_dir = posixpath.join(file_plot_folder, det)
                                det_file.plotEvents(channel_events, plot_dir, ch, det)
                                print(f'   Done {time.time() - t} s')
                            
                            else:
                                print('   No example plots set...')
                    
                # Calculate file statistics, even if there are no events, we write out noise info 
                print('   Calculating file statistics...')
                t = time.time()
                file_statistics[det] = self.calculate_avg_and_std(file_results[det])
                for key in self.noise_info[det]:
                    file_statistics[det][key] = self.noise_info[det][key]                     
                    
                print(f'   Done {time.time() - t}')
                print(f'   Channel analyzed, {time.time() - t_channel} s\n')
            
            # Create file summary file
            print('Creating file summary...')
            t = time.time()
            body, _ = os.path.splitext(files[i])
            file_path = posixpath.join(self.target_dir, body + '_analysis.ini')
            self.ini_handler.write_ini(file_statistics, file_path, all_defined = False, displayed = False)
            print(f'Done {time.time() - t}')

            print(f'File analyzed {time.time() - t_file} s')
            
        # Here we will plot and store the obtained data from a single file ramp if given
        if not self.single_file_ramp is None:
            self.store_single_file_ramp()    
        
        print(f'\nRun analyzed {time.time() - t_run} s')
        
        # Print out overall channel numbers
        for det, ch in self.ch_det.items():
            print(f'--> {det}: {num_run_peaks[ch]} found peaks')
        
        # Initialize a summary content for summary.ini after analysis
        summary_content = self.config_data.copy()
        summary_content['ANALYSIS_PARAMETERS'] = self.ana_params.copy()
        summary_content['ANALYZED_FILES'] = {}
        for i, file in enumerate(files):
            summary_content['ANALYZED_FILES'][str(i)] = file
                
        # Create the summary .ini file in the target directory
        self.result_config_file = 'run_' + self.run_no_str + '_summary.ini'
        self.result_config_path = posixpath.join(self.target_dir, self.result_config_file)
        print('Creating summary.ini file...')
        self.ini_handler.write_ini(summary_content, self.result_config_path, all_defined = False, displayed = False)

        print('-------------------------------------------------------')
        
        return output_files      

    def write_to_csv(self, data, detector):
        '''
        Writes the peak parameter into the corresponding .csv files. Returns the file path
        
        :param data: DataFrame of the peak data
        :param detector: Detector name according to .ini file to access the right file
        :returns: Directory of the csv-file
        '''
        
        index = self.channels.index(detector)
        file_dir = self.result_data_path[index]
        data.to_csv(file_dir, sep = ';', header = False, mode = 'a', index = False)
        
        return file_dir

    def calculate_avg_and_std(self, data):
        '''
        Receives the DataFrame from the analyse_peaks() method and returns a dict with the characteristics
        
        :param data: DataFrame including peak height, area and tot from the analyzed channel
        :param threshold: Threshold from the analyzed channel
        :returns: Dict with the avg and std values
        '''
        
        statistics = {}

        # If we have no events, we just write in zero peaks
        if data is None:
            statistics['peaks'] = 0
        else:
            statistics['peaks'] = len(data)
        
            for col in data.columns:
                statistics['avg_' + col] = np.mean(data[col])
                statistics['std_' + col] = np.std(data[col])

        return statistics         

    def analyse_csv(self, analyse_output):
        '''
        Analyses the resulting .csv file containing the peak data (tot, height, area) for each channel.
        Also creates histograms for each channel and parameter with corresponding names.
        Uses the methods get_raw_bin_data(), write_hist_to_csv() and remove_hist_outlier()
        
        :param analyse_output: dict for each channel containing the average threshold and the file path to 
                               the .csv file. This output is given by the method analyse_peaks()
        :returns: None
        '''  
        
        # Assign different plot colors to each channel consistent with the peak plot colors 
        print('Generating statistics...')
        t_stat = time.time()
        colors = ['green', 'blue', 'orange', 'black']
        plot_colors = {}
        
        # Extract the data from the .csv files
        if not analyse_output == None:
            # Assign plot colors to each channel
            for i, channel in enumerate(analyse_output.keys()):
                plot_colors[channel] = colors[i]
            results = {}
            
            for channel, file_dir in analyse_output.items():
                # Get the data from the .csv file
                if not file_dir is None:
                    channel_data = pd.read_csv(file_dir, sep = ';')
                    channel_results = self.calculate_avg_and_std(channel_data) 
                    results[channel] = channel_results
                    
                    # Go through each parameter in the channel data and create the histograms
                    # Due to discrete values from the OSCI, we count the abundance of these values and just plot these
                    # We base the bin width of non discrete values on the minimum of bins for the histograms 
                    # with discrete values. 
                    for param in channel_data.columns:
                        data = channel_data[param]
                        # Since we interpolate the start and stop samples upon the used threshold, we do not have 
                        # discrete values for the ToT anymore. Therefore we do not need the second procedure for
                        # binning and histogram generation. The code is left anyways, in case it is needed sometime
                        # in the future
                        
                        '''   
                        if param == 'time_over_threshold_[s]':
                            # Create discrete binning data
                            bins, vals, count = self.get_raw_bin_data(data)    
                            # Write raw information into .csv file and remove outliers for plotting
                            self.write_hist_to_csv(channel, bins, vals, param)
                            # Remove outlier for more aesthetic plotting
                            bins, vals, count = self.remove_hist_outlier(bins, vals, channel, param)
                            # For good plotting, we need to insert the discrete values that have not been hit
                            # and assign an abundance of zero to them
                            # The smallest value for the width between two bins is used as bin width
                            width = []
                            if not len(bins) == 1:
                                # Find the smallest discrete distance and use it as bin width 
                                for i in range(0, len(bins) - 1):
                                    width.append(bins[i + 1] - bins[i])
                                bin_width = np.min(width)
                                # Now fill in missing bins
                                for j in range(0, len(width)):
                                    overshoot = int(width[j] / bin_width) - 1
                                    if overshoot > 0:
                                        start = bins[j] + bin_width
                                        for k in range(0, overshoot):
                                            count[start + k * bin_width] = 0
                            
                            # Finally, extract the x and y data
                            x_data = list(count.keys())
                            x_data.sort()
                            y_data = []
                            for bin in x_data:
                                y_data.append(count[bin])

                            # Norm the data to relative abundancy
                            area = np.sum(y_data)
                            y_data = y_data / area * 100
                            # Plot the histogram
                            plt.close()
                            plt.step(x_data, y_data, color = plot_colors[channel], where = 'mid')
                            plt.fill_between(x_data, y_data, step = 'mid', color = plot_colors[channel])
                            plt.gca().set_xlim(left = 0)
                            plt.title(f'run_{self.run_no_str}, {channel}: {param}')
                            plt.xlabel(param)
                            plt.ylabel(f'relative abundance within {len(data)} peaks [%]')
                            plt.grid(True)
                            plt.tight_layout()
                            plt.savefig(posixpath.join(self.target_dir, f'{channel}_histogram_{param}.png'))
                            plt.close()
                            del(data)

                        else: 
                        '''
                        # Plot histograms of non discrete values. Maximal bin number is 1000
                        bins = int(len(data) / 10)
                        if bins > 1000:
                            bins = 1000
                        if bins <= 0:
                            bins = 1
                        # Remove outliers if wanted
                        if self.REMOVE_HIST_OUTLIERS:
                            x_data = self.remove_hist_outlier(data, [0] * len(data), channel, param)    
                            x_data = x_data[0]
                        else:
                            x_data = data

                        # Plot the histogram
                        plt.close()
                        bin_val, bin_edges, _ = plt.hist(x = x_data, bins = bins, color = plot_colors[channel], weights = np.ones(len(x_data)) / len(x_data) * 100)
                        # Only cut off the histogram if there can be no negative value in real life
                        if not param == 'hardware_offset_[V]':
                            plt.gca().set_xlim(left = 0)
                        plt.grid(True)
                        plt.xlabel(param)
                        plt.ylabel(f'relative abundance within {len(x_data)} peaks [%]')   
                        plt.title(f'run_{self.run_no_str}, {channel}: {param}')
                        plt.savefig(posixpath.join(self.target_dir, f'{channel}_histogram_{param}.png'))
                        plt.close()
                        del(data)
                            
                        # Write the histogram data to .csv (only the relative abundancies)
                        # We need the bin centres for that
                        bin_centres = []
                        for i in range(0, len(bin_edges) - 1):
                            bin_centres.append((bin_edges[i] + bin_edges[i+1]) / 2)
                        self.write_hist_to_csv(channel, bin_centres, bin_val, param, normed = True)
                    
                    print(f'{channel}: Histograms added to analysis folder')    
                    
                    # Also plot sequential plots for visual checking
                    self.plot_sequential_peaks(channel, plot_colors[channel])
            
                else:
                    results[channel] = {'peaks':0}
                
            # Already update the summary.ini file within this function
            for det in results.keys():
                # Add the rms of the noise, the used threshold and the compensated offset
                # Attention, currently only the values from the last analyzed files are written out
                for key in self.avg_noise_info[det].keys():
                    # If we deal with None values, we delete it
                    prev = len(self.avg_noise_info[det][key])
                    self.avg_noise_info[det][key] = list(filter(None.__ne__, self.avg_noise_info[det][key]))
                    if len(self.avg_noise_info[det][key]) < prev:
                        removed = prev - len(self.avg_noise_info[det][key])
                        self.warn.warn(f'{removed} elements removed for avg values for parameter \'{key}\' and detector \'{det}\'')
                    results[det][key] = np.mean(self.avg_noise_info[det][key])
                    
            # Update the summary.ini accordingly
            self.ini_handler.update_ini(results, self.result_config_path)    
            print(f'{det}: Statistics written to summary file')
        
        else:
            print('Result files unchanged')
        
        print(f'Done {time.time() - t_stat} s')
        print('-------------------------------------------------------')

        return None
        
    def get_raw_bin_data(self, raw_data):
        '''
        Creates raw histogram data for discrete valued raw_data by counting the values
        
        :param raw_data: Raw data of a peak
        :returns: Three lists corresponding to the bins, the bin heights and a count object of them
        '''
        
        # Extract the data
        count = dict(Counter(raw_data))
        x_data = list(count.keys())
        x_data.sort()
        y_data = []
        for bin in x_data:
            y_data.append(count[bin])

        return x_data, y_data, count
    
    def write_hist_to_csv(self, channel, x_data, y_data, column, normed = False):
        '''
        Writes the histogram data to the corresponding csv
        
        :param channel: Used channel
        :param x_data: bin data
        :param y_data: corresponding values
        :param column: name of the parameter investigated
        :param normed: If false, the absolute values are added and relative ones are calculated. If True, 
                       only the relative values are written
        :returns: None
        '''

        if not len(x_data) == len(y_data):
            raise(RuntimeError('Histogram data with inconsistent length!!!'))
        
        # Write data to a DataFrame to handle .csv-writing
        data = pd.DataFrame(columns = [column, 'rel_abundancy_%'])
        data[column] = x_data
        if not normed:
            data['abs_abundancy'] = y_data
            
            # Norm the data for the output
            area = np.sum(data['abs_abundancy'])
            relative = []
            for val in data['abs_abundancy']:
                relative.append(val / area * 100)
            data['rel_abundancy_%'] = relative
        else:
            data['rel_abundancy_%'] = y_data
            
        # Before editing the data, write it to a .csv file in the same folder
        file_name = str(channel) + '_' + column + '_histogram_data.csv'
        path = posixpath.join(self.target_dir, file_name)
        data.to_csv(path, sep = ';', header = True, index = False)  

        return None

    def remove_hist_outlier(self, x_data, y_data, channel = '???', column = '???'):
        '''
        Removes histogram outliers which surpass 2.5 the std
        
        :param x_data: binning data
        :param y_data: corresponding values
        :param channel: channel, only for writing information
        :param column: investigated parameter, also only for writing information
        :returns: Otlier corrected bin data, as separate lists and as dict
        '''
        
        # Calculate mean and std
        avg = np.mean(x_data)
        std = np.std(x_data)
        
        # Filter outliers
        removed = 0
        adapted_x = []
        adapted_y = []
        adapted_data = {}
        
        for i, bin in enumerate(x_data):
            if not (bin > avg + 2.5*std):
                adapted_x.append(bin)
                adapted_y.append(y_data[i])
                adapted_data[bin] = y_data[i]
            else:
                removed += 1

        print(f'{channel} - {column}: {removed} outliers have been removed for plotting')
        print(f'{channel} - {column}: Raw data written to .csv')


        


        return adapted_x, adapted_y, adapted_data

    def plot_sequential_peaks(self, channel, color = 'green'):
        '''
        Creates plots for each parameters of all peaks sequentially and places it into the result folder
        for visual checking. Places them into an individual subfolder.
        
        :param channel: Name of the source channel
        :param color: Plot color
        :returns: None
        '''
        
        # Get the data
        file_dir = self.result_data_path[self.channels.index(channel)]
        data = pd.read_csv(file_dir, sep = ';')
        
        # Create the plot folder and channel folder
        plot_folder = posixpath.join(self.target_dir, 'plots_sequential_peaks')
        try:
            os.mkdir(plot_folder)
        except:
            pass

        det_folder = posixpath.join(plot_folder, channel)
        try:
            os.mkdir(det_folder)
        except:
            pass
        
        # Now plot
        x_data = np.arange(1, len(data) + 1, 1)
    
        for param in self.result_params:            
            plt.close()
            plt.grid(True)
            plt.xlabel('peak no.')
            plt.ylabel(param)
            plt.title(f'run_{self.run_no_str}, {channel}: {param}')
            plt.plot(x_data, data[param], color = color)
            save_dir = posixpath.join(det_folder, f'{channel}_sequential_{param}.png')
            plt.savefig(save_dir)
            plt.close()
            
        # Here we have an additional option to access voltage data if given in the meta file
        
        return None
     
    def extract_single_file_ramp_info(self, ramp_info):
        '''
        Extracts the single file ramp info from the det file variable and stores it in here
        
        :param ramp_info: dict containing all necessary information coming from Det_File()
        :returns: None
        '''
        
        # Create the variable
        if self.single_file_ramp is None:
            self.single_file_ramp = dict.fromkeys(ramp_info)
            for ch in self.single_file_ramp.keys():
                self.single_file_ramp[ch] = dict.fromkeys(ramp_info[ch])
                for param in self.single_file_ramp[ch].keys():
                    self.single_file_ramp[ch][param] = dict.fromkeys(ramp_info[ch][param])
                    for ramp in self.single_file_ramp[ch][param].keys():
                        self.single_file_ramp[ch][param][ramp] = []
                            
        # Now we check if we have any new information that has not been included in previous files
        for ch in ramp_info.keys():
            if not ch in list(self.single_file_ramp.keys()):
                self.single_file_ramp[ch] = dict.fromkeys(ramp_info[ch])
            for param in ramp_info[ch].keys():
                for ramp in ramp_info[ch][param].keys():
                    if not ramp in list(self.single_file_ramp[ch][param].keys()):
                        self.single_file_ramp[ch][param][ramp] = []

        # And finally we can extract and store the data
        for ch, ch_info in ramp_info.items():
            for param, param_info in ch_info.items():
                for ramp, r_info in param_info.items():
                    [self.single_file_ramp[ch][param][ramp].append(val) for val in r_info]
                    
        return None
    
    def store_single_file_ramp(self):
        '''
        Createsa folder in the result folder and stores plots of the single file ramps as well as 
        an .ini file with the data
        
        :returns: None
        '''
        print(self.single_file_ramp)
        # Create the corresponding folder and subfolders for each channel
        dir = posixpath.join(self.target_dir, 'single_file_ramp')
        try:
            os.mkdir(dir)
        except:
            pass
        
        ch_dirs = {}
        for ch in self.single_file_ramp.keys():
            ch_dirs[ch] = posixpath.join(dir, str(ch))
            try:
                os.mkdir(ch_dirs[ch])
            except:
                pass
        
        # Now we create the plots:
        for ch, ch_info in self.single_file_ramp.items():
            for param, param_info in ch_info.items():
                plot_dir = posixpath.join(ch_dirs[ch], (ch + '_' + param + '.png'))
                x = np.zeros(len(param_info))
                avg = np.zeros(len(param_info))
                std = np.zeros(len(param_info))
                i = 0
                for ramp, vals in param_info.items():
                    x[i] = float(ramp)
                    avg[i] = np.mean(vals)
                    std[i] = np.std(vals)
                    i += 1
                    # Now we sort
                    sort = np.argsort(x)
                    x = x[sort]
                    avg = avg[sort]
                    std = std[sort]
                    
                # Convert to readable units
                avg *= self.ini_handler.pulse_params_conversion_factors[param]
                std *= self.ini_handler.pulse_params_conversion_factors[param]
                # And plot
                plt.close()
                _, ax = plt.subplots()
                ax.plot(x, avg, color = 'green', marker = 'o', markersize = 5, linestyle = '-')
                ax.fill_between(x, (avg + std), (avg - std), alpha = 0.4, color = 'green', edgecolor = None)
                ax.set_title(ch + ', ' + self.ini_handler.pulse_params_converted[param])
                ax.set_xlabel(self.single_file_ramp_param)
                ax.set_ylabel(self.ini_handler.pulse_params_converted[param])
                plt.savefig(plot_dir)
                plt.close()
                
        return None