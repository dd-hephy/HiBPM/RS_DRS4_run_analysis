'''
    File name: fileReader.py
    Author: Philipp Gaggl, Simon Waid
    Date created: 2021
    Python Version: 3.8, 3.9
'''

import os
import posixpath
from re import I
import numpy as np
import pandas as pd
import numpy as np
import matplotlib.pylab as plt
import scipy.integrate as integrate

from scipy.optimize import curve_fit

import readout_classes as RO
from Run_Config import Ini_Handler
from tool_box import Warn_Me
from tool_box import gauss_function, get_fit_params_estimations


class Det_File(RO.DRS4_file, RO.RS_file, RO.Weightfield_file, RO.CNM_file, RO.Allpix_file):
    '''
    Extends RS_File with analysis functions for detectors.
    '''
    
    SINGLE_FILE_RAMP = None
        
    def __init__(self, file_name, preview = None, analysis_ini = None):
        '''
        :param file_name: The file name without extension.
        :param analysis_ini: Data path to an analysis parameter file that should be used instead of the current_settings.ini within the ini_library
        :param preview: Number of peaks that ought to be returned as plots.This happens by calling the function execute_preview(), which performs a first analysis
        
        :returns: None
        '''

        # Warning helper
        self.warn = Warn_Me()
        
        # Ini helper
        self.ini_handler = Ini_Handler()
        
        # At the very first, check if preview mode is enabled
        if not preview is None:
            self.preview = True
            self.preview_peaks = preview[0]
            self.preview_axes = preview[1]
        else:
            self.preview = False

        # Variable used if an additional offset calculation is done. Will be changed by the method compensate_offset
        self.artificial_offset = None
        # Variable used for peak to peak noise
        self.noise_ptp = None
       
        # Instantiate the right class whether it is data from an RS or a DRS4 file
        if not os.path.exists(file_name):
            raise(RuntimeError('The targeted file does not exist!!!'))
        file_base = os.path.splitext(file_name)[1]
        
        # We need to differentiate from which osci class the method _getRaw and the property data_raw 
        # shall be taken as these usually initialte using standard mro order that can't be changed easily
        if file_base == '.bin':
            print('Using RS_file class')
            self.osci = 'RS'
            RO.RS_file.__init__(self, file_name)
            self._getRaw = lambda *args, **kwargs: RO.RS_file._getRaw(self, *args, **kwargs)
            self.data_raw = self.data_raw_RS
        elif file_base == '.dat':
            print('Using DRS4_file class')
            self.osci = 'DRS4'
            RO.DRS4_file.__init__(self, file_name)
            self._getRaw = lambda *args, **kwargs: RO.DRS4_file._getRaw(self, *args, **kwargs)
            self.data_raw = self.data_raw_DRS4
        elif file_base == '.txt':
            print('Using Weightfield_file class')
            self.osci = 'Weightfield'
            RO.Weightfield_file.__init__(self, file_name)
            self._getRaw = lambda *args, **kwargs: RO.Weightfield_file._getRaw(self, *args, **kwargs)
            self.data_raw = self.data_raw_Weightfield
        elif file_base == '.csv' or file_base == '.npz':
            # For .csv files we need a separation between Allpix_file and CNM_file
            if file_base == '.csv':
                try:
                    self.osci = 'CNM'
                    RO.CNM_file.__init__(self, file_name)
                    print('Using CNM_file class')
                    self._getRaw = lambda *args, **kwargs: RO.CNM_file._getRaw(self, *args, **kwargs)
                    self.data_raw = self.data_raw_CNM
                except:
                    print('Using Allpix_file class')
                    self.osci = 'Allpix'
                    RO.Allpix_file.__init__(self, file_name)
                    self._getRaw = lambda *args, **kwargs: RO.Allpix_file._getRaw(self, *args, **kwargs)
                    self.data_raw = self.data_raw_Allpix

        else:
            raise(RuntimeError('Data type of the target file is not supported!!!'))        
                    
        # Get the number of segments 
        self.segments = self.meta['NumberOfAcquisitions']
        self.segment_length = int(self.meta['length'] / self.meta['NumberOfAcquisitions'])
        self.seg_num = np.arange(0, self.segments, 1)
        # Little helper for vectorized functions
        self.unity = np.array([1.0] * self.segments)
        
        # Variable used to store pulse information in case of a ramp parameter present within a single file in the different events
        # This ramp parameter must be present within the meta information
        if not self.SINGLE_FILE_RAMP is None:
            # Check whether this variable is indeed present within the meta information and checks out with the event length
            if not self.SINGLE_FILE_RAMP in list(self.meta.keys()):
                self.warn.warn(f'The ramp parameter \'{self.SINGLE_FILE_RAMP}\' is not included in the meta information. Proceeding without considering single file ramp evaluation')
                self.SINGLE_FILE_RAMP = None
            elif not len(self.meta[self.SINGLE_FILE_RAMP]) == self.segments:
                self.warn.warn(f'The meta information on the single file ramp paramter \'{self.SINGLE_FILE_RAMP}\' is not of equal length as the given events within the file data. Proceeding without considering single file ramp evaluation')
                self.SINGLE_FILE_RAMP = None
            else: 
                # If everything is ok we create the corresponding variable to hold the information
                self.ramp_info = dict.fromkeys(self.meta['source_names'])
                for ch in self.ramp_info.keys():
                    self.ramp_info[ch] = dict.fromkeys(self.ini_handler.pulse_params)
                    for param in self.ramp_info[ch].keys():
                        self.ramp_info[ch][param] = dict.fromkeys(self.meta[self.SINGLE_FILE_RAMP])
                        for r in self.ramp_info[ch][param].keys():
                            self.ramp_info[ch][param][r] = []
                print(f'Single file ramp enabled. Using ramp parameter \'{self.SINGLE_FILE_RAMP}\'')
                                      
        # We make sure that conversion factor and offset are set to one and zero if apply_offset==False
        if not self.meta['apply_offset']:
            self.meta['conversion_factor'] = dict.fromkeys(self.meta['source_names'], 1.0)
            self.meta['offset'] = dict.fromkeys(self.meta['source_names'], 0.0)
                    
        # Load the analysis parameters. In case of a preview, we load a different file
        self.load_analysis_parameters(analysis_ini)                    

        # To keep track of discarded events
        self.valid_events = dict.fromkeys(self.meta['source_names'])
        for ch in self.valid_events.keys():
            self.valid_events[ch] = np.array([True] * self.segments)
                                
        # Apply the right search direction
        self.search_options = ['none', 'positive', 'negative']
        if not self.ana_params['search_direction'] in self.search_options:
            raise(RuntimeError('No valid search direction given (either \'none\', \'positive\' or \'negative\''))
        
        return None
        
    def load_analysis_parameters(self, ana_ini):
        '''
        Loads the analysis parameters from the corresponding setting.ini file. This happens according 
        to the mode (preview or not). It is also checked (and warned if present), if the plot-buffer
        is set lower than any analysis buffer. If so, it is adapted in the settings AND the setting file
        and a warning is given.
        
        :param ana_ini: path to analysis .ini file to use for the analysis parameters
        
        :returns: None
        '''

        if ana_ini is None:
            path = posixpath.join(os.getcwd(), 'ini_library', 'analysis_parameters') 

            # Choose parameter file according to preview mode or not
            if not self.preview:
                directory = posixpath.join(path , 'current_settings.ini')
            else:
                directory = posixpath.join(path, 'preview_settings.ini')
            
        else:
            directory = ana_ini
            
        # Load the parameter settings (Both in right format and in strings)
        self.ana_params = self.ini_handler.read_ini(directory, convert = True, all_defined = True, display = False)['ANALYSIS_PARAMETERS']
        self.ana_params_str = self.ini_handler.read_ini(directory, convert = False, all_defined = True, display = False)['ANALYSIS_PARAMETERS']
        
        # Check for valid buffer plot. If not, change it, overwrite the parameter file, and throw a warning.
        buffer_plot = self.ana_params['buffer_plot']
        min_buffer = max(self.ana_params['buffer_analysis_left'], self.ana_params['buffer_analysis_right'])
        
        if not buffer_plot > min_buffer:
            # Adjust minimal plotting range
            self.ana_params['buffer_plot'] = min_buffer
            
            # Rewrite and reload parameter file, throw warning
            content = {'ANALYSIS_PARAMETERS':self.ana_params}
            self.ini_handler.write_ini(content, directory, all_defined = True, displayed = False)
            self.warn.warn('The plotting range is less than the analysis range. This is not allowed. Plot settings have been adjusted and updated in the parameter file.')
            
            # Reload the to update values
            self.ana_params = self.ini_handler.read_ini(directory, convert = True, all_defined = True, display = False)['ANALYSIS_PARAMETERS']
            self.ana_params_str = self.ini_handler.read_ini(directory, convert = False, all_defined = True, display = False)['ANALYSIS_PARAMETERS']

        # Check if the advanced offset compensation and noise calculation is activated. We need to ensure that
        # we have multiple events (e.g. fast segmentation) and not just raw recording. Also, we need data taken from
        # an osci and not simulation data, as these usually do not include noise to calculate with. 
        if self.ana_params['advanced_noise_offset']:
            switch_advanced = False

            # Check if the data is from an osci and not from some simulation
            if not self.osci == 'RS' and not self.osci == 'DRS4' and not self.osci == 'CNM':
                switch_advanced = True
                self.warn.warn(f'\'advanced offset compensation and noise level calculation\' has been switched off, as we do not deal with experimental data, but simulation data from {self.osci}. Such data has no noise to calculate on.')
                
            # Switch off advanced mode if requirements are not met
            if switch_advanced:
                self.ana_params['advanced_noise_offset'] = False
                self.ana_params_str['advanced_noise_offset'] = 'False'
        
        # Pure simulation analysis only allowed for simulation data
        if self.ana_params['pure_simulation_analysis']:
            if self.osci == 'RS' or self.osci == 'DRS4' or self.osci == 'CNM':
                self.ana_params['pure_simulation_analysis'] = False
                self.ana_params_str['pure_simulation_analysis'] = 'False'
                self.warn.warn(f'\'pure simulation analysis\' only allowed for simulation data. The data is from the esource \'{self.osci}\' and the mode has been deactivated')
                
        # We set the analysis buffer to zero if we use rms-buffer
        if self.ana_params['analysis_till_rms_noise']:
            self.ana_params['buffer_analysis_right'] = 0
            self.ana_params['buffer_analysis_left'] = 0
            
            self.buffer_left = dict.fromkeys(self.meta['source_names'])
            self.buffer_right = dict.fromkeys(self.meta['source_names'])
            for key in self.buffer_left.keys():
                self.buffer_left[key] = [[] for _ in range(self.segments)]
                self.buffer_right[key] = [[] for _ in range(self.segments)]

        return None 
            
    def _rawToDtypeOut(self, data, event = 0):
        '''
        Helper function applying the conversion_factor, offset and scaling to the given data.        
        :param data: dict like self.data_raw
        '''

        for key in data.keys():
            data[key] = data[key].astype(np.float32)

            if self.ana_params['compensate_offset']:
                data[key] -= self.artificial_offset[key][event]

            if self.meta['apply_offset']:
                data[key] *= self.meta['conversion_factor'][key]
                data[key] += self.meta['offset'][key]
        
        return data

    def getAsDf(self, event = 0, start = None, stop = None, source = None, time = True):
        '''
        Returns a dataframe containin the data between start and stop. By default, also timestamps will be included.
        You can set time to False to exclue timestamps.
        This is useful for plotting or other processing of large files.
        
        :param start: Index of starting sample
        :param stop: Index of final sample
        :param source: Oscilloscope Source Name 
        :param time: Optional. Defaults to True. If false, no time data is returned.
        :type time: Boolean
        '''
        
        # Filtering is done a separate function. We get filtered data without time.
        my_data = self._getRaw(event = event, start = start, stop = stop, source = source)
        
        # We don't have time information in our data yet.
        # It's time to convert and scale.
        my_data = self._rawToDtypeOut(my_data, event)

        # We now have to add the time if requested by the user.
        # If xy data was selected we get a time axis from the oscilloscope. 
        # Otherwise we have to generate it.
        if time:
            if  'X-Time' in self.meta['channel_type']:
                tsChannel = self.meta['channel_type'].index('X-Time')
                name = self.meta['channel_name'][tsChannel]
                # If we have x-y data we also have to apply the start stop filter.
                my_data[name] = self.data_raw[name][start:stop]
            else:
                # Generate time stamps. We only generate them for the inteval between start and stop
                timestamps = self._genTimeAxis(start, stop)
                my_data['Time'] = timestamps

        # Generate a DataFrame
        try:
            df = pd.DataFrame(my_data)
        except:
            print('Breakpoint')
            raise
        
        return df

    def _genTimeAxis(self, start, stop):
        '''
        Generate a time axis for the given sample inverval
        
        :param start: Start of the interval as sample number
        :param stop: End of the interval as sample number
        '''
        
        # Things get easier if we have proper numbers as start and end 
        if start is None:
            start = 0
        if stop is None:
            stop = self.segment_length 
        
        # Calculate start and stop in the time domain
        xStart = start * self.meta['t_sample']
        # The -1 is necessary because we push the starting point in getTOT up one, such that it is above threshold, while we leave the length the same --> length must be -1
        xStop = (stop - 1) * self.meta['t_sample']
        length = stop - start

        timestamps = np.linspace(xStart, xStop, length, dtype = np.float64)

        return timestamps

    def advanced_noise_offset(self):
        '''
        Function that replaces the conventional offset compensation and noise calculation by a more 
        advanced method, but under stronger constraints for the data. A preceeding peak finding using 
        maximum voltage and peak validation filters already filters out events with non-sufficient peaks.
        For each validated one, the offset and noise are calculated using a Gaussian fit of 90% of the
        points before that peak. Events for which the fit is not good (e.g. if there is a second peak) are
        removed automatically. This should only be used for low fluxes and high/distinct peaks (e.g. Si).
        
        The idea is based on a software available on https://gitlab.cern.ch/egkougko/lgadutils
        However, some of the procedure is done different here, leading to the same outcome.
        
        We work on raw data for efficiency. Also, we apply peak finding, offset compensation and noise
        calculation directly in series, to avoid multiple loading of data.
        
        :returns: None  
        '''

        # Initialize start and stop samples variable
        noise = dict.fromkeys(self.meta['source_names'])
        offset = dict.fromkeys(self.meta['source_names'])
        polarity = dict.fromkeys(self.meta['source_names'])
        discarded = dict.fromkeys(self.meta['source_names'])
        for ch in noise.keys():
            noise[ch] = np.array([0.0] * self.segments)
            offset[ch] = np.array([0.0] * self.segments)
            polarity[ch] = np.array([0.0] * self.segments)
            discarded[ch] = 0
            
        # We need the built in offset to move the data to the zero line, conversion factor is not necessary
        offset_raw = self.get_built_in_offset()
        
        # Now we go through each event and channel
        for event in range(0, self.segments):
                        
            # Get data
            raw_dat = self._getRaw(event)             
            
            # We store the polarity of the returned peak too, in order to check for the most abundant 
            # polarity and remove events that do not fit
            for ch in noise.keys():
                
                # First, we apply the preceeding peak finding
                fit_result = self.get_advanced_noise_offset(raw_dat[ch], ch, event, offset_raw[ch]) 
                 
                # Remember discarded events for later  
                if fit_result is None:
                    self.valid_events[ch][event] = False
                    discarded[ch] += 1
                    
                else:
                    offset[ch][event] = fit_result[0]
                    noise[ch][event] = fit_result[1]
                    polarity[ch][event] = fit_result[2]
        
        # Print out information about discarded events
        print('   # of discarded events due to bad noise fits:')
        for ch, bad in discarded.items():
            previous_valid_events = np.sum(self.valid_events[ch]) + bad
            print(f'   {ch} --> {bad}/{previous_valid_events}')
        print()
        
        # Before going on, we filter out all events that do not obey the most abundant polarity. We also
        # consider the search direction option
        self.polarity = polarity
        self.filter_wrong_polarities()
                    
        # Finally, store them as same variables as used in conventional offset/noise calculation
        self.artificial_offset = offset
        self.noise = noise
        
        # Print out and save the converted offsets and different noise levels
        self.converted_offset = dict.fromkeys(self.meta['source_names'])
        self.print_and_store_avg_values(self.artificial_offset, self.converted_offset, 'Artificial offset', 'mV', 1000)
        self.converted_noise = dict.fromkeys(self.meta['source_names'])
        self.print_and_store_avg_values(self.noise, self.converted_noise, 'RMS noise levels', 'mV', 1000)
        self.converted_noise_ptp = dict.fromkeys(self.meta['source_names'])
        self.print_and_store_avg_values(self.noise_ptp, self.converted_noise_ptp, 'Peak-to-peak noise levels', 'mV', 1000)

        return None

    def get_advanced_noise_offset(self, event_data, channel, event, offset = 0):
        '''
        Searches for the maximum absolute voltage and checks if we deal with a peak or not. 
        If so, the start and stop points are taken from the zero-points of the second derivative.
        
        :param event_data: data of the event
        :param channel: channel name
        :param event: event number
        :param offset: offset which needs to be considered for the finding
        :returns: tuple of index values of start and stop samples as well as the value of the maximum absolute amplitude
        '''
        
        # First of all, we find the maximum absolute voltage
        max_pos = np.argmax(np.abs(event_data + offset))
        amp = event_data[max_pos] + offset
        pol = np.sign(amp)

        # We check if we don't deal with a maximum right at the beginning/end of an event
        if max_pos == 0 or max_pos == (self.segment_length - 1):
            return None

        # We require at least two sample points above 50% of V_max to count as signal
        req = (0.5 * amp) - offset
        if not (pol * event_data[max_pos + 1]) > (pol * req) and not (pol * event_data[max_pos - 1]) > (pol * req):
            return None
        
        # We only consider this as valid event if we can find at least one point on both sides of the maximum where 
        # the voltage drops to at least 20 % of the maximum again (excluding maxima right at the edges)    
        req = ((0.2 * amp) - offset) * pol
        # Left side
        if np.sum((pol * event_data[:max_pos]) < req) < 1:
            return None
        # Right side
        if np.sum((pol * event_data[max_pos:]) < req) < 1:
            return None
        #print('start')
        
        # Perform the Gauss fit of the noise data
        popt, noise_start, noise_stop = self.noise_fit(channel, event_data, max_pos, pol)
        
        # The offset is given by the mean value, while the std represents the noise
        if not popt is None:
            # Get the peak-to-peak noise
            self.get_peak_to_peak_noise(event_data[noise_start:noise_stop], channel, event, one_sided = False)
            return ((popt[1] + offset), popt[2], pol)
        else:
            return popt

    def noise_fit(self, channel, event_data, max_pos, pol, left_side = True, frac_start = 0.05, frac_stop = 0.8, min_frac = 0.2, tried_other = False):
        '''
        Performs the noise fit outgoing from the amplitude position. Also already performs a check for 
        corrupt peaks.
        
        :param channel: present channel
        :param event_data: whole event data
        :param max_pos: index of the amplitude
        :param pol: polarity (-1 or 1) to use corrupting peak check
        :param left_side: if True, the noise data is taken from th eleft side of the peak (default), otherwise from the right one
        :param frac_start: percentage of the distance from border to peak max (on the corresponding side) to start
        :param frac_stop: percentage of the distance from border to peak max (on the corresponding side) to stop
        :param min_frac: number of noise data samples must be at least this fraction of the whole event length
        :param tried_other: determines if the function has already been called on the other side of the main peak
        
        :returns: the fit parameters of the Gauss fit as well as start and stop samples if the data used for noise calculation
        '''

        # Cut the data to investigate
        if left_side:
            start = int(frac_start * max_pos)
            stop = int(frac_stop * max_pos)
            noise_dat = event_data[start:stop]
        else:
            dist = len(event_data) - max_pos
            start = int(max_pos + frac_start * dist)
            stop = int(max_pos + frac_stop * dist)
            noise_dat = event_data[start:stop]

        # Check if the number of samples is sufficient, if not we do the other side
        if not len(noise_dat) >= min_frac * self.segment_length:
            #print('   err-short')
            if not tried_other:
                #print('   cont-short')
                popt, start, stop = self.noise_fit(channel, event_data, max_pos, pol, not left_side, tried_other = True)
            else:
                #print('   stop-short')
                return None, start, stop
          
        else:
            # Obtain the histogram data (minimum of 100 bins)
            if len(noise_dat) > 10000:
                bin_no = int(0.01 * len(noise_dat))
            else:
                bin_no = 100
            hist = np.histogram(noise_dat, bins = bin_no)
            x_dat = (hist[1][:-1] + hist[1][1:]) / 2

            # Fit the data to a Gauss function. In case the fit does not work, we discard the event for the given channel
            try:
                popt, pcov = curve_fit(gauss_function, x_dat, hist[0], get_fit_params_estimations('Gauss', x_dat, hist[0], 100))
            except:
                return None, start, stop

            # Now we check for corrupting peaks. If so, we check the other side
            if not self.check_for_corrupting_peaks(x_dat, hist[0], pol, popt[1], popt[2]):
                #print('   err-bad')
                # Only try other side if we didn't already
                if not tried_other:
                    #print('   cont-bad')
                    popt, start, stop = self.noise_fit(channel, event_data, max_pos, pol, not left_side, tried_other = True)
                else:
                    #print('   stop-bad')
                    return None, start, stop
        #print('Yayyy') 
        return popt, start, stop

    def check_for_corrupting_peaks(self, hist_x, hist_y, pol, mu, sigma, sigma_factor = 5, fail_number = 3):
        '''
        Used to check for corrupting second peaks within the histogram data of the noise. This is done 
        via checking the border of the data on the corresponding polarity. If there are at least two samples that are outside the 
        two sigma range, we quite surely have a corrupting peak within the fit data. To consider outliers, 
        at least two samples must be outside the range.
        
        :param hist_x: bin positions if the histogram
        :param hist_y: bin heights of the histogram
        :param pol: -1 or 1, representing the polarity of the main signal
        :param mu: mean value of the obtained Gauss fit
        :param sigma: sigma of the obtained Gauss fit
        :param sigma_factor: multiplicity of sigma where good sample should reside in
        :param fail_number: number of samples outside of the trust region for which we call it corrupt
        
        :returns: True if everything is ok, False if a corrupting peak was detected
        '''
        
        # Depending on the polarity, corrupting peaks appear on the left or right side of the Gaussian noise
        if pol == -1:
            mask = hist_x < (mu - sigma_factor * sigma)
        elif pol == 1:
            mask = hist_x > (mu + sigma_factor * sigma)
        else:
            raise(RuntimeError('Unpredicted behaviour in corruptiing peak search'))
        
        no_corrupt = np.sum(hist_y[mask])
        if no_corrupt > fail_number:
            return False
        else:
            return True      

    def compensate_offset(self):
        '''
        Takes the raw data and checks for an additional offset coming from the oscilloscope. After first calculation,
        a removing of outliers coming from peaks is executed and a new offset is determined. This will be compensated
        in all future calls of getRaw. The function will be executed for each channel.
        Attention: This method is only suggested if really necessary and also if the used beam rates are rather low.
        
        :returns: None
        '''

        # Get the known offset
        self.artificial_offset = dict.fromkeys(self.meta['source_names'])
        osci_offset_raw = self.get_built_in_offset()
        for ch in self.meta['source_names']:
            self.artificial_offset[ch] = np.array([0.0] * self.segments)

        # We work with raw data to be faster
        for seg in range(0, self.segments):
            data = self._getRaw(seg)
            # Go through each channel
            for ch in data.keys():
                channel_data = np.array(data[ch].copy())

                # Now we calculate the offsets via mean. We repeat this procedure at least one time, each time removing
                # samples that are out of the one sigma region. This way we filter out peak values compromising our 
                # base line. If one wants to be more exact, but slower, just enlargen num_repetitions
                dev = (channel_data + osci_offset_raw[ch])
                offset = np.mean(dev)
                std_offset = np.std(dev)
                num_repetitions = 1
                for i in range(0, num_repetitions):
                    mask = np.abs(dev - offset) < std_offset 
                    dev = dev[mask]
                    offset = np.mean(dev)
                    if not i == (num_repetitions - 1):
                        std_offset = np.std(dev)
                
                self.artificial_offset[ch][seg] = offset
                
        # Print out and save the converted offsets
        self.converted_offset = dict.fromkeys(self.meta['source_names'])
        self.print_and_store_avg_values(self.artificial_offset, self.converted_offset, 'Artificial offset', 'mV', 1000)
        
        return None
 
    def filter_wrong_polarities(self):
        '''
        Uses the class variable polarity to update the values of valid_events of the polarity does not 
        fit to the most abundant one. Here, we also define our search direction if not given by the user. 
        We only want to only count those that are actually the opposite polarity. Those that have been 
        discarded earlier during the fit have value zero and need not to be considered
        
        :returns: None
        '''
        
        print('   Filtering signals with wrong polarities...')
        self.search_direction = {}
        
        discarded = dict.fromkeys(self.meta['source_names'])
        for ch in discarded.keys():
            discarded[ch] = 0
        
        # We of course consider search direction
        if not self.ana_params['search_direction'] == 'none':
            if self.ana_params['search_direction'] == 'positive':
                ref = +1
            elif self.ana_params['search_direction'] == 'negative':
                ref = -1
            else: 
                raise(RuntimeError('Undefined search direction during advanced offset/noise calculation'))
            for ch in self.meta['source_names']:
                self.search_direction[ch] = ref

        # Access the class variable
        for ch in self.polarity.keys():
            if self.ana_params['search_direction'] == 'none':
                self.search_direction[ch] = np.sign(np.sum(self.polarity[ch]))
            
            # Filter out events with false polarity. No need to update noise and offset values as they
            # are not considered if valid_events[i] is false
            mask = (self.polarity[ch] == -self.search_direction[ch])
            self.polarity[ch][mask] = False
        
        if self.ana_params['search_direction'] == 'none':
            for ch, pol in self.search_direction.items():
                if pol == 1:
                    print(f'   Detected polarity channel \'{ch}\' --> positive')
                if pol == -1:
                    print(f'   Detected polarity channel \'{ch}\' --> negative')
        
        # Print out information about discarded events
        print('   # of discarded events due to wrong polarity:')
        for ch, bad in discarded.items():
            previous_valid_events = np.sum(self.valid_events[ch]) + bad
            print(f'   {ch} --> {bad}/{previous_valid_events}')
        print()
                             
        return None

    def print_and_store_avg_values(self, get, store, name, unit = '', unit_conv = 1, include_conversion = True):
        '''
        Used to print out average values of e.g. noise or offset into the terminal, and also store it
        in a given variable.
        
        :param get: the dict which includes the data to calculate the average on
        :param store: the dict where the average values of the channels should be stored
        :param name: the name of the variable that should be printed out with the values
        :param unit: Unit to be printed out
        :param unit_conv: Conversion factor if the given unit is not in SI. The value will only be printed in the converted unit, not stored as such
        :param include_conversion: If True, the conversion factor of the source is used
        
        :returns: None
        '''
        
        # First print out our name
        print(f'   {name}...')
        
        # Get conversion factor
        conv = dict.fromkeys(self.meta['source_names'])
        for ch in conv.keys():
            if include_conversion:
                conv[ch] = self.meta['conversion_factor'][ch]
            else:
                conv[ch] = 1.0
        
        if get is None:
            self.warn.warn(f'Received \'None\' as argument when trying to print and store \'{name}\'')
            return None
        
        # We only work on valid events
        for ch, val in get.items():
            print(f'   {ch}:')
            filtered = val[self.valid_events[ch]]
            
            # We store the avg value in the given variable and remember the max and min for printing
            if len(filtered) > 0:
                store[ch] = np.mean(filtered) * conv[ch]
                max_std = np.amax(filtered) * conv[ch]
                min_std = np.amin(filtered) * conv[ch]
            else:
                store[ch] = 0
                max_std = 0
                min_std = 0
                
            print(f'   --> avg: {store[ch] * unit_conv} {unit}')
            print(f'   --> max: {max_std * unit_conv} {unit}')
            print(f'   --> min: {min_std * unit_conv} {unit}')
        print()
            
        return None

    def get_built_in_offset(self):
        '''
        :returns: A dict with the built in (raw) offsets of the channels as given in the meta data 
        '''

        raw_offset = {}
        for ch in self.meta['source_names']:
            if self.meta['apply_offset']:
                raw_offset[ch] = round(self.meta['offset'][ch] / self.meta['conversion_factor'][ch])

            else:
                raw_offset[ch] = 0
            
        return raw_offset

    def getStatistics(self, start = None, stop = None):
        '''
        Goes through each event and calculates the noise via calculating the statistics on the polarity
        side opposite to the maximum value.
        
        :param start: See :py:meth:`.getAsDf` 
        :param stop: See :py:meth:`.getAsDf`
        '''

        polarity = dict.fromkeys(self.meta['source_names'])
        noise = dict.fromkeys(self.meta['source_names'])
        for ch in self.meta['source_names']:
            if self.ana_params['search_direction'] == 'none':
                polarity[ch] = np.array([0] * self.segments)
            elif self.ana_params['search_direction'] == 'positive':
                polarity[ch] = np.array([1] * self.segments)
            elif self.ana_params['search_direction'] == 'negative':
                polarity[ch] = np.array([-1] * self.segments)
               
            noise[ch] = np.array([0.0] * self.segments)
                    
        # We operate on one channel at a time to keep the memory footprint low.
        for ch in self.meta['source_names']:

            # We work on the raw data to be faster. To do so, we need to get the zero line in raw
            centers = self.val_to_raw(0, ch, self.seg_num)
            
            # Iterate over all events
            for seg in range(0, self.segments):
                data_ch = self._getRaw(seg, start = start, stop = stop, source = ch)[ch]
            
                # Check over the whole sample whether peaks are on the negative or the positive side. 
                my_min = np.amin(data_ch)
                my_max = np.amax(data_ch)
            
                # Consider forced peak search direction
                if self.ana_params['search_direction'] == 'none':
                    if np.abs(my_min - centers[seg]) > np.abs(my_max - centers[seg]):
                        mask = data_ch >= centers[seg]
                        polarity[ch][seg] = -1
                    else:
                        mask = data_ch <= centers[seg]
                        polarity[ch][seg] = 1
                else:
                    if self.ana_params['search_direction'] == 'positive':
                        mask = data_ch <= centers[seg]
                    elif self.ana_params['search_direction'] == 'negative':
                        mask = data_ch >= centers[seg]
                    else:
                        raise(RuntimeError('Unexpected behaviour considering mean calculation on forced voltage side!!!'))
              
                masked = data_ch[mask]
                # Calculate the standard deviation around the center and append
                if len(masked) > 0:
                    noise[ch][seg] = (np.sqrt(np.mean(np.square(masked - centers[seg]))))
                    # Also get the peak-to-peak noise 
                    self.get_peak_to_peak_noise(masked, ch, seg, one_sided = True)
            
        # Discard events that do not obey the right polarity
        self.polarity = polarity
        self.filter_wrong_polarities()
        
        # Store the results in class variable
        self.noise = noise
        
        # Print and store the converted values for the different noise levels
        self.converted_noise = dict.fromkeys(self.noise)
        self.print_and_store_avg_values(noise, self.converted_noise, 'RMS noise levels', 'mV', 1000)
        self.converted_noise_ptp = dict.fromkeys(self.meta['source_names'])
        self.print_and_store_avg_values(self.noise_ptp, self.converted_noise_ptp, 'Peak-to-peak noise levels', 'mV', 1000)

        return None
    
    def get_peak_to_peak_noise(self, noise, channel, event, one_sided = False):
        '''
        receives noise data and stores the peak-to-peak noise value in self.noise_ptp
        
        :param noise: noise data
        :param channel: channel name
        :param event: event number
        :param one_sided: if True, only one side of noise data is given and the result is multiplied by two
        
        :returns: None
        '''
        
        # Initialize the holding variable if not already here
        if self.noise_ptp is None:
            self.noise_ptp = {}
            for ch in self.meta['source_names']:
                self.noise_ptp[ch] = np.array([0.0] * self.segments)
                
        min = np.min(noise)
        max = np.max(noise)
        
        if not one_sided:
            self.noise_ptp[channel][event] = (max - min)
        else:
            self.noise_ptp[channel][event] = (max - min) * 2
                    
        return None
    
    def get_threshold(self):
        '''
        Returns the threshold for each channel based the given std values as 
        we get it for each channel from _getStatistics
        
        :returns: None
        '''

        # Initialize the variables
        self.thresholds_raw = dict.fromkeys(self.meta['source_names'])
        self.thresholds = dict.fromkeys(self.meta['source_names'])
        self.avg_thresholds = dict.fromkeys(self.meta['source_names'])
        for ch in self.thresholds.keys():
            self.thresholds_raw[ch] = np.array([0] * self.segments)
            self.thresholds[ch] = np.array([0.0] * self.segments)
        std_factor = self.ana_params['std_factor']

        # Go through each channel and apply the procedure
        for ch, values in self.noise.items():
            # If we have a fixed threshold, we need to convert to raw
            if self.ana_params['use_fixed_threshold']:
                th = self.ana_params['fixed_threshold'] * self.search_direction[ch]
                self.thresholds[ch] = np.array([th] * self.segments)
                self.thresholds_raw[ch] = self.val_to_raw(th, ch, self.seg_num)
                self.avg_thresholds[ch] = th
                    
            # If we use the noise, we need to convert to voltage 
            else:
                # We need to use the value w.r.t. the center
                self.thresholds_raw[ch] = self.val_to_raw(0.0, ch, self.seg_num) + ([self.search_direction[ch]] * std_factor * values)
                self.thresholds[ch] = self.search_direction[ch] * std_factor * values * self.meta['conversion_factor'][ch]
                self.avg_thresholds[ch] = np.mean(self.thresholds[ch])
            
        # Print out the results
        print('Average thresholds:')
        for ch, thres in self.avg_thresholds.items():
            print(f'--> {ch}: {thres * 1e3} mV')

        return None

    def getTot(self, source, tmin_signal_raw = None, tmin_gap = None, slope = False):
        '''
        Get the time over threshold for signals. Analyses the peak signals over the given
        threshold and determines where peaks occured and how long they lasted. Returns a dataframe 
        containing the start indices and the length (in samples) reffering to the raw data. It includes
        a fine tuning process, changing the minimal required signal length time according to the found
        data. 
        
        :param source: The source name on which the operation should be carried out
        :param threshold: The threshold for detecting an event. Unit: Volts
        :param cmp: Either '>' or '<'. Determines if values above or below the threshold should be considered
        :param tmin_signal_raw: Number of samples a signal must be above threshold to be considered a signal
        :param tmin_gap: Search limit for the minimal required time between two signals. Unit: seconds
        :param slope: If True, slope finding is active, meaning only threshold surpass will be done
        :returns: Pandas DataFrame. The Columns are Start and Duration. They contain the beginning and end of peaks in sample indices.  
        '''
        
        t_sample = self.meta['t_sample']
        
        if tmin_signal_raw is None:
            tmin_signal_raw = self.ana_params['min_tot_factor']
        tmin_signal = tmin_signal_raw * t_sample
        if tmin_signal < t_sample:
            raise(RuntimeError('Initial signal length is smaller than the sample size!!!'))
        
        # Apply the minimal time between two separate peaks
        if tmin_gap is None:
            tmin_gap = self.ana_params['min_time_between_peaks']
        tmin_gap_raw = int(tmin_gap / t_sample)

        # Lists that will hold the locations of the found peaks in the end as well as the positions of the
        # interpolated solution
        peaks_in_segment = []
        starts_out = []
        ends_out = []
        starts_interp = []
        ends_interp = []
        rise_time = []
        rise_time_interp = []

        # Return value for slope finding results
        if slope:
            results = dict.fromkeys(['Start', 'Duration', 'Starts_interp', 'Ends_interp'])
            for key in results.keys():
                results[key] = []

        # Get the hardware offset for the rise time finding
        off_all = self.get_built_in_offset()
        off = off_all[source]

        # If pure_simulation_analysis is activated, we just generate a pulse list including the whole 
        # event
        if self.ana_params['pure_simulation_analysis']:
            peaks_in_segment = [1] * self.segments
            starts_out = [0] * self.segments
            ends_out = [self.segment_length - 2] * self.segments
            starts_interp = [0] * self.segments
            ends_interp = [self.segment_length - 2] * self.segments
        
        # Otherwise, iterate through each event
        else:
            for seg in range(0, self.segments):
                # Only work on events that have not been discarded during noise and offset calculation
                if self.valid_events[source][seg]:
                    # We are working on raw data for efficiency
                    data = self._getRaw(event = seg, source = source)[source]
                    # Convert the threshold to raw data units if working with raw data. 
                    threshold_raw = self.thresholds_raw[source][seg]
                    if self.search_direction[source] == 1:
                        det = data > threshold_raw
                    elif self.search_direction[source] == -1:
                        det = data < threshold_raw

                    # Free memory
                    det = det.astype(np.int8)
                    # Theory of operation: 
                    # Lets assume we have the following in a:
                    # a = [0 0 0 1 1 1 1 0 0]
                    # We shift by 1 to the left:
                    # b = a[1:]
                    # b = [0 0 1 1 1 1 0 0]
                    # Now let's subtract:
                    # a[:-1] - b = [ 0 0 -1 0 0 0 1 0]
                
                    det2 = det[:-1] - det[1:]
                    # Free memory
                    del det

                    # Get indices of starting and ending points for each peak
                    starts = np.where(det2 == -1)
                    ends = np.where(det2 == 1)
                    starts = starts[0]
                    ends = ends[0]

                    # Free memory
                    del det2
                    
                    # Quick and dirty solution to get slope results for now
                    if slope:
                        if len(starts) > 0:
                            peaks_in_segment.append(1)
                            results['Start'].append(starts[0] + 1)
                            results['Duration'].append(len(data) - 1 - starts[0])
                            
                            # Interpolation only for start
                            y1 = data[starts[0]]
                            y2 = data[starts[0] + 1]
                            dx = (threshold_raw - y1) / (y2 - y1)
                            results['Starts_interp'].append(starts[0] + dx)
                            results['Ends_interp'].append(0)
                        else:
                            peaks_in_segment.append(0)
                        continue
                    
                    else:
                        # Ensure the start and ends to be in sync if cut off peaks exist in the raw data
                        if len(starts) > len(ends):
                            starts = starts[:-1]
                        elif len(ends) > len(starts):
                            ends = ends[1:]
                            
                        # Find the corresponding peaks according to some required settings 
                        if len(starts) > 0:
                            seg_starts, seg_ends = self.peak_filter(starts, ends, tmin_signal_raw, tmin_gap_raw)
                            # Append to other event data
                            peaks_in_segment.append(len(seg_starts))
                            for s1, s2 in zip(seg_starts, seg_ends):
                                starts_out.append(s1)
                                ends_out.append(s2)

                            # Get the interpolated starts and stops right at threshold
                            starts_int, ends_int = self.interp_start_stop(seg_starts, seg_ends, data, threshold_raw)
                            for s1, s2 in zip(starts_int, ends_int):
                                starts_interp.append(s1)
                                ends_interp.append(s2)
                                
                            # We do the rise time calculation already here using raw data
                            # Therefore we will need the built in offset
                            t_rise, t_rise_int = self.get_rise_time(seg_starts, seg_ends, data, source, seg, off)
                            for t1, t2 in zip(t_rise, t_rise_int):
                                rise_time.append(t1)
                                rise_time_interp.append(t2)
                        
                        else:
                            peaks_in_segment.append(0)

                else:
                    peaks_in_segment.append(0)
                
        # If we are in slope finding, we can return the values here already
        if slope:
            results = pd.DataFrame(results)
            return[peaks_in_segment, results]
                
        if not len(starts_out) == len(ends_out):
            self.warn.warn(f'Unequal number of starts ({len(starts_out)}) and stops ({len(ends_out)}) for channel \'{source}\'')
            raise(RuntimeError('Inconsistent starts/stops numbers'))
        if not len(starts_out) == len(starts_interp):
            self.warn.warn(f'Unequal number of starts ({len(starts_out)}) and interpolated starts ({len(starts_interp)}) for channel \'{source}\'')
            raise(RuntimeError('Inconsistent starts/starts_interp numbers'))
        if not len(starts_interp) == len(ends_interp):
            self.warn.warn(f'Unequal number of interpolated starts ({len(starts_interp)}) and interpolated stops ({len(ends_interp)}) for channel \'{source}\'')
            raise(RuntimeError('Inconsistent starts_interp/stops_interp numbers'))
        if not len(rise_time) == len(rise_time_interp):
            self.warn.warn(f'Unequal number of rise times ({len(rise_time)}) and interpolated rise times ({len(rise_time_interp)}) for channel \'{source}\'')
            raise(RuntimeError('Inconsistent rise_time/rise_time_interp numbers'))
        
        # End if no found peaks
        if len(starts_out) < 1:
            return None

        # Before skipping, check for consistency
        self.check_peak_number_consistency(starts_out, ends_out, peaks_in_segment)
        self.check_peak_number_consistency(starts_interp, ends_interp, peaks_in_segment)
        self.check_peak_number_consistency(rise_time, rise_time_interp, peaks_in_segment)

        # If one wants to skip borders, it is applied already here
        skip_borders = self.ana_params['skip_borders']
        if skip_borders > 0 and skip_borders < len(starts_out):
            starts_out, ends_out, peaks_in_segment = self.skip_peaks_at_borders(starts_out, ends_out, peaks_in_segment, skip_borders, side = 'both')
            starts_interp, ends_interp, peaks_in_segment = self.skip_peaks_at_borders(starts_interp, ends_interp, peaks_in_segment, skip_borders, side = 'both', adapt_seg_info = False)
            rise_time, rise_time_interp, peaks_in_segment = self.skip_peaks_at_borders(rise_time, rise_time_interp, peaks_in_segment, skip_borders, side = 'both', adapt_seg_info = False)
            print(f'   Skipped {skip_borders} peaks at the very beginning and end')

        num_peaks = np.sum(peaks_in_segment)

        # If we are in preview_mode, we reduce the number of peaks to go on with
        if self.preview:
            print(f'   Channel: {source}')
            print(f'   --> Sample time: {t_sample * 1e12} ps')
            if num_peaks > 0:
                print(f'   --> Would have found {num_peaks} peaks')
            else:
                print('   --> Did not find any peaks')
            if not len(starts_out) < self.preview_peaks:
                to_skip = len(starts_out) - self.preview_peaks
                starts_out, ends_out, peaks_in_segment = self.skip_peaks_at_borders(starts_out, ends_out, peaks_in_segment, to_skip, side = 'right')
                starts_interp, ends_interp, peaks_in_segment = self.skip_peaks_at_borders(starts_interp, ends_interp, peaks_in_segment, to_skip, side = 'right', adapt_seg_info = False)
                rise_time, rise_time_interp, peaks_in_segment = self.skip_peaks_at_borders(rise_time, rise_time_interp, peaks_in_segment, to_skip, side = 'right', adapt_seg_info = False)

        # Now convert starts and ends to starts and length of signals
        if len(starts_out) == 0:
            return None
        # Attention!!! Due to our wisher-procedure to find start and stops, out start value will now always
        # be the last point below threshold, while our duration later will represent the number of points that 
        # are actually above threshold
        # But, we would like to have start AND stop (the stops that are later used within plotting and analyzing)
        # to be the ones that are above threshold. Because our getRaw functions in both readout-classes also cut
        # the last point, while starting with the start point and using length (Duration) to do so, we can solve
        # the whole problem right here by adding +1 to the starts, while leaving the duration unaffected!!!
                
        # Check once more for consistency
        self.check_peak_number_consistency(starts_out, ends_out, peaks_in_segment)
        self.check_peak_number_consistency(starts_interp, ends_interp, peaks_in_segment)
        self.check_peak_number_consistency(rise_time, rise_time_interp, peaks_in_segment)

        # Construct a DataFrame inhibiting information about raw and interpolated signal starts and stops
        result = pd.DataFrame((np.array(starts_out) + 1), columns = ['Start'])   
        result['Duration'] = np.array(ends_out) - np.array(starts_out)
        if np.any(result['Duration'] <= 0):
            raise(RuntimeError('Negative durations don\'t exist. This is a bug.'))
        
        # For the interpolated data, we do not convert to length but leave the timestamps as they are
        result['Starts_interp'] = np.array(starts_interp)
        result['Ends_interp'] = np.array(ends_interp)

        # Also store the rise times
        result['t_rise'] = np.array(rise_time)
        result['t_rise_interp'] = np.array(rise_time_interp)

        # The Starts now represent the index of the first point above threshold
        # The Durations represent the number of samples the signal is above threshold
        # Calling get_raw(start, stop) using start = Start and stop = Start + Duration returns all points above threshold
        return [peaks_in_segment, result]

    def peak_filter(self, starts, ends, lenght_min, tmin_gap_raw):
        '''
        Filters the detected peaks. For use by :py:meth:RS_Det_File.getTot_fine.
            
        :param starts: list of indices with a potential start of a peak
        :param ends: list of indices with a potential end of a peak. Must match starts
        :param lenght_min: minimum number of samples for a peak to valid
        :param tmin_gap_raw: Minimum gap beteen peeks.
            
        :returns starts_out, ends_out: Filtered lists like starts and ends
        '''

        starts_out = []
        ends_out = []

        # Init for the loop.
        max_i = len(starts)
        i = 0
        while(True):
            length = ends[i] - starts[i]
            # First condition: The distance between the start and end must be sufficient. 
            if length >= lenght_min:
                start = starts[i]
                starts_out.append(start)
                # Now find the end.
                while(True):
                    end = ends[i]
                    # Is the next start within tmin_gap_raw from the current end?
                    # If so, continue searching.
                    # There is one exception to it: We're already at the end.
                    # If we reach the last entry stop.
                    if i + 1 >= max_i:
                        ends_out.append(end)
                        break    
                    
                    if end + tmin_gap_raw > starts[i + 1]:
                        i += 1
                    else:
                        ends_out.append(end)
                        break
            i += 1
            if i >= max_i:
                break
            
        return starts_out, ends_out

    def interp_start_stop(self, starts, ends, data, threshold):
        '''
        Function to determine the linearly interpolated (float) indices of the data for which we would be 
        exactly on some threshold. 
        
        :param starts: start indices of the signals in the event
        :param ends: stop indices of the signals in the event
        :param data: raw data of the event
        :param threshold: used threshold for the pulse search within the event (in raw)
        
        :returns: two lists containing the respective interpolated starts and stops
        '''
        
        starts_inter = []
        ends_inter = []
        for i in range(0, len(starts)):
            # Start interpolation. We still work on the next sample, as +1 is added to start at the end of getTot
            if not starts[i] == 0:                
                y1 = data[starts[i]]
                y2 = data[starts[i] + 1]
                dx = (threshold - y1) / (y2 - y1)
                starts_inter.append(starts[i] + dx)
            else:
                starts_inter.append(float(starts[i]))
                
            # Stop interpolation
            if not ends[i] >= (self.segment_length - 2):
                y1 = data[ends[i]]
                y2 = data[ends[i] + 1]
                dx = (threshold - y1) / (y2 - y1)
                ends_inter.append(ends[i] + dx)
            else:
                ends_inter.append(float(ends[i]))
                
        return starts_inter, ends_inter

    def get_rise_time(self, starts, ends, data, channel, event, offset, low_frac = 0.1, high_frac = 0.9):
        '''
        Extracts the rise time of some data snippet, according to the relative borders of the pulse amplitude.
        Everything isdone on raw data to be faster.
        
        :param starts: array of stop indices
        :param ends: array of stop indices
        :param channel: channel
        :param event: event number
        :param offset: built in offset to move the data to the zero line
        :param begin: relative fraction of the amplitude which counts as start of the rise
        :param end: relative fraction of the amplitude which counts as the end of the rise
        :returns: the value of the rise time in s, 0.0 if not possible to get the rise time
        '''

        # Result variables
        t_rise = []
        t_rise_int = []
                
        # We get the polarity and the calculated offset
        pol = self.polarity[channel][event]
        if self.ana_params['compensate_offset']:
            off_cal = self.artificial_offset[channel][event]
        else:
            off_cal = 0

        # We go through each event characterized
        for start, stop in zip(starts, ends):
            # We get the maximum and the corresponding borders only within the signal range
            max_i = np.argmax(pol * (data[start:stop] + offset - off_cal)) + start
            max = data[max_i]
            dist_low = np.abs(max + offset - off_cal) * high_frac
            dist_high = np.abs(max + offset - off_cal) * low_frac
            
            t_start = None
            t_stop = None
            data_start = 0
            
            # First find the start going left from the maximum but only until the end of the last signal
            for i in range(max_i, (data_start - 1), -1):
                if pol * (max - data[i]) >= dist_low:
                    t_start = i
                    break
            
            if t_start is None:
                self.warn.warn('No start for rise time found...')
                t_rise.append(0.0)
                t_rise_int.append(0.0)
                
            else:
                # Now right from start until we surpass the upper border
                for i in range(t_start, (max_i + 1)):
                    if pol * (max - data[i]) <= dist_high:
                        t_stop = i - 1
                        break
                
                if t_stop is None:
                    self.warn.warn('No stop for rise time found...')
                    t_rise.append(0.0)
                    t_rise_int.append(0.0)
                    
                else:
                    # Store the value
                    t_rise.append((t_stop - t_start) * self.meta['t_sample'])
                    
                    # Now we interpolate both values
                    # Attention: This interpolation is done on the distance from max to the points
                    # As a result, we have y1 - y2 in the denominator since we do (max - y2) - (max - y1)
                    # Start
                    y1 = pol * (max - data[t_start])
                    dx = (dist_low - y1) / (data[t_start] - data[t_start + 1])
                    t_start_int = t_start + dx
                    # Stop
                    y1 = pol * (max - data[t_stop]) 
                    dx = (dist_high - y1) / (data[t_stop] - data[t_stop + 1])
                    t_stop_int = t_stop + dx
                    t_rise_int.append((t_stop_int - t_start_int) * self.meta['t_sample'])

            data_start = stop
        
        return t_rise, t_rise_int

    def get_rms_buffer(self, ch, channel_events):
        '''
        Goes through the found events of a channel and finds the corresponding buffer borders where the signal 
        goes below the corresponding rms-noise. Updates the corresponding class variables
        
        :param ch: channel name
        :param channel_events: structure as given by getTot
        
        :returns: same structure as getTot
        '''
        
        pulse_per_event = channel_events[0]
        starts = np.array(channel_events[1]['Start'])
        lengths = np.array(channel_events[1]['Duration'])
        
        # We need our new events
        new_peaks_in_segment = [int(0) for _ in range(self.segments)]
        new_results = dict.fromkeys(channel_events[1].columns)
        for key in new_results.keys():
            new_results[key] = []
        
        # Go through each valid event
        peak_no = -1
        discarded = 0
        for ev, pulses in enumerate(pulse_per_event):    
            if pulses > 0:
                for p in range(0, pulses):
                    peak_no += 1
                    
                    # Get the rms noise and the zero line in raw units
                    noise = self.noise[ch][ev]
                    zero = self.val_to_raw(0, ch, ev)

                    # Get the data of the whole event (maybe implement smaller ranges that are well chosen)
                    data = self._getRaw(event = ev, source = ch)[ch]
                    
                    valid = True
                    # Now search on the left and the right side for the first sample where the noise is underpassed
                    # Left
                    for i in range(starts[peak_no], -1, -1):
                        diff = np.absolute(data[i] - zero)
                        if diff <= noise:
                            left = starts[peak_no] - i
                            break
                        if i == 0:
                            valid = False
                    # Right
                    if valid:
                        for i in range((starts[peak_no] + lengths[peak_no] - 1), len(data), 1):
                            diff = np.absolute(data[i] - zero)
                            if diff <= noise:
                                right = i - (starts[peak_no] + lengths[peak_no] - 1)
                                break
                            if i == (len(data) - 1):
                                valid = False
                    if valid:
                        new_peaks_in_segment[ev] += 1
                        for key in new_results.keys():
                            new_results[key].append(channel_events[1][key][peak_no])
                        self.buffer_left[ch][ev].append(left)
                        self.buffer_right[ch][ev].append(right)

                    else:
                        discarded += 1
        
        print(f'   --> {ch}: {discarded}/{len(starts)} pulses removed')
        
        new_results = pd.DataFrame(new_results)
        
        if not np.sum(new_peaks_in_segment) > 0:
            return None
        else:
            return [new_peaks_in_segment, new_results]
        
    def get_event_index(self, signals_in_events, signal_index):
        '''
        Helper function, which receives the index of the desired signal and returns the index of the
        corresponding event.
        
        :param channel: Used channel
        :param signals_in_events: List of number of found signals in each event, as given by the first entry of the results given by getTot
        :param signal_index: Index of the signal in the DataFrame of events
        :returns: The index of the event 
        '''
        
        index = signal_index + 1
        cum_signals = np.cumsum(signals_in_events)

        if index > cum_signals[-1]:
            self.warn.warn(f'The given signal index exceeds the total number of found signals according to the peaks per event')
            return None
        
        under = cum_signals < index
        under = np.concatenate(([True], under[:-1]))
        over = cum_signals >= index  

        event_index = int(np.where(under == over)[0][0])
        
        return event_index        
                        
    def skip_peaks_at_borders(self, starts, stops, segment_info, skip, side = 'both', adapt_seg_info = True):
        '''
        Method to skip peaks of the getTot output. Can be done on just the let/right side or both.
        
        :param starts: list of start indices
        :param ends: list of end indices
        :param segment_info: list including the number of found peaks in the corresponding segment/event
        :param skip: number of peaks to skip
        :param side: either left, right or both
        :param adapt_seg_info: if True, the segment info contained in segment_info is adapted accordingly
        
        :returns: adapted lists of starts, ends and segment_info excluding the removed peaks
        '''
        
        if skip == 0:
            return starts, stops, segment_info
        else:
            skip_left = skip
            skip_right = skip
        
        # Check side
        if not side in ['left', 'right', 'both']:
            self.warn.warn(f'Undefined argument for the side to delete peaks from. Is: {side}, Must be \'left\', \'right\' or \'both\'. Switched to \'both\'')
        else:
            # Adapt from the left side
            if side == 'left' or side == 'both':
                starts = starts[skip:]
                stops = stops[skip:]
                if adapt_seg_info:
                    for i in range(0, len(segment_info)):
                        if segment_info[i] >= skip_left:
                            segment_info[i] -= skip_left
                            break
                        else:
                            skip_left -= segment_info[i]
                            segment_info[i] = 0    
            # Adapt from the right side
            if side == 'right' or side == 'both':
                starts = starts[:-skip]
                stops = stops[:-skip]
                if adapt_seg_info:
                    for i in range(0, len(segment_info)):
                        j = - (i + 1)
                        if segment_info[j] >= skip_right:
                            segment_info[j] -= skip_right
                            break
                        else:
                            skip_right -= segment_info[j]
                            segment_info[j] = 0
            
            # Check for consistency
            if self.check_peak_number_consistency(starts, stops, segment_info):
                return starts, stops, segment_info

    def check_peak_number_consistency(self, starts, stops, segment_info):
        '''
        Simple function to check if the infos in start indices, stop indices and the overlying segment info
        are agreeing with each other. Also prints out a corresponding warning
        
        :param starts: List of start indices
        :param stops: List of stop indices
        :param segment info: List for the number of peaks in the corresponding segment
        :returns: True if everything agrees, Fals else
        '''
        
        if not len(starts) == len(stops):
            self.warn.warn(f'Unequal length of starts ({len(starts)}) and stops ({len(stops)})')
            return False
        elif not np.sum(segment_info) == len(starts):
            self.warn.warn(f'Inconsistent peak numbers in segment info and start/stop indices')
            return False
        else:
            return True

    def constant_fraction_discrimination(self, events, source, CFD_factor):
        '''
        Receives the start and length values of one channel and applies constant fraction discrimination (CFD) for 
        each peak according to the set analysis variables in __init__()
        
        :param events: Dataframe with 'Start' and 'Duration' values from one channel analysis
        :param source: Channel name to access the raw data
        :param CFD_factor: Factor for CFD
        
        :returns: A DataFrame of the same format as events, but updated with new values
        '''

        start = events[1]['Start']
        length = events[1]['Duration']
        t_rise = events[1]['t_rise']
        t_rise_int = events[1]['t_rise_interp']
        event_location = events[0]

        # We need to enlargen our region. The maximum we are allowed to go is the right buffer used for peak 
        # finding. But if this is zero, we still need some region, so we choose at least three times the ToT,
        # which is applied later.
        t_sample = self.meta['t_sample']
        buffer = int(self.ana_params['buffer_analysis_right'] / t_sample)
        min_buffer = int(1e-8 / self.meta['t_sample'])
        
        new_starts = []
        new_stops = []
        new_starts_int = []
        new_stops_int = []
        new_t_rise = []
        new_t_rise_int = []
        new_event_loc = event_location.copy()
        
        # We also need the hardware offset 
        off_hard = self.get_built_in_offset()
        off_hard = off_hard[source]
        
        # We go through each peak, get the peak amplitude, and set a new start and stop according to CFD
        no_compromised_peaks = 0
        peak_count = 0

        for event, peaks in enumerate(event_location):
            if peaks > 0:
                for i in range(0, peaks):
                    s = start[peak_count]
                    l = length[peak_count]
                    peak_count += 1
                    
                    # If the buffer region is too small, we enlargen it nevertheless to at least three times the ToT
                    if buffer < 3 * l:
                        used_buffer = 3 * l
                    else:
                        used_buffer = buffer
                        
                    # Or at least 10 ns
                    if used_buffer < min_buffer:
                        used_buffer = min_buffer

                    border_left = s - used_buffer
                    border_right = s + l + used_buffer
                    
                    # Check if we are not out of bounds and adapt if necessary
                    if border_left < 0:
                        border_left = 0
                    if border_right > (self.segment_length + 1):
                        border_right = self.segment_length + 1
                    
                    # Get the voltage data and extract the maximum. We need to consider that the peaks could also be negative
                    data = self._getRaw(event, border_left, border_right, source)[source]

                    # We get the polarity and calculated offset
                    pol = self.polarity[source][event]
                    if self.ana_params['compensate_offset']:
                        off_cal = self.artificial_offset[source][event]
                    else:
                        off_cal = 0
                    
                    # We do not care if the peaks are on the negative or the positive side, so we work with absolutes
                    index_max = np.argmax(pol * (data + off_hard - off_cal))
                    max = data[index_max]
                    dist_min = np.abs(max + off_hard - off_cal) * (1.0 - CFD_factor)

                    # Apply CFD
                    # We need to apply the same procedure for start and stop indices as within the getTot() function. 
                    # This means, our initial start index is the last below threshold and the stop index is the 
                    # last above threshold before entering the interpolation. After that, all start indices are 
                    # incremented by 1, while the duration is calculated as difference between new start and old 
                    # stop indices. This way the getRaw() function delivers all points above threshold as data. 
                    # Go from start to left
                    compromised_peak = False
                    for i in range(index_max, -1, -1):
                        if pol * (max - data[i]) > dist_min:
                            new_starts.append(border_left + i)
                            start_index = i
                            break
                        if i == 0:
                            compromised_peak = True
                    
                    # Now from stop to right, but only if not already failed
                    if not compromised_peak:
                        for i in range(index_max, len(data)):
                            if pol * (max - data[i]) > dist_min:
                                new_stops.append(border_left + i - 1)
                                stop_index = i - 1
                                break
                            if i == (len(data) - 1):
                                compromised_peak = True
                                # Also remove the added start value!!!
                                new_starts = new_starts[:-1]

                    # Keep count of peaks where we could not reach the threshold at all and which were therefore removed
                    if compromised_peak:
                        # We need to take care that the compromised peak is also removed from the events itself
                        new_event_loc[event] -= 1
                        no_compromised_peaks += 1
                    
                    else:
                        # Now we need to interpolate
                        # Attention: This interpolation is done on the distance from max to the points
                        # As a result, we have y1 - y2 in the denominator since we do (max - y2) - (max - y1)
                        # Start
                        y1 = pol * (max - data[start_index])
                        dx = (dist_min - y1) / (data[start_index] - data[start_index + 1])
                        start_int = start_index + dx + border_left
                        # End
                        y1 = pol * (max - data[stop_index])
                        dx = (dist_min - y1) / (data[stop_index] - data[stop_index + 1])
                        end_int = stop_index + dx + border_left

                        new_starts_int.append(start_int)
                        new_stops_int.append(end_int)
                        new_t_rise.append(t_rise[peak_count - 1])
                        new_t_rise_int.append(t_rise_int[peak_count - 1])
            
        print(f'   --> {source}: {no_compromised_peaks}/{len(start)} pulses removed')
            
        if len(new_starts) == 0:
            return None
        # Convert lists to np.arrays and apply a to convert to raw_data indices
        # ATTENTION. The same way as in getTot, we later want to receive the first and last sample ABOVE threshold
        # and the length to represent the number of samples above threshold. To fit with the other getRaw functions, 
        # we apply the same procedure as in getTot
        if not len(new_starts) == len(new_stops):
            raise(RuntimeError('Inconsistent length of new starts and new stops during constant fraction discrimination!!!'))
        new_events = pd.DataFrame(np.array(new_starts) + 1, columns = ['Start'])
        new_events['Duration'] = np.array(new_stops) - np.array(new_starts)
        if np.any(new_events['Duration'] <= 0):
            raise(RuntimeError('Negative durations don\'t exist. This is a bug within CFD application.'))        
        new_events['Starts_interp'] = np.array(new_starts_int)
        new_events['Ends_interp'] = np.array(new_stops_int)
        new_events['t_rise'] = np.array(new_t_rise)
        new_events['t_rise_interp'] = np.array(new_t_rise_int)
        
        return [new_event_loc, new_events]

    def plotEvents(self, events, dir_out, source, det_name = None, no_events = None, buffer = None):
        '''
        Plot a chosen number of signal peaks for visual checking. Can include different analysis features too, 
        to allow for fine tuning of these. This is set in the analysis parameters at the beginning of this class.
        
        :param events: Output from getTot --> just one channel
        :param sources: The oscilloscope Source that should be processed.
        :param dir_out: Directory for plotting.
        :param threshold: For plotting it in
        :param det_name: In order not to just have the channel name in the title, but the detector stated within the .ini file
        :param no_events: Maximum number of events that should be plotted.
        :param buffer: How much data should be plotted before and after the event. Defaults to ns.
        '''
        
        if no_events is None:
            no_events = self.ana_params['no_example_plots']
        if buffer is None:
            buffer = self.ana_params['buffer_plot']
            
        # If the directory does not exist create it
        if not self.preview:
            if not os.path.exists(dir_out):
                os.mkdir(dir_out)
                
        # Make local variable for all relevant data
        starts = events[1]['Start'].values.copy()
        lengths = events[1]['Duration'].values.copy()
        starts_int = events[1]['Starts_interp'].values.copy()
        ends_int = events[1]['Ends_interp'].values.copy()
        t_rise_int = events[1]['t_rise_interp'].values.copy()
        event_location = events[0].copy()
        t_sample = self.meta['t_sample']
        source_names = source
        buffer_raw = int(buffer / t_sample)
        plot_index = self.meta['source_names'].index(source)
        plot_colors = ['green', 'blue', 'orange', 'black']
        plot_color = plot_colors[plot_index]
    
        # If we are in preview mode, we want to limit the plotting to the first events as specified by no_events
        if not self.preview:
            if len(starts) > no_events:
                to_skip = len(starts) - no_events
                starts, lengths, event_location = self.skip_peaks_at_borders(starts, lengths, event_location, to_skip, side = 'right')
                starts_int, ends_int, event_location = self.skip_peaks_at_borders(starts_int, ends_int, event_location, to_skip, side = 'right', adapt_seg_info = False)
                t_rise_int, _, event_location = self.skip_peaks_at_borders(t_rise_int, t_rise_int, event_location, to_skip, side = 'right', adapt_seg_info = False)

                
        # We count peaks also to mark their number in the title
        peak_count = 0
        skip_count = 0
        # Go through each event
        for event, peaks in enumerate(event_location):
            buffer_left = buffer_raw
            buffer_right = buffer_raw
            if peaks > 0:
                if not self.ana_params['plot_analysis_features'] and not self.ana_params['plot_peak_parameters']:
                    threshold = 0.0
                else: 
                    threshold = self.thresholds[source][event]
                
                for i in range(0, peaks):
                    # Get the analysis buffers to skip invalid peaks
                    if not self.ana_params['analysis_till_rms_noise']:
                        ana_buffer_left = int(self.ana_params['buffer_analysis_left'] / t_sample)
                        ana_buffer_right = int(self.ana_params['buffer_analysis_right'] / t_sample)
                    else:
                        ana_buffer_left = self.buffer_left[source][event][i]
                        ana_buffer_right = self.buffer_right[source][event][i]
                    start = starts[peak_count] - buffer_raw
                    stop = starts[peak_count] + lengths[peak_count] + buffer_raw
                    start_int = starts_int[peak_count] * t_sample
                    end_int = ends_int[peak_count] * t_sample
                    peak_count += 1
                    
                    # We check for the right plotting distances and adapt if necessary. As the plotting 
                    # buffer is always at least as big as the analysis buffer, we need to check these
                    # too if we adapt. If tese are surpassed, we must skip the pulse if not simulated
                    valid_peak, changed, start, stop, buffer_left, buffer_right, left, right = self.check_borders(start, stop, buffer_raw, buffer_raw)
                    
                    # If borders have been changed, we check if they exceed the analysis region. If not, the 
                    # pulse is still valid. For simulation data it is always valid
                    if changed:
                        if buffer_left < ana_buffer_left:
                            if valid_peak:
                                ana_buffer_left = buffer_left
                        else:
                            valid_peak = True
                            
                        if buffer_right < ana_buffer_right:
                            if valid_peak:
                                ana_buffer_right = buffer_right
                        else:
                            valid_peak = True

                    # If we would plot outside the given indices, we do not plot
                    if not valid_peak:
                        skip_count += 1
                        # If we eare in preview mode, we return empty axes here
                        if self.preview:
                            ax = self.preview_axes
                            if left:
                                side = 'left'
                            elif right:
                                side = 'right'
                            ax.text(0.0, 0.0, f'Analysis buffer too large on {side} side!\nNo data available!', fontsize = 20,  bbox={'facecolor':'red','alpha':1,'edgecolor':'none','pad':10}, ha='center', va='center') 
                        continue

                    data = self.getAsDf(event, start, stop, source, time = True)
                    time = data['Time']
                    
                    # Change axis to begin at position of the first sample exceeding the threshold
                    time_offset = time[buffer_left]
                    time -= time_offset
                    voltage = data[source_names]
                    
                    # We change the scales to ns and mV
                    time *= 1e9
                    voltage *= 1e3
                    start_int -= time_offset
                    start_int *= 1e9
                    end_int -= time_offset
                    end_int *= 1e9

                    # Plot events
                    plt.close()
            
                    if not self.preview:
                        fig, ax = plt.subplots()
                    else:
                        ax = self.preview_axes
                        ax.cla()
                    
                    # For the legend
                    legend = []
                    plot_signal, = ax.plot(time, voltage, '-', color = plot_color, label = 'signal')
                    legend.append(plot_signal)
            
                    # If desired, include all the analysis features into the peak plots
                    CFD_threshold = self.ana_params['cfd_factor'] * np.max(np.abs(voltage)) * np.sign(threshold)
                    if self.ana_params['plot_analysis_features']:
                        
                        # Plot the threshold and the ToT (if we not deal with pure simulation)
                        if not self.ana_params['pure_simulation_analysis']:
                            plot_threshold, = ax.plot([time[0], time[len(time) - 1]], [threshold * 1e3, threshold* 1e3], '-', color = 'r', label = 'threshold')
                            legend.append(plot_threshold)
                        
                        # If we use constant fraction discrimination, we want to plot the used threshold here
                        if self.ana_params['apply_constant_fraction_discrimination']:
                            display_cfd = '{:.3n}'.format(self.ana_params['cfd_factor'])
                            plot_CFD_threshold, = ax.plot([time[0], time[len(time) - 1]], [CFD_threshold, CFD_threshold], '-', color = 'orange', label = f'CFD: {display_cfd}')
                            legend.append(plot_CFD_threshold)
                        
                        # Plot the ToT    
                        if not self.ana_params['pure_simulation_analysis']:
                            if self.ana_params['apply_constant_fraction_discrimination']:
                                plot_tot, = ax.plot([start_int, end_int], [CFD_threshold, CFD_threshold], '-', color = 'b', label = 'ToT')    
                            else:    
                                plot_tot, = ax.plot([start_int, end_int], [threshold * 1e3, threshold * 1e3], '-', color = 'b', label = 'ToT')    
                            legend.append(plot_tot)
                        
                        # Plot samples above threshold in red
                        plot_samples_above_threshold, = ax.plot(time[(buffer_left):(len(time) - buffer_right)], voltage[(buffer_left):(len(voltage) - buffer_right)], linestyle = 'None', marker = 'o', color = 'red', markersize = 2, label = 'above threshold')                        
                        legend.append(plot_samples_above_threshold)
                        
                        # And start and stop samples in black
                        if not self.ana_params['apply_constant_fraction_discrimination']:
                            plot_samples_start_stop, = ax.plot([start_int, end_int], [threshold * 1e3, threshold * 1e3], linestyle = 'None', marker = 'o', color = 'black', markersize = 7, label = 'start/stop')
                        else:
                            plot_samples_start_stop, = ax.plot([start_int, end_int], [CFD_threshold, CFD_threshold], linestyle = 'None', marker = 'o', color = 'black', markersize = 7, label = 'start/stop')
                        legend.append(plot_samples_start_stop)
                        
                        # As well as a marked region of the area that is included in the peak area calculation based on the given buffer values left and right
                        time_area = list(time[(buffer_left - ana_buffer_left):(len(voltage) - buffer_right + ana_buffer_right)])
                        voltage_area = list(voltage[(buffer_left - ana_buffer_left):(len(voltage) - buffer_right + ana_buffer_right)])
                        base_line = [0] * len(voltage_area)
                        plot_area = ax.fill_between(time_area, voltage_area, base_line, alpha = 0.3, color = plot_color, edgecolor = None, label = 'analyzed_area')
                        legend.append(plot_area)
                        
                        # Also mark the endings of the buffer zone
                        buffer_edge, = ax.plot([time_area[0], time_area[-1]], [voltage_area[0], voltage_area[-1]], linestyle = 'None', marker = '*', color = 'black', markersize = 7, label = 'analysis_borders')
                        legend.append(buffer_edge)
                        
                        # Plot the noise region
                        noise = self.noise[source][event] * self.meta['conversion_factor'][source] * 1e3
                        plot_noise = ax.fill_between(time, noise, -noise, color = 'yellow', edgecolor = None, label = 'rms_noise', alpha = 0.25)
                        legend.append(plot_noise)
                        
                    # Optional peak parameters within the legend
                    if self.ana_params['plot_peak_parameters']:
                        legend_params = []
                        # We need to access the analyse_single_peak() function with the right input
                        # The clause is to avoid empty slice for equal analysis and plot buffer
                        if not buffer_right == ana_buffer_right:
                            peak_params = self.analyse_single_peak(voltage[(buffer_left - ana_buffer_left):-(buffer_right - ana_buffer_right)] * 1e-3, ana_buffer_left, ana_buffer_right, source, event, i, t_rise_int = t_rise_int[peak_count - 1], start_int = starts_int[peak_count-1], stop_int = ends_int[peak_count-1], SI = False)
                        else:
                            peak_params = self.analyse_single_peak(voltage[(buffer_left - ana_buffer_left):] * 1e-3, ana_buffer_left, ana_buffer_right, source, event, i, t_rise_int = t_rise_int[peak_count - 1], start_int = starts_int[peak_count-1], stop_int = ends_int[peak_count-1], SI = False)
                            
                        if not self.ana_params['pure_simulation_analysis']:
                            peak_params['threshold_[mV]'] = threshold * 1e3
                        peak_params['PtP_noise_[mV]'] = self.noise_ptp[source][event] * self.meta['conversion_factor'][source] * 1e3
                        if self.ana_params['compensate_offset']:
                            peak_params['offset_[mV]'] = self.artificial_offset[source][event] * self.meta['conversion_factor'][source] * 1e3
                        
                        # Also display the used CFD-threshold if it is applied
                        if self.ana_params['apply_constant_fraction_discrimination']:
                            peak_params['CFD_threshold_[mV]'] = CFD_threshold
                        
                        # Now, as we cannot simply add text to the legend, we need to make blank plots using the right label
                        for param, value in peak_params.items():
                            val = '{:.3n}'.format(value)
                            plot_param, = ax.plot([], [], ' ', label = f'{param}: {val}')
                            legend_params.append(plot_param)

                    # Construct the legend
                    ax.grid(True)
                    if threshold < 0:
                        legend_loc = 'lower left'
                        param_legend_loc = 'lower right'
                    else:
                        legend_loc = 'upper left'
                        param_legend_loc = 'upper right'
                    if self.ana_params['plot_peak_parameters']:
                        param_legend = ax.legend(loc = param_legend_loc, handles = legend_params, frameon = False, fontsize = 'small')
                    ax.legend(loc = legend_loc, handles = legend)
                    if self.ana_params['plot_peak_parameters']:
                        ax.add_artist(param_legend)
            
                    # Consider the detector name stated within the .ini file
                    # Depending on the mode, we print out the event number too
                    if self.segments == 1:
                        event_info = ''
                    else:
                        event_info = f' event_no: {str(event + 1)},'
                    
                    if det_name is None:
                        ax.set_title(f'{str(source)},{event_info} peak_no: {str(peak_count)}')
                    else: 
                        ax.set_title(f'{str(det_name)},{event_info} peak_no: {str(peak_count)}')
                        
                    ax.set_xlabel('time [ns]')
                    ax.set_ylabel('voltage [mV]')
            
                    # Only store as files if we are not within preview mode
                    if not self.preview:
                        outfile = posixpath.join(dir_out, ('peak' + str(peak_count)+'.png'))
                        plt.savefig(outfile, dpi = 300)

                    plt.close()
                                
        if skip_count > 0:
            self.warn.warn(f'{skip_count} peaks skipped in plotting due to too large analysis buffer')
        
        # Only return something if we are in preview mode (namely, the axes) 
        if self.preview:
            return self.preview_axes
        else:
            return None
    
    def analyse_peaks(self, events, source, buffer_left = None, buffer_right = None):
        '''
        Analyze pulses. Takes an events dataframe as returned by the function getTot()
        and returns a dataframe containing the maximal peak heigthh and the peak areas of each event. Has 
        to be launched for each file and channel.
        The method covers different signs of the voltage, such that returned values are always positive 
        and refer to the absolute values of height and area.
    
        :param events: Starting indices and length in samples of the found events from getTot().
        :param source: The Souce  number on which the operation should be carried out.
        :param cmp: < means too look for maxima in the negative direction, > otherwise.
        :param buffer_left: Extention of analysis to the left side from the start sample.
        :param buffer_right: Extention of analysis to the right side from the stop sample. 
        '''  
        
        t_sample = self.meta['t_sample']
        param_list = []
                    
        # Use buffer from analysis parameters if not given as arguments
        if buffer_left is None:
            buffer_left = self.ana_params['buffer_analysis_left']
        buffer_left_raw = int(buffer_left / t_sample)                

        if buffer_right is None:
            buffer_right = self.ana_params['buffer_analysis_right']
        buffer_right_raw = int(buffer_right / t_sample)
        
        # Failsafe if the buffer is smaller than the sample size. If one is wanted you get at least one sample
        if buffer_left < t_sample and buffer_left > 0:
            buffer_left_raw = 1
        if buffer_right < t_sample and buffer_right > 0:
            buffer_right_raw = 1  

        if len(events[1]) == 0:
            return None

        event_location = events[0].copy()
        starts = events[1]['Start'].values.copy()
        lengths = events[1]['Duration'].values.copy()
        starts_int = events[1]['Starts_interp'].values.copy()
        ends_int = events[1]['Ends_interp'].values.copy()
        t_rise_int = events[1]['t_rise_interp'].values.copy()

        # Go through all events and then peaks
        peak_count = 0
        bad_borders_left = 0
        bad_borders_right = 0
        for event, peaks in enumerate(event_location):
            if peaks > 0:
                for i in range(0, peaks):
                    
                    # We apply the rms buffer if desired
                    if self.ana_params['analysis_till_rms_noise']:
                        buffer_left_raw = self.buffer_left[source][event][i]
                        buffer_right_raw = self.buffer_right[source][event][i]
                    
                    start = starts[peak_count] - buffer_left_raw
                    stop = starts[peak_count] + lengths[peak_count] + buffer_right_raw
                    start_int = starts_int[peak_count]
                    end_int = ends_int[peak_count]
                    peak_count += 1
                    
                    # Check for right borders, if the data is not valid, we discard it
                    valid, changed, start, stop, buffer_left_raw, buffer_right_raw, left, right = self.check_borders(start, stop, buffer_left_raw, buffer_right_raw)
                    if changed:
                        bad_borders_left += int(left)
                        bad_borders_right += int(right)
                        if not valid:
                            continue

                    # Get the data of the peak. We expand one more sample to both sides in order to interpolte 
                    # linearly right to the given threshold.
                    data = self.getAsDf(event, start, stop, source, time = False)
                    voltage = data[source]
                    
                    # Now analyse the peak parameters
                    param_list.append(self.analyse_single_peak(voltage, buffer_left_raw, buffer_right_raw, source, event, i, t_rise_int = t_rise_int[peak_count - 1], start_int = start_int, stop_int = end_int))

        # Let the user know about adapted borders or discarded pulses
        if bad_borders_left > 0 or bad_borders_right > 0:
            if self.osci in self.ini_handler.simu_sources:
                self.warn.warn(f'Analysis borders for some peaks are exceeding event borders and have been adapted')
            else:
                self.warn.warn(f'Analysis borders for some peaks are exceeding event borders and have been discarded')
            self.warn.warn(f'Left border: {bad_borders_left}')
            self.warn.warn(f'Right border: {bad_borders_right}')

        # Create DataFrame
        results = pd.DataFrame.from_dict(param_list)

        return results

    def check_borders(self, start, stop, buffer_left, buffer_right):
        '''
        Checks if pulse data goes out of bounds, regarding start, stop and extended buffers on the sides.
        
        :param start: start sample of the pulse (including buffer)
        :param stop: stop sample of the pulse (including buffer)
        :param buffer_left: extended samples on the left side
        :param buffer_right: extended samples on the right side
        
        :returns: bool if the pulse is valid, bool if parameters have been changed, new values for start, stop buffer_left/right, left and right (booleans) depending on which side has been adapted
        '''
        
        left = False
        right = False
        
        # Check for start and stop out if bounds
        if start < 0 or stop > (self.segment_length - 1):
            new_start = start
            new_stop = stop
            new_buffer_left = buffer_left
            new_buffer_right = buffer_right
            # Left border
            if start < 0:
                new_start = 0
                new_buffer_left = buffer_left + start
                left = True
            # Right border
            if stop > (self.segment_length - 1):
                new_stop = self.segment_length - 1
                new_buffer_right = buffer_right - (stop - (self.segment_length - 1))
                right = True
            # If borders are adapted, the pulse is still valid if we deal with simulated data
            if self.osci in self.ini_handler.simu_sources:
                valid_pulse = True
            else:
                valid_pulse = False
            
            return valid_pulse, True, new_start, new_stop, new_buffer_left, new_buffer_right, left, right
            
        else:
            return True, False, start, stop, buffer_left, buffer_right, left, right
        
    def analyse_single_peak(self, voltage, buffer_left, buffer_right, channel, event, pulse_no, t_rise_int = None, start_int = None, stop_int = None, SI = True):
        '''
        Outsourced method analysing a single peak according to given parameters. Interpolated start and stop 
        values do not contribute to the calculation of any other result-parameter than the ToT. If given, the
        right position itself is not necessary, as they are always assumed to be right before/after the original 
        start/stop event. Therefore only the decimals are important
        
        :param voltage: list of voltage axis (must be non-raw and in SI units)
        :param time: list of time axis
        :param buffer_left: left region to include area calculation, in indices
        :param buffer_right: right region to include area calculation, in indices
        :param channel: the channel from which the event is --> Neede to get the right noise level for SNR
        :param event: number of the event (first is 0)
        :param peak_no: number of the pulse in the event
        :param t_rise_int: result for the interpolated rise time already calculated in getToT
        :param start_int: interpolated time-index of the start of the signal
        :param stop_int: interpolated time-inde of the stop of the signal
        :param SI: if False, x data is assumed in ns and y in mV, which will be rescaled after calculation
        
        :returns: Five peak values: Amplitude, ToT, area, rms and SNR
        '''
        
        result = {}

        # amplitude
        result['peak_maximum_[V]'] = max(np.abs(voltage))
        
        # area
        # The simple trapezoidal method is used currently. However, one can change this to simpson() or even higher order integrations.
        result['peak_area_[Vs]'] = abs(integrate.trapz(y = voltage, dx = self.meta['t_sample']))
        
        # area just below threshold
        if buffer_left == 0 and buffer_right == 0:
            result['peak_area_below_threshold_[Vs]'] = result['peak_area_[Vs]']
        else:
            result['peak_area_below_threshold_[Vs]'] = abs(integrate.trapz(y = voltage[buffer_left:(len(voltage) - buffer_right)], dx = self.meta['t_sample']))
        
        # ToT
        sot = (len(voltage) - buffer_left - buffer_right)
        if not start_int is None:
            add = (1 - (start_int - int(start_int)))
            sot += add
        if not stop_int is None:
            add = (stop_int - int(stop_int))
            sot += add
        result['time_over_threshold_[s]'] = sot * self.meta['t_sample']
        
        # Time over noise
        if not self.ana_params['analysis_till_rms_noise']:
            result['time_over_noise_[s]'] = sot * self.meta['t_sample']
        else:
            result['time_over_noise_[s]'] = (sot + self.buffer_left[channel][event][pulse_no] + self.buffer_right[channel][event][pulse_no]) * self.meta['t_sample']
                    
        # rise time (this one's a bit more tricky)
        if not t_rise_int is None:
            result['rise_time_[s]'] = t_rise_int
        else: 
            result['rise_time_[s]'] = 0.0
                    
        # rms value
        rms_data = np.array(voltage[buffer_left:(len(voltage) - buffer_right)])
        np.square(rms_data, out = rms_data)
        result['rms_[V]'] = np.sqrt(np.mean(rms_data))
        
        # rms noise
        noise = self.noise[channel][event] * self.meta['conversion_factor'][channel]
        result['rms_noise_[V]'] = noise
        
        # SNR
        if not noise == 0.0:
            result['SNR'] = 20 * np.log10(result['rms_[V]'] / noise) 
        else: 
            result['SNR'] = 0.0
        
        # hardware offset
        if self.artificial_offset is None:
            result['hardware_offset_[V]'] = 0
        else:
            result['hardware_offset_[V]'] = self.artificial_offset[channel][event] * self.meta['conversion_factor'][channel]
        
        # aspect ratio
        result['aspect_ratio_[V_per_s]'] = result['peak_maximum_[V]'] / result['time_over_threshold_[s]']

        # If the single file ramp analysis is activated, we use the result data in here to add the information into the corresponding variable
        if not self.SINGLE_FILE_RAMP is None:
            for param in result.keys():
                self.ramp_info[channel][param][self.meta[self.SINGLE_FILE_RAMP][event]].append(result[param])

        # Convert the results if we do not want to display in SI units        
        if not SI:
            SI_result = {}
            for param, val in result.items():
                SI_result[self.ini_handler.pulse_params_converted[param]] = val * self.ini_handler.pulse_params_conversion_factors[param]
            return SI_result
        else:
            return result

    def execute_preview(self):
        '''
        Performs a preview analysis and returns the plots of a number of requested peaks given when initializing Det_File()
        '''

        # Create our object to return. 
        events = {}
        
        # First, apply offset compensation and noise calculation if used
        if self.ana_params['advanced_noise_offset']:
            print('Performing advanced offset and noise calculation...')
            self.advanced_noise_offset()
        else:
            if self.ana_params['compensate_offset']:
                print('Compensate for artificial offset...')
                self.compensate_offset()
            print('Noise:')
            self.getStatistics()
            
        # Get threshold
        self.get_threshold()

        # Analyse each channel
        print('Get time over threshold...')
        for ch in self.meta['source_names']:
            
            # Get time over threshold (the reduction to the preview events is done within getTot)
            channel_events = self.getTot(ch)

            if not channel_events is None:
                # Apply CFD
                if self.ana_params['apply_constant_fraction_discrimination']:
                    CFD_factor = self.ana_params['cfd_factor']
                    print(f'Apply constant fraction discrimination using CFD_factor: {CFD_factor}...')
                    channel_events = self.constant_fraction_discrimination(channel_events, ch, CFD_factor)

                if not channel_events is None:
                    # Get rms buffer
                    if self.ana_params['analysis_till_rms_noise']:
                        print('Find rms-analysis-buffer...')
                        channel_events = self.get_rms_buffer(ch, channel_events)

                    events[ch] = channel_events
                
                else:
                    events[ch] = None
                
            else:
                events[ch] = None

            print()
            
        return events
   
    def val_to_raw(self, val, channel, segment_no):
        '''
        Converts a single value to the corresponding raw value to use it for raw-data. Attention, 
        we do not return integer values, as this can lead to great uncertainties. This must be done
        externally if desired.
        
        :param val: The value to convert
        :param channel: The channel for which the value will be used
        :param segment_no: Number of the corresponding event to apply the right artificial offset
        :returns: The converted value
        '''
        
        if not self.ana_params['compensate_offset'] and not self.meta['apply_offset']:
            return val * self.unity[segment_no]
        else:
            val2 = val * self.unity[segment_no]
                
        if self.meta['apply_offset']:
            val2 = val - self.meta['offset'][channel]
            val2 /= self.meta['conversion_factor'][channel] * self.unity[segment_no]
            
        if self.ana_params['compensate_offset']:
            val2 += self.artificial_offset[channel][segment_no]
            
        return val2
        
    def val_to_voltage(self, val, channel, segment_no):
        '''
        Converts a single raw value to the corresponding value to use it for converted data
        
        :param val: The value to convert
        :param channel: The channel for which the value will be used
        :param segment_no: The evet number to apply the right artificial offset
        :returns: The converted value
        '''
          
        if not self.ana_params['compensate_offset'] and not self.meta['apply_offset']:
            return val * self.unity[segment_no]
        else:
            val2 = val * self.unity[segment_no]
                
        if self.ana_params['compensate_offset']:
            val2 = val - self.artificial_offset[channel][segment_no]

        if self.meta['apply_offset']:
            val2 *= self.meta['conversion_factor'][channel]
            val2 += self.meta['offset'][channel] * self.unity[segment_no]
                    
        return val2