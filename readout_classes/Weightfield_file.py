'''
    File name: Weightfield_file.py
    Author: Andreas Gsponer
    Date created: 2022
'''

import numpy as np
from .Simulation_file import Simulation_file


class Weightfield_file(Simulation_file):
    '''
    Class to read in the output of a Weightfield2 Simulation.
    This class inherits from the Simulation_file class and only provides
    specific methods to read in WeightField2 .txt files

    '''

    def __init__(self, filename):
        """ Initialize using super class constructor
            Pass data read in method (read_from_wf_txt_file)

        Args:
            :param filename: Filename to be read in
        """        
        Simulation_file.__init__(self, filename, self.read_from_wf_txt_file)

    def read_from_wf_txt_file(self, filename):
        ''' Read the CSA signal from a WeightField2 file and set meta data.
            The data is returned as a dict just containg the CSA signal
            
            Because we use the direct output of WF2, each event/signal will be stored
            in an individual file. Therefore, the number of events is fixed as 1.
            Nevertheless, we need to match the same numpy array dimensions as data with
            multiple events.

        Args:
            :param filename: Path to file containing Weightfield data (.txt)

        Returns:
            :param data_as_dict: Dict containing CSA samples in V
        '''
        # Load WeightField2 text file
        # Ignore the first 5 header lines
        data = np.genfromtxt(self.filename, skip_header=5)
        assert np.shape(data)[1] == 9, 'The file ' + self.filename + ' is not a valid WeightField2 file'
        
        # The data columns are:
        # 0: Time [ns]
        # 1: Total current [uA]
        # 2: Electron Current [uA]
        # 3: Hole Current [uA]
        # 4: Gain Electron Current [uA]
        # 5: Gain Hole Current [uA]
        # 6: Broadband Amplifier (BB) Signal [mV]
        # 7: Charge Sensitive Amplifier (CSA) Signal [mV]
        # 8: Collected Charge [fC]]

        # We are only interested in column 0 (time), to calculate the time step
        # and column 7, the CSA/TI signal

        # Calculate time between samples from time axis (using np.diff)
        # Weightfield samples are equi-distributed
        self.meta['t_sample'] = np.diff(data[:, 0] * 1e-9)[0]

        # Update total length of data (number of events * samples per event)
        self.meta['length'] = 1 * np.shape(data)[0]
        # Update total number of events (particles)
        self.meta['NumberOfAcquisitions'] = 1


        data_as_dict = {}
        # Add CSA signal to dict, convert to SI units (mV -> V)
        CSA_signal = data[:, 7] * 1e-3
        # Add zero padding before signal, so that analysis code can
        # plot signal *before* and after peak 
        CSA_with_padding = np.pad(CSA_signal, (len(CSA_signal)//2, 0), 'constant')
        # Add dimension to data (list of events with length 1)
        data_as_dict['CSA'] = np.asarray([CSA_with_padding]) 
        
        return data_as_dict
    
    # Property required by fileReader.py
    @property
    def data_raw_Weightfield(self):
        if self._data is None:
            self._getRaw()

        return self._data