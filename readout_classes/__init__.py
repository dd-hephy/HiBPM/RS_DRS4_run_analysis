from readout_classes.Allpix_file import Allpix_file
from readout_classes.DRS4_file import DRS4_file
from readout_classes.RS_file import RS_file
from readout_classes.Weightfield_file import Weightfield_file
from readout_classes.CNM_file import CNM_file