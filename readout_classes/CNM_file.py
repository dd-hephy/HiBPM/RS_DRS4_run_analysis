'''
    File name: CNM_file.py
    Author: Philipp Gaggl
    Date created: 03.10.2022
    Python Version: 3.8, 3.9
'''

import numpy as np
import pandas as pd
import os

from collections import Counter

from tool_box import Warn_Me


class CNM_file():

    def __init__(self, file):
        """
        Class to read the .csv file output from TCT and Alpha measurements at CNM. We deal with different types of files, corresponding
        to the source, so there are switches in between to alllow for all of them
        
        :param file: Path to the .csv file that should be evaluated
        :returns: None
        """  
        
        # Initiate helpers
        self.warn = Warn_Me()
        
        # Some class variables
        self._data = None
        self.t_sample = None
        self.ch_name = 'CNM'
        self.source = None
        self.tct_info = None
        
        # Check for validity of the file
        self.file = file
        self.check_CNM_file()
        
        # Extract the given timesptamp and data and find out the source
        try:
            self.get_data_and_timestep_alpha()
            self.source = 'alpha'
        except:
            self.get_data_and_timestep_TCT()
            self.source = 'TCT'
        print(f'The found source of the CNM-file is \'{self.source}\'')

        # Now we just need the corresponding meta variable to be used by the fileReader's DetFile class
        self.create_meta()
        
        return None
        
    def check_CNM_file(self):
        '''
        Checks if the file exists and warns if not 
        
        :returns: None
        '''

        # Check for right format
        _, extension = os.path.splitext(self.file)
        if not extension == '.csv':
            self.warn.warn('The given CNM file MUST be a .csv file')
            raise(RuntimeError())
        
        if not os.path.isfile(self.file):
            self.warn.warn('The given file does not exist')
            raise(RuntimeError())
        
        return None
    
    def get_data_and_timestep_alpha(self):
        '''
        Assumes a file format as used for alpha and Xray measurements. Reads in the first 100 lines of the .csv file.Checks 
        the first 100 samples and extracts a constant timestamp and extracts the data
        
        :returns: None
        '''

        test_data = pd.read_csv(self.file, sep = ',', nrows = 100, header = None)
        test_data = test_data[0]

        # Get the differences. If one of them dominates more than 50% of the values, we take it
        t_diff = [test_data[i] - test_data[i - 1] for i in range(1, len(test_data))]
        t_samples = Counter(t_diff)
        for time, num in t_samples.items():
            if num / len(test_data) >= 0.5:
                self.t_sample = time
                break
            
        if self.t_sample is None:
            self.warn.warn('We have too much differing timestamps within the given data file!!!')
            raise(RuntimeError())
        
        # Now get the whole data. We should only deal with a single event per file, so we can just load it
        self.data = pd.read_csv(self.file, sep = ',', header = None)
        self.data = [np.array(self.data[1])]
                
        return None

    def get_data_and_timestep_TCT(self):
        '''
        Assumes a file format as used for TCT measurements, which includes much more informatioon on the stage position, voltage etc. Also can include multiple 
        waveforms.
        
        :returns: None
        '''

        first_no_commented = True
        self.data = []
        self.tct_info = dict.fromkeys(['posX', 'posY', 'posZ', 'voltage', 'current', 'temperature', 'event_length', 'start_times'])
        with open(self.file, 'r') as f:
            EOF = False
            posX, posY, posZ, volt, curr = [], [], [], [], []
            Npoints, xorigin, xincrement = [], [], []
            lsrtemp = []
            scan_name = None
            while not EOF:
                line = f.readline()
                if not line:
                    EOF = True
                    break
                if line[0] == '\n':
                    pass
                elif line[0] == '#':
                    if 'ScanName' in line:
                        scan_name = line.strip('#')
                        scan_name = scan_name.strip(' ScanName:\t')
                elif first_no_commented == True and len(line.split(',')) == 1:
                    first_no_commented == False
                    self.Version = int(line)
                elif len(line.split(',')) == 5: 
                    [[posX, posY, posZ, volt, curr][i].append(float(val)) for i, val in enumerate(line.split(','))]               
                elif len(line.split(',')) == 6:     
                    [[posX, posY, posZ, volt, curr, lsrtemp][i].append(float(val)) for i, val in enumerate(line.split(','))]                              
                elif len(line.split(',')) == 3:  
                    [[Npoints, xorigin, xincrement][i].append(val) for i, val in enumerate([int(line.split(',')[0]),float(line.split(',')[1]),float(line.split(',')[2])])]
                elif len(line.split(',')) > 10:
                    points = [float(i) for i in line.split(',')]
                    self.data.append(np.array(points))
                
        # Get the timestep
        if not np.all(np.array(xincrement) == xincrement[0]):
            self.warn.warn('Not all timesteps given in this file are equal...')
            raise(RuntimeError('Non equal timesteps within single file'))
        else:
            self.t_sample = xincrement[0]
            
        # Also check if the number of points stay the same
        if not np.all(np.array(Npoints) == Npoints[0]):
            self.warn.warn('Not all event lengths given in this file are equal...')
            raise(RuntimeError('Non equal event lengths within single file'))
        
        # Add the obtained other info to class variable for additional meta information
        self.tct_info = dict.fromkeys(['posX', 'posY', 'posZ', 'voltage', 'current', 'temperature', 'event_length', 'start_times', 'sample_times'])

        self.tct_info['posX'] = posX
        self.tct_info['posY'] = posY
        self.tct_info['posZ'] = posZ
        self.tct_info['voltage'] = volt
        self.tct_info['current'] = curr
        self.tct_info['temperature'] = lsrtemp
        self.tct_info['event_length'] = Npoints
        self.tct_info['start_times'] = xorigin
                
        return None

    def create_meta(self):
        '''
        Creates a self.meta variable corresponding to the one used by the DetFile class within the fileReader. 
        This is rather simple, since we deal with a single channel, single event and no data conversion
        
        :returns: None
        '''
        
        self.meta = {}        
        self.meta['source_names'] = [self.ch_name]
        self.meta['apply_offset'] = False
        self.meta['t_sample'] = self.t_sample
        self.meta['length'] = len(self.data[0]) * len(self.data)
        self.meta['channel_type'] = ['voltage']
        self.meta['NumberOfAcquisitions'] = len(self.data)
        self.meta['conversion_factor'] = {self.ch_name:1.0}
        self.meta['offset'] = {self.ch_name:0.0}
        
        # If we have TCT data, we can also store the other information given in there
        if not self.tct_info is None:
            for key, info in self.tct_info.items():
                self.meta[key] = info
        
        return None
        
    @property 
    def data_raw_CNM(self):
        '''
        Data property as used by fileReader
        '''
        
        if self._data is None:
            self._data = {self.ch_name:[event for event in self.data]}

        return self._data
        
    def _getRaw(self, event = 0, start = None, stop = None, source = None):
        '''
        getRaw function necessary to be compatible with the fileReader analysis. Returns a dict 
        of channels and corresponding data.
        
        :param start: Starting index
        :param stop: Stopping index
        :param source: Channel name
        '''

        if not source is None and not type(source) is list:
            source = [source]
        
        if source is None:
            my_data = {k:v[event] for (k,v) in self.data_raw.items() if k in self.meta['source_names']}
        else:
            my_data = {k:v[event] for (k,v) in self.data_raw.items() if k in source}
            
        # Things get easier if we can rely on having proper numbers as start and stop 
        if start is None:
            start = 0
        if stop is None:
            stop = int(self.meta['length'] / self.meta['NumberOfAcquisitions'])
            
        # Now apply the start stop filter
        for key in my_data.keys():
            my_data[key] = my_data[key][start:stop]
                
        return my_data   
        

        

        
    
    