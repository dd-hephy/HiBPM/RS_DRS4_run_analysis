'''
    File name: DRS4_file.py
    Author: Philipp Gaggl
    Date created: 02.05.2022
    Python Version: 3.8, 3.9
'''

import numpy as np
import os
import struct

from scipy.interpolate import interp1d

from io import FileIO
from datetime import datetime
from tool_box import Warn_Me


class DRS4_file():
    
    # The DRS4 osci always has 1024 samples per event (SPE)
    SPE = 1024
    
    def __init__(self, file, GROUNDLEVEL_TO_ZERO = True):
        """
        Class to read the binary Ouput File of the DRS4 evaluation software
        
        :param file: Path to the .dat file that should be evaluated
        :param GROUNDLEVEL_TO_ZERO: If True (which should be to get rid of artifacts), we
                                    perform an offset compensation for each event
        :returns: None
        """  
        
        # Initiate helpers
        self.warn = Warn_Me()
        
        # Check if file is ok
        self.check_file(file)

        # Initiate used variables
        self._data = None
        self.compensate_for_offset = GROUNDLEVEL_TO_ZERO
        self.meta = {}
        self.RO_info = {}
        # We always deal with a fixed voltage range of 1V, stretched over 65536 samples
        self.scale = 1 / 65535

        # Now read out the data
        self.file = file
        self.reader = FileIO(self.file, 'rb')
        if not self.reader.read(4) == b'DRS2':
            self.warn.warn('File does not seem to be a binary DRS4 file')
            raise(RuntimeError())
        if not self.reader.read(4) == b'TIME':
            self.warn.warn('File does not contain timestamps data')
            raise(RuntimeError())

        # Get some information (board id, channels)
        self.get_info_and_timestamps()

        # Get the event_data
        self.get_events_raw()

        # Create the meta file 
        self.generate_meta_and_info()

        return None
    
    def check_file(self, file):
        '''
        Checks if the file exists and warns if not 
        
        :param file: File path
        '''
        
        # Check for right format
        _, extension = os.path.splitext(file)
        if not extension == '.dat':
            self.warn.warn('The given file MUST be a .dat file')
            raise(RuntimeError())
        
        if not os.path.isfile(file):
            self.warn.warn('The given file does not exist')
            raise(RuntimeError())
        
        return None

    def get_info_and_timestamps(self):
        '''
        Reads the header of the data file. Stores the information about the board ID and the channels
        
        :returns: None
        '''
        
        board_id = None
        channels = []
        timestamps = {}
        
        header = self.reader.read(4)
        
        if header.startswith(b'B#'):
            # Get board ID
            board_id, = struct.unpack('H', header[2:])
            
            # Get channels
            header = self.reader.read(4)
            while(header.startswith(b'C')):
                channel = int(header[1:].decode())
                channels.append(channel)
                
                # Get timestamps. Attention. For .dat files, only one array of time widths for one 
                # channel is given. These come from a timing calibration over each point in the DRS4
                # buffer and are assumed to remain for each event. Comparison with raw data from .csv
                # files actually indicates that this is the case, which is why we trust them for now. 
                # However, a time calibration should be performed every time the setup itself is changed.
                # Also, we convert to SI units already here
                timestamps[channel] = np.frombuffer(self.reader.read(self.SPE * 4), 'float32') * 1e-9
                
                header = self.reader.read(4)
                
            # Go back to location jumped wrongly at last while loop
            self.reader.seek(-4, 1)
                
            # Store the variables
            self.board_id = board_id
            self.channels = channels
            self.time_widths = timestamps

        else:
            self.warn.warn('Could not get board ID')
        
        
        # We need to store the given time stamp
        for ch, widths in self.time_widths.items():
            self.event_length = np.sum(widths)
            self.t_sample = self.event_length / self.SPE

        # If we interpolate the events to equidistant times, we introduce the x axis once
        self.equi_time = np.linspace(self.t_sample, self.event_length, self.SPE)
        
        return None
    
    def get_events_raw(self):
        '''
        Extract the event data. We get all the data, even if it may not be important, for later 
        purposes. Also, data is given raw, conversion comes later.
        
        :returns: None
        '''
    
        num_events = 0
            
        event_data = dict.fromkeys(['ID', 'time', 'range_center', 'trigger_cells', 'scaler', 'data'])
        for key in event_data.keys():
            if not key == 'data' and not key == 'scaler':
                event_data[key] = []
            else: 
                event_data[key] = dict.fromkeys(self.channels)
                for ch in event_data[key].keys():
                    event_data[key][ch] = []

        # Try to access first event
        try:
            header = self.reader.read(4)
        except:
            self.warn.warn('Could not read further for event data')
            return None
        
        if not header == b'EHDR':
            self.warn.warn('No EHDR in first event header')
            return None
        
        # Now read all events and store each of their information
        else:
            while(header == b'EHDR'):
                
                num_events += 1
                # Store the event data
                # Event ID
                event_data['ID'].append(struct.unpack('I', self.reader.read(4))[0])  
                # Event time of recording        
                year, month, day, hour, minute, second, ms = struct.unpack('7H', self.reader.read(struct.calcsize('7H')))
                event_data['time'].append(datetime(year, month, day, hour, minute, second, ms * 1000))
                # Range center (don't know if in V or mV right now)
                event_data['range_center'].append(float(struct.unpack('H', self.reader.read(2))[0]))

                # Check consistent board ID
                if not self.reader.read(2) == b'B#':
                    self.warn.warn(f'No B# preceeding board-ID in channel data')
                    return None
                if not struct.unpack('H', self.reader.read(2))[0] == self.board_id:
                    self.warn.warn(f'Board-ID in channel data not consistent with the one in the header')
                    return None
                
                # Trigger cell
                if not self.reader.read(2) == b'T#':
                    self.warn.warn('No T# preceeding trigger cell data in channel data')
                    return None
                else:
                    event_data['trigger_cells'].append(struct.unpack('H', self.reader.read(2))[0])
                # Now go through each event
                for ch in self.channels:
                    
                    # Check for right order in channels
                    if not self.reader.read(4) == 'C{:03d}'.format(ch).encode('ascii'):
                        self.warn.warn(f'No consistent data storage for channel \'{ch}\' at event no. {num_events}')
                        return None
                    
                    # Scaler
                    # We do not really know what the scaler does. But it has always been zero and
                    # results are fine. Therefore we throw an error if it is not zero one time to 
                    # be aware to contribute for it
                    event_data['scaler'][ch].append(float(struct.unpack('I', self.reader.read(4))[0]))

                    # Finally, actual raw data
                    event_data['data'][ch].append(np.frombuffer(self.reader.read(self.SPE * 2), 'uint16'))
            
                # Go to next event and count back the event number for 1
                header = self.reader.read(4)
            
            # As the voltage range for one file will always be constant, we take the first occuring
            # one to get the minimum, for faster conversion later. However, we also check if it really
            # is the same for all events
            if not len(event_data['range_center']) == 0:
                RC = event_data['range_center'][0]
                if not all(center == RC for center in event_data['range_center']):
                    self.warn.warn('The range center is not consistent, this case is not implemented')
                    raise(RuntimeError('Inconsistent range centers'))
                self.min_volt = RC - 0.5
            
            # Store obtained raw data
            self.num_events = num_events
            self.event_data = event_data
            
        return None

    @property 
    def data_raw_DRS4(self):
        '''
        Data property. It includes the converted data, but will be changed to raw data. Also, the 
        time axis is given.
        '''
        
        if self._data is None:
            # Interpolate and convert the event data
            for ch in self.channels:
                for i in range(0, self.num_events):
                    # Already interpolate to equidistant x on the int for speed
                    self.event_data['data'][ch][i] = self.interpolate_data(self.event_data['data'][ch][i], ch, i)
                    # If one wants to convert from raw
                    # self.event_data['data'][ch][i] = self.convert_raw_data(self.event_data['data'][ch][i])
            
            self._data = self.event_data['data']

        return self._data
    
    def interpolate_data(self, data, channel, event):
        '''
        Interpolates (already converted data) in order to obtain equidistant timesteps. Returns
        an array (of same length) with the interpolated data. Only works if a full event is given.
        
        :param data: y data of a channel
        :param channel: channel
        :param event: event number
        :returns: interpolated y data
        '''

        # We add a value at time zero into the data to interpolate with the same value as the starting value.
        # This way, we are safe if the osci time hops around too much
        
        # We need to consider the trigger cell, meaning the cell index which marks the begin of the event
        tc = self.event_data['trigger_cells'][event]
        time_cum = np.cumsum(np.roll(self.time_widths[channel], -(tc -1)))
        time_cum -= time_cum[0]
        inter = interp1d(time_cum, data, kind = 'linear', bounds_error = False, fill_value = 'extrapolate')
        data_int = inter(self.equi_time)

        return data_int
        
    def convert_raw_data(self, raw_data):
        '''
        Receives a numpy array of raw data and converts them accordingly. Also applies offset 
        compensation if desired.
        
        :param raw_data: numpy array with raw data
        :returns: numpy array of converted data
        '''
        
        # Convert using the scaling and offset variable
        data = self.min_volt + self.scale * raw_data
        # Apply an offset compensation if it is activated
        if self.compensate_for_offset:
            # We can only compensate an offset if the range allows for negative voltages too
            if not self.min_volt >= 0:
                # We calculate avg and mean. To filter out peak data contributing to this calculation, we 
                # calculate a new average excluding all data outside one std
                mean = np.mean(data, dtype = np.float64)
                std = np.std(data, dtype = np.float64)
                groundlevel = np.mean(data[np.where(np.logical_and(data > (mean - std), data < (mean + std)))])
                data = data - groundlevel

        return data
                
    def generate_meta_and_info(self):
        '''
        Initializes the content of the meta class variable, which is an artifact for the 
        analysis software to be compatible with the RS readout class.
        
        :returns: None
        '''
        
        # Meta dict
        self.meta['source_names'] = self.channels
        # If we ever want to skip converting in here and work on raw data within the fileReader, 
        # this variable needs to be True (of course together with some other changes)
        self.meta['apply_offset'] = True
        self.meta['t_sample'] = self.t_sample
        self.meta['length'] = self.num_events * self.SPE
        self.meta['channel_type'] = ['voltage'] * len(self.channels)
        self.meta['NumberOfAcquisitions'] = self.num_events
        self.meta['conversion_factor'] = dict.fromkeys(self.channels)
        for ch in self.channels:
            self.meta['conversion_factor'][ch] = self.scale
        self.meta['offset'] = dict.fromkeys(self.channels)
        for ch in self.channels:
            self.meta['offset'][ch] = self.min_volt
        
        # Additional 
        self.RO_info['board_ID'] = self.board_id
        self.RO_info['time_of_recording'] = str(self.event_data['time'][0])
        self.RO_info['voltage_range'] = '['+ str(self.min_volt) + ', ' + str(self.min_volt + 1) + '] V'
        scaler = dict.fromkeys(self.channels)
        for ch in self.channels:
            scaler[ch] = self.event_data['scaler'][ch][0]
        self.RO_info['scaler'] = str(scaler)
        
        return None
    
    def _getRaw(self, event = 0, start = None, stop = None, source = None):
        '''
        getRaw function necessary to be compatible with the fileReader analysis. Returns a dict 
        of channels and corresponding data.
        
        :param start: Starting index
        :param stop: Stopping index
        :param source: Channel name
        '''

        if not source is None and not type(source) is list:
            source = [source]
        
        if source is None:
            my_data = {k:v[event] for (k,v) in self.data_raw.items() if k in self.meta['source_names']}
        else:
            my_data = {k:v[event] for (k,v) in self.data_raw.items() if k in source}
            
        # Things get easier if we can rely on having proper numbers as start and stop 
        if start is None:
            start = 0
        if stop is None:
            stop = int(self.meta['length'] / self.meta['NumberOfAcquisitions'])
            
        # Now apply the start stop filter
        for key in my_data.keys():
            my_data[key] = my_data[key][start:stop]
                
        return my_data
