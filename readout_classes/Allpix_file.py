'''
    File name: Allpix_file.py
    Author: Andreas Gsponer
    Date created: 2022
'''

import numpy as np
from .Simulation_file import Simulation_file
from os.path import splitext

class Allpix_file(Simulation_file):
    '''
    Class to read in the output of a Allpix^2 Simulation.
    This class inherits from the Simulation_file class and only provides
    specific methods to read in preprocessed .csv or .npz files.

    '''

    def __init__(self, filename):
        """ Initialize using super class constructor
            Pass data read in method (read_data_from_file)

        Args:
            :param filename: Filename to be read in
        """  
        Simulation_file.__init__(self, filename, self.read_data_from_file)

    def iter_loadcsv(self, filename, delimiter=' ', skiprows=2):
        """ 
        Helper-function to read pre-processed CSV files containing AllPix2 simulation output.
        An interator and np.fromiter are used in order to reduce the memory usage

        Args:
            filename (str): Path to file to read
            delimiter (str, optional): CSV delimiter. Defaults to space (' ').
            skiprows (int, optional): Number of rows to skip. Defaults to 2 (time bin header lines)

        Returns:
            np.array: Array with CSA signals, shape (n_events, n_time_bins)

        """
        # Read first line after headers to get length of row (i.e. number of bins in time)
        # Here we assume that all signals have the same length, which should hold for
        # individual AllPix Simulation files
        with open(filename, 'r') as file:
            for _ in range(skiprows):
                    next(file)
                
            line = next(file)
            line = line.rstrip().split(delimiter)
            rowlength = len(line)

        # iterator for np to read data
        def iter_func():
            with open(filename, 'r') as infile:
                for _ in range(skiprows):
                    next(infile)
                for line in infile:
                    line = line.rstrip().split(delimiter)
                    for item in line:
                        yield float(item)

        data = np.fromiter(iter_func(), dtype=float)
        # reshape to correct shape (n_events, n_time_bins)
        data = data.reshape((-1, rowlength))
        
        return data
    
    def read_data_from_file(self, filename):
        """ Reads Allpix Simulation data from a preprocessed .csv file
            The CSA/amplifier signal for each event is read in, the time axis /
            sample interval is read from the file header

        Args:
            :param filename: Path to file containing preprocessed Allpix^2 data (.csv or .npz)
        """
        # Check extension of filename to determine which reader we are going to use
        _, ext = splitext(filename)
        if ext == '.csv':
            # Load CSV data via lazy iterator
            signal_per_event = self.iter_loadcsv(filename)

            # Read header line (first line) and obtain time bin size
            with open(filename, 'r') as f:
                csa_time_bin_header = f.readline().strip()

                csa_time_bins = float(csa_time_bin_header.split('=')[1])

        elif ext == '.npz':
            # Read binary data directly from .npz file
            dat = np.load(filename)
            # The file contains two arrays:
            # The first is just an array of length 1 with the CSA timestamp in ns
            # The second array contains the signal [V] for each timebin
            csa_time_bins = dat['arr_0']
            signal_per_event = dat['arr_1']

        # Convert signal from kV (AllPix internal units) to mV
        signal_per_event = signal_per_event * 1e6


        # If there is only a single event, add a dimension to the array
        if len(np.shape(signal_per_event)) == 1:
            signal_per_event = np.asarray([signal_per_event])

        # Update Meta information
        # CSA time bin in ns from allpix
        self.meta['t_sample'] = csa_time_bins * 1e-9

        # Update total number of events (particles)
        self.meta['NumberOfAcquisitions'] = np.shape(signal_per_event)[0]
        # Update total length of data (number of events * samples per event)
        self.meta['length'] = np.shape(signal_per_event)[0] * np.shape(signal_per_event)[1]
        
        # Extra meta information needed
        self.meta['apply_offset'] = False

        data_as_dict = {}

        # Add some padding before the signal
        padding_before = 0
        padding_after = 0
        signal_per_event_with_padding = np.pad(
            signal_per_event, ((0,0), (padding_before, padding_after)), 'constant')

        data_as_dict['CSA'] = signal_per_event_with_padding

        return data_as_dict

    @property
    def data_raw_Allpix(self):
        if self._data is None:
            self._getRaw()

        return self._data