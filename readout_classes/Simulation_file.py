'''
    File name: Simulation_file.py
    Author: Andreas Gsponer
    Date created: 2022
'''


class Simulation_file():
    '''
    Generic Superclass for simulation data with equally spaced (in time) data

    This class is used by fileReader.py in order to read and analyze the data.
    Currently, Weightfield_file.py and Allpix_file.py inherit from this class.

    '''

    def __init__(self, filename, read_method):
        ''' Set generic metadata for simulation data

        Args:
            filename (str): Path to file containing data from a simulation
        '''
        self.filename = filename
        # Fixed number of channels (single channel)
        self.NumChannels = 1

        # Method to read in data
        self.read_method = read_method

        # Set meta information required by analysis code
        # Fixed 'CSA' (charge sensitive amplifier) channel name
        self.meta = {}
        self.meta['source_names'] = ['CSA']
        self.meta['channel_name'] = ['CSA']

        self.meta['apply_offset'] = False
        # Zero offset and unity conversion factor
        self.meta['offset'] = {'CSA': 0}
        self.meta['conversion_factor'] = {'CSA': 1.0}

        # We will only store the voltage (amplifier signal)
        # The time axis will be generated from a fixed time step
        self.meta['channel_type'] = ('voltage')

        self.meta['Start'] = 0
        self.meta['xStart'] = 0

        self._data = None

    def _getRaw(self, event = 0, start = None, stop = None, source = None):
        ''' Data getter method
            The data can be filtered by event number and by start/stop indices

        Args:
            :param start: Index of starting sample
            :param event: Index of event
            :param stop: Index of final sample
            :param source: Oscilloscope Source Name    

            If this method is called the first time, the data is read using
            self.read_method and stored in self._data. Afterwards, only ever
            a copy/slice of this data is returned in order to avoid having to
            read in the data from disk again.
            
        Returns:
            :param data_as_dict: Dict containing CSA samples in V
        '''
        
        if self._data is None:
            self._data = self.read_method(self.filename)
        
        return {'CSA': self._data['CSA'][event, start:stop].copy()}