import sys
import os
from multiprocessing import Pool, cpu_count, Process
import json
import logging
from PyQt5.QtWidgets import QMessageBox
import numpy as np


def _except_hook(cls, exception, traceback):
    '''
    Function used by :py:func:`pqt5_exception_workaround`. Don't call this directly.
    '''
    sys.__excepthook__(cls, exception, traceback)
        
def pqt5_exception_workaround():
    '''
    Workaround to prevent pyqt5 from silently eating exceptions.
    Call this at the beginning of your progam and you will experience relieve.
    '''
    sys.excepthook = _except_hook

def gauss_function(x_data, A, mu, sigma):
    '''
    Gaussian function to use for the scipy-curve fit. Returns a list of the y-data for given parameters
    
    :param x_data: x data
    :param A: Amplitude of the Gaussian
    :param mu: mean value of the Gaussian
    :param sigma: std of the Gaussian
    
    :returns: The function values according to given parameters
    '''
        
    return np.absolute(A) * np.exp(-(x_data - mu) ** 2 / (2 * sigma ** 2))

def get_fit_params_estimations(fit, x, y, factor):
    '''
    Calculates some estimations for fit parameters of Gauss, Langau and Landau fit to use as initial 
    values. 
        
    :param fit: Key which fit we are dealing with. Either Gauss, Langau, or Landau
    :param x: np.array of x-data
    :param y: np.array of y-data
    :param factor: We calculate sigma and eta as std on both or one side of the maximum. The values considered are all that fulfill val > max/factor
    
    :returns: list including the fit parameters in the same order as given in Ini_Handler() in ramp_fit_params
    '''
        
    # Get maximum value and its position
    mpv_est = x[np.argmax(y)]
    max_est = np.amax(y)
    
    # Get the std on one or both sides, depending on the fit
    mask = y > (max_est / factor)
    x_dev = x[mask]
    y_dev = y[mask]
    imax = np.argmax(y_dev)
        
    if fit == 'Gauss':
        sigma_est = np.sqrt(np.sum(y_dev * (x_dev - mpv_est) ** 2) / np.sum(y_dev))
        init_guess = [max_est, mpv_est, sigma_est]
        
    if fit == 'Langau':
        sigma_est = np.sqrt(np.sum(y_dev[:imax] * (x_dev[:imax] - mpv_est) ** 2) / np.sum(y_dev[:imax]))
        eta_est = np.sqrt(np.sum(y_dev[imax:] * (x_dev[imax:] - mpv_est) ** 2) / np.sum(y_dev[imax:]))
        init_guess = [mpv_est, eta_est, sigma_est, max_est]
            
    if fit == 'Landau':
        eta_est = np.sqrt(np.sum(y_dev[:imax] * (x_dev[:imax] - mpv_est) ** 2) / np.sum(y_dev[:imax]))
        init_guess = [mpv_est, eta_est, max_est]
        
    return init_guess

class Persival():
    '''
    Keeps values persistent between program runs. 
    
    '''
    
    def __init__(self, persival_file='persival.json'):
        '''
        Will try to load parameters from the persival_file. 
        If the file does not exists this will be silently ignored and the file will be created when :py:meth:`save` will be called. 

        '''

        self.persival_file = persival_file        
        if os.path.exists(persival_file):
            with open(self.persival_file, 'r') as file:
                self.data = json.load(file)
        else:
            self.data={}
        
    def setDefault(self, name, value):
        '''
        Sets the given value only if no information was present in the persival file or if the persival file was not found.
        '''
        if not name in self.data.keys():
            self.data[name]=value
    
    def get(self, name):
        '''
        Returns the saved value for the given parameter.
        '''
        return self.data[name]
    
    
    def set(self, name, value):
        '''
        Store the given value in memory. You have to call save to dump the value to the disk. 
        '''
        self.data[name] = value
       # print('Break')
    
        
    def save(self):
        '''
        Saves the content to a file.
        '''
        with open(self.persival_file, 'w') as file: 
            json.dump(self.data, file)
       
class ProcessingHelper():
    '''
    Helper working around the limitations of Pool.
    
    '''
    def __init__(self, parallel=False):
        self.funcs=[]
        self.parallel=parallel
    
    def add(self, function, args, kwargs={}):
        '''
        Add a function to the processing pool.
        
        :param function: function to be executed
        :param args: list. Provice the positional arguments
        :param kwargs: dict. Optional, provide the kwargs
        '''
        self.funcs.append({'func': function, 'args': args, 'kwargs': kwargs})
        
    def run(self):
        '''
        Start processing. 
        
        :returns: list of results. The orgdering matches the sequence of add calls.
        '''
        processes=cpu_count()
        # Windows does not handle full cpu loads well, so leave one CPU unutilized for Windows.
        if os.name == 'nt':
            processes -=1
            
        if self.parallel:
            with Pool(processes=processes) as pool:
                result=pool.map(self._myrun, range(len(self.funcs)))
        else:
            result=[]
            for i, f in enumerate(self.funcs):
                r=self._myrun(i)
                result.append(r)
                
        return result
    
    
    def _myrun(self, paramNo):
        '''
        wrapper around the function for pool
        
        :param paramNo:
        '''
        param=self.funcs[paramNo]
        func=param['func']
        args= param['args']
        kwargs= param['kwargs']
        retval=func(*args, **kwargs)
        return retval
               
class Warn_Me():
    '''
    Class used to print fast warnings in a desired color.
    '''
    
    def __init__(self):
        '''
        Just initializes the available colours. 
        
        :returns: None
        '''
        # Available terminal colors to call
        self.colors = {'red':'\033[31m', 'green':'\033[32m', 'yellow':'\033[33m', 'blue':'\033[34m', 'magenta':'\033[35m'}
        
        return None
    
    def warn(self, text, color = 'yellow'):
        '''
        Functions to be called to print out a colored waring. 
        
        :param text: Text that should be printed out
        :param color: Color in which it should be printed as string and according to self.colors
        :returns: None
        '''
        
        # Here we really want to be sure we at least throw some warning if there is some error
        if not type(text) is str or not type(color) is str:
            logging.warning('\033[31m' + 'Invalid arguments for warn() function (must be strings). You just missed a warning you intended to throw!!!' + '\033[0m')

        else:
            if not color in self.colors.keys():
                logging.warning('\033[31m' + 'Warning color not included in class Warn_Me...you just missed a warning you intended to throw!!!' + '\033[0m')
                return None
            else:
                start = self.colors[color]
                end = '\033[0m' 
                warning = start + text + end
                logging.warning(warning)
                        
        return None
    
class PopUp(QMessageBox):

    def __init__(self, parent = None):
        super(PopUp, self).__init__(parent) 
        
    def window(self, title, text):
        '''
        Pop up window that lets the user choose yes or no. 
        
        :param title: The title of the window
        :param text: The qestion the user should decide about
        '''         
        
        # Create a pop up window
        self.window = QMessageBox.question(self, title, text, QMessageBox.Yes | QMessageBox.No)
            
        if self.window == QMessageBox.Yes:
            return True
        
        elif self.window == QMessageBox.No:
            return False
        
        else:
            return False   
        