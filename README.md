This Python framework offers automated pulse finding and analysis based on user defined analysis parameters. It allows for single-file, multi-file analysis, as well as serial data of different settings. Results for each setting are stored into separate *results* folders, together with .ini files of the obtained data, histograms, signal plots and .csv files of those for further analysis. 

Also includes full ramp analysis, meaning the further analysis of obtained histograms from multiple settings (e.g. a bias voltage ramp) via Gauss/Langau/Landau fitting and plotting of the respective results.

Effort has been taken to apply *good coding*, commenting a lot within the code itself. If questions emerge, please contact: Philipp.Gaggl@oeaw.ac.at

# Python packages

This framework has been created using an Anaconda environment. If a similar shell is used, the environment can be loaded directly from the *python_environment.yml* file. Otherwise, the packages are listed in there as well. 

The only complicated thing may be to install PyLandau, which needs quite new versions of numpy and pandas. 

# Launch
There is a full implementation of GUIs for the handling of the framework. All but the main GUI are located in the subdirectory *GUIs*. Efforts have been taken to make the algorithm safe against user mistakes. 

If one just wants to check the program out and learn by doing, just run the program *GUI_Analyse_Run*. All operations can be launched from there, opening further GUIs. 

The order of the buttons in this main-GUI are also the workflow an analysis should undergo. 



# Typical workflow for a single run analysis
This guide summarizes the typical steps to analyze the data of a single run (acquired data that where taken under the same conditions and can thus be analyzed together). 

## Folder constellation:
> Create a *results directory* in which the corresponding results folder shall be stored.

> Put all data files which should be analyzed together into one directory, the *run folder*. Even if it is just one file, a folder is needed.

> Launch the script *GUI_Analyse_Run.py*, located in the main directory. The GUI should open.

## Create a configuration file:
To even start an analysis, one needs such a file containing some basic documentary and necessary information about the experimental settings.

> Click on the button *create run ini(s)*, another GUI should open. Within there, fill in the given information that you want to remember later. Make sure the run number has not been given to another run folder whose results you want to store in the same directory, otherwise it will be overwritten. Also make sure that the number of checked channels is equal to the actual number of recorded channels, otherwise there will be an error.

> You can also choose to load a configuration previously saved by clicking *load external config file*, or from any other .ini file (e.g. a results_summary.ini within a results folder) by choosing *load old settings from existing .ini file*.

> If the information is correct, click *choose target folder* and select the *run folder* containing your data to analyze, then click on *create single config.ini*. The config file should now be within the *run folder*.

> If you want to remember this configuration, click on *save as external config file*, where you can choose a name for it and save it in your personal user settings. 

> Exit the GUI

## Set the analysis parameters:
Before starting the analysis, choose the specific parameters for pulse finding, analysis and plotting.

> Click on *set analysis parameters / preview pulse analysis and raw data* in the main GUI. Another GUI should open. 

> Within there, you can change all available analysis parameters. A short description for each of them is given in the Basics/Analysis section. 

> The best way, is to visually check and adapt the parameters by using the preview function on a single data file:
- Click *choose data file for preview* and choose a test file to analyze. 
- Click on *start preview* to display the pulses the software would find using the given parameters in the GUI. The number of displayed pulses can be changed in the field *#of pulses to plot*. Use the arrow buttons to go forward and backward, the channel can be changed using the channel buttons. 
- You can change the parameters and just click again on *start preview*. 

> If the data consists of multiple events (e.g. DRS4 board, segmentation mode for RS), one can also just plot the raw events by clicking on *plot raw events*.

> User defined parameter settings can be accessed using *load user setting*, the documented settings from some other analysis can also be loaded by clicking *load from existing .ini file* and selecting the corresponding .ini file.

> If the settings are ok, you can store them in your individual user settings by clicking *export as user setting*.

> To rest the parameters as they were at the opening of the GUI, click *reset settings*.

> If the parameters are ok, click on *keep settings and exit*. These settings are then stored into a file and will be used until they are changed manually again.

## Execute the analysis
> Click on *choose results folder*. In the file dialog, select a directory where you want your result folder to be stored in. The other buttons now should be available.

> Click on *choose run folder to analyze*, select your *run folder* containing the data.

> To start the analysis, click on *analyze single run folder*. If everything is ok, and a config.ini is located within the *run folder* the analysis should start right away. Information about the progress and the obtained statistics is printed in the terminal throughout the analysis. 

> After finishing, a folder named after the run number stated in the corresponding config.ini should lie within your *results folder*.\
It should contain:
- A plot folder for some example pulses and sequential pulses
- Individual results.ini files for each analyzed file
- A summary.ini file containing all config/analysis parameter information as well as the obtained statistics
- A .csv file for each channel containing the obtained pulse data for all pulses for further processing
- A histogram for each channel and analysis parameter respectively, as well as .csv file with the corresponding histogram data for further processing


# Optionally, review result histograms and the their composition
Using the *review result histograms* option within the main GUI, you can choose a result folder and access all histograms. By playing around with the pulse parameters and their influence on the histograms, you can check whether you have a consistent pulse behaviour or not.


# Typical workflow for serial ramp analysis
A serial ramp is a ramp with multiple runs, whose configurations only differ by a single setting (e.g. bias voltage, beam rate, particle energy) and whose results can therefore be used for a serial analysis.\
The workflow does not differ very much from the one of the single run analysis, therefore only the differences are described here. 

> Create a parent data folder, which will contain all the run folders for serial analysis, each of them containing the obtained data of one setting.

> Instead of manually creating the multiple run folders and placing a single config.ini file within them, you can use the program to do so:
- This only works if your varying parameter is:
    - Bias voltage of a detector/channel
    - Beam rate
    - Particle energy
- Open the config GUI by clicking *make run ini(s) on the main GUI. 
- Choose all parameters except the ramp parameter the same way as described in the previous section. 
- After choosing a target folder, instead of *create single config.ini*, click on *create ramp configuration*. Another GUI should open. 
- Within this GUI, you can choose your ramping parameter, the detector/channel it applies to (if it is bias voltage), and its values. Hereby you can choose between an equidistant spacing only using min, max, and step, or individual values. 
- If the ramp configurations are ok, click on *create ramp folders including config files*. This will place a folder named after the ramp parameter and its value within your chosen *results folder* and place a config.ini file within each of them, adhering to individual run numbers and containing the right information about the ramp parameter.

> The analysis parameters should be set the same way as for the single run analysis, using the preview function. But one should take care to use a representative file for the preview. 

> After choosing a results folder, click on *choose parent-data folder for serial analysis*. In the file dialog, choose the parent folder which contains all the individual run folders. 

> To execute the serial analysis, click on *analyze parent-data folder*. The program now performs a single run analysis for each of the run folders contained in the parent data folder, and places an analysis folder for each of them into the chosen results folder. 

## Perform a ramp analysis
To now also analyze this ramp data, you can use the *Post analysis ramp evaluation* section in the main GUI. 

> Choose the ramp parameter, only one of the three given is available. 

> Chose the fitting method, either None (only average values), Gaussian, Langau, Landau or All.

> Click on *choose result folder and start ramp analysis*. In the file dialog choose the results folder which now contains all the folders of the analyzed single runs. 

> The software now goes through all folders and pulse parameters, and fits the corresponding histograms to the desired form. 

> After the analysis is finished, a ramp-analysis folder should have been placed within the results folder. It should include:
- A .ini file for each evaluation/fit method and detector respectively. 
- A plot folder for each detector, including the individual pulse parameters plotted against the ramp parameter as well as a picture of each histogram together with the fit for visual confirmation. 
- A comparison plot folder, showing the same plots as above but only with all given detectors. 


# Basics about the framework:
## Time axis and data-conversion
For efficient use, the data can (but not has to) be given without any time axis, but only using a sample-time, which will then be used for all purposes.\
For the same reason, it is possible to work with unconverted raw data. To do so, the information about the *offset* and the *conversion_factor* for converting into voltage data needs to be given. All operations (except for plotting) are then performed on the raw-data. For sources that do not provide equidistant timesteps, a linear interpolation is necessary (such as done for the DRS4 oscilloscope).


## Supported formats/sources, user extensions:
The *Det_File()* class, located in *fileReader.py*, inhibits all functions for pulse finding and analysis. It calls other classes dependent on the file format and source of the data. Currently, the supported sources are:

- R&S oscilloscope --> .bin file for raw data and .Wfm.bin file including metadata
- DRS4 oscilloscope --> .dat files
- CNM --> .csv files with one column, separated by a *,*, inhibiting the voltage and time data of a single event
- Weightfield2 simulation data --> .txt files
- AllPix² simulation data --> Currently via processed .csv files, efforts are taken to use binary data (ROOT files)
- Single event .csv files, time and voltage data separated by *,', as used at CNM for several measurement setups

To include an individual data format/source, one can write a class and include it into the fileReader-code. Such a class **must** include following things:

- A class property *data_raw* representing all the raw data. It is of following structure:\
dict(channel1:events[[event1_data], [event2_data], ...], channel2:[...., time:[time data]). In words, we need a dict with the channel names as keys, and the data as items. A data-item is a numpy array, including further numpy arrays of y-data of one recorded event. It can include an additional time-array, but does not have to. If one only dela with not segmented data, just use one single event containing all data.

- A method *_getRaw(event, start, stop, source)*.\
This function is used to access the data. It returns the same structure as *data_raw*, but can be filtered using the channel name, the event number and the indices of start and stop sample.

- A class variable *meta*. A dictionary including some necessary (and some unnecessary) information. The keys, items are as followed:
    - 'source_names' --> list of the channels
    - 'channel_type' --> list of the same order as source_names, 'voltage' if y-data, 'X-Time' if time axis
    - 'channel_names' --> should be the same as 'source_names
    - 'length' --> number of total recorded samples (We assume the same number for all channels)
    - 'NumberOfAcquisitions' --> number of recorded events
    - 't_sample' --> record length of a single sample (assuming equidistant samples) in s. This is especially important if one does not use a separate time axis within the raw-data
    - 'apply_offset' --> If the data needs to be converted this must be *True*
    - 'offset' --> offset value for conversion (in V) (dicr including each channel)
    - 'conversion_factor' --> conversion factor between raw units and V (dict including each channel)

In order to be usable in the GUI, additional changes need to be made so that *fileReader.py* recognizes the filename extension and can choose the correct *DetFile* class.
For a new class *Custom_file.py*, add the following code block to *fileReader.py:DetFile*:
```
elif file_base == '.csv':
            print('Using new custom class')
            self.osci = 'Custom'
            RO.Custom_File.__init__(self, file_name)
            self._getRaw = lambda *args, **kwargs: RO.Custom_File._getRaw(self, *args, **kwargs)
            self.data_raw = self.data_raw_Custom
```
In *Analyse_Run.py:Analyse_Run:init*: add the following line:
```
elif self.osci == 'Custom':
            self.file_format = '.custom'
```
Add the channel name to *GUI_Parameter_Setting.py:GUI_Parameter_Setting:get_channels*
```
elif self.preview_file.osci == 'Custom':
            channels = ['Channel Name']
```
Add the "oscilloscope" (e.g. data source) to the list of allowed values in *Run_Config.py:243*:
```
    self.all['oscilloscope'] = [str, None, False, None, None, None, None, ['RS', 'DRS4', 'Weightfield', 'Allpix', 'Custom'], None, True]

```

## Folder structure
Due to its frequent use for particle beam experiments, the read-out (RO) and write-out (WO) are done a very specific way. All data (single or multiple files), which represents a certain and identical experimental setting, needs to be located within the same folder. If so, it will be analyzed as if it was one big data-file, while still giving information about the single files as well.

Typically, these are ordered by a *run number*. A data folder containing the data of such identical settings is further called a ***run folder***  Although the folder can be named arbitrarily, the created result folders for each run are named after the corresponding run number. Therefore, it is **very important** to check for non-identical run numbers between the data.


## config.ini
A strict documentation of these settings is required. This happens via a config.ini file, that is mandatory within each run folder. It contains some basic information such as the particle type, energy, and frequency, as well as the names of the used channels/detectors. Most of the information is not used for analysis itself, but passed on to the results for later consistency. It is strongly recommended to use the GUI to create these config-files. The corresponding GUI also has an option to create ramp folders based on a predefined ramp parameter (e.g. bias voltage) or a user defined one, which already include the right .ini files with the paramter and run number in ascending order. 

Two important things to keep an eye on: 
- The number of channels given in the config.ini file **must** be equal to the number of channels used in recording. 
- The run number, easily the most important information in such a file, must be unique for each setting/run folder. This is, because the result folders are named after the run number. If one chooses to store the results of multiple runs into the same location and uses equal run numbers for different run folders, the results of the former will be overwritten. Although the GUI remembers already analyzed run numbers and asks for permission, this should not be forgotten. 

Other information is marked as OPTIONAL and are not mandatory. 

# Analysis
The pulse finding and analysis is based on settable analysis parameters. These are set within a *analysis_parameters.ini* file and extracted by the fileReader and can efficiently be changed, stored and loaded using the corresponding GUI within the folder *GUIs*.  
A short summary of them is given below. Note, all parameters are given in SI units within the .ini file, while the GUI allows for more convenient units. 

Generally it should be noted, that the GUI for analysis parameter setting includes a preview mode for a single data file. This function is suited perfectly to get a feeling for the influence of following parameters, and should also be used previous to analysis to check for the parameters to use.

**Pulse finding parameters**
- **min_tot_factor**\
Number of samples a signal must be above threshold continuously to be recognized as signal. This of course strongly depends on the sample size. For very noisy oscilloscopes or environments, as well as oscilloscopes with high sampling rates, this variable may be quite high and vice versa. Use the signal-example plots to check whether you do not find pulses which are obviously noise.

- **min_time_between_peaks**\
Crucial for pulse finding itself, as it marks the minimal time span that needs to be between two events (where the threshold is surpassed) in order to count them both as separate pulses. If the time span is below this value, the pulse region is enlarged and both (or more) pulses are taken as one.\
This may lead to the detection of pile ups, but these are at least visible in the histograms, while multiple counting of overlapping pulses compromises the whole result without recognizing it.

- **use_fixed_threshold**\
If True, a fixed threshold for pulse finding and analysis will be used instead of the one automatically calculated by the noise level. The sign must not be considered here, as it is adapted according to forced search direction or automatic maximum-search. 

- **std_factor**\
The getStatistics() method calculates the standard deviation over the whole data to receive the dark current. This std multiplied with the *std_factor** gives the threshold above which a signal is considered a signal and tot starts. Choose this factor high enough such that noise is never detected as a pulse, and such that small pulses are still found.

- **fixed_threshold**\
The fixed threshold to use if the variable above is set to true.

**Analysis parameters**

- **analysis_till_rms_noise**\
If this parameter is set to True, the following two parameters are not used. Instead of analysing the pulse area in a user defined range, the pulse is considered until the points on both sides where the first sample is below the calculated rms-noise.

- **buffer_analysis_left/right**\
This buffer will be added on the left and right side of the start/stop sample accordingly during the signal pulse analysis. This means, that e.g. the pulse area will include the region of the buffer. 

- **skip_borders**\
Some measurements seem to have very high resonances at the beginning or the end of measuring. To cancel these out, set this variable to the number of signals that should be omitted at the beginning and at the end of measurement data. Use the example plots to verify. 

- **compensate_offset**\
If we have to deal with an external offset that is visible in the analysis plots, this offset can be compensated when setting this variable to True. The offset will be calculated (bit more time-consuming) and compensated all time later on. For higher beam rates, this makes no sense anymore, as efficient calculation needs enough data only including noise.

- **advanced_noise_offset**\
If this is activated, offset compensation is mandatory. This mode replaces the simple calculations of offset and noise via average and stds with a more precise, but slower method. More information in the subsection *andvanced offset compensation and noise level calculation*. But it is to be said, that this mode should rather be used for low fluxes, where only one pulse per event occurs, otherwise larger fractions of the events can be discarded even before pulse finding. This is only possible for osci-data, not for simulation data which does not include any noise.

- **pure_simulation_analysis**\
If this is activated, no pulse finding is executed. Instead, it is assumed that one deals with purely simulational data, which has no considerable noise, offset or multiple signals. Instead, a single pulse per event is assumed, stretching over the whole event. This means, that the whole event data is passed on directly to the pulse analysis. This option should not be used in any other case. The user is therefore asked once more if this should really be used.

**Plotting parameters**

- **buffer_plot**\
Represents the buffer left and right that is added in the signal-example-plots. Left from first sample above threshold and right from last sample above threshold.

- **no_example_plots**\
As much exemplary pulse plots will be saved in the result folder for each channel and file respectively.

- **plot_analysis_featutures**\
If True, all the important things from the analysis are plotted into the pulse plots. This includes the used threshold, the start and stop samples, samples above threshold and the region (with buffer) that is included within the analysis.

- **plot_peak_parameters**\
If True, the corresponding pulse parameters are also written into the example plots of the single signals.

**Parameters for time resolution**

- **apply_constant_fraction_discrimination**\
If this value is set to True, the next variable (CFD_FACTOR) is used to apply constant fraction discrimination. This means, that the threshold for pulse analysis is reevaluated and set to a fraction of the pulse amplitude (CFD_factor * peak_amplitude). This is not done during pulse finding, but before the pulse analysis. The start and stop samples are updated accordingly.

- **cfd_factor**\
The fraction of the pulse-amplitude, at which the new threshold for each pulse shall be set when doing constant fraction discrimination.

## Offset compensation
If offset compensation is desired, an artificial offset (e.g. coming from the osci) is calculated for each event, stored separately and applied for all following procedures. Depending on the analysis parameter *advanced_noise_offset* a faster a slower, but more precise and secure one is applied to compensate for the offset.

The slower one simply calculates the average over al samples and then filters out outliers simply by removing samples greater than 2.5 times the std. This way, most of the pulse samples are not included, but still some of them are. 

The faster one is described in *andvanced offset compensation and noise level calculation*

## Noise level calculation
The noise level is needed to calculate the SNR value. Also, if no fixed threshold is used, it is needed to obtain a threshold. Same as for offset compensation, there are two methods switched on and off by the analysis parameter *advanced_noise_offset*. 

The slower one obtains a noise for each event, via determining the std value for all samples, which lie on the opposite side of the pulse polarity. If the zero level is not good, it is crucial to use offset compensation as well. The more precise but slower method is described in the following subsection. 

## Andvanced offset compensation and noise level calculation
This method is quite precise and, most of all, secure, as it removes events where either offset or noise could not be determined as well. This mostly is the case if we deal with multiple pulses in a single recorded event. Therefore, this method of course can only be applied in fast segmentation mode and only for data recorded by an oscilloscope, not gathered via simulation. Part of the ideas have been taken from an existing pulse analysis software which can be found at GitLab:
https://gitlab.cern.ch/egkougko/lgadutils

Following procedure is used to calculate the offset and the noise for each event:

- For each event, the maximal divergence from the zero line is determined. The polarity is stored. 

- If either the preceeding or following sample is above 80% of the maximum value, i is considered a pulse, otherwise, the event is discarded. 

- 10%-90% of the region preceeding the maximum are considered noise. It is checked if this region covers at least 20% of the event length for enough data. If not, the right side of the maximum is used.

- The histogram of the noise values is fitted using a Gaussian. 

- To check, if there is no other pulse or other corrupting data present, we check if at least two noise samples are outside the 3*sigma region of the obtained fit (as pulse data would either be far on the left or right side of the noise histogram, depending on the polarity). If this is the case, the other side of the pulse is used for noise data. 

- If these things do not work on both sides, the whole event is marked invalid and not considered during pulse finding and analysis. 

- Additionally, the dominating polarity is checked, and each event which has maximum with another polarity is also marked invalid. 

- From the mean value of the fit, we obtain the offset for the event, while the sigma determines the noise level. 

Due to the fits, the advanced method is much slower, first calculations show about a factor 1.5-5 in computing time. However, it is much more secure, but also uses less data as ivalid one is not used.

## Pulse finding
The pulse finding itself is based on a simple time over threshold (ToT) determination. If the number of samples, that are above the threshold given by *fixed_threshold* (if *use_fixed_threshold* is True), or the product of the average noise std and *std_factor*, exceeds the minimal required samples above threshold, given by *min_tot_factor*, it is considered as signal.

In cases a pulse is interrupted by only a single or few samples that fall below threshold (e.g. due to noise) and then immediately jumps above threshold, this could lead to the finding of two separate pulses which are actually just one. Therefore, if the time span covered by these interrupting samples does not exceed *min_time_between_peaks*, such a case is still weighed as a single pulse. However, if the variable is too high, the next pulse could be included in the finding. 

The start and stop samples of the signal, marking the first and last sample above threshold respectively, are stored and then used for later analysis and plotting. 


## Pulse analysis
There are several parameters that are calculated and extracted for each found signal. 
- pulse maximum
- pulse area (proportional to collected charge)
- pulse area below threshold (independent of either user buffers or the setting to expand pulse until noise)
- time over threshold (ToT)
- time over noise (if *analysis_till_rms_noise* is used, otherwhise the same as ToT)
- rise time (time it takes the pulse from 10% of its amplitude to 90%)
- rms --> root mean squared value of the pulse
- rms_noise
- SNR ratio
- hardware offset
- aspect ratio 


All but the pulse area and the rise time are calculated within the region between start and stop sample of a pulse. For the area, one usually wants to extend the pulse further (mostly only to the right). By setting the variables *buffer_analysis_left* and *buffer_analysis_right*, one can extend the region for area calculation on the respective sides by a constant time. One should check the signal plots for this setting. A more intuitive method is used when setting *analyse_till_rms_noise* to True, where the pulse area is expanded until the first samples reach the rms-noise. \
One can easily include a new pulse parameter. To do so, the new parameter only must be added within the Run_Config class hardcoded (including units and conversion factors for display units) and a method of calculation within the *analyse_single_peak()* function in fileReader.py. Everything else is the done automatically. 



# Structure and description of the included files:

## Data readout files/classes
There are several data sources (DRS4 and RS osci, but also Weightfield and Allpix data). Each of them has an individual readout class, which is located in the folder *readout_classes*. Each includes a function called _getRaw(). It ALWAYS returns the data in the same format of following hierachy:

Dictionary, channels as keys --> A list containing np.arrays of all event data --> event data, meaning the voltage data.

If a source does not give separate events, the structure still needs to be kept, however only containing a single np.array. Furthermore, there are some metadata that are crucial (they originate from the first readout class, the RS_File class, which got them from an individual metadata file). These are information such as the number of events, the scaling factor of the voltage data, eventual hardware offset, the sample length, the number of recorded samples, if the data needs to be converted or not...

If all this is given within a readout class, the rest of the software can use this and theoretically analyze data from all kind of sources.

### RS_file.py:
Class from Simon Waid which receives a .bin file with necessary metadata and formats the raw data of the corresponding .Wfm.bin file (with same name). Up to four channels are possible. There are a lot of different included cases, depending on the data format, the bitsize of the data, the segmentation mode of the osci... If any questions or errors occur within this class, Simon Waid is the person to call. 

### DRS4_file.py: 
Class from Philipp Gaggl formatting a given .dat file and able to return its data for up to four channels.

### Simulation_file.py:
Class by Andreas Gsponer to provide a common super class for simulated/synthetic data with equidistant time bins.
This class sets some metadata common for simulated data (single channel only, no offset, etc.) and takes a method to read in data from specific implementations such as WeightField2 or AllPix².

### Weigthfield_file.py:
Class from Andreas Gsponer to read in Weigthfield2 simulation outputs.
Input for this class can be generated using the "Output files for Signals" button in WF2.
Because WeightField generates a file for each event, a Run analysis needs to be used to read in multiple signals.
Another possibility would be to merge multiple WF2 files into a generic .csv beforehand and using *Allpix_file.py*

### Allpix_file.py:
Class from Andreas Gsponer to format AllPix² simulation output. Currently, preprocessed .csv files are used where the time bin size is stored in the header of the csv file.
Binary files might be supported in the future, but as the ROOT files of AllPix² require the AllPix² shared libraries this would be difficult to cleanly distribute (maybe Docker can be used)?

### CNM_file.py:
Class for the readout of single event data in .csv files, where timesteps are equidistant and separated by a *,*. This is a file format used for various measurements setups at IMB-CNM-Barcelona.

### GUI_Analyse_Run.py
This is the main GUI from where all operations are started and controlled. Each of the buttons usually opens another GUI or lets you choose a data/result folder. 

First of all, config.ini files containing necessary documentation and run information must be created. These ought to be placed within each run folder.

After that, the analysis parameters should be set, ideally by using the preview function. 

A results folder must be chosen. There it is not necessary to create a results folder for each run. Rather, it should be some parent-results folder, where all result folders are then stored in. 

For the analysis, one can choose either a single run analysis, or a parent data folder, containing multiple run folders. If a serial run analysis is performed due to some ramp (e.g. bias voltage) one may choose an individual result folder to store those in before.

If one deals with data of the same settings (detector, particle type) with only one varying parameter, one has also the option to perform a whole ramp analysis over either the particle energy, the beam rate, or the bias voltage. To do so, all result folders must be located within the same directory, which is then chosen.

## File analysis

### fileReader.py:
Class from Simon Waid and Philipp Gaggl. Includes all pulse finding, analysis and processing and inherits from one of the data readout classes. It uses the information contained in self.meta (dict) to work on the raw data which is received by the _getRaw method in each of the parent classes.

The class contains all methods needed to analyze the data of a single file. Calculating the dark current and a corresponding pulse threshold, compensating eventual hardware offsets, find pulses using a time over threshold method, analyze them w.r.t. their height, area and ToT, as well as SNR and rms and plot a number of exemplary pulses for visual validation.


## INI-file managment

### GUI_Make_Run_Ini.py
As every run containing data recorded at equal settings must include a config.ini file, there is a practical GUI to do so. It can be launched by the main GUI by clicking *create run ini(s)*. 

The information to give is quite self-explanatory. Although there is some information that must be given, most of them are not used for further analysis. Others are only optional. However, there are some very important parameters, which are summarized below:

- **run number**\
The most important one. The created result folder will be named accordingly. If one uses the same number for different data folders, results could be overwritten. 

- **used channels**\
This information **must** be equal to the actual number of recorded channels. Otherwise, you will encounter an error. 

- **particle energy / beam rate / biasvoltage**\
These are the three parameters where a later ramp analysis can be performed. One should check if the given information is correct to not screw that up later.

If satisfied, choose a target folder (usually the run folder containing the data) for the .ini to be stored in. To finalize, click *create single config.ini*. 

However, sometimes, one executes a certain ramp (e.g. over the bias voltage). To not be forced to create multiple config.inis only differing by one value, one can choose *create ramp configuration*. The current settings in the GUI are used, while one can choose a ramp parameter, as well as the different values of it (via range or individual values). This option also creates the correspondingly named run folders and places the config files in there. 

One can also choose to store a given configuration as a user config, by clicking *save as external config file*. Saved files can be loaded via *load external config file*.

If one forgets to save the config settings, one can also load the information from any other .ini file. Additionally, the summary.ini files in the result folders include the same information and can also be used. To do so, just use *load old settings from existing .ini file*. 

The last settings are stored and will be used as initial values when opening the GUI the next time.


### GUI_Serial_Ramp_Ini.py
This GUI can be used to avoid excessive and long taking config.ini (and folder) creation for each single run of some serial analysis. It can be launched from the GUI_Make_Run_Ini(), taking the target folder chosen and adapting the config information set therein. One can choose between the ramp parameters **energy**, **rate** and **bias_voltage**, which are also the ones that can be serially analyzed by the RampEvaluation() class. The user can choose between automatically generated steps, using min, max and step values (but needs to be precise), or custom values typed in manually.\
The GUI checks for preexisting non-empty folders and asks the user whether to overwrite them. If all settings are accepted (they are checked rather carefully), correctly named data folders to later store the run-data in are created within the beforehand chosen result folder. In each of them, an individual config.ini file is placed, identical to the settings chosen in the Ini-GUI, but with ascending run numbers and the corresponding ramp parameter changed. If bias_voltage is chosen, the user also can choose for which given detector the change should apply, as such a ramp usually is only executed for one.


### Run_Config.py
Includes the class Ini_Handler(). This class includes various methods especially tailored to our needs for handling ini files. Ini data can be read out and written very efficiently, only using these functions. Depending on the needs, conversion of ini strings into the right type, format, as well as checking if all parameters are valid (boundaries, non-negative, lists, boolean,...) can all be accomplished by using the correct arguments.

Furthermore, even a conversion of parameter units for better display in GUIs and plots (s into ns) can be done using the readout, writing or conversion can be done. Also, checking can be done via an independent method, which is used by the ini-creator-GUI.

This all is possible, because the class includes a full list of all essential parameters we access and communicate through ini files, such as analysis-parameters and basic/optional run parameters. This list contains important information to deal, convert, check and display the parameters.

Effort has been taken to write it in a way, such that new parameters (time resolution variables) that are added in the future, can be easily added to this list, as long as they do not differ too much in structure from old parameters. However, if they do, some reformation of the methods may be necessary.\

Short method summary:\
**def read_ini(self, dir, convert = True, all_defined = True, display = False):**\
Reads file from directory. all_defined = True-->All parameters must be predefined in the class list. Will be checked when reading. convert = True-->All predefined parameters are converted correctly (into int, float,...) and also checked if they are valid (within boundaries). display = True-->Converts into more displayable units (s into ns, V into mV) if predefined.\

**write_ini(self, content, filename, dir, all_defined = True, displayed = False):**\
Basically the same as read_ini(), just vice versa. 

**convert_from_ini(self, content, display = False):**\
This can be used to convert an ini-string-content directly within a code and returns the converted one. 

**check_content(self, content):**\
All parameters are checked. For the first, if they are contained in the defined list, which is mandatory, and then if they match their boundaries,... Returns a boolean and can therefore be used to check settings in the ini-Gui


## Analysis parameter handling
By now, there are multiple parameters that are used by the fileReader for finding, analyzing and plotting pulses. These are hanled via .ini files that are used by the fileReader. Predefined and saved user settings can be found within

ini_library / analysis_parameters
 
A short summary of the available parameters and their use and influence is given in the Analysis section.


### GUI_Parameter_Setting.py
This GUI can be launched by the main GUI (GUI_Analyse_Run.py) by clicking *set analysis parameters / preview pulse analysis & raw data*. It will load the last parameter settings that where used for analysis. Within the GUI, one can change all parameters as desired, and then test them out on an example data file (*choose data file for preview*) and start a preview (*start preview*) to observe a chosen number of found and analyzed example pulses according to the set parameters. This can be repeated until the results look acceptable.

In addition, one has the option to export the shown settings as an external user setting (*export user setting*) to use for later purposes. Such exported user settings can of course be imported too (*load user setting*). By clicking reset settings, one can change all modifications to the status before the GUI was opened.

After the user is satisfied with the preview, it is necessary to save the settings by clicking:
*keep settings and exit*. This stores the settings into current_settings.ini, which is accessed by the fileReader, while all analysis done during the preview accesses a temporal file during the GUI-execution.

All settings should be safe against all bad user behavior.  

Additionally, one also has the options to load old settings from some .ini file (e.g. the run_summary.ini within a results folder), by clicking *load from existing .ini file*


### GUI_Hist_Reviw.py
This GUI allows the user to access the histogram data either directly by choosing a corresponding .csv file, or by choosing a result folder. All histograms can be viewed specifically and the change of all available pulse parameter and their influence on the corresponding histograms can be checked. The used settings can then be exported as .ini file and later accessed through such. 


## Run analysis
### Analyse_Run.py:
Class to analyze a whole run folder, all contained data files, and make summaries, plots, histograms...
Must be initialized with a path of the run folder and a target folder where the results are then stored in an extra folder.

Depending on the source found in the corresponding run_config.ini, lets the fileReader inherit from the right readout class and uses their output function _getRaw() to extract all file data. The pulse parameters are all appended to a csv file, one for each channel. Statistics are later made by reading these files and creating histograms. 


## Serial Analysis

### RampEvaluation.py
This program accesses a result parent-folder, containing multiple results folders from different runs and executes an analysis, summarizing fitted key parameters obtained from histogram fits w.r.t. the ramp parameter. It is important, that different experiments have different detector names within their config.inis correspondingly.

Currently, three predefined scan parameters are accessible, bias voltage, particle energy and beam rate. However, using the config.ini GUI, the user can also choose to define an individual parameter for an experiment, including informations about its name, unit and value.

It is even possible to mix different runs containing different detectors, as the program generates an own structure for each. Created plots will include an analysis for each detector as well as a comparison between all present detectors over the whole range of the ramp parameter.

Data can be fitted using Gauss, Langau and Landau distributions.


## Test files
There are test scripts for each class in the *working_folder*, all with the prefix 'test'. Due to importing, it may be necessary to move them into the main directory, so one folder back, for them to compile correctly.

All GUIs do not have a testfile, but can be launched themselves to test them out.


## Others
Testdata for RS (RS_testdata(.Wfm).bin) and DRS4 (DRS4_testdata_single_channel.dat) to validate the scripts.\
.gitignore for git\ 
The file python_environment.yml is an environmental file which can be loaded (e.g. in Anaconda) to build an environment suitable for executing the pulse analysis software.

# TODO