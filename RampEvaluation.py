'''
    File name: VoltageRampAnalysis.py
    Author: Philipp Gaggl
    Date created: 10.02.2022, happy birthday to me ;)
'''

import os
import shutil
import posixpath
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import pylandau

from scipy.optimize import curve_fit

from Run_Config import Ini_Handler
from tool_box import Warn_Me
from tool_box import gauss_function, get_fit_params_estimations

class RampEvaluation():
    '''
    Class that enables the evaluation of a voltage ramp with multiple runs. Accesses each run, extracts
    .ini data included for average values of important parameters, as well as the correspondent 
    histogram data. This data can be used to either be analysed using a Gaussian fit or Landau/Langau. 
    Output data will be summarized and stored in the same parent folder, where the analyzed run data is 
    stored. Also, statistics are plotted over the voltage range in various combinations. 
    Attention: All analyzed data should have been done with the corresponding readout programs, no 
    additional folders or files must be added artificially here!!!
    The program currently does not check for consistent settings like particle, energy, osci..., this should 
    be guaranteed by the analysis and/or the user.
    '''
    
    APPLY_OUTLIER_REMOVAL = True
    OUTLIER_REMOVAL_FACTOR = 3
    FIT_ERROR_FACTOR = 10
    ONLY_ONE_PEAK = False
    CUT_AT_VAL = None
    
    def __init__(self, source_path, fit_method, ramp_parameter = 'bias_voltage'):
        '''
        :param source_path: Path to the parent folder containing all the analysis data from all runs
        :param fit_method: Must be either 'None', 'Gauss', 'Langau', 'Landau' or 'All'. Defines the fitting method for contained histogram data
        :param ramp_parameter: Parameter which should be used for the ramp
        
        :returns: None
        '''
        
        # Warning helper
        self.warn = Warn_Me()
        # Ini handler
        self.ini_handler = Ini_Handler()
        
        self.source_path = source_path
        self.fit_method = fit_method
        
        # Ramp parameter information
        self.ramp_param = ramp_parameter
        # Check if ramp parameter is valid
        ramp_parameters = self.ini_handler.ramp_parameters.copy()
        # Add user_parameter artificially
        ramp_parameters.append('user_parameter')
        if not self.ramp_param in ramp_parameters:
            raise(RuntimeError('The given ramp parameter is not valid. Either \'energy\', \'rate\' or \'bias_voltage\''))
        
        # If we use the user_parameter, we need to access one .ini file before beginning tthe ramp evaluation in 
        # order to get the parameter name ad unit
        if not self.ramp_param == 'user_parameter':
            # Get the unit for displaying
            self.ramp_unit = self.ini_handler.all[self.ramp_param]['unit']
            if self.ramp_unit is None:
                self.ramp_unit = ''
        else:
            self.ramp_param, self.ramp_unit = self.get_valid_runs(find_user_param = True)
            if self.ramp_param is None:
                self.warn.warn('Did not find any vslid user parameter. Make sure \'use_custom_ramp_param\' is True and a name is set under \'custom_ramp_param\' in the config.ini file(s)...')
            
        # List of the parameters to analyze as given in the .ini files --> case insensitive
        self.parameter_names = self.ini_handler.pulse_params
        
        # Fit variables of each evaluation method
        self.evaluation_variables = self.ini_handler.ramp_fit_params

        # Check whether the source folder exists
        if not os.path.isdir(source_path):
            raise(RuntimeError('The given source directory does not exist!!!'))
        
        # Check, if fitting method is valid
        fitting_options = self.ini_handler.ramp_options
        self.plot_colours = {'average':'green', 'Gauss':'red', 'Langau':'blue', 'Landau':'cyan'}
        self.multiple_colours = ['g', 'r', 'b', 'c', 'm', 'y']
        if not self.fit_method in fitting_options:
            raise(RuntimeError('The given fit method is not valid. Either \'None\', \'Gauss\', \'Langau\' or \'Landau\''))
                
        # For later use
        if not self.fit_method == 'All':
            self.used_method = [self.fit_method]
        else:
            self.used_method = self.ini_handler.ramp_fit_methods
        self.all_methods = self.used_method.copy()
        self.all_methods.insert(0, 'average')
                
        # Pylandau fits need rescaling due to package constraints
        self.to_scale = self.ini_handler.ramp_fit_params_to_scale
        
        # Fit parameters representing the value and uncertainty for the ramp plots
        self.plot_params = self.ini_handler.ramp_plot_values
                
        print(f'\nInitializing ramp evaluation for ramp parameter \'{self.ramp_param}\'...')
                
        return None

    def data_check(self):
        '''
        Executes the whole data checking before the analysis itself can start. 
        
        :returns: Boolean if checking worked
        '''

        # Get valid subfolders to analyze
        self.valid_runs = self.get_valid_runs()
        if not len(self.valid_runs) > 0:
            return False
        else:
            # Check ramp parameter values and extract structure for coming analysis
            try:
                self.structure = self.get_structure()
            except:
                self.warn.warn('Error in get_structure()')
                return False

            # Extract existing data from the ini files of the analysis
            try:
                self.data = self.extract_data()
            except:
                self.warn.warn('Error in extract_data()')
                return False

            # Check if extracted data is complete
            try:
                check = self.check_completeness()
            except:
                self.warn.warn('Error in check_completeness()')
                return False
            
        return check
          
    def execute_analysis(self):
        '''
        After data is checked for completeness, executes the ramp analysis and plotting.
        
        :returns: Boolean if everything worked 
        '''
        
        # First, we create the main and subdirectories
        try:
            self.create_directories()
        except:
            self.warn.warn('Error in create_directories')
            return False
        
        # Fit the histograms
        #try: 
        self.apply_fitting()
        #except:
        #    self.warn.warn('Error in apply_fitting')
        #    return False
        
        # Create summary ini files 
        try:
            self.plot_data = self.create_summary_ini()
        except:
            self.warn.warn('Error in create_summary_ini')
            return False
        
        # Create summary plots
        try:
            self.create_summary_plots()
        except:
            self.warn.warn('Error in create_summary_plots')
            return False
        self.plot_basic_infos()

        return True             
            
    def get_valid_runs(self, find_user_param = False):
        '''
        Checks each dubdirectory for an run_summary.ini file. If contained, the subfolder is valid.
        
        :returns: A list of lists with two entries with the valid run-folders and the name of the 
                  included summary.ini file
        '''
        
        if not find_user_param:
            print('---Checking for run-folders with valid summary.ini file...')
        
        # Get all subdirectories
        subfolders = next(os.walk(self.source_path))[1]
        
        # Remove those without valid summary.ini file
        valid_subfolders = []
        search_key = 'summary.ini'
        for folder in subfolders:
            valid = False
            files = os.listdir(posixpath.join(self.source_path, folder))
            for file in files:
                if search_key in file:
                    if not find_user_param:
                        # Add to valid subfolders      
                        valid_subfolders.append([folder, file])
                        valid = True
                        
                    # If we just want to find the user parameter, we access the ini file
                    else:
                        config = self.ini_handler.read_ini(posixpath.join(self.source_path, folder, file))
                        if config['BASIC']['use_custom_ramp_param']:
                            user_param = config['BASIC']['custom_ramp_param']
                            user_unit = config['BASIC']['custom_ramp_unit']
                            return user_param, user_unit
                        
            if not valid:
                if not find_user_param:
                    print(f'   Subdirectory \'{folder}\' removed due to missing summary.ini file')
        
        if not find_user_param:     
            print(f'   Found {len(valid_subfolders)} valid run analysis')
            for folder in valid_subfolders:
                print(f'   -->{folder}')
        else:
            return None, None
            
        return valid_subfolders
        
    def get_structure(self):
        '''
        Checks the detector settings and the corresponding ramp_parameter settings. In case of multiple equal 
        ramp-values for one detector, only the file with the most peaks will be considered for evaluation.
        
        :returns: A dict containing the data structure of used detectors, run folders and bias voltages
        '''
        
        # First, we extract all data contained in the summary.ini files
        print('---Extract detectors and information...')
        self.raw_data = {}
        for run in self.valid_runs:
            ini_dir = posixpath.join(self.source_path, run[0], run[1])
            self.raw_data[run[0]] = self.ini_handler.read_ini(ini_dir, convert = True, all_defined = False, display = False)
            
        # Now, we check which detectors are included, we allow multiple and also different constellations
        # Also, we already create the data structure which will later be used to store average values, fit 
        # values and for plotting
        info = {}
        for run, data in self.raw_data.items():
            try:
                detectors = data['BASIC']['detectors'].copy()
            except:
                self.warn.warn(f'\'{run}\' does not contain detectors in summary.ini')
            try:
                # Option for user parameter
                if self.ramp_param not in self.ini_handler.ramp_parameters:
                    if data['BASIC']['use_custom_ramp_param']:
                        param = data['BASIC']['custom_ramp_val']
                else:
                    param = data['BASIC'][self.ramp_param]
            except:
                self.warn.warn(f'\'{run}\' does not contain the ramp parameter \'{self.ramp_param}\' in summary.ini')
            
            # Build a structure dictionary with detectors, runs and ramp parameters for sorting extracting
            for i, det in enumerate(detectors):
                # Store and overwrite if we have compensated_offset
                if 'compensated_offset_[V]' in list(self.raw_data[run][det].keys()):
                    self.got_offset = True
                else:
                    self.got_offset = False
                # Add detector if not already included
                if not det in info.keys():
                    info[det] = []
                # Allow lists as in bias voltage and single numbers as e.g. energy
                if type(param) is list:
                    to_append = param[i]
                else:
                    to_append = param
                # We check, if there even were found peaks 
                try:
                    peak_no = int(data[det]['peaks'])
                except:
                    self.warn.warn(f'No data on peak number in run \'{run}\' for detector \'{det}\'')
                if peak_no == 0:
                    self.warn.warn(f'Run \'{run}\' not added for {self.ramp_param}-analysis of detector \'{det}\' due to zero peaks')
                else:
                    # We check if we already have the same value to analyze, and exclude the one where we 
                    # have less peaks
                    if not to_append in [val[1] for val in info[det]]:
                        info[det].append([run, to_append])
                    else:
                        peaks_new = int(data[det]['peaks'])
                        for i, values in enumerate(info[det]):
                            if values[1] == to_append:
                                self.warn.warn(f'Same {self.ramp_param} value ({to_append} {self.ramp_unit}) for run \'{values[0]}\' and \'{run}\'')
                                peaks_old = int(self.raw_data[values[0]][det]['peaks'])
                                # If the former has more peaks, we need no change at all, otherwise, append new and delete old
                                if peaks_new > peaks_old:
                                    info[det].append([run, to_append])
                                    del(info[det][i])
                                    self.warn.warn(f'Removed run \'{values[0]}\' for {self.ramp_param}-analysis of detector \'{det}\' due to less peaks')
                                else:
                                    self.warn.warn(f'Run \'{run}\' not added for {self.ramp_param}-analysis of detector \'{det}\' due to less peaks')
                                    
                                break

        # Delete a detector if no valid data is present
        remove = []
        for det in info.keys():
            if info[det] == []:
                self.warn.warn(f'{det} removed from analysis because it has not a single file with viable data')
                remove.append(det)
        for det in remove:
            del(info[det])
        
        self.detectors = list(info.keys())
        print(f'   Found {len(info)} detector(s): {list(info.keys())}')

        # Create a data structure for all later purposes which is sorted according to the ramp parameter
        print(f'---Sorting according to {self.ramp_param}...')
        structure = dict.fromkeys(info.keys())
        for det in info.keys():
            structure[det] = {'runs':[], self.ramp_param:[]}
            runs = []
            param = []
            for run in info[det]:
                runs.append(run[0])
                param.append(run[1])
                
            sort_index = np.argsort(param)
            runs = [runs[index] for index in sort_index]
            param = [param[index] for index in sort_index]
            structure[det]['runs'] = runs
            structure[det][self.ramp_param] = param
            
        for det in structure.items():
            min = np.min(det[1][self.ramp_param])
            max = np.max(det[1][self.ramp_param])
            measurements = len(det[1]['runs'])
            print(f'   {det[0]} --> {measurements} measurements from {min} {self.ramp_unit} to {max} {self.ramp_unit}')    

        return structure
        
    def extract_data(self):
        '''
        Expands the data structure with the preexisting values from the .ini files
        
        :returns: A dict founded on self.structure, modified with all extracted parameter values
        '''
        
        print('---Extracting existing data from files...')
        data = self.structure.copy()

        # Expand data structure for existing evaluation method and extract from raw data of .ini files
        for det in data.keys():
            for run in data[det]['runs']:
                for param in self.parameter_names:
                    if not param in data[det].keys():
                        data[det][param] = {}
                    for var in self.evaluation_variables['average']:
                        if not var in data[det][param].keys():
                            data[det][param][var] = []
                        try:
                            data[det][param][var].append(float(self.raw_data[run][det][(var + '_' + param)]))
                        except:
                            self.warn.warn(f'Encountered problems when extracting \'{param}\' values for detector \'{det}\' in run \'{run}\'.')
                                
        print('   Done')

        return data
    
    def check_completeness(self):
        '''
        Checks, if all histogram data files are named correctly and are present. Prints out missing or 
        corrupted files and throws an error if not. 
        
        :returns: Boolean if everything is complete
        '''
        
        print('---Checking for complete histogram data...')
        complete = True
                
        structure = self.structure
        for det in structure.keys():
            for run in structure[det]['runs']:
                folder_path = posixpath.join(self.source_path, run)
                files = os.listdir(folder_path)
                files = [file.lower() for file in files]  
                for param in self.parameter_names:
                    file_name = str(det + '_' + param + '_histogram_data.csv')
                    # Check if file is present
                    if not file_name.lower() in files:
                        complete = False
                        self.warn.warn('No histogram data of \'{param}\' for detector \'{det}\' in \'{run}\'')
        
        if not complete:
            self.warn.warn('One or multiple histogram_data.csv files are missing or inconsequently named, see warnings')
        else: 
            print('   Done')
        
        return complete

    def create_directories(self):
        '''
        Voltage-ramp folder and subfolders for the detectors and fitting methods
        
        :returns: None
        '''       
        
        print('---Creating summary folder and subdirectories...') 
        if not self.ONLY_ONE_PEAK:
            summary_name = f'{self.ramp_param}_ramp_analysis'
        else:
            summary_name = f'{self.ramp_param}_ramp_analysis_one_peak'

        self.summary_path = posixpath.join(self.source_path, summary_name)
        # First, check if a ramp folder already exists. If so, it is deleted
        if os.path.isdir(self.summary_path):
            print('   Found old ramp analysis')
            shutil.rmtree(self.summary_path)
            print('   Old data deleted')

        # Create summary folder and subfolders
        os.mkdir(self.summary_path)
        # Store the plot folder paths for later
        self.plot_folders = {}
        for det in self.structure.keys():
            det_path = posixpath.join(self.summary_path, (det + '_plots'))
            os.mkdir(det_path)
            self.plot_folders[det] = det_path
            for method in self.used_method:
                fit_path = posixpath.join(det_path, (method + '_fit'))
                os.mkdir(fit_path)
                for param in self.parameter_names:
                    param_path = posixpath.join(fit_path, param)
                    os.mkdir(param_path)
            if self.fit_method == 'All':
                comp_path = posixpath.join(det_path, 'fit_comparison')
                os.mkdir(comp_path)
                for param in self.parameter_names:
                        param_path = posixpath.join(comp_path, param)
                        os.mkdir(param_path)
                        
        # Also make a summary folder for the case of multiple detectors
        if len(list(self.structure.keys())) > 1:
            self.comp_path = posixpath.join(self.summary_path, 'detector_comparison')
            os.mkdir(self.comp_path)
        
        print('   Folders created')

        return None
    
    def apply_fitting(self):
        '''
        Accesses all valid run folders for each detector, applies a gaussian fit for all paramters, updates the
        data structure with the resulting values, and calls the plotting function.
        Dependent on the argument of which fitting should be done, applies it accordingly and also not just
        creates single plots of the fits, but also plots wits all data and fits for better comparison.
        
        :returns: None
        '''
        
        # Only use this function if intended
        if not self.fit_method == 'None':
            print('---Apply fitting, store parameters and plot results')
            data = self.data.copy()
            for det in data.keys():
                for i, run in enumerate(data[det]['runs']):
                    current_ramp = data[det][self.ramp_param][i]
                    print(f'   {det}: {run}, {current_ramp} {self.ramp_unit}')
                    for param in self.parameter_names:
                        print(f'   --> {param}')
                        # Create variables in data structure
                        for method in self.used_method:
                            for var in self.evaluation_variables[method]:
                                if not var in data[det][param].keys():
                                    data[det][param][var] = []
                        hist_data = self.get_hist_data(det, run, param)
                        
                        # If one wants to remove outliers from the histogram data, set the class-variable APPLY_OUTLIER_REMOVAL to True and use the variable OUTLIER_REMOVAL_FACTOR to control the confidence interval
                        if self.APPLY_OUTLIER_REMOVAL:
                            x_raw, y_raw = self.remove_hist_outlier(hist_data[hist_data.columns[0]], hist_data[hist_data.columns[1]])

                        # If one only wants to keep one peak
                        if self.ONLY_ONE_PEAK:
                            if self.APPLY_OUTLIER_REMOVAL:
                                x_raw, y_raw = self.keep_big_peak(x_raw, y_raw, det = det)
                            else:
                                x_raw, y_raw = self.keep_big_peak(hist_data[hist_data.columns[0]], hist_data[hist_data.columns[1]], det = det)
                        
                        # Otherwhise just take the data 
                        if not self.APPLY_OUTLIER_REMOVAL and not self.ONLY_ONE_PEAK:
                            x_raw = np.array(hist_data[hist_data.columns[0]])
                            y_raw = np.array(hist_data[hist_data.columns[1]])
                        
                        del(hist_data)
                        
                        fit_params = dict.fromkeys(self.used_method)
                        scale = dict.fromkeys(self.used_method)
                        
                        # Now apply the fit according to initial program arguments
                        for method in self.used_method:
                            fit_params[method], scale[method] = self.perform_fit(method, x_raw, y_raw)
                            print(f'       {method} done...')
                            '''
                            # Gauss
                            if method == 'Gauss':
                                fit_params[method], fit_errors = self.gauss_fit(x_raw, y_raw)
                                print('       Gauss done...')
                                                
                            # Langau
                            if method == 'Langau':
                                fit_params[method], fit_errors, scale = self.langau_fit(x_raw, y_raw)
                                print('       Langau done...')
                            '''
                            # Store fitting parameters within data structure
                            for v, var in enumerate(self.evaluation_variables[method]):
                                data[det][param][var].append(fit_params[method][v])
                                   
                        # Plot the fit together with the raw_data
                        if not len(x_raw) == 0:
                            x_fit = np.linspace(x_raw[0], x_raw[-1], num = 1000)
                            self.plot_fits(det, data[det][self.ramp_param][i], param, x_raw, y_raw, fit_params, x_fit, scale)     
                    
                print('   Fitting parameters obtained')
                print('   Plots saved\n')  
                
                self.data = data
                                                          
        return None

    def get_hist_data(self, detector, run, parameter):
        '''
        Returns a dataframe of the histogram data for a given detector, run and desired parameter
        
        :param detector: detector 
        :param run: name of the run-folder
        :parameter: name of the desired parameter according to the self.parameter_names --> case insensitive
        :returns: Dataframe of the data from the corresponding .csv file
        '''
        
        files = os.listdir(posixpath.join(self.source_path, run))
        file_name = str(detector + '_' + parameter  + '_histogram_data.csv')
        
        for file in files:
            if file_name.lower() == file.lower():
                file_path = posixpath.join(self.source_path, run, file)
        
        hist_data = pd.read_csv(file_path, sep = ';')

        return hist_data
        
    def remove_hist_outlier(self, x_data, y_data):
        '''
        Removes histogram outliers which surpass 2.5 the std
        
        :param x_data: binning data
        :param y_data: corresponding values
        
        :returns: Two lists of x and y data with removed outliers
        '''
        
        # Calculate avg and std
        avg = np.sum(x_data * y_data) / np.sum(y_data)
        std = np.sqrt(sum(y_data * (x_data - avg) ** 2) / sum(y_data))

        # Filter outliers
        mask = (x_data > (avg - self.OUTLIER_REMOVAL_FACTOR * std)) * (x_data < (avg + self.OUTLIER_REMOVAL_FACTOR * std))
        adapted_x = x_data[mask]
        adapted_y = y_data[mask]
        removed = len(mask) - np.sum(mask)
        if removed > 0:
            print(f'       {removed} outliers removed (total length: {len(mask)})')
        
        return np.array(adapted_x), np.array(adapted_y)

    def keep_big_peak(self, x_data, y_data, thres = 0.1, det = None):
        '''
        Removes a second peak if desired. The bigger one will remain
        
        :param x_data: binning data
        :param y_data: corresponding values
        
        :returns: Two lists of x and y data with removed outliers
        '''

        if len(x_data) > 0:

            # Preceeding cutting if we have another peak that's higher before
            if not self.CUT_AT_VAL is None:
                for i, val in enumerate(x_data):
                    if val < self.CUT_AT_VAL:
                        y_data[i] = 0
                    else:
                        break

            # We find the maximum
            x_max = np.argmax(y_data)
            if (x_max - 10) < 0:
                left = 0
            else:
                left = x_max - 10
            if (x_max + 11) > len(x_data):
                right = len(x_data)
            else:
                right = x_max + 12

            # Go left and right some samples and calculate an avg maximum
            y_max = np.mean(y_data[left:right])

            # Find a starting and stopping point where half the maximum is surpassed
            start = -1
            stop = -1
            thr = y_max * thres
            for i in range(x_max, -1, -1):
                start += 1
                if y_data[i] < thr and y_data[i + 1]:
                    break
            
            for i in range(x_max, len(y_data), 1):
                stop += 1
                if y_data[i] <thr and y_data[i - 1]:
                    break

            # Cut out the region two times the distance from stop to x_max (same for start)

            x_data_new = x_data[(x_max - start):(x_max + stop + 1)]
            y_data_new = y_data[(x_max - start):(x_max + stop + 1)]

            removed = len(x_data) - len(x_data_new)
            print(f'       {removed}/{len(y_data)} points removed to keep a single peak')

            return np.array(x_data_new), np.array(y_data_new)
        
        else:
            return np.array(x_data), np.array(y_data)

    def perform_fit(self, fit, x_data, y_data):
        '''
        Performs the the fit determined by the key 'fit'. 
        
        :param fit: Either Gauss, Langau or Landau
        :param x_data: np.array of x data
        :param y_data: np.array of y data
        
        :returns: A list for the obtained fit parameters for the given fit function and the scaling factor used for 
        '''
        
        # Get the number of fit parameters to obtain
        num_params = len(self.evaluation_variables[fit])
        
        # First, we intitialize a null-result in case we have stupid data with none or only one entry
        popt = [0] * num_params
        if len(y_data) < 2:
            return popt, 1.0
        
        # In case of Pylandau, we unfortunately need to rescale due to the package constraints
        if fit in list(self.to_scale.keys()):
            scale = x_data[1] - x_data[0]
            start = int(x_data[0] / scale)
            x = np.arange(start, (start + len(x_data)), 1.0)
            y = np.array(y_data)
        else:
            scale = 1
            x = x_data.copy()
            y = y_data.copy()
            
        # To obtain a fit we can apply different factors to change the region for sigma/eta estimation
        factors = [1.5, 1.75, 2, 2.25, 2.5, 2.75, 3]
        for i, f in enumerate(factors):
            if i > 0:
                self.warn.warn(f'Trying next estimation factor for sigma/eta estimation --> f = {f}')
            worked = True
            init_guess = get_fit_params_estimations(fit, x, y, f)

            # Perform fast fitting using Levenberg-Marquardt
            try:
                if fit == 'Gauss':
                    popt, pcov = curve_fit(gauss_function, x, y, p0 = init_guess)
                elif fit == 'Langau':
                    popt, pcov = curve_fit(pylandau.langau, x, y, p0 = init_guess)
                elif fit == 'Landau':
                    popt, pcov = curve_fit(pylandau.landau, x, y, p0 = init_guess)
                    
                # Check the errors, we have a baseline of self.FIT_ERROR_FACTOR times the value itself
                for e, err in enumerate(np.sqrt(np.diag(pcov))):
                    if np.absolute(err) >= (self.FIT_ERROR_FACTOR * np.absolute(popt[e])):
                        self.warn.warn(f'Great error; {self.evaluation_variables[fit][e]}: {popt[e]}; Error: {err}')
                        raise(RuntimeError())
                
            # If fast fitting fails, we skip up our game with a more robust algorithm
            except:
                self.warn.warn('Levenberg-Marquardt failed --> switched to trust-region-reflective-algorithm')
                # We need boundaries for our fit parameters for this algorithm. These are quite generous. If the fit
                # still doesn't work, one can surely constrain them harder to maybe reach convergence
                try:
                    boundaries = ([0.1 * guess for guess in init_guess], [self.FIT_ERROR_FACTOR * guess for guess in init_guess])
        
                    if fit == 'Gauss':
                        popt, pcov = curve_fit(gauss_function, x, y, p0 = init_guess, method = 'trf', bounds = boundaries)
                    elif fit == 'Langau':
                        popt, pcov = curve_fit(pylandau.langau, x, y, p0 = init_guess, method = 'trf', bounds = boundaries)
                    elif fit == 'Landau':
                        popt, pcov = curve_fit(pylandau.landau, x, y, p0 = init_guess, method = 'trf', bounds = boundaries)

                    # Again, check the errors
                    for e, err in enumerate(np.sqrt(np.diag(pcov))):
                        if err >= (self.FIT_ERROR_FACTOR * np.absolute(popt[e])):
                            self.warn.warn(f'Great error; {self.evaluation_variables[fit][e]}: {popt[e]}; Error: {err} (unscaled)')
                    
                # If it still fails, we either try the next factor, or have to return a null result
                except:
                    self.warn.warn('No relevant fitting parameters obtained')
                    worked = False
                    popt = np.array([0] * num_params)
            
            if worked:
                break

        # Now we rescale the corresponding fitting parameters in x for those necessary
        if fit in list(self.to_scale.keys()):
            for i, param in enumerate(self.to_scale[fit]):
                if param:
                    popt[i] *= scale

        return popt, scale

    def plot_fits(self, detector, ramp_step, parameter, x_raw, y_raw, fit_params, x_fit, scale):
        '''
        Creates a plot of the calculated fit together with the raw data and stores it in the corresponding folder
        
        :param detector: Detector
        :param bias_voltage: Bias voltage connected to the run
        :param parameter: Parameter for which the fit was
        :param x_raw: raw x data
        :param y_raw: raw y data
        :param fit_params: resulting fit parameters to use the corresponding function
        :param scale: The used scaling for the x data in the fit
        
        :returns: None
        '''  
        
        # Figure for combined fit plots
        all = False
        if len(fit_params) > 1:
            all = True
            fig_all = plt.figure()
            ax_all = fig_all.add_subplot()
            ax_all.grid(linewidth = 0.4, color ='black')
            ax_all.fill_between(x_raw, y_raw, color = 'green', step = 'mid', label ='raw_data')
            ax_all.set_xlim(left = 0)
            ax_all.set_xlabel(parameter)
            ax_all.set_ylabel('relative_abundance_[%]')
            ax_all.set_title(f'{detector} - {ramp_step} {self.ramp_unit}\n{parameter} with all available fits')
        
        for method in fit_params.keys():
            plot_name = str(ramp_step) + self.ramp_unit + '.png'  
            plot_path = posixpath.join(self.summary_path, detector + '_plots', method + '_fit', parameter, plot_name)
            fit_color = self.plot_colours[method]
            
            plt.close()
            # Plot the underlying histogram
            plt.grid(linewidth = 0.4, color ='black')
            plt.fill_between(x_raw, y_raw, color = 'green', step = 'mid', label = 'raw_data')
            plt.gca().set_xlim(left = 0)
            
            # We need dummy x and dummy fit params to consider for scaling
            x_fit_scale = x_fit / scale[method]
            params_scale = fit_params[method].copy()
            if method in list(self.to_scale.keys()):
                for i, param in enumerate(self.to_scale[method]):
                    if param:
                        params_scale[i] /= scale[method]
                        
            # We get the y data of the fit
            if method == 'Gauss':
                y_fit = gauss_function(x_fit_scale, *params_scale)
            if method == 'Langau':
                y_fit = pylandau.langau(x_fit_scale, *params_scale)
            if method == 'Landau':
                y_fit = pylandau.landau(x_fit_scale, *params_scale)
                
            # Plot the results
            plt.plot(x_fit, y_fit, color = fit_color, label = f'{method}_fit', linewidth = 2.0)
            if all:
                ax_all.plot(x_fit, y_fit, color = fit_color, label = f'{method}_fit', linewidth = 2.0)
                
            # Also include the parameters in the single fits
            if not all:
                unit = self.ini_handler.pulse_params_unit_converted[parameter]
                f_conv = self.ini_handler.pulse_params_conversion_factors[parameter]
                for i, f_param in enumerate(self.evaluation_variables[method]):
                    if f_param in self.plot_params[method]:
                        val = '{:.3n}'.format(fit_params[method][i] * f_conv)
                        plt.plot([], [], linestyle = '', marker = '', label = f'{f_param}: {val} {unit}')
            
            # Labeling of axes, title and filename
            plt.legend(loc = 'best', framealpha = 1.0)
            plt.xlabel(parameter)
            plt.ylabel('relative_abundance_[%]')
            plt.title(f'{detector} - {ramp_step} {self.ramp_unit}\n{parameter} with {method}-fit')
            plt.savefig(plot_path)
            plt.close()
        
        if all:
            ax_all.legend(loc = 'best', framealpha = 1.0)
            plot_name = str(ramp_step) + self.ramp_unit + '.png'  
            plot_path = posixpath.join(self.summary_path, detector + '_plots', 'fit_comparison', parameter, plot_name)
            fig_all.savefig(plot_path)
            del(fig_all)

        return None

    def create_summary_ini(self):
        '''
        Transforms all obtained data from the data structure into dictionaries that can be written into voltage_ramp
        summary.ini files. One file is created for each detector and fitting method respectively, sorted by the 
        used ramp parameter. Each contains the respective parameters from the evaluation method, as well as basic 
        information about the whole voltage ramp. 
        Attention: Basic information about the whole ramp (aside from ramp parameter) is automatically 
        considered equal across all included runs and is not checked. This needs to be done by the user!!!
        
        :returns: A dict containing the plot data for summary plots
        '''
        
        print('---Write summary.ini files for ramp analysis...')
        data = self.data.copy()
        raw_data = self.raw_data.copy()

        # Create plot_data structure
        plot_data = {}
        for det in data.keys():
            plot_data[det] = {}
            for param in self.parameter_names:
                plot_data[det][param] = {}
                for method in self.all_methods:
                    plot_data[det][param][method] = {}

        # First, we extract basic data from the first run stored in the raw_data structure (it should be the same
        # for all others). We need to take this stupid approach because otherwise some raw dat was deleted
        basic_data = {}
        exclude = ['run_no', 'channels', 'detectors', self.ramp_param]
        for key, val in raw_data[data[self.detectors[0]]['runs'][0]]['BASIC'].items():
            if not key in exclude:
                basic_data[key] = val
        
        # Now go for each detector and method
        for det in data.keys():
            # Get evaluated runs
            for method in self.all_methods:
                ini_data = {}
                ini_data['BASIC'] = basic_data
                ini_data['BASIC']['evaluated_runs'] = data[det]['runs']
                ini_data['BASIC']['evaluated_' + self.ramp_param] = data[det][self.ramp_param]
                ini_data['BASIC']['evaluation_method'] = method
                
                # Get index of value and std from definitions in Run_Config-->Ini_Handler()
                val_key = self.plot_params[method][0]
                std_key = self.plot_params[method][1]
                
                for param in self.parameter_names:
                    # Assign values to plot structure
                    plot_data[det][param][method].update({self.ramp_param:data[det][self.ramp_param]})
                    plot_data[det][param][method].update({'value':data[det][param][val_key]})
                    plot_data[det][param][method].update({'std':data[det][param][std_key]})

                    # Extract variables for .ini file   
                    param_data = {}                    
                    for i, step in enumerate(data[det][self.ramp_param]):
                        ramp_step = []
                        for var in data[det][param].items():
                            if var[0] in self.evaluation_variables[method]:
                                ini_var = var[0]
                                # Shorten the variable names by subtracting the fitting method already contained within the later file name
                                if method in self.used_method:
                                    ini_var = ini_var.replace(('_' + method), '')
                                ramp_step.append(ini_var + '=' + str(var[1][i]))
                        param_data[str(step) + ' ' + self.ramp_unit] = ramp_step
                    ini_data[param] = param_data

                # Now save the .ini file using our methods from Run_Config.py
                ini_name = det + '_' + self.ramp_param + '_ramp_' + method + '.ini'
                ini_path = posixpath.join(self.summary_path, ini_name)
                self.ini_handler.write_ini(ini_data, ini_path, all_defined = False, displayed = False)
                
                del(ini_data)

        print(f'   .ini files created and saved in {self.ramp_param}_ramp folder')

        return plot_data
        
    def create_summary_plots(self):
        '''
        Last but not least, the summary plots are created. For each detector, we plot the behaviour over bias voltage
        for each method and parameter respectively. In addition, if fitting itself is used, also combined plots
        allowing a comparison between all evaluation methods are created. 
        All of them are stored right into the summary folder. 
        
        :returns: None
        '''
        
        print('---Creating summary plots...')
        plot_data = self.plot_data.copy()

        # First we make plots for each detector
        for det in plot_data.keys():
            det_path = self.plot_folders[det]
            for param in plot_data[det].keys():
                # Now we loop two times, one for the single plot of each evaluation method, and one for the combined
                #Single plots
                for method in plot_data[det][param].keys():
                    file_name = f'{det}_{method}_{param}_ramp.png'
                    x_data = np.array(plot_data[det][param][method][self.ramp_param])
                    y_data = np.array(plot_data[det][param][method]['value'])
                    y_err = np.array(plot_data[det][param][method]['std'])
                    # Convert to readable units
                    y_data *= self.ini_handler.pulse_params_conversion_factors[param]
                    y_err *= self.ini_handler.pulse_params_conversion_factors[param]
                    col = self.plot_colours[method]
                    plt.close()
                    plt.grid(linewidth = 0.4, color ='black')
                    plt.plot(x_data, y_data, color = col, marker = 'o', markersize = 5.0)
                    plt.fill_between(x_data, (y_data - y_err), (y_data + y_err), alpha = 0.4, color = col, edgecolor = None)
                    plt.title(f'{det}\n{self.ini_handler.pulse_params_converted[param]}, {method}')
                    plt.xlabel(f'{self.ramp_param} ({self.ramp_unit})')
                    plt.ylabel(self.ini_handler.pulse_params_converted[param])
                    plt.savefig(posixpath.join(det_path, file_name))
                    plt.close()
                
                # Combined plots
                file_name = f'{det}_comparison_ramp_{param}.png'
                plt.close()
                plt.grid(linewidth = 0.4, color ='black')
                for method in plot_data[det][param].keys():
                    x_data = np.array(plot_data[det][param][method][self.ramp_param])
                    y_data = np.array(plot_data[det][param][method]['value'])
                    y_err = np.array(plot_data[det][param][method]['std'])
                    # Convert to readable units
                    y_data *= self.ini_handler.pulse_params_conversion_factors[param]
                    y_err *= self.ini_handler.pulse_params_conversion_factors[param]
                    col = self.plot_colours[method]
                    plt.plot(x_data, y_data, color = col, marker = 'o', markersize = 5.0, label = method)
                    plt.fill_between(x_data, (y_data - y_err), (y_data + y_err), alpha = 0.4, color = col, edgecolor = None)
                plt.legend(loc = 'upper left')
                plt.title(f'{det}\n{self.ini_handler.pulse_params_converted[param]}')   
                plt.xlabel(f'{self.ramp_param} ({self.ramp_unit})')
                plt.ylabel(self.ini_handler.pulse_params_converted[param])
                plt.savefig(posixpath.join(det_path, file_name))
                plt.close()
    
        # Now we also create combined plots which summarize different detectors, for each method and parameter
        if len(list(plot_data.keys())) > 1:
            print('   Also plot detector comparison...')

            for param in self.parameter_names:
                for method in self.all_methods:
                    file_name = f'detector_comparison_{method}_{param}_ramp.png'
                    plt.close()
                    plt.grid(linewidth = 0.4, color ='black')
                    for col, det in enumerate(list(plot_data.keys())):
                        x_data = np.array(plot_data[det][param][method][self.ramp_param])
                        y_data = np.array(plot_data[det][param][method]['value'])
                        y_err = np.array(plot_data[det][param][method]['std'])
                        # Convert into readable units
                        y_data *= self.ini_handler.pulse_params_conversion_factors[param]
                        y_err *= self.ini_handler.pulse_params_conversion_factors[param]
                        colour = self.multiple_colours[col]
                        plt.plot(x_data, y_data, color = colour, marker = 'o', markersize = 5.0, label = det)
                        plt.fill_between(x_data, (y_data - y_err), (y_data + y_err), alpha = 0.4, color = colour, edgecolor = None)  
                    plt.legend(loc = 'upper left')
                    plt.title(f'detector comparison\n{method}, {self.ini_handler.pulse_params_converted[param]}')
                    plt.xlabel(f'{self.ramp_param} ({self.ramp_unit})')
                    plt.ylabel(self.ini_handler.pulse_params_converted[param])
                    plt.savefig(posixpath.join(self.comp_path, file_name))
                    # Also save as pdf to obtain vector graphics for papers
                    #plt.savefig(posixpath.join(self.comp_path, (file_name[:-4] + '.pdf')))
                    plt.close()
    
        print(f'   Plots created and saved in {self.ramp_param}_ramp folder')
        
        return None
    
    def plot_basic_infos(self):
        '''
        Also adds a plot of the total peak number of each subfolder for visual checking
        
        :returns: None
        '''    
        
        print('---Creating plots for visual checking...')
        
        # If one wants to plot something, just add the parameter name and the corresponding info
        # If the section is the used detector, for which one needs to differentiate, type 'det'
        # example: [parameter_name, section_in_results_ini, type, unit]
        
        to_plot = []
        to_plot.append(['peaks', 'det', int, ''])
        to_plot.append(['rms_noise_[V]', 'det', float, 'V'])
        if self.got_offset:
            to_plot.append(['compensated_offset_[V]', 'det', float, 'V'])
         
        # We go through each detector
        for det in self.data.keys():
            plot_folder = self.plot_folders[det]
            
            x_data = self.data[det][self.ramp_param]
            run_no = [int(self.raw_data[run]['BASIC']['run_no']) for run in self.data[det]['runs']]
            
            # Now go through each desired entry to plot and create them
            for entry in to_plot:
                if entry[1] == 'det':
                    section = det
                else:
                    section = entry[1]
                y_data = [entry[2](self.raw_data[run][section][entry[0]]) for run in self.data[det]['runs']]
            
                fig = plt.figure(figsize = (10, 7))
                ax_param = fig.add_subplot(111)
                ax_run = ax_param.twiny()

                ax_param.plot(x_data, y_data, linestyle = 'dashed', color = 'green', marker = 'o', markersize = 10)
                ax_param.set_xlabel(self.ramp_param + ' ' + self.ramp_unit)

                # We add a second x-axis containing the run number
                new_tick_locations = x_data
                ax_run.set_xlim(ax_param.get_xlim())
                ax_run.set_xticks(new_tick_locations)
                ax_run.set_xticklabels(run_no)
                ax_run.set_xlabel('run #')
                
                ax_param.set_ylabel(entry[0] + ' ' + entry[3])
                ax_param.set_title(entry[0])
                
                ax_param.grid(linewidth = 0.4, color ='black')

                
                path = posixpath.join(plot_folder, ('visual_check_' + entry[0]) + '.png')
                plt.savefig(path)
                plt.close()

        print('   Done')
                
        return None
    