import sys 
import os
# Allow imports from parent directory if this test file is launched directly
if __name__ == '__main__':
    current = os.path.dirname(os.path.realpath(__file__))
    parent = os.path.dirname(current)
    sys.path.append(parent)

from RampEvaluation import RampEvaluation

path = r'C:\Users\pgagg\Documents\Arbeit_HEPHY\virtual_machine\readout_programms\RS_DRS4_run_analysis\Philipp\ramp_test_simu'
# Choose evaluation method from Gauss, Langau, Langau or All
eval_method = 'All'
# Execute the voltage ramp evaluation
#param = 'bias_voltage'
#param = 'energy'
param = 'rate'

ramp_eval = RampEvaluation(path, eval_method, param)

if ramp_eval.data_check():
    if ramp_eval.execute_analysis():
        print('\nRamp analysis sucessful')
    
else:
    print('\nRamp analysis not sucessful')