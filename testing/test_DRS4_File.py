# -*- coding: utf-8 -*-
"""
Created on Mon Nov  8 16:57:24 2021

@author: Markus Göbel
"""

import sys 
import os
# Allow imports from parent directory if this test file is launched directly
if __name__ == '__main__':
    current = os.path.dirname(os.path.realpath(__file__))
    parent = os.path.dirname(current)
    sys.path.append(parent)

import posixpath
from readout_classes.DRS4_file import DRS4_file

folder = r'C:\Users\pgagg\Documents\Arbeit_HEPHY\virtual_machine\readout_programms\RS_DRS4_run_analysis\Philipp\DRS4_data'
file = 'si_lgad_btr4_f_da_1000.dat'

path = posixpath.join(folder, file)


test = DRS4_file(path)

signal_data = test.data_raw_DRS4
