"""
Created on 03.10.2022

@author: Philipp Gaggl
"""

import sys 
import os
import numpy as np
import matplotlib.pyplot as plt

# Allow imports from parent directory if this test file is launched directly
if __name__ == '__main__':
    current = os.path.dirname(os.path.realpath(__file__))
    parent = os.path.dirname(current)
    sys.path.append(parent)

import posixpath
from readout_classes.CNM_file import CNM_file

folder = r'C:\Users\pgagg\Documents\Arbeit_HEPHY\virtual_machine\RS_DRS4_run_analysis\Philipp\TCT_readout\data'
file = 'VoltageScan_22-08-22_12-23-56.csv'

path = posixpath.join(folder, file)

test = CNM_file(path)

#for i, event in enumerate(test.data_raw_CNM[test.ch_name]):
#    #x = np.linspace(test.meta['start_times'][0], test.meta['t_sample'], len(event))
#    x = np.linspace(0, len(event) - 1, len(event))
#    plt.plot(x, event)
#    plt.show()

print('All done')