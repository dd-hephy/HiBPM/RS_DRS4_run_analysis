import sys 
import os
# Allow imports from parent directory if this test file is launched directly
if __name__ == '__main__':
    current = os.path.dirname(os.path.realpath(__file__))
    parent = os.path.dirname(current)
    sys.path.append(parent)

import time

from fileReader import Det_File

# RS file
#path = "C:/Users/pgagg/Documents/Arbeit_HEPHY/virtual_machine/readout_programms/rs_drs4_processing/Philipp/test_run/test_data.bin"

# DRS4 file
#path = r"C:\Users\pgagg\Documents\Arbeit_HEPHY\virtual_machine\readout_programms\RS_DRS4_run_analysis\Philipp\Patrick\DRS4\data\lgad6_46_da_notrig.dat"

# CNM file
path = r'C:\Users\pgagg\Documents\Arbeit_HEPHY\virtual_machine\RS_DRS4_run_analysis\Philipp\TCT_readout\data\VoltageScan_22-08-22_12-23-56.csv'




# Initalizing
t = time.time()
test = Det_File(path)
print(f'Initialized, {time.time() - t} s\n')



# Create our object to return. 
events = {}
        
        
        
# First, apply offset compensation and noise calculation if used
t = time.time()
if test.ana_params['advanced_noise_offset']:
    print('Performing advanced offset and noise calculation...')
    test.advanced_noise_offset()
else:
    if test.ana_params['compensate_offset']:
        print('Compensate for artificial offset...')
        test.compensate_offset()
    print('Noise:')
    test.getStatistics()
    
# Get threshold
test.get_threshold()
print(f'Got offset, statistics and threshold, {time.time() - t} s\n')



# Analyse each channel
t = time.time()
print('Get time over threshold...')
for ch in test.meta['source_names']:
            
    # Get time over threshold (the reduction to the preview events is done within getTot)
    channel_events = test.getTot(ch)

    if not channel_events is None:
        # Apply CFD
        if test.ana_params['apply_constant_fraction_discrimination']:
            CFD_factor = test.ana_params['cfd_factor']
            print(f'Apply constant fraction discrimination using CFD_factor: {CFD_factor}...')
            channel_events = test.constant_fraction_discrimination(channel_events, ch, CFD_factor)

        if not channel_events is None:
            # Get rms buffer
            if test.ana_params['analysis_till_rms_noise']:
                print('Find rms-analysis-buffer...')
                channel_events = test.get_rms_buffer(ch, channel_events)
            events[ch] = channel_events
        else:
            events[ch] = None
        
        if not channel_events is None:
            test.analyse_peaks(channel_events, ch)
                
    else:
        events[ch] = None

print(f'Anylysis done, {time.time() - t} s\n')
print(test.ramp_info)
print('All done')
            
    