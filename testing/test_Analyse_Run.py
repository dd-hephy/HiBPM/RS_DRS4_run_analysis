import sys 
import os
# Allow imports from parent directory if this test file is launched directly
if __name__ == '__main__':
    current = os.path.dirname(os.path.realpath(__file__))
    parent = os.path.dirname(current)
    sys.path.append(parent)

import time
from Analyse_Run import Analyse_Run
                
source = r'C:\Users\pgagg\Documents\Arbeit_HEPHY\virtual_machine\readout_programms\RS_DRS4_run_analysis\Philipp\test_rise_time\data_RS'                
target = r'C:\Users\pgagg\Documents\Arbeit_HEPHY\virtual_machine\readout_programms\RS_DRS4_run_analysis\Philipp\test_rise_time'
                
t = time.time()
# Instantiate Analyse_Run class. Checks for any run_info.ini file, extracting its data and creating 
# the analysis folder as well as summary and .csv files within. (According to given channels and run number).
run_ana = Analyse_Run(source, target)

# Starts the analysis of the run folder. Finds peaks, plots and analyzes them, and stores these data into .csv files.
analysis_output = run_ana.analyse_peaks()

# Analyses the given .csv files and writes the results into the .summary file.
run_ana.analyse_csv(analysis_output)

# Remember that this run number has been analyzed at least once.
print(f'Took {time.time() - t} s') 
