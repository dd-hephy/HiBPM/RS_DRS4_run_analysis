import os
import shutil
import posixpath

from tool_box import Warn_Me
from Run_Config import Ini_Handler

class First_Call():
    '''
    Class that will be called right at the beginning of the first GUI-launch. Checks if the user already has
    current ini files for analysis parameters and run config. If not, makes them via copying the default 
    files in the main directory. That way, user settings on each repo do not affect the main repo. 
    '''
    
    def __init__(self):
        '''
        :returns: None
        '''
        
        warner = Warn_Me()
        ini_handler = Ini_Handler()
        
        # Initializing all default paths. This happens in here and will be taken by the corresponding GUIs        
        self.this_path = os.getcwd()
        
        # Folders 
        self.ini_folder = 'ini_library'
        self.config_folder = 'run_config'
        self.config_path = posixpath.join(self.this_path, self.ini_folder, self.config_folder)
        self.parameter_folder = 'analysis_parameters'
        self.parameter_path = posixpath.join(self.this_path, self.ini_folder, self.parameter_folder)
        
        # Create the folders if not present
        try:
            os.mkdir(self.ini_folder)
        except:
            pass
        try:
            os.mkdir(self.config_path)
        except:
            pass
        try:
            os.mkdir(self.parameter_path)
        except:
            pass

        # Names of the corresponding default files to copy
        self.default_config = posixpath.join(self.this_path, self.ini_folder, 'default_run_config.ini')
        self.default_parameters = posixpath.join(self.this_path, self.ini_folder, 'default_analysis_parameters.ini')
        
        # We throw an error if the default files are not given
        if not os.path.isfile(self.default_config) or not os.path.isfile(self.default_parameters):
            raise(RuntimeError('You are missing one or both defualt .ini files for run_config/analysis_parameters in your main directory!!!'))
        
        # Names of the files that must exist and are created otherwise
        self.config_file = 'current_config.ini'
        self.current_config = posixpath.join(self.config_path, self.config_file)
        self.parameter_file = 'current_settings.ini'
        self.current_parameters = posixpath.join(self.parameter_path, self.parameter_file)
        
        # Now check for current files. If they are not present, they will be copied from the default files
        if not os.path.isfile(self.current_config):
            shutil.copy(self.default_config, self.current_config)
            warner.warn('First call: Created current_config.ini from default')
        if not os.path.isfile(self.current_parameters):
            shutil.copy(self.default_parameters, self.current_parameters)
            warner.warn('First call: Created current_settings.ini from default')
            
        # Also check, if all keys stored in the Ini_Handler() are also present in the 'current' files 
        # settings
        current_params = ini_handler.read_ini(self.current_parameters)
        current_params = ini_handler.decompose(current_params)[0]
        all_in = True
        for param in ini_handler.ana_params:
            if not param in list(current_params.keys()):
                all_in = False
                missing = param
        if not all_in:
            shutil.copy(self.default_parameters, self.current_parameters)
            warner.warn(f'Missing parameter \'{missing}\' in crrent_settings.ini. Created current_settings.ini from default')
        # configs
        current_config = ini_handler.read_ini(self.current_config)
        current_config = ini_handler.decompose(current_config)[0]
        all_in = True
        for config in ini_handler.basic_run_params:
            if not config in list(current_config.keys()):
                all_in = False
                missing = config
        for config in ini_handler.optional_run_params:
            if not config in list(current_config.keys()):
                all_in = False
                missing = config
        if not all_in:
            shutil.copy(self.default_config, self.current_config)
            warner.warn(f'Missing configuration \'{missing}\': Created current_config.ini from default')

        return None