"""
    File name: test_Allpix_file.py
    Author: Andreas Gsponer
    Date created: 2022

    Unit tests for Allpix_file readout class
"""

import unittest

# Run unit tests with: python -m unitest discover test/
# Also report coverage using: coverage run -m unittest discover test 
# This will write a coverage.xml that can be read by IDEs (coverage "gutters")
# One can also use coverage report to generate a short summary

# TODO: Think about how this imports all other readout classes
# (see __init__.py in readout_classes)
from readout_classes import Allpix_file

class Test_AllPix(unittest.TestCase):
    def test_constructor(self):
        """
        Test if we can construct an Allpix_file object
        and if the metadata is correctly set
        """

        TESTDATA_FILE = 'testing/allpix_data.csv'

        file = Allpix_file(TESTDATA_FILE)

        self.assertEqual(file.NumChannels, 1)
        self.assertEqual(file.filename, TESTDATA_FILE)

        # check meta information
        meta = file.meta
        self.assertEqual(meta['source_names'], ['CSA'])
        self.assertEqual(meta['channel_name'], ['CSA'])
        self.assertEqual(meta['conversion_factor']['CSA'], 1.0)
        self.assertEqual(meta['channel_type'], 'voltage')

    def test_getRaw(self):
        """
        Read in a test file and check the dimensions of the data
        """
        # Known dimension of data
        NUMBER_OF_EVENTS = 20
        LENGTH_OF_EVENT = 800
        # time bins in seconds
        TIME_BINS = 1e-10

        file = Allpix_file('testing/allpix_data.csv')

        data = file._getRaw(event=0)
        self.assertEqual(list(data.keys()), ['CSA'])
        data = data['CSA']
        self.assertEqual(len(data), 800)

        meta = file.meta
        self.assertAlmostEqual(meta['t_sample'], TIME_BINS)
        # Check total number of events
        self.assertEqual(meta['NumberOfAcquisitions'], NUMBER_OF_EVENTS)

        # Check that each event has the same length (LENGTH_OF_EVENT)
        for event_number in range(meta['NumberOfAcquisitions']):
            self.assertEqual(len(file._getRaw(event=event_number)['CSA']), LENGTH_OF_EVENT)

        self.assertEqual(meta['length'], NUMBER_OF_EVENTS * LENGTH_OF_EVENT)
    
if __name__ == '__main__':
    unittest.main()