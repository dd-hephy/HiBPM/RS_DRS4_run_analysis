'''
    File name: GUI_Analyse_Run.py
    Author: Philipp Gaggl
    Date created: 29.11.2021
    Python Version: 3.8, 3.9
'''


from PyQt5.QtCore import pyqtRemoveInputHook
from PyQt5.QtWidgets import QApplication, QCheckBox, QDialog, QGroupBox, QHBoxLayout, QLabel, QPushButton, QVBoxLayout, QFileDialog, QLineEdit
from PyQt5.QtGui import QFont, QIntValidator

import sys
import os
import posixpath
import time
from multiprocessing import Pool, cpu_count

from Analyse_Run import Analyse_Run
from Run_Config import Ini_Handler
from RampEvaluation import RampEvaluation
import GUIs
from tool_box import Persival, PopUp
from tool_box import Warn_Me
from tool_box import pqt5_exception_workaround
from First_Call import First_Call
from collections import Counter


class WidgetGallery(QDialog):
    '''
    GUI that lets the user choose a run folder that should be analyzed, as well as an analysis folder
    where the run analysis folder will be placed with all it's content.
    '''
    
    LOTR_BOOL = False
    def __init__(self, parent = None):
        super(WidgetGallery, self).__init__(parent)

        # If first start of GUI, default ini.files need to be loaded
        first_launch = First_Call()
        del(first_launch)

        # Warning helper
        self.warn = Warn_Me()
        # Ini handler
        self.ini_handler = Ini_Handler()
        # Popup window helper
        self.popup = PopUp()

        # Initialize needed parameters
        self.target = None
        self.run_source = None
        self.serial_source = None
        self.folder_appendix = ''
        
        self.run_no = None
        
        self.analyzed_runs = []
        self.analyzed_folders = []

        # Evaluation options for ramp evaluation
        self.ramp_options = self.ini_handler.ramp_options
        self.ramp_parameters = self.ini_handler.ramp_parameters.copy()
        # Add optional parameter
        self.ramp_parameters.append('user_parameter')
        
        # Set size and position
        self.resize(500,300)
        self.move(50, 10)
        
        # Create the button box
        self.create_button_box()
        
        # Remembers settings between sessions
        self.persival = Persival()
        pwd = os.getcwd()
        self.persival.setDefault('execute_target', pwd)
        self.persival.setDefault('execute_run_source', pwd)
        self.persival.setDefault('execute_serial_source', pwd)
        self.persival.setDefault('execute_ramp_analysis', pwd)
        
    def create_button_box(self):
        '''
        Layout, buttons, and everything else that is displayed within the GUI
        
        :returns: None
        '''
        
        # Ini creating box
        self.ini_box = QGroupBox('Create ini(s)')
        self.ini_box.setFont(QFont('Times, 8'))
        # Ini creation button launching ini GUI
        self.ini_button = QPushButton('create run ini(s)')
        self.ini_button.clicked.connect(self.execute_ini)
        # Layout
        ini_layout = QVBoxLayout()
        ini_layout.addWidget(self.ini_button)
        self.ini_box.setLayout(ini_layout)        
        
        # Parameter settings box
        self.param_box = QGroupBox('Analysis parameters and analysis/raw preview')
        self.param_box.setFont(QFont('Times', 8))
        # Parameter setting button
        self.param_button = QPushButton('set analysis parameters / preview pulse analysis && raw data')
        self.param_button.clicked.connect(self.execute_params)
        # Layout 
        param_layout = QVBoxLayout()
        param_layout.addWidget(self.param_button)
        self.param_box.setLayout(param_layout)
        
        # Mandatory box
        self.mandatory_box = QGroupBox('Result directory')
        self.mandatory_box.setFont(QFont('Times', 8))
        # Target folder button
        self.target = QPushButton('choose results folder')
        self.target.clicked.connect(self.execute_target)
        # User target folder name
        layout_folder_name = QHBoxLayout()
        self.use_result_folder_name = QCheckBox('use appendix for result folder name')
        self.result_folder_name = QLineEdit('user_name')
        self.result_folder_name.setDisabled(True)
        self.use_result_folder_name.setChecked(False)
        self.use_result_folder_name.stateChanged.connect(self.execute_result_folder_appendix)
        layout_folder_name.addWidget(self.use_result_folder_name)
        layout_folder_name.addWidget(self.result_folder_name)
        # Layout
        mandatory_layout = QVBoxLayout()
        mandatory_layout.addWidget(self.target)
        mandatory_layout.addLayout(layout_folder_name)
        self.mandatory_box.setLayout(mandatory_layout)
        
        # Run analysis box
        self.single_box = QGroupBox('Single run analysis')
        self.single_box.setFont(QFont('Times', 8))
        # Source folder button
        self.source = QPushButton('choose run folder to analyze')
        self.source.clicked.connect(self.execute_run_source)
        self.source.setDisabled(True)
        # Analyze once button
        self.once = QPushButton('analyse single run folder')
        self.once.clicked.connect(self.execute_run_analysis)
        self.once.setDisabled(True)

        # Layout
        single_box_layout = QVBoxLayout()
        single_box_live_layout = QHBoxLayout()
        single_box_layout.addWidget(self.source)
        single_box_layout.addWidget(self.once)
        single_box_layout.addLayout(single_box_live_layout)
        self.single_box.setLayout(single_box_layout)
        
        # Parent folder analysis box
        self.multiple_box = QGroupBox('Data folder analysis')
        self.multiple_box.setFont(QFont('Times', 8))
        # Data folder button
        self.data_source = QPushButton('choose parent-data folder for serial analysis')
        self.data_source.clicked.connect(self.execute_serial_source)
        self.data_source.setDisabled(True)
        # Analyze whole data folder button
        self.analyze_data = QPushButton('analyze parent-data folder')
        self.analyze_data.clicked.connect(self.execute_serial_analysis)
        self.analyze_data.setDisabled(True)

        # Multiprocessing parameters
        multiprocessing_parameters_label = QLabel('Multiprocessing parameters')
        # Enable / Disable Multiprocessing button
        self.multiprocessing_enabled_button = QCheckBox('Enable Multiprocessing?')
        self.multiprocessing_enabled_button.setChecked(True)
        self.multiprocessing_enabled_button.setDisabled(True)

        # Grey out and disable cpu core count entry if unchecked
        self.multiprocessing_enabled_button.stateChanged.connect(lambda state : self.multiprocessing_core_count.setEnabled(state))
        
        # Label for QLinEdit Widget
        self.multiprocessing_core_count_label = QLabel('Number of processes:')
        
        # QLinEdit Widget for number of processes
        # Per default, set number of processes to core count of machine
        self.multiprocessing_core_count = QLineEdit(str(cpu_count()))
        self.multiprocessing_core_count.setDisabled(True)

        # Input validator for number of processes
        # We only allow values between 1 and cpu_count()
        core_count_validator = QIntValidator()
        core_count_validator.setRange(1, cpu_count())  
        self.multiprocessing_core_count.setValidator(core_count_validator)

        # Add multiprocessing parameters to QHBox
        multiprocessing_params = QHBoxLayout()
        multiprocessing_params.addWidget(multiprocessing_parameters_label)
        multiprocessing_params.addWidget(self.multiprocessing_enabled_button)
        multiprocessing_params.addWidget(self.multiprocessing_core_count_label)
        multiprocessing_params.addWidget(self.multiprocessing_core_count)

        # Layout
        multiple_box_layout = QVBoxLayout()
        multiple_box_layout.addWidget(self.data_source)
        multiple_box_layout.addWidget(self.analyze_data)
        multiple_box_layout.addLayout(multiprocessing_params)
        self.multiple_box.setLayout(multiple_box_layout)
        
        # Hist review box
        self.hist_box = QGroupBox('post-analysis histogram review')
        self.hist_box.setFont(QFont('Times, 8'))
        # Button launching HIST GUI
        self.hist_button = QPushButton('review result histograms')
        self.hist_button.clicked.connect(self.execute_hist)
        # Layout
        hist_layout = QVBoxLayout()
        hist_layout.addWidget(self.hist_button)
        self.hist_box.setLayout(hist_layout)        
        
        # Ramp analysis box
        self.ramp_box = QGroupBox('Post-analysis ramp evaluation')
        self.ramp_box.setFont(QFont('Times', 8))
        # Ramp parameters checkbox
        ramp_param_label = QLabel('Ramp parameter')
        self.ramp_param_buttons = {}
        for param in self.ramp_parameters:
            self.ramp_param_buttons[param] = QCheckBox(param)
            if param == 'bias_voltage':
                self.ramp_param_buttons[param].setChecked(True)
            self.ramp_param_buttons[param].stateChanged.connect((lambda parameter = param: lambda: self.execute_ramp_param_check(parameter))())
        # Evaluation method checkboxes
        ramp_eval_label = QLabel('Evaluation method')
        self.ramp_fit_buttons = {}
        for opt in self.ramp_options:
            self.ramp_fit_buttons[opt] = QCheckBox(opt)
            if opt == 'All':
                self.ramp_fit_buttons[opt].setChecked(True)
            self.ramp_fit_buttons[opt].stateChanged.connect((lambda method = opt: lambda: self.execute_ramp_fit_check(method))())

        # Ramp folder button 
        self.ramp_analysis = QPushButton('choose result folder and start ramp analysis')
        self.ramp_analysis.clicked.connect(self.execute_ramp_analysis)
        #Layout
        ramp_layout = QVBoxLayout()
        ramp_param_layout = QHBoxLayout()
        ramp_param_layout.addWidget(ramp_param_label)
        for param in self.ramp_param_buttons.keys():
            ramp_param_layout.addWidget(self.ramp_param_buttons[param])
        ramp_layout.addLayout(ramp_param_layout)
        
        ramp_eval_layout = QHBoxLayout()
        ramp_eval_layout.addWidget(ramp_eval_label)
        for fit in self.ramp_fit_buttons.keys():
            ramp_eval_layout.addWidget(self.ramp_fit_buttons[fit])
        ramp_layout.addLayout(ramp_eval_layout)
        
        ramp_layout.addWidget(self.ramp_analysis)
        self.ramp_box.setLayout(ramp_layout)
        
        # Exit box
        self.exit_box = QGroupBox('')
        self.exit_box.setFont(QFont('Times', 8))
        # Exit button
        self.exit = QPushButton('exit')
        self.exit.clicked.connect(self.execute_exit)
        # Layout
        exit_layout = QVBoxLayout()
        exit_layout.addWidget(self.exit)
        self.exit_box.setLayout(exit_layout)
        
        # GUI layout
        main_layout = QVBoxLayout()
        main_layout.addWidget(self.ini_box)
        main_layout.addWidget(self.param_box)
        main_layout.addWidget(self.mandatory_box)     
        main_layout.addWidget(self.single_box)
        main_layout.addWidget(self.multiple_box)
        main_layout.addWidget(self.hist_box)
        main_layout.addWidget(self.ramp_box)
        main_layout.addWidget(self.exit_box)        
        self.setLayout(main_layout)   
        
        return None   
 
    def execute_ramp_param_check(self, parameter):
        '''
        Assures that only one button for ramp parameter is checked.
        
        :param parameter: String of the parameter as in self.ramp_parameters
        :returns: None
        '''

        # We need to disconnect in between to not cause an avalanche
        for box in self.ramp_param_buttons.values():
            box.disconnect()
        # Assure right checking
        for param, box in self.ramp_param_buttons.items():
            box.setChecked(param is parameter)
        # Connect again
        for param, box in self.ramp_param_buttons.items():
            box.stateChanged.connect((lambda parameter = param: lambda: self.execute_ramp_param_check(parameter))())
            
        return None
    
    def execute_ramp_fit_check(self, fit):
        '''
        Assures that only one button for fitting method is checked.
        
        :param fit: String of the fit as in self.ramp_options
        :returns: None
        '''

        # We need to disconnect in between to not cause an avalanche
        for box in self.ramp_fit_buttons.values():
            box.disconnect()
        # Assure right checking
        for method, box in self.ramp_fit_buttons.items():
            box.setChecked(method is fit)
        # Connect again
        for method, box in self.ramp_fit_buttons.items():
            box.stateChanged.connect((lambda fit = method: lambda: self.execute_ramp_fit_check(fit))())
            
        return None
      
    def execute_ini(self):
        '''
        Launches the ini creation GUI.
        
        returns: None
        '''     
        
        print('\n-------------------------------------------------------')
        print('Creating ini file(s)...')
        
        # Open the ini GUI
        self.ini_gui = GUIs.GUI_Make_Run_Ini()
        print('-------------------------------------------------------')   
        
        return None
        
    def execute_params(self):
        '''
        Opens a new GUI-window where the user can choose the analysis parameters using interactive plotting.
        Initializes a GUI_Parameter_Settings class to do so. 
        
        :returns: None
        '''
        
        print('\n-------------------------------------------------------')
        print('Setting analysis parameters...')
        
        # Open the settings GUI
        self.param_gui = GUIs.GUI_Parameter_Setting()
        print('-------------------------------------------------------')

        return None
       
    def execute_target(self):
        '''
        Lets the user choose an analysis folder. This usually is only chosen once and not changed afterwards,
        as the analysis programm creates individual run analysis folders within.
        
        :returns: None
        '''
        
        print('\n-------------------------------------------------------')
        print('Choosing general results folder...')
        
        # Let the user choose a folder
        defaultDir = self.persival.get('execute_target')
        defaultDir = posixpath.join(defaultDir, '../')
        folder = str(QFileDialog.getExistingDirectory(self, "Select Directory", defaultDir))
        
        # In case the choosing is aborted
        if os.path.isdir(folder): 
            print('Got result folder')
            self.target = folder
            # Enable other buttons
            self.source.setDisabled(False)
            self.data_source.setDisabled(False)
            self.persival.set('execute_target', folder)

        else:
            print('No valid folder chosen')
            
        print('-------------------------------------------------------')
        
        return None
   
    def execute_result_folder_appendix(self):
        '''
        If checked, lets the user type in an appendix for the result folder
        '''
        
        if self.use_result_folder_name.isChecked():
            self.result_folder_name.setDisabled(False)
        else:
            self.result_folder_name.setDisabled(True)
            
        return None
   
    def execute_run_source(self):
        '''
        Lets the user choose the run folder which should be analyzed. Searches for an .ini file within.
        
        :returns: None
        '''
        
        print('\n-------------------------------------------------------')
        print('Choosing run folder to analyze...')
        
        if self.target is None:
            print('Please choose a result folder first')
        else: 
            # First, let the user choose a folder and search for an ini file within
            defaultDir = self.persival.get('execute_run_source')
            defaultDir = posixpath.join(defaultDir, '../')
            folder = str(QFileDialog.getExistingDirectory(self, "Select Directory", defaultDir))
            
            # In case the choosing is aborted
            if not os.path.isdir(folder):
                print('No valid folder chosen')
        
            # Otherwise, we check the ini 
            else:    
                ini_content = self.check_for_ini_file(folder)
                if ini_content is None:
                        print('Error while checking for ini file, check the warnings')
                else:
                    # Prints missing optional information
                    self.run_source = folder
                    self.run_no = ini_content['BASIC']['run_no']
                    # Enable run analysis button
                    self.once.setDisabled(False)
                    self.persival.set('execute_run_source', folder)

                    print(f'Got run folder, the given run number is {self.run_no}')
        print('-------------------------------------------------------')
        
        return None  

    def execute_serial_source(self):
        '''
        Lets the user choose the parent data folder which should be serially  analyzed. Searches for an .ini 
        file within the first found subfolder to access the oscilloscope to use for analysis.
        
        :returns: None
        '''
        
        print('\n-------------------------------------------------------')
        print('Choosing data folder with multiple runs to to analyze sequentially...')
        
        if self.target is None:
            print('Please choose a result folder first')
        else:
            # First, let the user choose a folder and search for an ini file within
            defaultDir = self.persival.get('execute_serial_source')
            defaultDir = posixpath.join(defaultDir, '../')
            folder = str(QFileDialog.getExistingDirectory(self, "Select Directory", defaultDir))
            
            # In case the choosing is aborted
            if not os.path.isdir(folder):
                print('No valid folder chosen')
            else:
                # Check for ini consistency
                if not self.check_for_ini_consistency(folder):
                    print('No ini file consistency, see warnings')
                
                else:
                    self.serial_source = folder   
                    # Enable serial analysis button
                    self.analyze_data.setDisabled(False)
                    self.multiprocessing_enabled_button.setDisabled(False)
                    self.multiprocessing_core_count.setDisabled(False)
                    self.persival.set('execute_serial_source', folder)
                    print('Got data folder for sequential analysis')
        print('-------------------------------------------------------')        

        return None
    
    def check_for_ini_file(self, folder_dir, convert = True):
        '''
        Searches the folder for a single ini file, checks its content for validity and returns the 
        converted content. 
        
        :param folder_dir: Directory of the run folder (or any folder)
        :param convert: If True, ini content is converted
        :returns: Converted content of the ini file
        '''
        
        # Get all 
        ini = []
        
        for file in os.listdir(folder_dir):
            base, extension = os.path.splitext(file)
            if extension == '.ini':
                ini.append(file)
                    
        # There has to be exactly one .ini file in the folder!!!
        if len(ini) == 0:
            self.warn.warn('No ini file found in run folder')
            return None
        elif len(ini) > 1:
            self.warn.warn('Multiple ini files in run folder. Only one is allowed')
            return None
            
        # Load the valid ini data via the Ini_Handler class and check once again for its validity
        elif len(ini) == 1:                    
            ini_dir = posixpath.join(folder_dir, ini[0])
            ini_content = self.ini_handler.read_ini(ini_dir, convert = convert, all_defined = True, display = False)
                    
        return ini_content
    
    def check_for_ini_consistency(self, folder_dir):
        '''
        Checks each subfolder for valid ini structure. Then checks, whether each run number is only
        present once. 
        
        :param folder_dir: Directory of the parent folder housing the run folders
        :returns: Boolean if everything ini structure is consistent 
        '''
        
        consistent_ini = True
        run_numbers = {}
        
        # Get all folders
        run_folders = os.listdir(folder_dir)
        
        # Forst check, if all directories are even directories and not files
        for folder in run_folders:
            dir = posixpath.join(folder_dir, folder)
            if not os.path.isdir(dir):
                self.warn.warn('One or more directories in data folder are not run folders, but single files')
                return False

        # Check for valid ini in each file and extract the run number information
        for folder in run_folders:
            dir = posixpath.join(folder_dir, folder)
            ini_content = self.check_for_ini_file(dir, convert = True)
            
            if ini_content is None:
                self.warn.warn(f'Bad folder: {folder}')
                consistent_ini = False
            else:
                run_numbers[folder] = ini_content['BASIC']['run_no']
                                
        # Check, if every run number is only present once
        count = Counter(run_numbers.values())
        
        for run, freq in count.items():
            if freq > 1:
                consistent_ini = False
                # Print out warning
                failed = [folder for folder, run_no in run_numbers.items() if run_no == run]
                self.warn.warn(f'The folders {failed} in the data folder are assigned the same run number ({run}).')
                
        return consistent_ini
    
    def execute_run_analysis(self, from_serial = False):
        '''
        Executes a run folder analysis exactly once
        
        :param from_serial: Bool if function is called during a serial analysis, to leave out some printing
        :returns: None
        '''
        
        if not from_serial:
            print('\n-------------------------------------------------------')
            print('Run analysis...')

        # Check if we already have source, target and ini information
        if self.target is None:
            print('Please choose a result folder for the results first')
        elif self.run_source is None:
            print('Please choose a run folder to analyze first')
        else:
            # Check if we already had an analysis using the same run number. That way incorrect run numbers
            # in ini files can be detected easier.
            execute = True
            
            if not from_serial:
                if self.run_no in self.analyzed_runs:             
                    if not self.popup.window(f'Run already executed', f'Run-{self.run_no} has already been analyzed. Reevaluation will lead to overwriting. Do you want to analyse the run again?'):
                        print('No analysis initialized')
                        execute = False        
                # We need to overwrite the variable to be able to call it again
                self.popup = PopUp()
                    
            if execute:
                t = time.time()
                
                # Get the folder appendix
                if self.use_result_folder_name.isChecked():
                    self.folder_appendix = str(self.result_folder_name.text())
                else:
                    self.folder_appendix = None
                
                # Instantiate Analyse_Run class. Checks for any run_info.ini file, extracting its data and creating 
                # the analysis folder as well as summary and .csv files within. (According to given channels and run number).
                run_ana = Analyse_Run(self.run_source, self.target, folder_appendix = self.folder_appendix)

                # Starts the analysis of the run folder. Finds peaks, plots and analyzes them, and stores these data into 
                # .csv files.
                analysis_output = run_ana.analyse_peaks()

                # Analyses the given .csv files and writes the results into the .summary file.
                run_ana.analyse_csv(analysis_output)

                # Remember that this run number has been analyzed at least once.
                print(f'Took {time.time() - t} s')  
                if not from_serial:
                    if not self.run_no in self.analyzed_runs:      
                        self.analyzed_runs.append(self.run_no)
                del(run_ana)
        
        if from_serial:
            print('-------------------------------------------------------')

        return None

    def execute_serial_analysis(self):
        '''
        Executes a serial analysis of a parent data directory
        
        :returns: None
        '''
        
        print('\n-------------------------------------------------------')
        print('Serial data analysis...')
        
        # Check if we already have source, target and ini information
        if self.target is None:
            print('Please choose a result folder for the results first')
        elif self.serial_source is None:
            print('Please choose a parent data folder to analyze first')
        else:
            # Check if we already had an analysis using the same data folder.
            execute = True
            if self.serial_source in self.analyzed_folders:
                if not self.popup.window('Data folder already analyzed', f'The chosen data folder has already been analyzed. Reevaluation will lead to overwriting. Do you want to analyse the run again?'):
                    print('No analysis initialized')
                    execute = False       
                # We need to overwrite the variable to be able to call it again
                self.popup = PopUp()

            if execute:
                # Go through each run folder and analyse it
                runs = os.listdir(self.serial_source)
                print(f'{len(runs)} runs to analyze')
                t_serial = time.time()

                if not self.multiprocessing_enabled_button.isChecked():
                    for i, run in enumerate(runs):
                        print(f'Run {i + 1}/{len(runs)}...')
                        self.run_source = posixpath.join(self.serial_source, run)
                        self.execute_run_analysis(from_serial = True)
                else:
                    target = self.target
                    run_sources = [posixpath.join(self.serial_source, run) for run in runs]
                    
                    args = [(run_source, target) for run_source in run_sources]
                    num_cores = int(self.multiprocessing_core_count.text())
                    with Pool(processes = num_cores) as pool:
                        pool.starmap(execute_analysis_parallel, args)

                    # Remember that we analyzed this data folder
                    self.analyzed_folders.append(self.serial_source)
                
                print(f'\nSerial analysis finished, took {time.time() - t_serial} s')

        print('-------------------------------------------------------')            

        return None

    def execute_hist(self):
        '''
        Starts the hist-review GUI
        
        :returns: None
        '''
        
        print('\n-------------------------------------------------------')
        print('Review histograms...')
        
        # Open the ini GUI
        self.hist_gui = GUIs.GUI_Hist_Review()
        print('-------------------------------------------------------')   
        
        return None        

    def execute_ramp_analysis(self):
        '''
        Lets the user evaluate a ramp analysis. First, a folder can be chosen. If valid, the evaluation starts
        immediately. 
        
        :returns: None
        '''
            
        print('\n-------------------------------------------------------')
        print('Choosing ramp analysis folder...')
        defaultDir = self.persival.get('execute_ramp_analysis')
        defaultDir = posixpath.join(defaultDir, '../')
        folder = str(QFileDialog.getExistingDirectory(self, "Select Directory", defaultDir))
        
        # In case the choosing is aborted
        if not os.path.isdir(folder): 
            print('No valid folder chosen')
        else:
            print('Got ramp folder to analyse')
            self.persival.set('execute_ramp_analysis', folder)
                    
            # Get the ramp parameter
            for param, box in self.ramp_param_buttons.items():
                if box.isChecked():
                    ramp_param = param
            print(f'Ramp parameter: {ramp_param}')
            
            # Extract and check the evaluation methods
            for fit, box in self.ramp_fit_buttons.items():
                if box.isChecked():
                    eval_method = fit
            print(f'Evaluation method: {eval_method}')
            
            # Initialize ramp analyzer
            ramp_eval = RampEvaluation(folder, eval_method, ramp_param)

            # Start data extraction
            if ramp_eval.data_check():
                # Start ramp analysis
                if ramp_eval.execute_analysis():
                    print('\nRamp analysis sucessful')
                else:
                    print('\nRamp analysis not sucessful')
            else:
                print('\nEncountered problems at data extraction, see warnings')

        print('-------------------------------------------------------')
            
        return None

    def closeEvent(self, event):
        '''
        Before closing the application we need to save the data persival collected
        ''' 
        
        self.persival.save()
        event.accept() 
        
        return None

    def execute_exit(self):
        '''
        Deletes data and closes the GUI
        
        :returns: None
        '''
        
        self.persival.save()
        print('\n-------------------------------------------------------')
        print('GUI is closed')
        print('-------------------------------------------------------')
        sys.exit()         


def execute_analysis_parallel(run_source, target):
    ''' Helper method which call the Analyse Run method without accessing the GUI (self) object

        :param run_source: Source directory to read a run from
        :param target: Target directory to write the results to
        :returns: None
    '''                 
    
    run_ana = Analyse_Run(run_source, target)

    # Starts the analysis of the run folder. Finds peaks, plots and analyzes them, and stores these data into 
    # .csv files.
    analysis_output = run_ana.analyse_peaks()

    # Analyses the given .csv files and writes the results into the .summary file.
    run_ana.analyse_csv(analysis_output)
                
if __name__ == '__main__':
    import sys
    pqt5_exception_workaround()
    pyqtRemoveInputHook()
    ini_gui = QApplication(sys.argv)
    gallery = WidgetGallery()
    gallery.show()
    sys.exit(ini_gui.exec_()) 

