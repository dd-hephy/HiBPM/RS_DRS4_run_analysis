'''
    File name: GUI_Parameter_Setting.py
    Author: Philipp Gaggl
    Date created: 30.03.2022
    Python Version: 3.8, 3.9
'''

import sys
import os

# Allow imports from parent directory if this GUI is launched directly
if __name__ == '__main__':
    current = os.path.dirname(os.path.realpath(__file__))
    parent = os.path.dirname(current)
    sys.path.append(parent)
    
import shutil
import time
import posixpath

from PyQt5.QtCore import pyqtRemoveInputHook, pyqtSignal
from PyQt5.QtWidgets import QApplication, QCheckBox, QDialog, QGroupBox, QHBoxLayout, QLabel, QPushButton, QVBoxLayout, QFileDialog, QLineEdit, QGridLayout
from PyQt5.QtGui import QFont

import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas

from fileReader import Det_File
from Run_Config import Ini_Handler
from tool_box import Persival, pqt5_exception_workaround, Warn_Me, PopUp
from First_Call import First_Call


# We overwrite events for focusing in an out of QlineEdit instances. That way we can check valid values
# as soon as the user exits the textfield even without pressing enter
class QLineEdit(QLineEdit):
    focus_in_signal = pyqtSignal()
    focus_out_signal = pyqtSignal()

    def focusInEvent(self, event):
        self.focus_in_signal.emit()
        super().focusInEvent(event)

    def focusOutEvent(self, event):
        super().focusOutEvent(event)
        self.focus_out_signal.emit()

class GUI_Parameter_Setting(QDialog):
    '''
    Class for a GUI-window which lets the user visually test out the analysis parameters on an example file.
    It reads and writes the parameters onto .ini files, which are then used by the analysis classes. Also, it 
    lets the user load different predefined parameter settings. 
    The user also has the option, to store a parameter set and access it later. Accepted settings are stored within 
    a current_settings.ini that is used the next time when launching the GUI.
    '''
    
    def __init__(self, parent = None):
        '''
        Initializes the GUI
        
        :returns: None
        '''
        
        super(GUI_Parameter_Setting, self).__init__(parent)
                
        # Initialize call (Creates current_settings.ini if first start of GUI after cloning repo)
        first_call = First_Call()
        self.warn = Warn_Me()
                
        # Parameters needed later on
        self.data_file = None
        self.current_ini = first_call.parameter_file
        self.preview_ini = 'preview_settings.ini'
        self.preview_file = None
        self.preview_events = None
        self.preview_channels = None
        self.selected_channel = None
        self.current_plot_number = None
        
        # First, we copy the current settings onto a working copy for the GUI.
        self.settings_path = first_call.parameter_path
        self.current_ini_path = first_call.current_parameters
        self.preview_ini_path = posixpath.join(self.settings_path, self.preview_ini)
        shutil.copy(self.current_ini_path, self.preview_ini_path)
        
        # Initiate the Ini_Handler() for reading/writing ini files and checking the settings.
        # Also the class variables where converted and unconverted ini content will be stored
        self.ini_handler = Ini_Handler()
        self.popup = PopUp()
        self.params = None
        self.params_str = None
        self.loaded = None
        self.loaded_str = None
        
        # Load settings last used.
        self.get_ini_settings()    
        
        # Remembers files between sessions.
        self.persival = Persival()
        pwd = os.getcwd()
        self.persival.setDefault('execute_choose_file', pwd)
        self.persival.setDefault('execute_load_params_from_ini', pwd)
          
        # Initialize the class variable storing all editable content. Create and open the GUI window.
        self.gui = dict.fromkeys(self.params)
        self.make_gui_widgets()
        self.create_window()

        return None

    def get_ini_settings(self, user_setting = None):
        '''
        Loads the parameter information of a user_setting.ini, using the ini_handler. Does not return
        the content, but rather stores it into class variables in order for it to be called by 
        signal events of the GUI. If the preview_settings.ini is read out, the content is stored into
        self.param, into self.loaded otherwise. Note, the PARAMETER section handle will not be
        included. The data is converted, while the unconverted content is stored in a variable with the
        prefix _str.
        
        :param user_setting: Name of the user setting (with or without .ini)
        :returns: None
        '''
        
        # If called without specific setting file, the preview_settings.ini is loaded
        if user_setting == None:
            user_setting = self.preview_ini
        # Accept both, only the filename as well as with the right data format
        else:
            if not user_setting.split('.')[-1] == 'ini':
                user_setting += '.ini'
        file = posixpath.join(self.settings_path, user_setting)
        
        # Still check
        if not os.path.isfile(file):
            print('No valid user setting...')
            return None
        else:
            if user_setting == self.preview_ini:
                self.params = self.ini_handler.read_ini(file, convert = True, all_defined = True, display = True)['ANALYSIS_PARAMETERS']
                self.params_str = self.ini_handler.read_ini(file, convert = False, all_defined = True, display = True)['ANALYSIS_PARAMETERS']
            else:
                self.loaded = self.ini_handler.read_ini(file, convert = True, all_defined = True, display = True)['ANALYSIS_PARAMETERS']
                self.loaded_str = self.ini_handler.read_ini(file, convert = False, all_defined = True, display = True)['ANALYSIS_PARAMETERS']
        
        return None
    
    def set_ini_settings(self, params, user_setting = None):
        '''
        Stores parameters in a setting file. 
        
        :param params: Ini content
        :param user_setting: Name of the user setting file
        '''

        # By default, we update the preview settings
        if user_setting is None:
            user_setting = self.preview_ini
            
        # Accept both, only the filename as well as with the right data format
        if not user_setting.split('.')[-1] == 'ini':
            user_setting += '.ini'
        
        # Get directory
        directory = posixpath.join(self.settings_path, user_setting)
        
        # Make sure to store them using the right section and write the ini
        content = {'ANALYSIS_PARAMETERS':params}
        self.ini_handler.write_ini(content, directory, all_defined = True, displayed = True)
                    
        return None
      
    def read_window(self):
        '''
        Reads all parameters in the window and returns a corresponding setting dict. Similar to 
        make_gui_widgets, where we use the definitions of the parameters in Run_Config.py to create
        the widgets and their initial settings, we use them here to efficiently read them out.
        Updates the preview_settings.ini and the internal parameter data.
        
        :returns: None
        '''
 
        window = dict.fromkeys(self.params_str)
        
        # Loop over all widgets and extract information according to their parameter definition in the
        # Ini_Handler() class. 
        for param in window.keys():
            definition = self.ini_handler.all[param]
            
            # If no mandatory parameter options
            if definition['options'] is None:
                
                # int, float and str are read out simply from QLineEdit text.
                if not definition['type'] is bool:
                    window[param] = self.gui[param].text()
                    
                # For bool, the checkboxes are controlled
                else:
                    window[param] = self.gui[param].isChecked()
            
            # If we deal with different options, we check each checkbox of those options
            else:
                for opt, box in self.gui[param].items():
                    if box.isChecked():
                        window[param] = opt
                        break
        
        # Write the window data into the preview_settings.ini and overwrite the internal data
        self.set_ini_settings(window)
        self.get_ini_settings()
        
        return None
     
    def update_window(self):
        '''
        Updates the displayed settings with the current internal data. The process is similar to the
        the widget creation in make_gui_widgets, as the predefinitions in the Ini_Handler() class are 
        used to do it efficiently.
        
        :returns: None
        '''
        
        # Check if we have all necessary parameters and not deal with an old file
        for param in self.ini_handler.ana_params:
            if not param in list(self.params.keys()):
                missing = param
                self.warn.warn(f'The parameter \'{missing}\' is missing in the loaded data, probably because it is too old. Add it artificially or create a new one.')
                return None
        
        for param in self.gui.keys():
            definition = self.ini_handler.all[param]
            
            # If we have no mandatory options connected to the parameter.
            if definition['options'] is None:
                
                # For int, float and str, we just set the QLineEdit text.
                if not definition['type'] is bool:
                    self.gui[param].setText(self.params_str[param])
                    
                # Otherwise, we check the boxes
                else:
                    self.gui[param].setChecked(self.params[param])
                 
            # If we deal with mandatory options, we check the corresponding boxes according to the
            # internal data.   
            else:
                for opt in definition['options']:
                    self.gui[param][opt].setChecked(opt == self.params_str[param])
        
        return None    
      
    def make_gui_widgets(self):
        '''
        Using the parameter definitions in Run_Congif.py, we assign all analysis parameters to the 
        corresponding widgets by a loop, including their initial values according to the current settings.
        Booleans get a checkbox, int and float a QlineEdit. Parameters that need to be one of several
        options get multiple checkboxes. 
        These widgets can then be built in into the layout with the desired labels and connected to 
        further events and functions in the corresponding methods.
        
        :returns: None 
        '''
        
        for param in self.params.keys():
            definition = self.ini_handler.all[param]
            
            # Cases with no options.
            if definition['options'] is None:
                
                # QLineEdit for str, int and float.
                if not definition['type'] is bool:
                    self.gui[param] = QLineEdit(self.params_str[param])
                    # These boxes are connected for validation after editing (pressing enter and going out of focus).
                    self.gui[param].returnPressed.connect((lambda object = self.gui[param], parameter = param: lambda: self.check_parameter(object, parameter))())
                    self.gui[param].focus_out_signal.connect((lambda object = self.gui[param], parameter = param: lambda: self.check_parameter(object, parameter))())
                                       
                # QCheckBox for bool.
                else:
                    self.gui[param] = QCheckBox()
                    self.gui[param].setChecked(self.params[param]) 
            
            # If we deal with different possible options for parameters they are given as multiple 
            # checkboxes stored in a dict with the keys corresponding to the options.   
            else:
                self.gui[param] = {}
                for opt in definition['options']:
                    self.gui[param][opt] = QCheckBox(opt)
                    self.gui[param][opt].setChecked(self.params[param] == opt)
                               
        return None       
      
    def create_window(self):
        '''
        Layout, buttons, and everything else that is displayed within the GUI.
        
        :returns: None
        '''
        
        # Size and position of the window
        self.resize(1500, 600)
        self.move(50, 10)
        
        # Create all boxes
        self.create_finding_parameters_box()
        self.create_fixed_threshold_box()
        self.create_analysis_box()
        self.create_plot_box()
        self.create_search_direction_box()
        self.create_careful_box()
        self.create_timeres_box()
        self.create_signal_box()
        
        # Set layout
        main_layout = QGridLayout()
        main_layout.setSpacing(0)
        main_layout.addWidget(self.finding_box, 0, 0)
        main_layout.addWidget(self.ft_box, 1, 0)
        main_layout.addWidget(self.ana_box, 2, 0) 
        main_layout.addWidget(self.direction_box, 3, 0)
        main_layout.addWidget(self.plot_box, 4, 0)
        main_layout.addWidget(self.careful_box, 5, 0)
        main_layout.addWidget(self.timeres_box, 6, 0)
        main_layout.addWidget(self.signal_box, 0, 1, 7, 1)
        self.setLayout(main_layout)        
        
        # Open GUI
        pqt5_exception_workaround()
        pyqtRemoveInputHook()
        self.exec_()
        
        return None
          
    def create_finding_parameters_box(self):
        '''
        Includes layout, labels and additional event connections to the peak finding parameters.
        
        :returns: None
        '''
        
        self.finding_box = QGroupBox('Peak finding parameters')
        self.finding_box.setFont(QFont('Times',8))
        
        # MIN_TOT_FACTOR
        layout_mintot = QVBoxLayout()
        mintot_label = QLabel('minimum samples above threshold')
        layout_mintot.addWidget(mintot_label)
        layout_mintot.addWidget(self.gui['min_tot_factor'])
        
        # MIN_TIME_BETWEEN_PEAKS
        layout_time_between = QVBoxLayout()
        time_between_label = QLabel('minimum time between peaks (ns)')
        layout_time_between.addWidget(time_between_label)
        layout_time_between.addWidget(self.gui['min_time_between_peaks'])
        
        # STD_FACTOR
        layout_std = QVBoxLayout()
        std_label = QLabel('threshold from rms-noise: factor')
        layout_std.addWidget(std_label)
        layout_std.addWidget(self.gui['std_factor'])
        
        # Box layout
        layout_finding = QHBoxLayout()
        layout_finding.addLayout(layout_mintot)
        layout_finding.addLayout(layout_time_between)
        layout_finding.addLayout(layout_std)
        
        self.finding_box.setLayout(layout_finding)
        
        return None
    
    def create_fixed_threshold_box(self):
        '''
        Includes layout, labels and additional event connections to the fixed threshold parameters.
        
        :returns: None
        '''
        
        self.ft_box = QGroupBox('Fixed threshold options')
        self.ft_box.setFont(QFont('Times', 8))
        
        # APPLY_FIXED_THRESHOLD
        layout_apply_ft = QHBoxLayout()
        apply_ft_label = QLabel('use fixed threshold')
        layout_apply_ft.addWidget(apply_ft_label)
        layout_apply_ft.addWidget(self.gui['use_fixed_threshold'])
        # Connect checkbox to activate and deactivate corresponding editing fields
        self.gui['use_fixed_threshold'].stateChanged.connect(self.execute_ft_check)   
             
        # FIXED_THRESHOLD
        layout_ft = QHBoxLayout()
        ft_label = QLabel('fixed threshold (mV)')
        # Disable the corresponding fields 
        if self.gui['use_fixed_threshold'].isChecked():
            self.gui['std_factor'].setDisabled(True)
        else:
            self.gui['fixed_threshold'].setDisabled(True)
        layout_ft.addWidget(ft_label)
        layout_ft.addWidget(self.gui['fixed_threshold'])
        
        # Box layout
        layout_ft_box = QHBoxLayout()
        layout_ft_box.addLayout(layout_apply_ft)
        layout_ft_box.addLayout(layout_ft)
        
        self.ft_box.setLayout(layout_ft_box)
        
        return None
    
    def create_analysis_box(self):
        '''
        Includes layout, labels and additional event connections to the analysis parameters.
        
        :returns: None
        '''
        
        self.ana_box = QGroupBox('Analysis region')
        self.ana_box.setFont(QFont('Times', 8))
        
        # ANALYSIS_TILL_RMS_NOISE
        layout_rms_buffer = QVBoxLayout()
        rms_buffer_label = QLabel('extract analysis region\ndown to RMS-noise')
        layout_rms_buffer.addWidget(rms_buffer_label)
        layout_rms_buffer.addWidget(self.gui['analysis_till_rms_noise'])
        # Connect checkbox to activate and deactivate left and right user analysis buffer
        self.gui['analysis_till_rms_noise'].stateChanged.connect(self.execute_ana_buffer_check)   
        
        # BUFFER_ANALYSIS_LEFT
        layout_buffer_left = QVBoxLayout()
        left_label = QLabel('left from start sample (ns)')
        layout_buffer_left.addWidget(left_label)
        layout_buffer_left.addWidget(self.gui['buffer_analysis_left'])
        if self.gui['analysis_till_rms_noise'].isChecked():
            self.gui['buffer_analysis_left'].setDisabled(True)
        
        # BUFFER_ANALYSIS_RIGHT
        layout_buffer_right = QVBoxLayout()
        right_label = QLabel('right from stop sample (ns)')
        layout_buffer_right.addWidget(right_label)
        layout_buffer_right.addWidget(self.gui['buffer_analysis_right'])
        if self.gui['analysis_till_rms_noise'].isChecked():
            self.gui['buffer_analysis_right'].setDisabled(True)
        
        # Box layout
        layout_ana = QHBoxLayout()
        layout_ana.addLayout(layout_rms_buffer)
        layout_ana.addLayout(layout_buffer_left)
        layout_ana.addLayout(layout_buffer_right)
        
        self.ana_box.setLayout(layout_ana)
        
        return None
      
    def create_search_direction_box(self):
        '''
        Includes layout, labels and additional event connections to the fixed threshold parameters.
        
        :returns: None
        '''
        
        # SEARCH_DIRECTION
        self.direction_box = QGroupBox('Search direction')
        self.direction_box.setFont(QFont('Times', 8))
        # Connect search direction check boxes to function ensuring only one is checked
        for opt, box in self.gui['search_direction'].items():
            box.stateChanged.connect((lambda option = opt: lambda: self.execute_search_check(option))())

        # Box layout
        layout_direction = QHBoxLayout()
        for option in self.gui['search_direction'].values():
            layout_direction.addWidget(option)
    
        self.direction_box.setLayout(layout_direction)
        
    def create_plot_box(self):
        '''
        Includes layout, labels and additional event connections to the plotting parameters.
        
        :returns: None
        '''     

        self.plot_box = QGroupBox('Example plot options')
        self.plot_box.setFont(QFont('Times', 8))
        
        # BUFFER_PLOT
        layout_buffer_plot = QVBoxLayout()
        buffer_plot_label = QLabel('plotting region left/right (ns)')
        layout_buffer_plot.addWidget(buffer_plot_label)
        layout_buffer_plot.addWidget(self.gui['buffer_plot'])
        
        # NO_EXAMPLE_PLOTS
        layout_no_plots = QVBoxLayout()
        no_plots_label = QLabel('# of example plots')
        layout_no_plots.addWidget(no_plots_label)
        layout_no_plots.addWidget(self.gui['no_example_plots'])
        
        # PLOT_ANALYSIS_FEATURES and PLOT_PEAK_PARAMETERS
        layout_plot_include = QVBoxLayout()
        plot_include_label = QLabel('include in example plots')
        layout_check_plot_features = QHBoxLayout()
        # PLOT_ANALYSIS_FEATURES
        plot_graphics_label = QLabel('graphics')
        layout_check_plot_features.addWidget(plot_graphics_label)
        layout_check_plot_features.addWidget(self.gui['plot_analysis_features'])
        # PLOT_PEAK_PARAMETERS
        plot_params_label = QLabel('parameters')
        layout_check_plot_features.addWidget(plot_params_label)
        layout_check_plot_features.addWidget(self.gui['plot_peak_parameters'])
        # Combine them
        layout_plot_include.addWidget(plot_include_label)
        layout_plot_include.addLayout(layout_check_plot_features)

        # Box layout
        layout_plot_box = QHBoxLayout()
        layout_plot_box.addLayout(layout_buffer_plot)
        layout_plot_box.addLayout(layout_no_plots)
        layout_plot_box.addLayout(layout_plot_include)
        
        self.plot_box.setLayout(layout_plot_box)

        return None
        
    def create_careful_box(self):
        '''
        Includes layout, labels and additional event connections to some additional parameters.
        
        :returns: None
        '''
        
        self.careful_box = QGroupBox('Carefully chosen options')
        self.careful_box.setFont(QFont('Times', 8))
        
        # COMPENSATE_OFFSET
        layout_offset = QVBoxLayout()
        offset_label = QLabel('compensate offset')
        layout_offset.addWidget(offset_label)
        layout_offset.addWidget(self.gui['compensate_offset'])
        
        # SKIP_BORDERS
        layout_skip = QVBoxLayout()
        skip_label = QLabel('remove compromised peaks at borders')
        layout_skip.addWidget(skip_label)
        layout_skip.addWidget(self.gui['skip_borders'])
        
        # ADVANCED_NOISE_OFFSET
        layout_adv = QVBoxLayout()
        adv_label = QLabel('use advanced noise\nand offset calculation')
        layout_adv.addWidget(adv_label)
        layout_adv.addWidget(self.gui['advanced_noise_offset'])
            
        # Pure simulation analysis
        layout_sim = QVBoxLayout()
        sim_label = QLabel('analyse as pure\nsimulation event')
        layout_sim.addWidget(sim_label)
        layout_sim.addWidget(self.gui['pure_simulation_analysis'])
            
        # Box layout
        layout_careful_box = QHBoxLayout()
        layout_careful_box.addLayout(layout_offset)
        layout_careful_box.addLayout(layout_skip)
        layout_careful_box.addLayout(layout_adv)
        layout_careful_box.addLayout(layout_sim)
        
        self.careful_box.setLayout(layout_careful_box)
        
        return None
        
    def create_timeres_box(self):
        '''
        Includes layout, labels and additional event connections to parameters for time resolution.
        
        :returns: None
        '''
    
        self.timeres_box = QGroupBox('Time resolution parameters')
        self.timeres_box.setFont(QFont('Times', 8))
        
        # APPLY_CFD
        layout_apply_cfd = QHBoxLayout()
        apply_cfd_label = QLabel('Apply constant fraction discrimination')
        # Connect box to function for disabling other settings.
        self.gui['apply_constant_fraction_discrimination'].stateChanged.connect(self.execute_cfd_check)
        layout_apply_cfd.addWidget(apply_cfd_label)
        layout_apply_cfd.addWidget(self.gui['apply_constant_fraction_discrimination'])
        
        # CFD_FACTOR
        layout_cfd = QHBoxLayout()
        cfd_label = QLabel('CFD relative to peak maximum')
        # Disable edit if cfd checkbox is not checked
        self.gui['cfd_factor'].setDisabled(not self.params['apply_constant_fraction_discrimination'])
        layout_cfd.addWidget(cfd_label)
        layout_cfd.addWidget(self.gui['cfd_factor'])

        # Box layout
        layout_timeres = QHBoxLayout()
        layout_timeres.addLayout(layout_apply_cfd)
        layout_timeres.addLayout(layout_cfd)
        
        self.timeres_box.setLayout(layout_timeres)
        
        return None

    def create_signal_box(self):
        '''
        Window where the plots will be shown
        
        :returns: None
        '''
        
        self.signal_box = QGroupBox()
        self.signal_box.setFont(QFont('Times', 8))
        
        # Plot window
        layout_plot_window = QVBoxLayout()
        self.figure = plt.figure()
        self.canvas = FigureCanvas(self.figure)
        self.axes = self.canvas.figure.add_subplot(111)
        # Some random plot
        self.axes.plot([1,30, 32, 29, 21, 14, 9, 5, 3, 1, 0])
        self.toolbar = NavigationToolbar(self.canvas, self)
        
        # Previous/next buttons and no of peaks   
        self.previous = QPushButton('<--')
        self.previous.clicked.connect(self.execute_previous)
        self.previous.setDisabled(True)
        self.next = QPushButton('-->')
        self.next.clicked.connect(self.execute_next)
        self.next.setDisabled(True)
        choose_peak_no_label = QLabel('# of pulses to plot')
        if self.params['no_example_plots'] == 0:
            self.preview_peak_no = QLineEdit('10')
        else:
            self.preview_peak_no = QLineEdit(self.params_str['no_example_plots'])
        self.preview_peak_no.returnPressed.connect(lambda: self.check_parameter(self.preview_peak_no, 'no_example_plots'))
        self.preview_peak_no.focus_out_signal.connect(lambda: self.check_parameter(self.preview_peak_no, 'no_example_plots'))

        layout_no_peaks = QVBoxLayout()
        layout_no_peaks.addWidget(choose_peak_no_label)
        layout_no_peaks.addWidget(self.preview_peak_no)
        
        # Channel buttons
        self.ch = [QPushButton('CH1'), QPushButton('CH2'), QPushButton('CH3'), QPushButton('CH4')]
        for i, ch in enumerate(self.ch):
            ch.setDisabled(True)
            ch.clicked.connect((lambda index = i: lambda: self.execute_channel_button(index))())
        layout_signal_buttons = QHBoxLayout()   
        layout_signal_buttons.addWidget(self.previous)
        layout_signal_buttons.addWidget(self.next)
        layout_signal_buttons.addLayout(layout_no_peaks)
        for ch in self.ch:
            layout_signal_buttons.addWidget(ch)
        layout_plot_window.addWidget(self.toolbar)
        layout_plot_window.addWidget(self.canvas)
        layout_plot_window.addLayout(layout_signal_buttons)
        
        # Other buttons
        layout_preview_buttons = QGridLayout()
        self.choose_file = QPushButton('choose data file for preview')
        self.choose_file.clicked.connect(self.execute_choose_file)
        self.start_preview = QPushButton('start preview')
        self.start_preview.setDisabled(True)
        self.start_preview.clicked.connect(lambda: self.execute_preview(False))
        self.plot_raw_signals = QPushButton('plot raw events')
        self.plot_raw_signals.setDisabled(True)
        self.plot_raw_signals.clicked.connect(lambda: self.execute_preview(True))
        self.reset_settings = QPushButton('reset settings')
        self.reset_settings.clicked.connect(self.execute_reset_settings)
        self.load_param_file = QPushButton('load user setting')
        self.load_param_file.clicked.connect(self.execute_load_user_setting)
        self.export_setting = QPushButton('export as user setting')
        self.export_setting.clicked.connect(self.execute_export_settings)
        self.load_from_ini = QPushButton('load from existing .ini file')
        self.load_from_ini.clicked.connect(self.execute_load_params_from_ini)
        self.exit = QPushButton('keep settings and exit')
        self.exit.clicked.connect(self.execute_save_and_exit)
        layout_preview_buttons.addWidget(self.choose_file, 0, 0)
        layout_preview_buttons.addWidget(self.start_preview, 0, 1)
        layout_preview_buttons.addWidget(self.plot_raw_signals, 0, 2)
        layout_preview_buttons.addWidget(self.reset_settings, 0, 3)
        layout_preview_buttons.addWidget(self.load_param_file, 1, 0)
        layout_preview_buttons.addWidget(self.export_setting, 1, 1)
        layout_preview_buttons.addWidget(self.load_from_ini, 1, 2)
        layout_preview_buttons.addWidget(self.exit, 1, 3)
        
        # Box layout
        layout_signal = QVBoxLayout()
        layout_signal.addLayout(layout_plot_window)
        layout_signal.addLayout(layout_preview_buttons)
        
        self.signal_box.setLayout(layout_signal)
        
        return None

    def execute_ft_check(self):
        '''
        Enables the choosing of a fixed threshold value if the corresponding checkbox is checked
        
        :returns: None
        '''
        
        self.gui['fixed_threshold'].setDisabled(not self.gui['use_fixed_threshold'].isChecked())
        self.gui['std_factor'].setDisabled(self.gui['use_fixed_threshold'].isChecked())
        
        return None
   
    def execute_ana_buffer_check(self):
        '''
        Enables the choosing of a user values for the left and right analysis buffer if 'analyse down to
        rms noise' checkbox is not checked
        
        :returns: None
        '''
        
        self.gui['buffer_analysis_left'].setDisabled(self.gui['analysis_till_rms_noise'].isChecked())
        self.gui['buffer_analysis_right'].setDisabled(self.gui['analysis_till_rms_noise'].isChecked())

        return None
   
    def execute_search_check(self, option):
        '''
        Unchecks other search directions if one is checked
        
        :param option: option (and key) of the button as stored in self.gui['search_direction']
        :returns: None
        '''
        
        # We need to disconnect in between to not cause an avalanche
        if self.gui['search_direction'][option].isChecked():
            for opt, box in self.gui['search_direction'].items():
                if not opt == option and box.isChecked():
                    box.disconnect()
                    box.setChecked(False)
                    box.stateChanged.connect((lambda option = opt: lambda: self.execute_search_check(option))())            
        else:
            self.gui['search_direction'][option].setChecked(True)
            
        return None
           
    def execute_cfd_check(self):
        '''
        Disables the choosing of a CFD factor if the corresponding checkbox is not checked
        
        :returns: None
        '''
        
        self.gui['cfd_factor'].setDisabled(not self.gui['apply_constant_fraction_discrimination'].isChecked())
        
        return None

    def check_parameter(self, object, param):
        '''
        Checks the current text in the given LinEdit-object for validity using the predefinitions connected
        to the given parameter in Run_Config.py.
        If not valid, the text is reset to the former value.
        
        :param object: The QlineEdit object
        :param param: Name of the parameter as stored in the definitions in Run_Config.py
        :returns: None
        '''

        # We need the text as a dict with the parameter name to use the methods of Ini_Handler()
        to_check = {param:object.text()}
        
        # The conversion function already checks the value and returns None if something went wrong
        to_check = self.ini_handler.convert_from_ini(to_check, display = True)
        if to_check is None:
            object.setText(self.params_str[param])
            print('Invalid value, reset to initial settings')
            
        return None
    
    def execute_choose_file(self):
        '''
        Lets the user choose a file to analyse. If a whole run folder is chosen, the first file will be used. 
        
        :returns: None
        '''

        self.read_window()

        # First, let the user choose a data file
        defaultDir = self.persival.get('execute_choose_file')
        data_file = str(QFileDialog.getOpenFileName(self, "Select Directory", defaultDir)[0])
        valid_file = False

        if os.path.exists(data_file):
            body, base = os.path.splitext(data_file)
            
            # Check if file ending is valid (i.e. corresponding to one of the readout classes)
            if base == '.dat' or base == '.txt' or base == '.csv' or base == '.npz':
                valid_file = True
            
            # If we have a file, we need to ensure a .bin file also has a .Wfm.bin file
            if base == '.bin':
                if body[(len(body) - 4):len(body)] == '.Wfm':
                    meta = body[0:(len(body) - 4)] + base
                    waveform = body + base
                else:
                    meta = body + base
                    waveform = body + '.Wfm' + base
                    
                # If waveform and metadata files are present, we can continue
                if os.path.isfile(meta) and os.path.isfile(waveform):
                    data_file = meta
                    valid_file = True
        
        if valid_file:
            self.data_file = data_file
            self.plot_raw_signals.setDisabled(False)
            self.start_preview.setDisabled(False)
            self.persival.set('execute_choose_file', data_file)
            print('Preview file chosen...')
        else:
            print('No valid data file...')        
                
        return None    
    
    def execute_preview(self, raw = False):
        '''
        Executes the preview to look at some pulses for a given file and the used settings. We allow for 
        normal preview, where the analysis parameters from the GUI are taken, as well as raw plotting, 
        where we do not eve analyse the data, but just plot the raw data.
        
        :param raw: If False, normal preview according to GUI parameters is done. Else, we just plot raw data
        :returns: None
        '''
        
        # Ensure the GUI-settings are stored into the current_settings.ini file
        self.read_window()
        
        # Reset variables to ensure consistency with failsafes
        self.preview_file = None
        self.preview_events = None
        self.preview_channels = None
        self.selected_channel = None
        self.current_plot_number = None
        
        # Disable all buttons that can be enabled again after preview
        for ch in self.ch:
            ch.setDisabled(True)
        self.next.setDisabled(True)
        self.previous.setDisabled(True)
        
        t = time.time()
        if not raw:
            print('\nStart preview...')
        else:
            print('\nAcquire raw data...')
            
        # First, we check if we have the necessary information
        try:
            preview_peaks = int(self.preview_peak_no.text())
        except:
            print('Error: # of preview pulses must be a number and greater than zero!')
            return None
        
        if not preview_peaks > 0:
            self.warn.warn('# of pulses to plot is set to 0, change that')
            return None
        
        try:
            os.path.isfile(self.data_file)
            file = self.data_file
        except:
            print('Error: The data file does not exist!')
            return None
        
        # Analyse file in preview mode. 
        if self.preview_file is None:
            self.preview_file = Det_File(file, [preview_peaks, self.axes])
            
            # Here we differ between the actual preview and the plotting of raw data
            if not raw:
                self.preview_events = self.preview_file.execute_preview()
            else:
                # We need the information about the channels, number of events and samples per event
                self.preview_events = dict.fromkeys(self.preview_file.meta['source_names'])
                events = self.preview_file.segments
                event_length = self.preview_file.segment_length
                
                # Make sure to not try to access more events than available
                if events < preview_peaks:
                    preview_peaks = events
                    
                # Now create the artificial events. We only need to do so once, as the structure 
                # is the same for all channels            
                dummy_seg = [1] * preview_peaks
                dummy_df = pd.DataFrame.from_dict({'Start':[0] * preview_peaks, 'Duration':[event_length - 1] * preview_peaks, 'Starts_interp':[0] * preview_peaks, 'Ends_interp':[event_length - 1] * preview_peaks, 't_rise':[0] * preview_peaks, 't_rise_interp':[0] * preview_peaks})
                
                for ch in self.preview_events.keys():
                    self.preview_events[ch] = [dummy_seg, dummy_df]

                # Now we also need to set the plotting variables correctly
                self.preview_file.ana_params['buffer_plot'] = 0
                self.preview_file.ana_params['plot_analysis_features'] = False
                self.preview_file.ana_params['plot_peak_parameters'] = False
                self.preview_file.ana_params['compensate_offset'] = False
                
                # We need to set a buffer if rms buffer is on
                if self.preview_file.ana_params['analysis_till_rms_noise']:
                    for ch in self.preview_file.meta['source_names']:
                        self.preview_file.buffer_left[ch] = [[0] for _ in range(self.preview_file.segments)]
                        self.preview_file.buffer_right[ch] = [[0] for _ in range(self.preview_file.segments)]
        
        # Get channels of the file and unlock channel buttons. The first channel is already clicked, 
        # causing also its first signal to be plotted
        self.get_channels()
        
        print(f'Finished preview, plots available, took {time.time() - t} s...')

        # Reload settings and window, in case the fileReader has adjusted buffer plot
        self.get_ini_settings()
        self.update_window()

        return None

    def get_channels(self):
        '''
        After preview, checks for the used osci and channels. Stores them and activates the corresponding
        buttons.
        
        :returns: None
        '''
        
        self.preview_channels = {}
        self.current_plot_number = {}
        # Get the osci and the corresponding channel options
        if self.preview_file.osci == 'DRS4':
            channels = [1, 2, 3, 4]
        elif self.preview_file.osci == 'Weightfield':
            channels = ['CSA']
        elif self.preview_file.osci == 'RS':
            channels = ['CH1_TR1', 'CH2_TR1', 'CH3_TR1', 'CH4_TR1']
        elif self.preview_file.osci == 'Allpix':
            channels = ['CSA']
        elif self.preview_file.osci == 'CNM':
            channels = ['CNM']

        # Enable the found channels within the GUI and store them for plotting
        for ch in self.preview_events.keys():
            if ch in channels:
                # Connect channel names to indices for easier handling
                self.preview_channels[channels.index(ch)] = ch
                # Enable all chanel buttons available
                self.ch[channels.index(ch)].setDisabled(False)
                # Set the current plot of all channels to 0
                self.current_plot_number[ch] = 0
        
        # Already click the first channel that is available
        for ch in self.ch:
            if ch.isEnabled():
                ch.click()
                break 
            
        return None
    
    def execute_channel_button(self, index):
        '''
        When a channel is clicked, the first available plot is shown. Also, the 'next' buttin is enabled
        if there is more than one available plot. 
        
        :returns: None
        '''
 
        self.selected_channel = self.preview_channels[index]  
        
        # Mark the selected channel
        for i, ch in enumerate(self.ch):
            if i == index:
                ch.setStyleSheet('background-color : yellow')
            else:
                ch.setStyleSheet('background-color : None')
        
        # Enable and disable next/previous buttons according to available plots
        if self.preview_events[self.selected_channel] is None:
            self.next.setDisabled(True)
            self.previous.setDisabled(True)    
        else:
            if self.current_plot_number[self.selected_channel] > 0:
                self.previous.setDisabled(False)
            else:
                self.previous.setDisabled(True)
            if self.current_plot_number[self.selected_channel] < (len(self.preview_events[self.selected_channel][1]) - 1):
                self.next.setDisabled(False)
            else:
                self.next.setDisabled(True)
                
        self.plot_signal()
        
        return None        
    
    def execute_previous(self):
        '''
        When previous plot button is clicked, the previous signal is plotted. Also, disables the buttons
        according to the available events.
        
        :returns: None
        '''
        
        # Count up the current plot number of the used channel
        self.current_plot_number[self.selected_channel] -= 1

        # Disable the button if we don't have more events
        if not self.preview_events[self.selected_channel] is None:
            if self.current_plot_number[self.selected_channel] == 0:
                self.previous.setDisabled(True)
            self.next.setDisabled(False)
            
        # Plot the next event
        self.plot_signal()
        
        return None
    
    def execute_next(self):
        '''
        When next plot button is clicked, the next signal is plotted. Also, disables the buttons
        according to the available events.
        
        :returns: None
        '''
        
        # Count up the current plot number of the used channel
        self.current_plot_number[self.selected_channel] += 1

        # Disable the button if we don't have more events
        if not self.preview_events[self.selected_channel] is None:
            if self.current_plot_number[self.selected_channel] == (len(self.preview_events[self.selected_channel][1]) - 1):
                self.next.setDisabled(True)
            self.previous.setDisabled(False)
            
        # Plot the next event
        self.plot_signal()
        
        return None
    
    def plot_signal(self):
        '''
        Executes the signal plot of currently chosen channel and signal number
        
        :returns: None
        '''
        
        # Get the necessary arguments from the stored variables
        channel = self.selected_channel
        if not self.preview_events[channel] is None:
            signal = self.preview_events[channel][1].iloc[[self.current_plot_number[channel]]]
            event_index = self.preview_file.get_event_index(self.preview_events[channel][0], self.current_plot_number[channel])
            event = [0]*(event_index) + [1]
            event_input = [event, signal]
            dir = 'need no dir in preview'
            # Get the plot
            self.preview_file.plotEvents(event_input, dir, channel)
            # We need to change the titled plot number manually
            if self.preview_file.segments == 1:
                self.axes.set_title(f'{self.selected_channel}, peak_no: {str(self.current_plot_number[channel] + 1)}')
            else:
                self.axes.set_title(f'{self.selected_channel}, event_no: {str(event_index + 1)}, peak_no: {str(self.current_plot_number[channel] + 1)}')
            
        else:
            self.axes.cla()
            #self.axes.text(f'{self.selected_channel} does not contain any events', fontsize=15)
            self.axes.plot([1,30, 32, 29, 21, 14, 9, 5, 3, 1, 0])
            self.axes.text(4.5,10.5,f'{self.selected_channel} does not contain any events', fontsize = 20,  bbox={'facecolor':'red','alpha':1,'edgecolor':'none','pad':10}, ha='center', va='center') 

        # Draw the signal plot into the GUI        
        self.canvas.draw()
        
        return None

    def execute_reset_settings(self):
        '''
        Resets the settings to the current settings when opening the tool
        
        :returns: None
        '''
        
        # We get the data from current_settings.ini...
        self.get_ini_settings(self.current_ini)
        
        # ...set the preview_settings.ini to the resetted ones...
        self.set_ini_settings(self.loaded_str)
        
        # ...update the GUI internal data...
        self.params = self.loaded.copy()
        self.params_str = self.loaded_str.copy()
        
        # ...and update the displayed values.
        self.update_window()
        print('Settings reset to initial status...')
        
        return None
    
    def execute_load_user_setting(self):
        '''
        Opens a window with a list of all available user settings. After choosing, the setting is loaded
        and stored in the variables self.loaded and self.loaded_str.
        
        :returns: None
        '''
        
        self.read_window()
        
        # Create another window
        stored_settings = QDialog()
        layout = QVBoxLayout()
        layout.addWidget(QLabel('Stored settings'))
        # Get the user settings and remove the temporal and current one for this GUI
        user_settings = [setting[:-4] for setting in os.listdir(self.settings_path) if setting.split('.')[-1] == 'ini' and not setting == self.current_ini and not setting == self.preview_ini]
        
        # Loop over the avalilable files and open a window to choose from
        load_buttons = {}
        for setting in user_settings:
            load_buttons[setting] = QPushButton(setting)
            # If one of the buttons is clicked, the file data is loaded and the window is closed
            # Don't ask why this notation is like this. Getting Buttons in a loop and referencing them right is a pain in the ass
            load_buttons[setting].clicked.connect((lambda text = setting:  lambda: self.get_ini_settings(text))())
            load_buttons[setting].clicked.connect(stored_settings.close)
            layout.addWidget(load_buttons[setting])
        stored_settings.setLayout(layout)

        # Open the window
        stored_settings.exec_()
        del(load_buttons, stored_settings, layout)
        
        # Now we use the saved settings to update our stored settings as well as our window
        if not self.loaded is None:
            if not self.loaded == self.params:
                self.set_ini_settings(self.loaded_str)
                self.params = self.loaded
                self.params_str = self.loaded_str
                self.update_window()
                print('Loaded settings...')
            else:
                print('Same settings...')
        else:
            print('No setting loaded...')        
        
        return None
  
    def execute_export_settings(self):
        '''
        Lets the user type in a setting name and export the current settings for later or choose an existing
        one to overwrite
        
        :returns: None
        '''
        
        self.read_window()
        
        # A pop up window to enter the file name or choose from existing ones should appear
        type_name = QDialog()
        layout = QVBoxLayout()
        # Export as new file
        layout.addWidget(QLabel('Insert name of of settings to export\nfinish by pressing \'Enter\''))
        export_name = QLineEdit('')
        export_name.returnPressed.connect(lambda: self.set_ini_settings(self.params_str, export_name.text()))
        export_name.returnPressed.connect(lambda: print(f'\'{export_name.text()}\' added to user settings...'))
        export_name.returnPressed.connect(type_name.close)
        layout.addWidget(export_name)
        # Overwrite existing one
        layout.addWidget(QLabel('Overwrite existing user setting'))
        files = os.listdir(self.settings_path)
        files = [file[:-4] for file in files if file[-4:] == '.ini']
        overwrite_buttons = {}
        for file in files:
            overwrite_buttons[file] = QPushButton(file)
            overwrite_buttons[file].clicked.connect((lambda name = file: lambda: self.set_ini_settings(self.params_str, name))())
            overwrite_buttons[file].clicked.connect((lambda name = file: lambda: print(f'\'{name}\' overwritten...'))())
            overwrite_buttons[file].clicked.connect(type_name.close)
            layout.addWidget(overwrite_buttons[file])
        type_name.setLayout(layout)
        type_name.exec_()
        del(type_name)
        
        return None
    
    def execute_load_params_from_ini(self):
        '''
        Allows the user to choose any .ini file to load the analysis parameters from there. Checks, if all 
        parameters are given and loads them into the GUI if so. 
        
        :returns: None
        '''
        
        self.read_window()
        
        # Let the user choose a file
        defaultDir = self.persival.get('execute_load_params_from_ini')
        file = str(QFileDialog.getOpenFileName(self, 'Select Directory', defaultDir)[0])
        
        valid_file = False
        if not os.path.exists(file):
            self.warn.warn('The chosen directory does not exist...')
        else:
            body, base = os.path.splitext(file)
            
            # Check for right format
            if not base == '.ini':
                self.warn.warn('Chosen file is not .ini file...')
            else:
                # Read the whole .ini file without conversion
                ini_content = self.ini_handler.read_ini(file, convert = False, all_defined = False, display = False)
                
                # Filter out the desired parameters
                if ini_content is None:
                    self.warn.warn('Error during .ini reading...')
                else:
                    all_keys = True
                    filtered = {}
                    sec = 'ANALYSIS_PARAMETERS'
                    if not sec in list(ini_content.keys()):
                        self.warn.warn(f'Section {sec} missing in chosen .ini file...')
                        all_keys = False
                    else:
                        for param in self.params.keys():
                            if not param in list(ini_content[sec].keys()):
                                self.warn.warn(f'Parameter {param} missing in chosen .ini file...')
                                all_keys = False
                            else:
                                filtered[param] = ini_content[sec][param]
                    
                    # Now convert the data and load it into the GUI
                    if all_keys:
                        filtered = self.ini_handler.convert_from_ini(filtered, display = True)
                        if filtered is None:
                            self.warn.warn('Error during conversion of parameters, see warnings...')
                        else:
                            self.set_ini_settings(filtered)
                            self.get_ini_settings()
                            self.update_window()
                            valid_file = True
        
        if valid_file:
            self.persival.set('execute_load_params_from_ini', file)
            print('Analysis parameters loaded...')
        else:
            print('No analysis parameters loaded...')
        
        return None
    
    def closeEvent(self, event):
        '''
        Before closing the application we need to save the data persival collected

        :returns: None
        ''' 
        
        # Delete the working copy
        os.remove(self.preview_ini_path)
        
        self.persival.save()
        event.accept() 
        
        return None
    
    def execute_save_and_exit(self):
        '''
        Saves the current settings, deletes the working copy and exits the GUI
        
        :returns: None (duh)
        '''
        
        self.persival.save()

        # Get the latest GUI-settings
        self.read_window()
        
        # If pure_simulation_analysis is activated, the user must be asked again if this is intentional
        if self.params['pure_simulation_analysis']:
            if not self.popup.window(f'pure simulation analysis', f'Attention. You checked the option \'analyse as pure simulation event\'. This is only recommended if you have a single simulated signal stretching across the whole event. If kept, no pulse finding will be executed. Rather, the whole event data will be used for the pulse analysis itself. Are you sure you want to analyze using this option?'):
                self.popup = PopUp()
                self.params['pure_simulation_analysis'] = False
                self.set_ini_settings(self.params, self.current_ini)
                self.update_window()
                self.warn.warn('\'pure simulation analysis\' switched off')
                return None
        
        # Write the stored settings into the current settings file
        self.set_ini_settings(self.params, self.current_ini)
        
        # Close the GUI
        print('Settings saved...')
        self.close()
        
        return None
        
        
if __name__ == '__main__':
    ini_gui = QApplication(sys.argv)
    gui = GUI_Parameter_Setting()
    sys.exit()