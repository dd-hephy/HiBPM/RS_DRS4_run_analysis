'''
    File name: GUI_Hist_Review.py
    Author: Philipp Gaggl
    Date created: 30.09.2022
    Python Version: 3.8, 3.9
'''

import sys
import os

# Ignore deprecation warnings from the slider
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning) 

# Allow imports from parent directory if this GUI is launched directly
if __name__ == '__main__':
    current = os.path.dirname(os.path.realpath(__file__))
    parent = os.path.dirname(current)
    sys.path.append(parent)
    
import posixpath

from PyQt5.QtCore import pyqtRemoveInputHook, pyqtSignal, Qt
from PyQt5.QtWidgets import QApplication, QDialog, QGroupBox, QHBoxLayout, QLabel, QPushButton, QVBoxLayout, QFileDialog, QLineEdit, QGridLayout, QSlider
from PyQt5.QtGui import QFont

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas

from Run_Config import Ini_Handler
from tool_box import Persival, pqt5_exception_workaround, Warn_Me, PopUp


# We overwrite events for focusing in an out of QlineEdit instances. That way we can check valid values
# as soon as the user exits the textfield even without pressing enter
class QLineEdit(QLineEdit):
    focus_in_signal = pyqtSignal()
    focus_out_signal = pyqtSignal()

    def focusInEvent(self, event):
        self.focus_in_signal.emit()
        super().focusInEvent(event)

    def focusOutEvent(self, event):
        super().focusOutEvent(event)
        self.focus_out_signal.emit()

# We need an own class to allow float for the sliders
class DoubleSlider(QSlider):
    
    # create our our signal that we can connect to if necessary
    doubleValueChanged = pyqtSignal(float)

    def __init__(self, decimals=3, *args, **kargs):
        super(DoubleSlider, self).__init__( *args, **kargs)
        self._multi = 10 ** decimals

        self.valueChanged.connect(self.emitDoubleValueChanged)

    def emitDoubleValueChanged(self):
        value = float(super(DoubleSlider, self).value()) / self._multi
        self.doubleValueChanged.emit(value)

    def value(self):
        return float(super(DoubleSlider, self).value()) / self._multi

    def setMinimum(self, value):
        return super(DoubleSlider, self).setMinimum(value * self._multi)

    def setMaximum(self, value):
        return super(DoubleSlider, self).setMaximum(value * self._multi)

    def setSingleStep(self, value):
        return super(DoubleSlider, self).setSingleStep(value * self._multi)

    def singleStep(self):
        return float(super(DoubleSlider, self).singleStep()) / self._multi

    def setValue(self, value):
        super(DoubleSlider, self).setValue(int(value * self._multi))

class GUI_Hist_Review(QDialog):
    '''
    Class for a GUI-window which lets the user visually play with the different analysis parameters and review
    the different histograms of some run analysis. Accesses the peak_data.csv and adapts the shown histograms
    accordingly.
    '''

    def __init__(self, parent = None):
        '''
        Initializes the GUI
        
        :returns: None
        '''
        super(GUI_Hist_Review, self).__init__(parent)
                
        # Some helpers
        self.warn = Warn_Me()
        self.popup = PopUp()
        self.ini_handler = Ini_Handler()

        # Parameters needed later on
        # Found detectors and channels to switch between
        self.detectors = ['CH1', 'CH2', 'CH3', 'CH4']
        self.channels = [1, 2, 3, 4]
        self.ch_to_det = dict.fromkeys(self.channels)
        for i, ch in enumerate(list(self.ch_to_det.keys())):
            self.ch_to_det[ch] = self.detectors[i]
        self.plot_ch = None
        self.previous_ch = None
        # Data folder
        self.folder = None
        # Pulse parameters to view, including conversion and naming
        self.params = self.ini_handler.pulse_params
        self.params_con_n = self.ini_handler.pulse_params_converted
        self.params_con_f = self.ini_handler.pulse_params_conversion_factors
        self.params_unit = self.ini_handler.pulse_params_unit
        self.params_unit_conv = self.ini_handler.pulse_params_unit_converted
        # Path to the used data file(s)
        self.data_files = None
        # Extracted data
        self.data = None
        # Number of pulses within the corresponding data file
        self.pulse_num = None
        # Current settings as shown in the GUI, globals borders corresponds to the minima and maxima within the data
        self.borders = dict.fromkeys(self.params)
        for p in self.params:
            self.borders[p] = [0.0, 100.0]
        self.global_borders = None
        # Booleans representing the valid pulse indices as constrained by the set borders
        self.valid_indices = None
        self.valid_pulses = None
        self.pulse_num_valid = None
        # Currently chosen parameter to plot, as well as the one before any change
        self.plot_param = None
        self.previous_param = None
        self.rest_valid = None
        # If True, changes at the sliders do not plot immediately
        self.init = True
        # Plotting normalized or absolute
        self.normalized = True
        
        self.det = None 
        
        # Remembers files between sessions.
        self.persival = Persival()
        pwd = os.getcwd()
        self.persival.setDefault('execute_result_folder', pwd)
        self.persival.setDefault('execute_result_file', pwd)
        self.persival.setDefault('execute_save_settings', pwd)
        self.persival.setDefault('execute_load_settings', pwd)
          
        # Initialize the class variable storing all editable content. Create and open the GUI window.
        self.create_param_widgets()
        self.create_window()

        return None
    
    def create_param_widgets(self):
        '''
        Creates all buttons and sliders corresponding to the pulse params and stores them as class variables
        
        :returns: None
        '''
        
        self.det_buttons = {}
        self.plot_buttons = {}
        self.slider_left = {}
        self.slider_right = {}
        self.border_left_field = {}
        self.border_right_field = {}
        
        # Buttons for the different detectors
        for ch in self.channels:
            self.det_buttons[ch] = QPushButton(self.ch_to_det[ch])
            self.det_buttons[ch].clicked.connect((lambda object = self.det_buttons[ch], channel = ch: lambda: self.det_change(object, channel))())
        
        for param in self.params:
            # Buttons to choose between the plots
            self.plot_buttons[param] = QPushButton(self.params_con_n[param])
            self.plot_buttons[param].clicked.connect((lambda object = self.plot_buttons[param], parameter = param: lambda: self.param_change(object, parameter))())
            
            # Sliders
            self.slider_left[param] = DoubleSlider(Qt.Orientation.Vertical)
            self.slider_left[param].setMinimum(0.0)
            self.slider_left[param].setMaximum(100.0)
            self.slider_left[param].setSingleStep(1.0)
            self.slider_left[param].valueChanged.connect((lambda object = self.slider_left[param], parameter = param, dir = 'left': lambda: self.slider_change(object, parameter, dir))())
            
            self.slider_right[param] = DoubleSlider(Qt.Orientation.Vertical)
            self.slider_right[param].setMinimum(0.0)
            self.slider_right[param].setMaximum(90.0)
            self.slider_right[param].setSingleStep(1.0)
            self.slider_right[param].setValue(100.0)
            self.slider_right[param].valueChanged.connect((lambda object = self.slider_right[param], parameter = param, dir = 'right': lambda: self.slider_change(object, parameter, dir))())
            
            # Left and right borders
            self.border_left_field[param] = QLineEdit('0')
            self.border_left_field[param].setFixedWidth(90)
            self.border_left_field[param].focus_out_signal.connect((lambda object = self.border_left_field[param], parameter = param, dir = 'left': lambda: self.range_field_change(object, parameter, dir))())
            self.border_left_field[param].returnPressed.connect((lambda object = self.border_left_field[param], parameter = param, dir = 'left': lambda: self.range_field_change(object, parameter, dir))())

            self.border_right_field[param] = QLineEdit('100')
            self.border_right_field[param].setFixedWidth(90)
            self.border_right_field[param].focus_out_signal.connect((lambda object = self.border_right_field[param], parameter = param, dir = 'right': lambda: self.range_field_change(object, parameter, dir))())
            self.border_right_field[param].returnPressed.connect((lambda object = self.border_right_field[param], parameter = param, dir = 'right': lambda: self.range_field_change(object, parameter, dir))())

        return None

    def create_window(self):
        '''
        Creates the GUI
        
        :returns: None
        '''
        
        # Size and position of the window
        self.resize(1500, 900)
        self.move(50, 10)
                
        # Set layout
        main_layout = QGridLayout()
        main_layout.setSpacing(0)
        
        # Range boxes
        self.create_range_boxes()
        main_layout.addWidget(self.range_box, 0, 0)

        # Plot box
        self.create_plot_box()
        main_layout.addWidget(self.plot_box, 0, 1)
            
        # Choose results folder
        self.create_rest_box()
        main_layout.addWidget(self.rest_box, 1, 0, 2, 1)    
    
        # Vievable parameter buttons
        self.create_option_box()
        main_layout.addWidget(self.option_box, 1, 1)
        
        # Available detectors
        self.create_det_box()
        main_layout.addWidget(self.det_box, 2, 1)
        
        # Open GUI
        self.setLayout(main_layout)
        pqt5_exception_workaround()
        pyqtRemoveInputHook()
        self.exec_()
        
        return None

    def create_range_boxes(self):
        '''
        Creates the brange boxes for each analysis parameter, including the corresponding sliders as well
        as the editable fields
        
        :returns: None
        '''
        
        # Number of rows and columns of the range box
        n_col = 4
        
        self.range_box = QGroupBox()
        self.range_box.setFont(QFont('Times', 9))
        range_layout = QGridLayout()
        range_layout.setSpacing(2)
        
        # We need one for each available pulse parameter
        for i, param in enumerate(self.params):
            box = QGroupBox(self.params_con_n[param])
            layout = QGridLayout()
            
            # Minimum
            layout.addWidget(QLabel(f'min:'), 0, 0)
            layout.addWidget(self.slider_left[param], 1, 0)
            layout.addWidget(self.border_left_field[param], 2, 0)
            
            # Maixmum
            layout.addWidget(QLabel(f'max:'), 0, 1)
            layout.addWidget(self.slider_right[param], 1, 1)
            layout.addWidget(self.border_right_field[param], 2, 1)
            
            box.setLayout(layout)
            
            row = int(i / n_col)
            col = i % n_col
            range_layout.addWidget(box, row, col)
            
        self.range_box.setLayout(range_layout)
            
        return None

    def create_plot_box(self):
        '''
        Creates the displaying part of the historgrams
        
        :returns: None
        '''
        
        self.plot_box = QGroupBox()
        self.plot_box.setFont(QFont('Times', 8))
        layout = QVBoxLayout()

        self.figure = plt.figure()
        self.canvas = FigureCanvas(self.figure)
        self.axes = self.canvas.figure.add_subplot(111)
        # Some random plot
        self.axes.plot([1, 2, 4, 7, 11, 15, 16, 17, 17, 17, 16, 15, 11, 7, 4, 2, 1])
        self.toolbar = NavigationToolbar(self.canvas, self)
        
        layout.addWidget(self.toolbar)
        layout.addWidget(self.canvas)
        
        self.plot_box.setLayout(layout)
        
        return None

    def create_option_box(self):
        '''
        Creates a box including the buttons with the available parameters
        
        :returns: None
        '''
        
        self.option_box = QGroupBox('Viewable pulse parameters')
        self.option_box.setFont(QFont('Times', 8))
        layout = QGridLayout()
        n_col = 3
        
        for i, param in enumerate(self.params):
            row = int(i / n_col)
            col = i % n_col
            layout.addWidget(self.plot_buttons[param], row, col)
        
        self.option_box.setLayout(layout)
        
        return None        

    def create_det_box(self):
        '''
        Box including the buttons to choose between the available detectors
        
        :returns: None
        '''
        
        self.det_box = QGroupBox('Accessable detectors')
        self.det_box.setFont(QFont('Times', 8))
        layout = QHBoxLayout()
        
        for ch in self.channels:
            layout.addWidget(self.det_buttons[ch])
            
        self.det_box.setLayout(layout)
        
        return None        

    def create_rest_box(self):
        '''
        Box including different buttons and fields for the number of histogram bins, folder choosing, 
        storing of the used borders and so on
        
        :returns: None
        '''
        
        self.rest_box = QGroupBox()
        layout = QGridLayout()
        
        # Choose file button
        self.file_button = QPushButton('choose pulse data .csv file')
        self.file_button.clicked.connect(self.execute_result_file)
        layout.addWidget(self.file_button, 0, 0, 1, 2)
        
        # Choose folder button
        self.folder_button = QPushButton('choose results folder')
        self.folder_button.clicked.connect(self.execute_result_folder)
        layout.addWidget(self.folder_button, 0, 2, 1, 2)
        
        # Load border settings
        self.load_settings = QPushButton('load border settings from existing file')
        self.load_settings.clicked.connect(self.execute_load_settings)
        layout.addWidget(self.load_settings, 1, 0)
        
        # Store border settings
        self.store_settings = QPushButton('store border settings as .ini')
        self.store_settings.clicked.connect(self.execute_save_settings)
        layout.addWidget(self.store_settings, 1, 1)
        
        # Bin numbers
        layout.addWidget(QLabel('# of histogram bins'), 1, 2)
        self.bin_field = QLineEdit('200')
        layout.addWidget(self.bin_field, 1, 3)
        
        # Store settings, pictures and data
        self.save_all = QPushButton('store settings, pictures and data')
        self.save_all.clicked.connect(lambda: self.warn.warn('Not implemented yet...'))
        self.save_all.setDisabled(True)
        layout.addWidget(self.save_all, 2, 0, 1, 4)
        
        # Reset borders of current channel
        self.reset = QPushButton('reset all borders of currently viewed source')
        self.reset.clicked.connect((lambda gl = True: lambda: self.set_borders(gl))())
        layout.addWidget(self.reset, 4, 0, 1, 4)
        
        self.rest_box.setLayout(layout)
                
        return None

    def execute_result_file(self):
        '''
        Lets the user choose a data file and executes initialization
        
        :reuturns: None
        '''
        
        print('\n-------------------------------------------------------')
        print('Choosing pulse data .csv file for histogram view...')
        
        # Let the user choose a folder
        defaultDir = self.persival.get('execute_result_file')
        defaultDir = posixpath.join(defaultDir, '../')
        file = str(QFileDialog.getOpenFileName(self, "Select Directory", defaultDir)[0])
        
        # In case it is aborted
        if not os.path.isfile(file):
            print('No valid file chosen')
            
        else:
            print('Got data file')
            self.channels = [1]
            self.detectors = ['CH1']
            self.adapt_detectors()
            self.data_files = {}
            self.data_files[1] = file
            self.init_files()
            self.persival.set('execute_result_file', file)
        
        return None 

    def execute_result_folder(self):
        '''
        Lets the user choose a result folder and loads the peak data
        
        :returns: None
        '''
        
        print('\n-------------------------------------------------------')
        print('Choosing results folder for histogram view...')
        
        # Let the user choose a folder
        defaultDir = self.persival.get('execute_result_folder')
        defaultDir = posixpath.join(defaultDir, '../')
        folder = str(QFileDialog.getExistingDirectory(self, "Select Directory", defaultDir))
        
        # In case the choosing is aborted
        if not os.path.isdir(folder): 
            print('No valid folder chosen')

        else:
            print('Got result folder')
            self.folder = folder
            self.get_det_ch_from_ini()
            
            # Check for peak data file(s)
            data_files = {}
            files = os.listdir(self.folder)
            det_sort = np.flip(np.sort(self.detectors))
            for det in det_sort:
                for file in files:
                    if 'peak_data' in file:
                        if det in file:
                            data_files[det] = posixpath.join(self.folder, file)
                            files.remove(file)
                            break

            if not len(data_files) == len(self.detectors):
                self.warn.warn(f'Inconsistency. Found {len(self.detectors)} detectors in summary.ini but {len(data_files)} corresponding data files...')

            self.data_files = {}
            for i, det in enumerate(self.detectors):
                self.data_files[self.channels[i]] = data_files[det]
                
            # Load peak data
            else:
                self.init_files()
                self.persival.set('execute_result_folder', folder)
                        
        print('-------------------------------------------------------')
        
        return None 
     
    def get_det_ch_from_ini(self):
        '''
        Searches for a summary.ini file in the given folder, extracts detector and channel information and 
        adapts the variables and buttons accordingly
        
        :returns: None
        '''
        
        config = None
        
        # Extract .ini information
        for file in os.listdir(self.folder):
            if 'summary.ini' in file:
                try:
                    config = self.ini_handler.read_ini(posixpath.join(self.folder, file), convert = True)
                except: 
                    pass
                break
        
        if config is None:
            self.warn.warn('No summary.ini in folder to extract detector and channel information...')
        else:
            self.detectors = config['BASIC']['detectors']
            self.channels = config['BASIC']['channels']
        
        # Apply changes to the GUI
        self.adapt_detectors()
                
        return None
             
    def adapt_detectors(self):
        '''
        After getting detector and channel information, we need to adapt the corresponding buttons and names
    
        :returns: None
        '''
        
        # Change their link
        self.ch_to_det = {}
        for i, ch in enumerate(self.channels):
            self.ch_to_det[ch] = self.detectors[i]
        
        # Enable buttons
        for ch in self.det_buttons.keys():
            if not ch in self.channels:
                self.det_buttons[ch].setDisabled(True)
            else:
                self.det_buttons[ch].setDisabled(False)
                self.det_buttons[ch].setText(self.ch_to_det[ch])
                
        return None
                     
    def init_files(self):
        '''
        Uses the data file variable and initializes all data and adapts sliders and borders accordingly
        
        :returns: None
        '''

        self.init = True
        
        self.pulse_num = {}
        self.valid_pulses = {}
        self.pulse_num_valid = {}
        self.valid_indices = {}
        self.data = {}
        
        for ch in self.channels:
            #try:
            if True:
                data = pd.read_csv(self.data_files[ch], sep = ';', dtype = float)
                        
                # Get the pulse number and initialize variables for later
                self.pulse_num[ch] = len(data)
                self.valid_pulses[ch] = np.array([True for _ in range(self.pulse_num[ch])])
                self.pulse_num_valid[ch] = len(self.valid_pulses[ch])
                self.valid_indices[ch] = dict.fromkeys(data.columns)
                    
                # Convert into dict of numpy arrays
                self.data[ch] = dict.fromkeys(data.columns)
                for param in self.data[ch].keys():
                    self.data[ch][param] = np.array(data[param])
                    self.valid_indices[ch][param] = np.array([True for _ in range(self.pulse_num[ch])])

                # We disable the corresponding options if not yet given in the data
                for param in self.params:
                    if not param in self.data[ch].keys():
                        self.plot_buttons[param].setDisabled(True)
                        self.slider_left[param].setDisabled(True)
                        self.slider_right[param].setDisabled(True)
                        self.border_left_field[param].setDisabled(True)
                        self.border_right_field[param].setDisabled(True)
                    else:
                        self.plot_buttons[param].setDisabled(False)
                        self.slider_left[param].setDisabled(False)
                        self.slider_right[param].setDisabled(False)
                        self.border_left_field[param].setDisabled(False)
                        self.border_right_field[param].setDisabled(False)   
                        
                # Convert data into more practical units
                for param in self.data[ch].keys():
                    self.data[ch][param] *= self.params_con_f[param]  
                                            
                print(f'{self.ch_to_det[ch]} --> pulse data loaded')
                
            #except:
             #   self.warn.warn('Loading peak data from file not possible...')
        
        # Load the parameter borders and apply to the GUI
        self.rest_valid = np.array([True for _ in range(self.pulse_num[ch])])
        self.get_borders()

        # Now activate the first channel and first plot parameter
        self.plot_ch = self.channels[0]
        self.plot_param = self.params[0]
        self.det_change(self.det_buttons[self.channels[0]], self.channels[0])
        self.param_change(self.plot_buttons[self.params[0]], self.params[0])

        self.init = False
            
        return None
                
    def get_borders(self):
        '''
        Extracts the borders for all parameters from the file data. Adjusts the handles and text fields
        of the range widgets accordingly
        
        :returns: None
        '''
        
        # Initialize the class variables
        self.borders = {}
        self.global_borders = {}
        for ch in self.channels:
            self.borders[ch] = dict.fromkeys(self.data[ch])
            self.global_borders[ch] = {}
        
            # Extract the borders
            for param in self.borders[ch].keys():
                self.borders[ch][param] = []
                min = np.amin(self.data[ch][param])
                max = np.amax(self.data[ch][param])

                # Append to the border variable
                self.borders[ch][param].append(min)
                self.borders[ch][param].append(max)
                self.global_borders[ch][param] = self.borders[ch][param].copy()
                                        
        return None
        
    def set_borders(self, use_global = False):
        '''
        Depending on the current channel, the global borders are used to set all sliders and values 
        according to the given minima and maxima
        
        :param use_global: If True, the global settings are used. If False the last one used
        :returns: None
        '''
        
        if use_global:
            borders = self.global_borders[self.plot_ch].copy()
        else:
            borders = self.borders[self.plot_ch].copy()
        
        for param, val in borders.items():
            min = float(val[0])
            max = float(val[1])
            gmin = self.global_borders[self.plot_ch][param][0]
            gmax = self.global_borders[self.plot_ch][param][1]
            
            # Set the minima and maxima
            self.slider_left[param].setMinimum(gmin)
            self.slider_left[param].setMaximum(gmax)
            self.slider_left[param].setValue(min)
            self.slider_right[param].setMinimum(gmin)
            self.slider_right[param].setMaximum(gmax)
            self.slider_right[param].setValue(max)
            step = (gmax - gmin) / 100
            self.slider_left[param].setSingleStep(step)
            self.slider_right[param].setSingleStep(step)
            
            # Apply the borders to sliders and text fields
            self.border_left_field[param].setText('{:.3n}'.format(min))
            self.border_right_field[param].setText('{:.3n}'.format(max))
                
        return None
        
    def slider_change(self, slider, param, dir):
        '''
        Executes changes when a slider is moved
        
        :returns: None
        '''
        
        val = slider.value()
        ch = self.plot_ch

        # Left/lower borders
        if dir == 'left':
            # Don't exceed the upper border
            upper_val = self.slider_right[param].value()
            if not val <= upper_val:
                val = upper_val
                slider.setValue(val)
            # Change text in corresponding text field
            self.border_left_field[param].setText('{:.3n}'.format(val))
            # Change value in border variable
            self.borders[ch][param][0] = val
        
        # Right/upper borders
        if dir == 'right':
            # Don't surpass the lower border
            lower_val = self.slider_left[param].value()
            if not val >= lower_val:
                val = lower_val
                slider.setValue(val)
            # Change text in corresponding text field
            self.border_right_field[param].setText('{:.3n}'.format(val))
            # Change value in border variable
            self.borders[ch][param][1] = val
        
        # Only update histogram at 
        if not self.init:
            self.filter_hist_data(param)
            self.make_hist()
            
        return None        
       
    def range_field_change(self, field, param, dir):
        '''
        Applies the same changes as slider change
        
        :returns: None
        '''
        
        ch = self.plot_ch
        
        # Get value and restore if invalid
        try:
            val = float(field.text())
        except:
            self.warn.warn('Invalid value, reset to previous...')
            if dir == 'left':
                field.setText('{:.3n}'.format(self.borders[ch][param][0]))
                val = self.borders[ch][param][0]
            else:
                field.setText('{:.3n}'.format(self.borders[ch][param][1]))
                val = self.borders[ch][param][1]
        
        # Left/lower borders
        if dir == 'left':
            # Don't exceed the upper border
            upper_val = float(self.border_right_field[param].text())
            if not val <= upper_val:
                val = upper_val
                field.setText('{:.3n}'.format(self.borders[ch][param][1]))
                self.warn.warn('Inserted value larger lower border, reset...')
            
            if not self.global_borders is None:
                min_val = self.global_borders[ch][param][0]
                if not val >= min_val:
                    val = min_val
                    field.setText('{:.3n}'.format(min_val))
                    self.warn.warn('Inserted value lower than minimum, reset...')
                
            # Change value in corresponding slider
            self.slider_left[param].setValue(val)
            # Change value in border variable
            self.borders[ch][param][0] = val
        
        # Right/upper borders
        if dir == 'right':
            # Don't surpass the lower border
            lower_val = float(self.border_left_field[param].text())
            if not val >= lower_val:
                val = lower_val
                field.setText('{:.3n}'.format(self.borders[ch][param][0]))
                self.warn.warn('Inserted value smaller than lower border, reset...')
                
            if not self.global_borders is None:
                max_val = self.global_borders[ch][param][1]
                if not val <= max_val:
                    val = max_val
                    field.setText('{:.3n}'.format(max_val))
                    self.warn.warn('Inserted value larger than maximum, reset...')
                
            # Change value in corresponding slider
            self.slider_right[param].setValue(val)    
            # Change value in border variable
            self.borders[ch][param][1] = val  
                    
        return None
        
    def det_change(self, button, channel):
        '''
        Executes a change of displaye detector
        
        :param button: Button object which has been pressed
        :param channel: Corresponding channel
        :returns: None
        '''
        
        # Change color of the pressed button
        button.setStyleSheet('background-color : yellow')
        for ch in self.channels:
            if not ch == channel:
                self.det_buttons[ch].setStyleSheet('background-color : none')
                
        self.plot_ch = channel
        self.init = True
        self.set_borders()
        self.init = False
        self.make_hist()
                
        return None
        
    def param_change(self, button, param):
        '''
        Applies changes when the parameter to view is changed
        
        :returns: None
        '''
        
        # First of all change the color of the pressed button
        button.setStyleSheet('background-color : yellow')
        for p in self.params:
            if not p == param:
                self.plot_buttons[p].setStyleSheet('background-color : none')
        
        self.plot_param = param
        if not self.data is None:
            self.make_hist()
        
        return None
             
    def filter_hist_data(self, param):
        '''
        Uses the borders of all parameters to filter valid pulses and create a histogram
        
        :param param: parameter which is currently changed to adapt histogram
        :returns: None
        '''
        
        ch = self.plot_ch
        
        # First we check if the parameter of applied changes is the same as before or not. If not we need to
        # adjust the valid indices regarding all other parameters
        if not self.previous_param == param or not self.previous_ch == ch:
            self.rest_valid = np.array([True for _ in range(self.pulse_num[ch])])
            for p, i in self.valid_indices[ch].items():
                if not p == param:
                    self.rest_valid *= i
        
        # We go through the current parameter and get the valid pulses
        self.valid_indices[ch][param] = (self.data[ch][param] >= self.borders[ch][param][0]) * (self.data[ch][param] <= self.borders[ch][param][1])

        # Update the overall valid pulses
        self.valid_pulses[ch] = self.rest_valid * self.valid_indices[ch][param] 
        self.pulse_num_valid[ch] = np.sum(self.valid_pulses[ch])
        
        self.previous_param = param
        self.previous_ch = ch
            
        return None   
   
    def make_hist(self):
        '''
        Applies the changes accessed by some slider
        
        :returns: None
        '''
        
        # Get the bin number from the GUI field
        bins = int(self.bin_field.text())
        
        # Plot the histogram
        self.axes.cla()
        if self.normalized:
            self.axes.hist(x = self.data[self.plot_ch][self.plot_param][self.valid_pulses[self.plot_ch]], bins = bins, color = 'green', weights = np.ones(self.pulse_num_valid[self.plot_ch]) / self.pulse_num_valid[self.plot_ch] * 100)
        else:
            self.axes.hist(x = self.data[self.plot_ch][self.plot_param][self.valid_pulses[self.plot_ch]], bins = bins, color = 'green')

        self.axes.set_title(f'{self.ch_to_det[self.plot_ch]}, {self.pulse_num_valid[self.plot_ch]}/{self.pulse_num[self.plot_ch]} recorded pulses')
        self.axes.grid(True)
        self.axes.set_xlabel(self.params_con_n[self.plot_param])
        if self.normalized:
            self.axes.set_ylabel('%')
        else:
            self.axes.set_ylabel('#')
        self.canvas.draw()
        
        return None
        
    def execute_save_settings(self):
        '''
        Gathers all information about the border settings and lets the user store them within an .ini file
        
        :returns: None
        '''
        
        print('\n-------------------------------------------------------')
        print('Store border settings for histograms...')
        
        # Let the user choose a save path
        defaultDir = self.persival.get('execute_save_settings')
        defaultDir = posixpath.join(defaultDir, '../')
        defaultDir = posixpath.join(defaultDir, 'histo_settings')
        save_path, _ = QFileDialog.getSaveFileName(self, 'Save histogram borders settings', defaultDir, 'Ini Files (*.ini)')
        
        if os.path.isdir(posixpath.join(defaultDir, '../')):
            self.persival.set('execute_save_settings', save_path)

            # Gather the settings
            settings = {}
            for ch in self.channels:
                settings[self.ch_to_det[ch]] = {}
                for param, vals in self.borders[ch].items():
                    settings[self.ch_to_det[ch]][param] = vals
                    
            # Store as .ini
            self.ini_handler.write_ini(settings, save_path)
            print('Settings stored...')
        
        else:
            print('No save path chosen...')
            
        print('-------------------------------------------------------')
        
        return None
    
    def execute_load_settings(self):
        '''
        Lets the user choose an .ini file containing histogram border settings. If the detector names
        of the given file agree, the settings are applied automatically. If not, the user is asked which
        settings to choose for all detectors
        
        :returns: None
        '''
        
       
        print('\n-------------------------------------------------------')
        print('Load border settings for histograms...')
        
        # Let the user choose a save path
        defaultDir = self.persival.get('execute_load_settings')
        defaultDir = posixpath.join(defaultDir, '../')
        load_path = str(QFileDialog.getOpenFileName(self, 'Select .ini file', defaultDir)[0])
        
        if os.path.isfile(load_path):

            # Gather the settings
            settings = self.ini_handler.read_ini(load_path, convert = True)
            
            # Convert the data and check if it is valid
            dets = list(settings.keys())
            data_good = True
            settings_conv = {}
            for det, info in settings.items():
                settings_conv[det] = {}
                for param, vals in info.items():
                    if not param in self.params:
                        self.warn.warn(f'Invalid parameter in loaded .ini file ({param})')
                        data_good = False
                    else:
                        try:
                            borders = vals.strip('[]').split(',')
                            borders = [float(val) for val in borders]
                            settings_conv[det][param] = borders
                            
                        except:
                            self.warn.warn(f'Invalid values encountered at detector \'{det}\' and parameter \'{param}\'')          
                            data_good = False
                    
            if not data_good:
                print('Invalid data')
            else:
                self.persival.set('execute_load_settings', load_path)
                print('Data is valid...')
                
                # Now we need to check if we deal with the same detectors
                same_det = True
                for det in dets:
                    # If we have the same, we directly transfer the borders
                    if not det in self.detectors:
                        same_det = False
                
                if same_det:
                    # Change detector to correpsonding channel names
                    for det in settings_conv:
                        ch = self.channels[self.detectors.index(det)]
                        self.borders[ch] = settings_conv[det]
                    
                    # Set new borders
                    self.set_borders()
                    
                # Otherwhise the user chooses which settings should be taken
                else:
                    # A pop up window to choose the detector from which settings should be taken is necessary
                    choose_det = QDialog()
                    layout = QVBoxLayout()
                    # Initialize PopUp
                    layout.addWidget(QLabel('One or all detectors given in the .ini file\nare not equal to the data sources.\nChoose detector from which the settings should be adapted'))
                    det_buttons = {}
                    for det in dets:
                        det_buttons[det] = QPushButton(det)
                        det_buttons[det].clicked.connect((lambda opt = det: lambda: self.catch_button(opt))())
                        det_buttons[det].clicked.connect(choose_det.close)
                        layout.addWidget(det_buttons[det])
                    choose_det.setLayout(layout)
                    choose_det.exec_()
            
                    if self.pushbutton in dets:
                        for ch in self.channels:
                            for param in self.borders[ch]:
                                self.borders[ch][param] = settings_conv[self.pushbutton][param]
                        self.set_borders()
                        print('Settings from \'{self.pushbutton}\' loaded...')
                        
                    else:
                        print('Invalid choice')

        else:
            print('Aborted...')
            
        print('-------------------------------------------------------')
        
        return None
    
    def catch_button(self, option):
        '''
        Solely to feed a class variable with the return string of a button
        
        :param option: Said string
        :returns: None
        '''
        
        self.pushbutton = option
                
        return None
    
    def closeEvent(self, event):
        '''
        Before closing the application we need to save the data persival collected

        :returns: None
        ''' 
                
        self.persival.save()
        event.accept() 
        
        return None
        

if __name__ == '__main__':
    ini_gui = QApplication(sys.argv)
    gui = GUI_Hist_Review()
    sys.exit()