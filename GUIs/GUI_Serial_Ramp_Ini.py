'''
    File name: GUI_Serial_Ramp_Ini.py
    Author: Philipp Gaggl
    Date created: --.--.2022
    Python Version: 3.8, 3.9
'''

import sys
import os

# Allow imports from parent directory if this GUI is launched directly
if __name__ == '__main__':
    current = os.path.dirname(os.path.realpath(__file__))
    parent = os.path.dirname(current)
    sys.path.append(parent)

import posixpath
from tool_box import Warn_Me, pqt5_exception_workaround, PopUp
from Run_Config import Ini_Handler

from PyQt5.QtCore import pyqtSignal, pyqtRemoveInputHook
from PyQt5.QtWidgets import (QApplication, QCheckBox, QDialog, QGroupBox, QHBoxLayout, QLineEdit, QPushButton, QGridLayout)
from PyQt5.QtGui import QFont


# We overwrite events for focusing in an out of QlineEdit instances. That way we can check valid values
# as soon as the user exits the textfield even without pressing enter
class QLineEdit(QLineEdit):
    focus_in_signal = pyqtSignal()
    focus_out_signal = pyqtSignal()

    def focusInEvent(self, event):
        self.focus_in_signal.emit()
        super().focusInEvent(event)

    def focusOutEvent(self, event):
        super().focusOutEvent(event)
        self.focus_out_signal.emit()

class GUI_Serial_Ramp_Ini(QDialog):
    '''
    Window that lets the user choose a ramp parameter, min max and increment to create multiple config
    files and place them in the corresponding folder.
    '''
    
    def __init__(self, target_dir, config, parent = None):
        '''
        :returns: None
        '''
        
        super(GUI_Serial_Ramp_Ini, self).__init__(parent)
        
        # Helpers
        self.ini_handler = Ini_Handler()
        self.warn = Warn_Me()
        self.popup = PopUp()        
        
        # Some variables
        self.options = self.ini_handler.ramp_parameters.copy()
        self.input_options = ['equidistant', 'custom']
        self.units = ['MeV', 'kHz', 'V']
        # Allow for custom parameter if checked
        if config['BASIC']['use_custom_ramp_param']:
            self.options.append(config['BASIC']['custom_ramp_param'])
            self.units.append(config['BASIC']['custom_ramp_unit'])
            # Also we need to change the key-list in the Ini_Handler() for the duration of this GUI
            self.ini_handler.valid_keys.append(config['BASIC']['custom_ramp_param'])
        self.label_options = ['max', 'min', 'step']
        self.min = None
        self.max = None
        self.increment = None
        self.target = target_dir
        self.set_option = None
        self.set_input = None
        self.config = config
        
        # Get the detectors
        self.detectors = self.config['BASIC']['detectors']
        self.ramp_det = None
        
        # We warn the user if there is already content within the target directory
        if len(os.listdir(self.target)) > 0:
            self.warn.warn('Attention: There already are elements in the target directory')
        
        self.make_window()
        
        return None
        
    def make_window(self):
        '''
        Creates the window
        
        :returns: None
        '''
        
        # Set size and position
        self.resize(100, 100)
        self.move(1300, 400)
        
        layout = QGridLayout()
        
        # Options
        param_box = QGroupBox('Ramp parameter')
        param_box.setFont(QFont('Times', 8))
        param_layout = QHBoxLayout()
        self.option_buttons = {}
        for opt in self.options:
            self.option_buttons[opt] = QCheckBox(opt)   
            if opt == 'bias_voltage':
                self.option_buttons[opt].setChecked(True) 
                self.set_option = 'bias_voltage'
            param_layout.addWidget(self.option_buttons[opt])
            self.option_buttons[opt].stateChanged.connect((lambda option = opt: lambda: self.execute_option_param_check(option))())
        param_box.setLayout(param_layout)
        
        # Detectors for bias voltage
        det_box = QGroupBox('Detector to ramp')
        det_box.setFont(QFont('Times', 8))
        det_layout = QHBoxLayout()
        self.det_buttons = {}
        for i, det in enumerate(self.detectors):
            self.det_buttons[det] = QCheckBox(det)
            if i == 0:
                self.det_buttons[det].setChecked(True)
                self.ramp_det = det
            det_layout.addWidget(self.det_buttons[det])
            self.det_buttons[det].stateChanged.connect((lambda detector = det: lambda: self.execute_det_check(detector))())
        det_box.setLayout(det_layout)
        
        # Input mode
        input_box = QGroupBox('Ramp format')
        input_box.setFont(QFont('Times', 8))
        input_layout = QHBoxLayout()
        self.input_buttons = {}
        for mode in self.input_options:
            self.input_buttons[mode] = QCheckBox(mode)
            if mode == 'equidistant':
                self.input_buttons[mode].setChecked(True)
                self.set_input = 'equidistant'
            input_layout.addWidget(self.input_buttons[mode])
            self.input_buttons[mode].stateChanged.connect((lambda input = mode: lambda: self.execute_input_check(input))())
        input_box.setLayout(input_layout)
        
        # Min
        self.min_box = QGroupBox('min [V]')
        self.min_box.setFont(QFont('Times', 8))
        min_layout = QHBoxLayout()
        self.min = QLineEdit('10')
        self.min.returnPressed.connect((lambda object = self.min, opt = 'min': lambda: self.check_value(object, opt))())
        self.min.focus_out_signal.connect((lambda object = self.min, opt = 'min': lambda: self.check_value(object, opt))())

        min_layout.addWidget(self.min)
        self.min_box.setLayout(min_layout)
        
        # Min
        self.max_box = QGroupBox('max [V]')
        self.max_box.setFont(QFont('Times', 8))
        max_layout = QHBoxLayout()
        self.max = QLineEdit('100')
        self.max.returnPressed.connect((lambda object = self.max, opt = 'max': lambda: self.check_value(object, opt))())
        self.max.focus_out_signal.connect((lambda object = self.max, opt = 'max': lambda: self.check_value(object, opt))())
        max_layout.addWidget(self.max)
        self.max_box.setLayout(max_layout)
        
        # Increment
        self.inc_box = QGroupBox('step [V]')
        self.inc_box.setFont(QFont('Times', 8))
        inc_layout = QHBoxLayout()
        self.inc = QLineEdit('10')
        self.inc.returnPressed.connect((lambda object = self.inc, opt = 'step': lambda: self.check_value(object, opt))())
        self.inc.focus_out_signal.connect((lambda object = self.inc, opt = 'step': lambda: self.check_value(object, opt))())
        inc_layout.addWidget(self.inc)
        self.inc_box.setLayout(inc_layout)
        
        # User values
        self.custom_box = QGroupBox('Custom values (separate with \';\')')
        custom_layout = QHBoxLayout()
        self.custom = QLineEdit('10; 20')
        self.custom.setDisabled(True)
        custom_layout.addWidget(self.custom)
        self.custom_box.setLayout(custom_layout)
        
        # Create button
        self.create = QPushButton('create ramp folders including config files')
        self.create.clicked.connect(self.execute_create)
        
        # Layout
        layout.addWidget(param_box, 0, 0, 1, 3)
        layout.addWidget(det_box, 1, 0, 1, 3)
        layout.addWidget(input_box, 2, 0, 1, 3)
        layout.addWidget(self.min_box, 3, 0, 1, 1)
        layout.addWidget(self.max_box, 3, 1, 1, 1)
        layout.addWidget(self.inc_box, 3, 2, 1, 1)
        layout.addWidget(self.custom_box, 4, 0, 1, 3)
        layout.addWidget(self.create, 5, 0, 1, 3)

        self.setLayout(layout)
        
        # Open GUI
        pqt5_exception_workaround()
        pyqtRemoveInputHook()
        self.exec_()
        
        return None

    def execute_option_param_check(self, option):
        '''
        Ensures that only one option is checked at once
        
        :returns: None
        '''

        # We need to disconnect in between to not cause an avalanche
        for box in self.option_buttons.values():
            box.disconnect()
        # Assure right checking
        for opt, box in self.option_buttons.items():
            box.setChecked(opt == option)
        # Connect again
        for opt, box in self.option_buttons.items():
            box.stateChanged.connect((lambda op = opt: lambda: self.execute_option_param_check(op))())

        # Also, we need to adapt the displayed units for the values
        i = self.options.index(option)
        self.min_box.setTitle(f'min [{self.units[i]}]')
        self.max_box.setTitle(f'max [{self.units[i]}]')
        self.inc_box.setTitle(f'step [{self.units[i]}]')
        
        # Also update the set option
        self.set_option = option
        
        # Emit a signal for the values, to force a checking 
        self.check_value(self.min, 'min')
        self.check_value(self.max, 'max')
        self.check_value(self.inc, 'step')
        
        # Disable detectors if not bias voltage
        for box in self.det_buttons.values():
            box.setDisabled(not self.set_option == 'bias_voltage')
        
        return None
    
    def execute_det_check(self, detector):
        '''
        Ensures only one detector is checked and adapts the class variable
        
        :param detector: The handle of the button
        :returns: None
        '''
        
        # Disconnect
        for box in self.det_buttons.values():
            box.disconnect()
        
        # Uncheck other
        for det, box in self.det_buttons.items():
            if not det == detector:
                box.setChecked(False)

        # Connect again
        for det, box in self.det_buttons.items():
            box.stateChanged.connect((lambda detector = det: lambda: self.execute_det_check(detector))())
        
        self.ramp_det = detector
        
        return None
        
    def execute_input_check(self, input):
        '''
        Ensures only one input option is checked and enables/disables other options accordingly
        
        :param input: Handle of the box
        :returns: None
        '''
        
        # Disconnect
        for box in self.input_buttons.values():
            box.disconnect()
        
        # Uncheck other
        for mode, box in self.input_buttons.items():
            if not mode == input:
                box.setChecked(False)

        # Connect again
        for mode, box in self.input_buttons.items():
            box.stateChanged.connect((lambda input = mode: lambda: self.execute_input_check(input))())
        
        self.set_input = input
        # Disable corresponding options
        self.custom.setDisabled(not input == 'custom')
        self.min.setDisabled(input == 'custom')
        self.max.setDisabled(input == 'custom')
        self.inc.setDisabled(input == 'custom')
        
        return None       
        
    def check_value(self, object, opt):
        '''
        Checks if the written value is a positive integer and defaults to zero if not.
        
        :param object: The QLineEdit object itself
        :param opt: min, max or step
        :returns: None
        '''
        
        val = object.text()
        
        # Check for integer
        try:
            val = float(val)
        except:
            self.warn.warn(f'{val} is no valid integer')
            object.setText('10')
            return None
            
        # Check if non zero
        if val == 0:
            self.warn.warn('0 is not an allowed value')
            object.setText('10')
            return None
        
        # Check sign
        if not self.set_option == 'bias_voltage' and not self.set_option == self.config['BASIC']['custom_ramp_param'] or opt == 'step':
            if val < 0:
                self.warn.warn('Negative values only allowed for bias voltage')
                object.setText('10')
                return None
            
        # Check sizes
        min = float(self.min.text())
        max = float(self.max.text())
        inc = float(self.inc.text())
        if opt == 'min':
            if not min < max:
                self.min.setText(str(max - inc))
                self.warn.warn('min must be smaller than max')
        elif opt == 'max':
            if not max > min:
                self.max.setText(str(min + inc))
                self.warn.warn('max must be greater than min')
        elif opt == 'step':
            if not inc <= (max - min):
                self.inc.setText(str(max - min))
                self.warn.warn('step must fit between min and max')

        return None
    
    def execute_create(self):
        '''
        Checks for the validity of the given values. If so, creates folders with corresponding naming within the
        target folder and places the right config.inis therein.
        
        :returns: None
        '''
        
        # Check the validity of the folder
        if not os.path.isdir(self.target):
            self.warn.warn('The given directory does not exist')
            return None
        
        # We check the given values one more time and then create a value list depending on the input mode
        # For equdistant
        if self.set_input == 'equidistant':
            min = float(self.min.text())
            max = float(self.max.text())
            inc = float(self.inc.text())
            
            # First, check for validity of the values
            if not max > (min + inc):
                self.warn.warn('Maximum too small')
                return None
            if not min < (max - inc):
                self.warn.warn('Minimum too large')
                return None
            if not (max - min) % inc == 0:
                self.warn.warn('Equidistant steps not possible for given values')
                return None
            
            # Create the list
            val_list = []
            for i in range(0, int((max - min + inc) / inc)):
                val_list.append(min + (i * inc))
        
        # For custom values
        else:
            # First extract the values
            val_list = self.custom.text().split(';')
            for i in range(0, len(val_list)):
                val_list[i] = val_list[i].replace(' ', '')

            # Now check if all parameters are valid
            for i in range(0, len(val_list)):
                try:
                    val_list[i] = float(val_list[i])
                except:
                    self.warn.warn(f'The value {val_list[i]} is not valid')
                    return None
        
        # Sort the values
        val_list.sort()
        
        # Now that we have our values, we create our folders
        opt_i = self.options.index(self.set_option)
            
        folder_names = []
        for val in val_list:
            folder_names.append("{:.1f}".format(val) + self.units[opt_i])
        
        # For each one, we check if it already exists. If it is empty, we just delete it. Otherwise we ask the
        # user if it should be overwritten
        go_on = False
        for folder in folder_names:
            dir = posixpath.join(self.target, folder)
            print(dir)
            # Check if folder already exists. If it is empty, skip it. If not, ask the user ONCE if we still want to place the config file there. 
            if os.path.isdir(dir):
                # Check if it is empty
                if len(os.listdir(dir)) > 0:
                    # We only ask one time
                    if not go_on:
                        go_on = self.popup.window('Existing folder found', f'The folder {folder} already exists and is not empty. If you want to create the .ini nontheless, click \'Yes\'. This will not be asked again and the procedure will be repeated for similar cases. If you choose no, the operation will be aborted to allow for manual checking.')
                    if go_on:
                        print(f'Config files will be placed within existing...')
                    else:
                        self.warn.warn('Aborted to check for existing folders')
                        return None
                    self.popup = PopUp()
            else:
                os.mkdir(dir)
            
        print('Created ramp folders...')
        
        # Now we fill the folders with the corresponding .ini files
        for i, val in enumerate(val_list):
            # Create the ini content
            ini = self.adapt_config(val, i)
            
            # Store it into the corresponding folder
            if not ini is None:
                ini_name = f'run{int(val)}{self.units[self.options.index(self.set_option)]}.ini'
                ini_dir = posixpath.join(self.target, folder_names[i], ini_name)
                try:
                    self.ini_handler.write_ini(ini, ini_dir, all_defined = True, displayed = False)
                except:
                    self.warn.warn(f'Error for config creation in folder {folder_names[i]}')
                    return None
        
        print('Created ramp config files...')
        
        return None
        
    def adapt_config(self, value, run_no):
        '''
        Receives the options for the ramp config.ini and returns a corresponding dict to write out based on the 
        original settings from Gui_Make_Run_Ini.
        
        :param value: The value this parameter should be changed to
        :param run_no: Integer of the run number to give
        :returns: A dict ready to write out as .ini. None if something failed
        '''
        
        if not self.set_option in self.options:
            self.warn.warn('Unknown parameter, no ini created')
            return None
        
        ini = self.config.copy()
        
        # We also adapt the run number and assign the same value as the ramp value as integer
        ini['BASIC']['run_no'] = run_no
        
        # Adapt the value
        if self.set_option == 'bias_voltage':
            det_i = self.detectors.index(self.ramp_det)
            ini['BASIC'][self.set_option][det_i] = value
        else:
            # We need to have a check for the custom parameter
            if not self.set_option in self.ini_handler.ramp_parameters:
                ini['BASIC']['custom_ramp_val'] = value
            else:
                ini['BASIC'][self.set_option] = value
                
        return ini
            
    def closeEvent(self, event):
        '''
        Exits the GUI
        
        :returns: None
        '''
        
        print('Ramp GUI closed...')
        event.accept()
        
        return None
        
    
if __name__ == '__main__':
    
    # Minimal dict to launch the ramp GUI
    config = {'BASIC':{'detectors':['Si', 'SiC'], 'bias_voltage':[100, 200]}}
    # Change directory as desired
    dir = r'C:\Users\pgagg\Documents\Arbeit_HEPHY\virtual_machine\readout_programms\RS_DRS4_run_analysis\Philipp\ini_check'
    import sys
    pyqtRemoveInputHook()
    ini_gui = QApplication(sys.argv)
    gallery = GUI_Serial_Ramp_Ini(dir, config)
    sys.exit() 