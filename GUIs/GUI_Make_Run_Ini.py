'''
    File name: GUI_Make_Run_Ini.py
    Author: Philipp Gaggl
    Date created: 26.11.2021
    Python Version: 3.8, 3.9
'''

import sys
import os

# Allow imports from parent directory if this GUI is launched directly
if __name__ == '__main__':
    current = os.path.dirname(os.path.realpath(__file__))
    parent = os.path.dirname(current)
    sys.path.append(parent)

import shutil
import posixpath

from tool_box import Persival, PopUp, Warn_Me, pqt5_exception_workaround
from First_Call import First_Call
from PyQt5.QtCore import pyqtRemoveInputHook
from PyQt5.QtWidgets import (QApplication, QCheckBox, QDialog, 
        QGridLayout, QGroupBox, QHBoxLayout, QLabel, QLineEdit, QPushButton, 
        QVBoxLayout, QFileDialog)
from PyQt5.QtGui import QFont

from Run_Config import Ini_Handler
from GUIs.GUI_Serial_Ramp_Ini import GUI_Serial_Ramp_Ini
from GUIs.GUI_Serial_Ramp_Ini import QLineEdit


class GUI_Make_Run_Ini(QDialog):
    '''
    Opens a GUI to type in necessary and optional run information, choose a run folder and then create 
    the corresponding .ini file (necessary for the analysis) inside said folder
    '''
    
    def __init__(self, parent = None):
        super(GUI_Make_Run_Ini, self).__init__(parent)

        # Initialize call (Creates current_config.ini if first start of GUI after cloning repo)
        first_call = First_Call()

        # Parameters needed later on
        self.config_file = None
        self.current_ini = first_call.config_file
        self.temp_ini = 'temp_config.ini'
        self.target_folder = None
        
        # First, we copy the current configurations onto a working copy for the GUI.
        self.config_path = first_call.config_path
        self.current_ini_path = first_call.current_config
        self.temp_ini_path = posixpath.join(self.config_path, self.temp_ini)
        shutil.copy(self.current_ini_path, self.temp_ini_path)
        
        # Initiate the Ini_Handler() for reading/writing ini files and checking the configs.
        # Also the class variables where converted and unconverted ini content will be stored
        self.ini_handler = Ini_Handler()
        self.warn = Warn_Me()
        self.popup = PopUp()
        self.config = None
        self.config_str = None
        self.loaded = None
        self.loaded_str = None
        
        # Load settings last used.
        self.get_ini_config() 
        # Remembers files between sessions.
        self.persival = Persival()
        pwd = os.getcwd()
        self.persival.setDefault('execute_ini_folder', pwd)
        self.persival.setDefault('execute_load_from_ini', pwd)
        
        # Initialize the class variable storing all editable content. Create and open the GUI window.
        self.gui = dict.fromkeys(self.config, {})
        self.make_gui_widgets()

        self.create_window()
        
        return None
    
    def get_ini_config(self, user_config = None):
        '''
        Loads the configuration information of a user_config.ini, using the ini_handler. Does not return
        the content, but rather stores it into class variables in order for it to be called by 
        signal events of the GUI. If the temp_config.ini is read out, the content is stored into
        self.config, into self.loaded otherwise. The data is converted, while the unconverted content
        is stored in a variable with the prefix _str.
        
        :param user_config: Name of the user configuration (with or without .ini)
        :returns: None
        '''
        
        # If called without specific setting file, the temp_settings.ini is loaded
        if user_config == None:
            user_config = self.temp_ini
        # Accept both, only the filename as well as with the right data format
        else:
            if not user_config.split('.')[-1] == 'ini':
                user_config += '.ini'
        file = posixpath.join(self.config_path, user_config)
        
        # Still check
        if not os.path.isfile(file):
            print('No valid user configuration...')
            return None
        else:
            if user_config == self.temp_ini:
                self.config = self.ini_handler.read_ini(file, convert = True, all_defined = True, display = False)
                self.config_str = self.ini_handler.read_ini(file, convert = False, all_defined = True, display = False)
            else:
                self.loaded = self.ini_handler.read_ini(file, convert = True, all_defined = True, display = False)
                self.loaded_str = self.ini_handler.read_ini(file, convert = False, all_defined = True, display = False)
        
        return None
    
    def set_ini_config(self, config, user_config = None):
        '''
        Stores parameters in a config file. If none is given, the temp_config.ini file is set. 
        
        :param config: Dictionary with the config information
        :user_config: Name of the user config file. If none is given, the temporal is overwritten
        '''

        # By default, we update the temporal settings
        if user_config is None:
            user_config = self.temp_ini
            
        # Accept both, only the filename as well as with the right data format
        if not user_config.split('.')[-1] == 'ini':
            user_config += '.ini'
        
        # Get directory
        directory = posixpath.join(self.config_path, user_config)
        
        # Write the ini
        self.ini_handler.write_ini(config, directory, all_defined = True, displayed = False)

        return None
      
    def read_window(self):
        '''
        Reads all parameters in the window and returns a corresponding config dict. Similar to 
        make_gui_widgets, where we use the definitions of the parameters in Run_Config.py to create
        the widgets and their initial settings, we use them here to efficiently read them out.
        Updates the temp_config.ini and the internal parameter data.
        
        :returns: None
        '''
 
        window = {}
        
        # Loop over all widgets and extract information according to their parameter definition in the
        # Ini_Handler() class. 
        for sec, params in self.config.items():
            window[sec] = {}
            for param in params.keys():
                definition = self.ini_handler.all[param]
                
                # Cases with no options
                if definition['options'] is None:
                
                    # Cases without list nature. int, float and str are read out simply from QLineEdit text.
                    if definition['max_list_entries'] is None:
                        # Boolean 
                        if definition['type'] is bool:
                            window[sec][param] = self.gui[sec][param].isChecked()
                        # Text
                        else:
                            window[sec][param] = self.gui[sec][param].text()

 
                    # Otherwise, we need some formatting. Also, we need two cases for 'channels' and others
                    else:
                        list_val = []
                        if param == 'channels':
                            for ch_num, box in self.gui[sec][param].items():
                                if box.isChecked():
                                    list_val.append(ch_num)
                        else:
                            for ch_num, box in self.gui[sec][param].items():
                                # Only use if the channel is checked too, even if there is some text
                                if self.gui[sec]['channels'][ch_num].isChecked():
                                    list_val.append(box.text())
                        window[sec][param] = list_val
                
                # If we deal with different options, we check each checkbox of those options
                else:
                    for opt, box in self.gui[sec][param].items():
                        if box.isChecked():
                            window[sec][param] = opt
                            break
            
            # Write the window data into the preview_settings.ini and overwrite the internal data
            self.set_ini_config(window)
            self.get_ini_config()
            
        return None
     
    def update_window(self):
        '''
        Updates the displayed configurations with the current internal data. The process is similar to the
        the widget creation in make_gui_widgets, as the predefinitions in the Ini_Handler() class are 
        used to do it efficiently.
        
        :returns: None
        '''
        
        # Check if we have all necessary parameters and not deal with an old file
        for param in self.ini_handler.basic_run_params:
            if not param in list(self.config['BASIC'].keys()):
                missing = param
                self.warn.warn(f'The mandatory parameter \'{missing}\' is missing in the loaded data, probably because it is too old. Add it artificially or create a new one.')
                return None
        for param in self.ini_handler.optional_run_params:
            if not param in list(self.config['OPTIONAL'].keys()):
                missing = param
                self.warn.warn(f'The optional parameter \'{missing}\' is missing in the loaded data, probably because it is too old. Add it artificially or create a new one.')
                return None  
        
        # Loop over all widgets and extract information according to their parameter definition in the
        # Ini_Handler() class. 
        for sec, params in self.config.items():
            for param in params.keys():
                definition = self.ini_handler.all[param]
                
                # Cases with no options
                if definition['options'] is None:
                
                    # Cases without list nature. int, float and str are read out simply from QLineEdit text.
                    if definition['max_list_entries'] is None:
                        # Bool
                        if definition['type'] is bool:
                            self.gui[sec][param].setChecked(self.config[sec][param])
                        # Text
                        else:
                            self.gui[sec][param].setText(self.config_str[sec][param])
                
                    # Otherwise, we need some formatting. Also, we need two cases for 'channels' and others
                    else:
                        if param == 'channels':
                            for ch, box in self.gui[sec][param].items():
                                box.setChecked(ch in self.config[sec][param])
                        else:
                            # Only use if the channel is checked too
                            for i, val in enumerate(self.config[sec][param]):
                                channel = self.config['BASIC']['channels'][i]
                                self.gui[sec][param][channel].setText(str(val))
                
                # If we deal with different options, we only check the right box
                else:
                    self.gui[sec][param][self.config[sec][param]].setChecked(True)
                    
        return None
            
    def make_gui_widgets(self):
        '''
        Using the parameter definitions in Run_Congif.py, we assign all analysis parameters to the 
        corresponding widgets by a loop, including their initial values according to the current settings.
        Booleans get a checkbox, int and float a QlineEdit. Parameters that need to be one of several
        options get multiple checkboxes. 
        These widgets can then be built in into the layout with the desired labels and connected to 
        further events and functions in the corresponding methods.
        
        :returns: None 
        '''
        
        for sec in self.config.keys():
            for param in self.config[sec].keys():
                definition = self.ini_handler.all[param]

                # Cases with no options.
                if definition['options'] is None:
                    
                    # Cases without list nature
                    if definition['max_list_entries'] is None:
                        # Checkbox for bool
                        if definition['type'] is bool:
                            self.gui[sec][param] = QCheckBox()
                            self.gui[sec][param].setChecked(self.config[sec][param])
                        # QLineEdit for str, int and float.
                        else:
                            self.gui[sec][param] = QLineEdit(self.config_str[sec][param])
                            # These boxes are connected for validation after editing (pressing enter and going out of focus).
                            self.gui[sec][param].returnPressed.connect((lambda object = self.gui[sec][param], section = sec, parameter = param: lambda: self.check_parameter(object, section, parameter))())
                            self.gui[sec][param].focus_out_signal.connect((lambda object = self.gui[sec][param], section = sec, parameter = param: lambda: self.check_parameter(object, section, parameter))())

                    # If we have a list, we create as much widgets as entries
                    else:
                        self.gui[sec][param] = {}
                        # Also, we need different widgets. Checkboxes for 'channels' and QLineEdits else
                        for i in range(1, (definition['max_list_entries'] + 1)):
                            if param == 'channels':
                                self.gui[sec][param][i] = QCheckBox(str(i))
                                self.gui[sec][param][i].setChecked(i in self.config[sec][param])
                            else:
                                if i in self.config['BASIC']['channels']:
                                    index = self.config['BASIC']['channels'].index(i)
                                    text = str(self.config[sec][param][index])
                                    self.gui[sec][param][i] = QLineEdit(text)
                                else:
                                    self.gui[sec][param][i] = QLineEdit('')                
                                # Connect to checking function
                                self.gui[sec][param][i].returnPressed.connect((lambda object = self.gui[sec][param][i], section = sec, parameter = param, ch_num = i: lambda: self.check_channel_parameter(object, section, parameter, ch_num))())
                                self.gui[sec][param][i].focus_out_signal.connect((lambda object = self.gui[sec][param][i], section = sec, parameter = param, ch_num = i: lambda: self.check_channel_parameter(object, section, parameter, ch_num))())

                # If we deal with different possible options for parameters they are given as multiple 
                # checkboxes stored in a dict with the keys corresponding to the options.   
                else:
                    self.gui[sec][param] = {}
                    for opt in definition['options']:
                        self.gui[sec][param][opt] = QCheckBox(opt)
                        self.gui[sec][param][opt].setChecked(self.config[sec][param] == opt)
                               
        return None       
      
    def create_window(self):
        '''
        Layout, buttons, and everything else that is displayed within the GUI.
        
        :returns: None
        '''
        
        # Set size and position
        self.resize(700,700)
        self.move(675, 10)
        
        # Create all boxes
        self.create_beam_box()
        self.create_osci_box()
        self.create_optional_box()
        self.create_button_box()
        
        # Set layout
        main_layout = QGridLayout()
        main_layout.setSpacing(0)
        main_layout.addWidget(self.basicBox, 0, 0)
        main_layout.addWidget(self.osci_box, 1, 0)
        main_layout.addWidget(self.optional_box, 0, 1)
        main_layout.addWidget(self.button_box, 1, 1)
        self.setLayout(main_layout)
        
        # Open GUI
        pqt5_exception_workaround()
        pyqtRemoveInputHook()
        self.exec_()
        
        return None
          
    def create_beam_box(self):
        '''
        Includes mandatory beam settings
        
        :returns: None
        '''
        
        self.basicBox = QGroupBox('Beam properties -- mandatory')
        self.basicBox.setFont(QFont('Times',8))
    
        # Nested layouts
        layout = QVBoxLayout()
        layout_run = QHBoxLayout()
        layout_particle = QHBoxLayout()
        layout_energy = QHBoxLayout()
        layout_rate = QHBoxLayout()
        layout_custom = QHBoxLayout()
        layout_config = QHBoxLayout()

        layout_run.addWidget(QLabel('run number'))
        layout_run.addWidget(self.gui['BASIC']['run_no'])
        layout_particle.addWidget(QLabel('particle'))
        layout_particle.addWidget(self.gui['BASIC']['particle'])
        layout_energy.addWidget(QLabel('particle energy (MeV)'))
        layout_energy.addWidget(self.gui['BASIC']['energy'])
        layout_rate.addWidget(QLabel('beam rate (kHz)'))        
        layout_rate.addWidget(self.gui['BASIC']['rate'])
        layout_custom.addWidget(QLabel('user parameter'))
        layout_custom.addWidget(self.gui['BASIC']['use_custom_ramp_param'])
        self.gui['BASIC']['use_custom_ramp_param'].stateChanged.connect(self.execute_custom_param_check)   
        layout_custom.addWidget(QLabel('name'))
        layout_custom.addWidget(self.gui['BASIC']['custom_ramp_param'])
        if not self.gui['BASIC']['use_custom_ramp_param'].isChecked():
            self.gui['BASIC']['custom_ramp_param'].setDisabled(True)
        layout_custom.addWidget(QLabel('value'))
        layout_custom.addWidget(self.gui['BASIC']['custom_ramp_val'])
        if not self.gui['BASIC']['use_custom_ramp_param'].isChecked():
            self.gui['BASIC']['custom_ramp_val'].setDisabled(True)
        layout_custom.addWidget(QLabel('unit'))
        layout_custom.addWidget(self.gui['BASIC']['custom_ramp_unit'])
        if not self.gui['BASIC']['use_custom_ramp_param'].isChecked():
            self.gui['BASIC']['custom_ramp_unit'].setDisabled(True)
        layout_config.addWidget(QLabel('beam configuration'))
        layout_config.addWidget(self.gui['BASIC']['configuration'])
        
        layout.addLayout(layout_run)
        layout.addLayout(layout_particle)
        layout.addLayout(layout_energy)
        layout.addLayout(layout_rate)
        layout.addLayout(layout_custom)
        layout.addLayout(layout_config)
        
        self.basicBox.setLayout(layout)
        
        return None
        
    def create_osci_box(self):
        '''
        Includes mandatory oscilloscope settongs.
        
        :returns: None
        '''
        
        self.osci_box = QGroupBox('Measurement information -- mandatory')
        self.osci_box.setFont(QFont('Times',8))
        layout = QVBoxLayout()
        
        # Oscilloscope
        layout_osci = QHBoxLayout()
        layout_osci.addWidget(QLabel('used source:'))
        for key, box in self.gui['BASIC']['oscilloscope'].items():
            layout_osci.addWidget(box)
            box.stateChanged.connect((lambda osci = key: lambda: self.check_osci(osci))())
            
        # Channels
        layout_channel = QHBoxLayout()
        layout_channel.addWidget(QLabel('used channels:'))
        for num, box in self.gui['BASIC']['channels'].items():  
            layout_channel.addWidget(box)
            # Assure enabling and disabling of detector names and bias voltages according to ticked channels
            box.stateChanged.connect((lambda ch_num = num: lambda: self.check_used_channels(ch_num))())
            
        # Detectors
        layout_det = QHBoxLayout()
        layout_det.addWidget(QLabel('detectors:'))
        for num, box in self.gui['BASIC']['detectors'].items():
            layout_det.addWidget(box)
            box.setDisabled(not self.gui['BASIC']['channels'][num].isChecked())
            
        # Bias voltages
        layout_bias = QHBoxLayout()
        layout_bias.addWidget(QLabel('bias voltages:'))
        for num, box in self.gui['BASIC']['bias_voltage'].items():
            layout_bias.addWidget(box)
            box.setDisabled(not self.gui['BASIC']['channels'][num].isChecked())
        
        # Layout
        layout.addLayout(layout_osci)
        layout.addLayout(layout_channel)
        layout.addLayout(layout_det)
        layout.addLayout(layout_bias)
        
        self.osci_box.setLayout(layout)
        
        return None
        
    def create_optional_box(self):
        '''
        Includes optional settings.
        
        :returns: None
        '''
        
        self.optional_box = QGroupBox('Additional information -- optional')
        self.optional_box.setFont(QFont('Times',8))
        
        layout = QVBoxLayout()
        # Degrader
        layout_degrader = QHBoxLayout()
        layout_degrader.addWidget(QLabel('degrader setting'))
        layout_degrader.addWidget(self.gui['OPTIONAL']['degrader'])
        
        # Geometry
        layout_geom = QHBoxLayout()
        layout_geom.addWidget(QLabel('geometry information'))
        layout_geom.addWidget(self.gui['OPTIONAL']['geometry'])
        
        # ISO distance
        layout_iso = QHBoxLayout()
        layout_iso.addWidget(QLabel('iso distance (mm)'))
        for num, box in self.gui['OPTIONAL']['iso_distance'].items():
            layout_iso.addWidget(box)
            box.setDisabled(not self.gui['BASIC']['channels'][num].isChecked())
        
        # Start time
        layout_time = QHBoxLayout()
        layout_time.addWidget(QLabel('start time'))
        layout_time.addWidget(self.gui['OPTIONAL']['start_time'])
        
        # Additional info
        layout_info = QHBoxLayout()
        layout_info.addWidget(QLabel('additional information'))
        layout_info.addWidget(self.gui['OPTIONAL']['additional_info'])
        
        # Layout
        layout.addLayout(layout_degrader)
        layout.addLayout(layout_geom)
        layout.addLayout(layout_iso)
        layout.addLayout(layout_time)
        layout.addLayout(layout_info)
        
        self.optional_box.setLayout(layout)
        
        return None
        
    def create_button_box(self):
        '''
        Includes all buttons to execute orders.
        
        :returns: None
        '''
        
        self.button_box = QGroupBox('Execute settings')
        layout = QVBoxLayout()
                
        # Choose target folder
        self.target_box = QGroupBox('Target folder selection')
        self.target_box.setFont(QFont('Times',8))
        layout_target = QVBoxLayout()
        # Folder button
        self.target = QPushButton('choose target folder')
        self.target.clicked.connect(self.execute_ini_folder)
        # Layout
        layout_target.addWidget(self.target)
        self.target_box.setLayout(layout_target)
        
        # Create config ini(s)
        self.create_ini_box = QGroupBox('Create configuration file(s)')
        layout_create = QHBoxLayout()
        # Single config ini
        self.create_single = QPushButton('create single config .ini')
        self.create_single.setDisabled(True)
        self.create_single.clicked.connect(self.execute_create_ini)
        # Ramp config inis
        self.create_ramp = QPushButton('create ramp configuration')
        self.create_ramp.setDisabled(True)
        self.create_ramp.clicked.connect(self.execute_create_ramp)
        ####
        # Layout
        layout_create.addWidget(self.create_single)
        layout_create.addWidget(self.create_ramp)
        self.create_ini_box.setLayout(layout_create)
        
        # Load/save in external configs
        self.external_box = QGroupBox('Load and save user configurations')
        layout_external = QGridLayout()
        # Load button
        self.load_config = QPushButton('load external config file')
        self.load_config.clicked.connect(self.execute_load_external_config)
        # Save button
        self.save_config = QPushButton('save as external config file')
        self.save_config.clicked.connect(self.execute_save_external_config)
        # Load settings from summary.ini button
        self.load_from_sum = QPushButton('load old settings from existing .ini file')
        self.load_from_sum.clicked.connect(self.execute_load_from_ini)
        # Reset button
        self.reset_config = QPushButton('reset configurations')
        self.reset_config.clicked.connect(self.execute_reset_configs)
        # Layout
        layout_external.addWidget(self.load_config, 0, 0, 1, 1)
        layout_external.addWidget(self.save_config, 0, 1, 1, 1)
        layout_external.addWidget(self.load_from_sum, 1, 0, 1, 2)
        layout_external.addWidget(self.reset_config, 2, 0, 1, 2)
        self.external_box.setLayout(layout_external)
        
        # Main layout
        layout.addWidget(self.target_box)
        layout.addWidget(self.create_ini_box)
        layout.addWidget(self.external_box)
        self.button_box.setLayout(layout)
                
        return None
        
    def check_parameter(self, object, section, param):
        '''
        Checks the current text in the given LinEdit-object for validity using the predefinitions connected
        to the given parameter in Run_Config.py.
        If not valid, the text is reset to the former value.
        
        :param object: The QlineEdit object
        :param param: Name of the parameter as stored in the definitions in Run_Config.py
        :returns: None
        '''

        # We need the text as a dict with the parameter name to use the methods of Ini_Handler()
        to_check = {param:object.text()}
        
        # The conversion function already checks the value and returns None if something went wrong
        to_check = self.ini_handler.convert_from_ini(to_check, display = False)
        if to_check is None:
            object.setText(self.config_str[section][param])
            print('Invalid value, reset to initial settings')
            
        return None

    def check_channel_parameter(self, object, section, param, ch_num):
        '''
        Similar to check_parameter, but for channel parameters like channels, detectors and bias voltage.
        There needs to be a reformatting first.
        
        :param object: The Widgted object itself, to set values of user value is not valid
        :param section: BASIC or OPTIONAL
        :param param: Parameter handle as defined in Run_Config.py
        :returns: None
        '''
        
        # Create a dict with fitting parameter handle and as a list
        to_check = {param:('[' + object.text() + ']')}
        
        # Use the conversion function of the ini handler to also get the correct warnings
        to_check = self.ini_handler.convert_from_ini(to_check, display = False)
        if to_check is None:
            if ch_num in self.config['BASIC']['channels']:
                index = self.config['BASIC']['channels'].index(ch_num)
                object.setText(str(self.config[section][param][index]))
            else:
                if param == 'detectors':
                    text = 'det' + str(ch_num)
                elif param == 'bias_voltage':
                    text = '100.0'
                elif param == 'iso_distance':
                    text = '0.0'
                object.setText(text)
                print('Invalid value, reset to initial settings')
        
        return None

    def check_osci(self, osci):
        '''
        Ensures that only one osci is checked at the same time.
        
        :param osci: Osci name 
        :returns: None
        '''

        # We need to disconnect in between to not cause an avalanche
        if self.gui['BASIC']['oscilloscope'][osci].isChecked():
            for osc, box in self.gui['BASIC']['oscilloscope'].items():
                if not osc == osci and box.isChecked():
                    box.disconnect()
                    box.setChecked(False)
                    box.stateChanged.connect((lambda osci = osc: lambda: self.check_osci(osci))())            
        else:
            self.gui['BASIC']['oscilloscope'][osci].setChecked(True)
        
        
        '''
        for os, box in self.gui['BASIC']['oscilloscope'].items():
            if not os == osci:
                box.setChecked(not self.gui['BASIC']['oscilloscope'][osci].isChecked())
        '''
        return None
    
    def check_used_channels(self, ch_num):
        '''
        Enables and disables detector names, bias voltages and iso distances according to checked channels.
        
        :param ch_num: Channel number
        :returns: None
        '''

        # We need to ensure, that at least one channel is clicked
        if all(not checked.isChecked() for checked in self.gui['BASIC']['channels'].values()):
            self.gui['BASIC']['channels'][1].setChecked(True)
            print('At least one channel needs to be activated')

        # Enable and disable other channel options according to ticked channel
        enable = self.gui['BASIC']['channels'][ch_num].isChecked()
        self.gui['BASIC']['detectors'][ch_num].setDisabled(not enable)
        self.gui['BASIC']['bias_voltage'][ch_num].setDisabled(not enable)
        self.gui['BASIC']['iso_distance'][ch_num].setDisabled(not enable)
        
        # Also, make sure that there is some text within the field if checked
        if enable:
            if self.gui['BASIC']['detectors'][ch_num].text() == '':
                self.gui['BASIC']['detectors'][ch_num].setText('det' + str(ch_num))
            if self.gui['BASIC']['bias_voltage'][ch_num].text() == '':
                self.gui['BASIC']['bias_voltage'][ch_num].setText('100.0')
            if self.gui['BASIC']['iso_distance'][ch_num].text() == '':
                self.gui['BASIC']['iso_distance'][ch_num].setText('0.0')
                                
        return None
          
    def execute_custom_param_check(self):
        '''
        Enables the settings of a custom parameter including name, value and unit which will also be available 
        for ramping. Only possible if the corresponding checkbox is checked
        
        :returns: None
        '''
        
        self.gui['BASIC']['custom_ramp_param'].setDisabled(not self.gui['BASIC']['use_custom_ramp_param'].isChecked())
        self.gui['BASIC']['custom_ramp_val'].setDisabled(not self.gui['BASIC']['use_custom_ramp_param'].isChecked())
        self.gui['BASIC']['custom_ramp_unit'].setDisabled(not self.gui['BASIC']['use_custom_ramp_param'].isChecked())

        return None
          
    def execute_ini_folder(self):
        '''
        Opens a dialog to choose a folder for the ini file. Warns the user if there are already ini files
        within the chosen folder
        
        :returns: None
        '''

        self.read_window()

        # Let the user choose a folder
        defaultDir = self.persival.get('execute_ini_folder')
        defaultDir = posixpath.join(defaultDir, '../')
        folder = str(QFileDialog.getExistingDirectory(self, "Select Directory", defaultDir))
        
        # In case the choosing is aborted
        if os.path.isdir(folder): 
            self.target_folder = folder
            # Enable other buttons
            self.create_single.setDisabled(False)
            self.create_ramp.setDisabled(False)

            for file in os.listdir(folder):
                if file[-4:] == '.ini':
                    self.warn.warn(f'The ini file \'{file}\' is already located in the chosen folder. Only one is allowed for analysis')
            
            self.persival.set('execute_ini_folder', folder)
            print('Ini folder chosen...')
            
        else:
            print('No valid folder chosen...')
            
        return None

    def execute_create_ini(self):
        '''
        Creates a single config file. If other ini files are found within the folder, the user is asked
        if he/she wants to proceed and delete all existing .ini files.
        '''

        self.read_window()

        if not os.path.isdir(self.target_folder):
            self.warn.warn('The given directory does not exist.')
        else:
            # We assure correct notation (run 01 for run 1)
            if not self.config['BASIC']['run_no'] % 10 > 0:
                add_zero = '0'
            else:
                add_zero = ''
            file_name = 'run' + add_zero + self.config_str['BASIC']['run_no'] + '_config.ini'
            file_dir = posixpath.join(self.target_folder, file_name)
            
            make_ini = True
            
            # Check for other ini files
            to_delete = []
            for file in os.listdir(self.target_folder):
                if file[-4:] == '.ini':
                    to_delete.append(posixpath.join(self.target_folder, file))
                
            if len(to_delete) > 0:
                make_ini = self.popup.window('Old ini files found', f'There already are {len(to_delete)} ini files within the chosen target folder, only one is allowed. Proceed and delete existing ini files?')
                    
            if make_ini:
                # Delete eventual preexisting ini files
                for file in to_delete:
                    os.remove(file)
                # Create ini
                try:
                    self.ini_handler.write_ini(self.config, file_dir, all_defined = True, displayed = False)
                    print('Ini file created...')
                except:
                    self.warn.warn('Problems during ini creation, see warnings')
            else:
                print('No ini file created...')
            
            # We need to overwrite the variable to be able to call it again
            self.popup = PopUp()
        
        return None                
 
    def execute_create_ramp(self):
        '''
        Opens another GUI for the ramp parameters and makes the corresponding folders with the .ini files 
        in it.
        
        :returns: None
        '''
        
        print('-------------------------------------------------------')
        print('Setting ramp parameters...')
        self.ramp_gui = GUI_Serial_Ramp_Ini(self.target_folder, self.config)
        print('-------------------------------------------------------')
        
        return None
 
    def execute_load_external_config(self):
        '''
        Opens a window with a list of all available user configs. After choosing, the configurations are
        loaded and stored in the variables self.loaded and self.loaded_str.
        
        :returns: None
        '''
        
        self.read_window()
        
        # Create another window
        stored_configs = QDialog()
        layout = QVBoxLayout()
        layout.addWidget(QLabel('Stored configurations'))
        
        # Get the user settings and remove the temporal and current one for this GUI
        user_configs = [config[:-4] for config in os.listdir(self.config_path) if config.split('.')[-1] == 'ini' and not config == self.current_ini and not config == self.temp_ini]
        
        # Loop over the avalilable files and open a window to choose from
        load_buttons = {}
        for config in user_configs:
            load_buttons[config] = QPushButton(config)
            # If one of the buttons is clicked, the file data is loaded and the window is closed
            load_buttons[config].clicked.connect((lambda text = config:  lambda: self.get_ini_config(text))())
            load_buttons[config].clicked.connect(stored_configs.close)
            layout.addWidget(load_buttons[config])
        stored_configs.setLayout(layout)

        # Open the window
        stored_configs.exec_()
        del(load_buttons, stored_configs, layout)
        # Now we use the saved settings to update our stored settings as well as our window
        if not self.loaded is None:
            if not self.loaded == self.config:
                self.set_ini_config(self.loaded)
                self.config = self.loaded
                self.config_str = self.loaded_str
                self.update_window()
                print('Loaded configurations...')
            else:
                print('Same configuration...')
        else:
            print('No configuration loaded...')        
        
        return None

    def execute_save_external_config(self):
        '''
        Lets the user type in a config name and export the current configurations for later or choose an
        existing one to overwrite
        
        :returns: None
        '''
        
        self.read_window()
        
        # A pop up window to enter the file name should appear
        type_name = QDialog()
        layout = QVBoxLayout()
        # Create new user config
        layout.addWidget(QLabel('Insert name of configurations to export\nfinish by pressing \'Enter\''))
        export_name = QLineEdit('')
        export_name.returnPressed.connect(lambda: self.set_ini_config(self.config, export_name.text()))
        export_name.returnPressed.connect(lambda: print(f'\'{export_name.text()}\' added to user configurations...'))
        export_name.returnPressed.connect(type_name.close)
        layout.addWidget(export_name)
        # Overwrite existing one
        layout.addWidget(QLabel('Overwrite existing user config'))
        files = os.listdir(self.config_path)
        files = [file[:-4] for file in files if file[-4:] == '.ini']
        overwrite_buttons = {}
        for file in files:
            overwrite_buttons[file] = QPushButton(file)
            overwrite_buttons[file].clicked.connect((lambda name = file: lambda: self.set_ini_config(self.config_str, name))())
            overwrite_buttons[file].clicked.connect((lambda name = file: lambda: print(f'\'{name}\' overwritten...'))())
            overwrite_buttons[file].clicked.connect(type_name.close)
            layout.addWidget(overwrite_buttons[file])
        type_name.setLayout(layout)
        type_name.exec_()
        del(type_name)
        
        return None

    def execute_load_from_ini(self):
        '''
        Allows the user to choose any .ini file to load the run config from there. Checks, if all 
        parameters are given and loads them into the GUI if so. 
        
        :returns: None
        '''
        
        self.read_window()
        
        # Let the user choose a file
        defaultDir = self.persival.get('execute_load_from_ini')
        file = str(QFileDialog.getOpenFileName(self, 'Select Directory', defaultDir)[0])
        
        valid_file = False
        if not os.path.exists(file):
            self.warn.warn('The chosen directory does not exist...')
        else:
            body, base = os.path.splitext(file)
            
            # Check for right format
            if not base == '.ini':
                self.warn.warn('Chosen file is not .ini file...')
            else:
                # Read the whole .ini file without conversion
                ini_content = self.ini_handler.read_ini(file, convert = False, all_defined = False, display = False)
                
                # Filter out the desired parameters
                if ini_content is None:
                    self.warn.warn('Error during .ini reading...')
                else:
                    all_keys = True
                    filtered = {}
                    for sec, cont in self.config.items():
                        if not sec in list(ini_content.keys()):
                            self.warn.warn(f'Section {sec} missing in chosen .ini file...')
                            all_keys = False
                        else:
                            filtered[sec] = {}
                            for param in cont.keys():
                                if not param in list(ini_content[sec].keys()):
                                    self.warn.warn(f'Parameter {param} missing in chosen .ini file...')
                                    all_keys = False
                                else:
                                    filtered[sec][param] = ini_content[sec][param]
                    
                    # Now convert the data and load it into the GUI
                    if all_keys:
                        filtered = self.ini_handler.convert_from_ini(filtered, display = False)
                        if filtered is None:
                            self.warn.warn('Error during conversion of parameters, see warnings...')
                        else:
                            self.set_ini_config(filtered)
                            self.get_ini_config()
                            self.update_window()
                            valid_file = True
                            
        if valid_file:
            self.persival.set('execute_load_from_ini', file)
            print('Config settings loaded...')
        else:
            print('No configuration loaded...')
        
        return None

    def execute_reset_configs(self):
        '''
        Resets the configurations to the current_config.ini when opening the tool
        
        :returns: None
        '''
        
        # We get the data from current_settings.ini...
        self.get_ini_config(self.current_ini)
        
        # ...set the preview_settings.ini to the resetted ones...
        self.set_ini_config(self.loaded)
        
        # ...update the GUI internal data...
        self.config = self.loaded.copy()
        self.config_str = self.loaded_str.copy()
        
        # ...and update the displayed values.
        self.update_window()
        print('Configurations reset to initial status...')
        
        return None

    def closeEvent(self, event):
        '''
        Before closing the application we need to save the data persival collected
        as well as set the current run_config.ini to the window settings for next session
        '''
        
        # Overwrite the current_config.ini with the window settings for next time
        self.read_window()
        self.set_ini_config(self.config, self.current_ini)
        
        # Delete the working copy
        os.remove(self.temp_ini_path)

        self.persival.save()
        event.accept() 
        
        return None
         

if __name__ == '__main__':
    
    import sys
    pyqtRemoveInputHook()
    ini_gui = QApplication(sys.argv)
    gallery = GUI_Make_Run_Ini()
    sys.exit() 